@isTEST
private class  DISH_Contracts_CopyAttachmentsSignd_Test{

    private static testmethod void testTriggerForCase(){
        Account a = new Account();
            a.Name = 'Test Account';
  //          a.Programming__c = 'Starter';
  //          a.phone = '(303) 555-5555';
        insert a;
        
        Opportunity o = new Opportunity();
            o.Name = 'Test';
            o.AccountId = a.Id;
    //        o.BTVNA_Request_Type__c = 'Site Survey';
    //        o.LeadSource='BTVNA Portal';
    //        o.Property_Type__c='Private';
    //        o.misc_code_siu__c='N999';
            o.CloseDate=system.Today();
            o.StageName='Closed Won';
    //        o.Restricted_Programming__c = 'True';
        insert o;
       
      Case c = new Case();
           c.Opportunity__c = o.id;
           c.AccountId = a.id;
           c.Status = 'Request Completed';
           c.Origin = 'PRM';
//           c.RequestedAction__c = 'Activation';
           c.RequestedAction__c = 'General Inquiry';
           c.Description = 'Test';
//           c.RecordTypeId = '012600000001EeC';
           c.CSG_Account_Number__c = '82551234567890';
           c.Requested_Actvation_Date_Time__c = system.Today();
      insert c;
      update c;

        Contact ctc = new Contact();
//            ctc.Account = a.id;
            ctc.FirstName='Test First';
            ctc.LastName='Last Name';
            ctc.email='test@dish.com';
            ctc.Phone='512-383-5201';
        insert ctc;

        DISH_Contract__c dc1 = new DISH_Contract__c();
            dc1.Account__c = a.id;
            dc1.Opportunity__c=o.id;
            dc1.Case__c=c.id;
            dc1.Contact__c=ctc.id;
            dc1.Contract_Start_Date__c=system.Today();
            dc1.Contract_Term_Months__c=120;
            dc1.Contract_Type__c='Direct Bulk';
            dc1.Status__c='Inactive';
        insert dc1;
        
        DISH_Contract__c dc2 = new DISH_Contract__c();
            dc2.Account__c = a.id;
            dc2.Opportunity__c=o.id;
            dc2.Case__c=c.id;
            dc2.Contact__c=ctc.id;
            dc2.Contract_Start_Date__c=system.Today();
            dc2.Contract_Term_Months__c=120;
            dc2.Contract_Type__c='Direct Digital';
            dc2.Status__c='Inactive';
            dc2.Related_DISH_Contract__c=dc1.id;
        insert dc2;
        
        String s1 = dc2.id;
        String s2 = s1.Left(15);
                    
        Attachment attachment = new Attachment();
            attachment.body = Blob.valueOf( 'this is an attachment test' );
            attachment.name = 'test' + ' - ' + s2;
            attachment.parentId = s2;
        insert attachment;

        dc2.Status__c='Send attachment to parent contract record for E-Signature';
        update dc2;

        dc1.Status__c='Fully Signed';        
        update dc1;
        



//     }
}
}