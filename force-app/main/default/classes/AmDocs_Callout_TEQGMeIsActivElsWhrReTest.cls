@IsTest
public class AmDocs_Callout_TEQGMeIsActivElsWhrReTest {
    
    @testSetup static void testSetupdata(){
        AmDocs_Login__c aml = TestDataFactoryForGuideMe.createAmDocsLogin();
        Account acc = TestDataFactoryForGuideMe.createAccount();
        Opportunity opp = TestDataFactoryForGuideMe.createOpportunity(acc.Id);
        Tenant_Account__c ta = TestDataFactoryForGuideMe.createTenantAccount(opp.Id);
        Tenant_Equipment__c te3 = TestDataFactoryForGuideMe.createTenantEquipment7(opp.Id, ta.Id);
    }
    
    @isTest
    static void testTEDisconnect1()
    {
        List<Tenant_Equipment__c> TE=[Select Id,Amdocs_Tenant_ID__c from Tenant_Equipment__c where Name='R223456789'];
        
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete',
        	'{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
			null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);       
        AmDocs_Callout_TEQGMeIsActivElsWhrREMOVE.TellAmdocs2REMOVETEQOnServiceID(TE[0].Amdocs_Tenant_ID__c);
        Test.stopTest();    
    }
    
    @isTest
    static void testTEDisconnect2()
    {
        List<Tenant_Equipment__c> TE=[Select Id,Amdocs_Tenant_ID__c from Tenant_Equipment__c where Name='R223456789'];
        
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(100, 'Complete',
        	'{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
			null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);       
        AmDocs_Callout_TEQGMeIsActivElsWhrREMOVE.TellAmdocs2REMOVETEQOnServiceID(TE[0].Amdocs_Tenant_ID__c);
        Test.stopTest();    
    }
}