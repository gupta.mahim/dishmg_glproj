@isTest
Public class CommLeadFormTests{
    public static testMethod void testCommLeadForm(){
    
    PageReference pageRef = Page.Commercial_Lead;
    Test.setCurrentPageReference(pageRef);
    
    Lead newlead = new Lead (INS_Lead_Hold__c=true,
    OwnerId='00G600000016h6A',
    RecordTypeId='012600000005D4m',
    ANI_DNIS__c='0000000',
    DNIS__c='0000000',
    LeadSource='Internal',
    Lead_Source_External__c='Internal Com Lead Form',
    Number_of_TVs__c='1-2',
    Number_of_Locations__c=1,
    Number_of_Units__c=1,
    Building_Stories__c='1-2',
    FirstName='Test',
    LastName='Test',
    Company='Test Co');
    
        Lead newlead2 = new Lead (INS_Lead_Hold__c=true,
    OwnerId='00G600000016h6A',
    RecordTypeId='012600000005D4m',
    ANI_DNIS__c='0000000',
    DNIS__c='0000000',
    LeadSource='Internal',
    Lead_Source_External__c='Internal Com Lead Form',
    Number_of_TVs__c='1-2',
    Number_of_Locations__c=1,
    Number_of_Units__c=1,
    Building_Stories__c='1-2',
    FirstName='Test',
    LastName='Test',
    Company='Test Co',
    Type_of_Business__c='',
    Street='');
  ApexPages.StandardController sc = new ApexPages.standardController(newlead);  
CommLeadForm myPageCon = new CommLeadForm(sc);
  ApexPages.StandardController sc2 = new ApexPages.standardController(newlead2);  
CommLeadForm myPageCon2 = new CommLeadForm(sc2);
myPageCon.onsave();
myPageCon2.onsave();
}


}