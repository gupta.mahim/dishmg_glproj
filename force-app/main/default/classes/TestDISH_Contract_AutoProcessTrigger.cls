@isTest
private class TestDISH_Contract_AutoProcessTrigger { 
    private static testMethod void Test_DISH_Contract_AutoProcessTrigger () { 
        try{
            DISH_Contract_Auto__c fw = new DISH_Contract_Auto__c(); 
            insert fw; 
            System.assertNotEquals(null, fw.id); 
            update fw; 
            delete fw;
        } catch(Exception e){
            FSTR.COTestFactory.FillAllFields=true;
            DISH_Contract_Auto__c fw = (DISH_Contract_Auto__c)FSTR.COTestFactory.createSObject('DISH_Contract_Auto__c',true); 
            insert fw; 
            System.assertNotEquals(null, fw.id); 
            update fw; 
            delete fw;
        }
    } 
}