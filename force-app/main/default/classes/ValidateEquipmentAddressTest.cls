@isTest
public class ValidateEquipmentAddressTest {
        
   
    public static TestMethod void CheckInvalidEquipment()
    {
       
        list<ValidateEquipAddress.FlowInput>inputlist= new list<ValidateEquipAddress.flowInput>();
        list<ValidateEquipAddress.FlowOutput>outputlist= new list<ValidateEquipAddress.flowoutput>();
        
        ValidateEquipAddress.flowInput input= new ValidateEquipAddress.flowInput();
        Opportunity opp= TestDataFactoryforInternal.createOpportunity();
        Opportunity opp1= TestDataFactoryforInternal.createOpportunity();
      
        
        Tenant_Equipment__c TE1= TestDataFactoryforInternal.createTenantEquipment(opp1);
        Tenant_Equipment__c TE= TestDataFactoryforInternal.createTenantEquipment(opp);
        Smartbox__c SB =TestDataFactoryforInternal.createSBEquipment(opp);
        Smartbox__c SB1 =TestDataFactoryforInternal.createSBEquipment(opp1);
        Equipment__c HE1=TestDataFactoryforInternal.createHeEquipment(opp);
        Equipment__c HE2=TestDataFactoryforInternal.createHeEquipment(opp1);
        
        
        input.oppId= opp.id;
        inputlist.add(input);
         ValidateEquipAddress.CheckforInvalidanddupeEquip(inputlist);
      
      
        
    }
    
    
}