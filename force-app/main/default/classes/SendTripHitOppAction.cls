public class SendTripHitOppAction
{
    @AuraEnabled
    public static void doSendTripHitAction(String oppId)
    {
        Opportunity opp=[Select id, AmDocs_ResendAll__c  from Opportunity where id=:oppId];
        opp.AmDocs_ResendAll__c=true;
        
        update opp;
    }
}