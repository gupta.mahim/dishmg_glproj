@IsTest
public class EquipmentEdit_LEXTest {
    
    public static testMethod void testReadFile() {
        
        Opportunity newOpp = TestDataFactoryforInternal.createOpportunity();
        
        //Milestone# MS-001252 - 24Aug20 - Removed SeeAllData=True from this test class. Updated the below code accordingly  - Start
        //Document lstDoc = [select id,name,Body from Document where name = 'eqtest'];
        //String fileContent=lstDoc.Body.toString();
        //List<Equipment__c> lexAccstoupload = EquipmentEdit_LEX.getEquipments(newOpp.id);
        String equipData;
        list<String> equipTemp = new List<String>(2);
        equipTemp[0] = 'Receiver #,SmartCard #,Receiver Model,Programming Package,Channel Name,Channel Number,Satellite,Satellite Channel,Output Channel';
        equipTemp[1] = 'R32155533221,S321321321321,311,Essentials,HGTV,4136,118.7,123,52';
        
        for(String eD : equipTemp){
            equipData = equipData + eD +  '\n' ;  
        }
        
        List<Equipment__c> lexAccstoupload = EquipmentEdit_LEX.getEquipments(newOpp.id);
        //Milestone# MS-001252 - End
        
        String str = JSON.serialize(lexAccstoupload);
        EquipmentEdit_LEX.saveRecords(str);
        EquipmentEdit_LEX.deleteRecords(str);
        EquipmentEdit_LEX.getUserAccess();
    }
}