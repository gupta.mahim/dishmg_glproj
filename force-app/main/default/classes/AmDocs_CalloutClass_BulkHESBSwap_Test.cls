@isTest
private class AmDocs_CalloutClass_BulkHESBSwap_Test { 
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

Pricebook2 pb2 = new Pricebook2();
            pb2.Name='Test PB2';
            pb2.IsActive=false;
        insert pb2;
        
        pb2.IsActive=true;
        update pb2;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp abc123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.LeadSource='Test Place';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Digital Bulk';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Pricebook2Id=pb2.id;
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
        insert opp1;
        
        Product2 p2 = new Product2();
            p2.Name='AT120 Test';
            p2.IsActive=true;
            p2.Family='Core';
            p2.Category__c='Core';
            p2.Amdocs_ProductID__c = '123456789';
        insert p2;
        
        Product2 p3 = new Product2();
            p3.Name='SPS Test';
            p3.IsActive=true;
            p3.Family='Add-On';
            p3.Category__c='Add-On';
            p3.Amdocs_Caption2__c = 'ABC123';
            p3.Amdocs_ProductID__c = '123456789';
        insert p3;
        
        Product2 p4 = new Product2();
            p4.Name='Component Test';
            p4.IsActive=true;
            p4.Family='Promotion';
            p4.Category__c='Promotion';
            p4.Amdocs_Caption2__c = 'XYZ789';
            p4.Amdocs_ProductID__c = '123456789';
        insert p4;
        
        //add to standrd pricebook
        Pricebookentry pbe1 = new pricebookentry();
            pbe1.Product2Id=p2.Id;
            pbe1.Pricebook2Id='01s60000000EIELAA4';
            pbe1.UnitPrice=100;
            pbe1.IsActive=true;
        insert pbe1;

        //add to standrd pricebook
        Pricebookentry pbe2 = new pricebookentry();
            pbe2.Product2Id=p3.Id;
            pbe2.Pricebook2Id='01s60000000EIELAA4';
            pbe2.UnitPrice=100;
            pbe2.IsActive=true;
        insert pbe2;

        //add to standrd pricebook        
        Pricebookentry pbe3 = new pricebookentry();
            pbe3.Product2Id=p4.Id;
            pbe3.Pricebook2Id='01s60000000EIELAA4';
            pbe3.UnitPrice=100;
            pbe3.IsActive=true;
        insert pbe3;


        // Add product to new pricebook
        Pricebookentry pbe = new pricebookentry();
            pbe.Product2Id=p2.Id;
            pbe.Pricebook2Id=pb2.Id;
            pbe.UnitPrice=100;
            pbe.IsActive=true;
        insert pbe;
        
        // Add product to new pricebook
        Pricebookentry pbe2a = new pricebookentry();
            pbe2a.Product2Id=p3.Id;
            pbe2a.Pricebook2Id=pb2.Id;
            pbe2a.UnitPrice=100;
            pbe2a.IsActive=true;
        insert pbe2a;
        
        // Add product to new pricebook
        Pricebookentry pbe3a = new pricebookentry();
            pbe3a.Product2Id=p4.Id;
            pbe3a.Pricebook2Id=pb2.Id;
            pbe3a.UnitPrice=100;
            pbe3a.IsActive=true;
        insert pbe3a;
        
        OpportunityLineItem oli1 = New OpportunityLineItem();
            oli1.OpportunityId = opp1.id;
            oli1.PriceBookEntryId=pbe.id;
            oli1.Quantity=1;
            oli1.UnitPrice=1;
            oli1.Action__c='ADD';
        insert oli1;
        
        OpportunityLineItem oli2 = New OpportunityLineItem();
            oli2.OpportunityId = opp1.id;
            oli2.PriceBookEntryId=pbe2a.id;
            oli2.Quantity=1;
            oli2.UnitPrice=1;
            oli2.Action__c='ADD';
        insert oli2;
        
        OpportunityLineItem oli3 = New OpportunityLineItem();
            oli3.OpportunityId = opp1.id;
            oli3.PriceBookEntryId=pbe3a.id;
            oli3.Quantity=1;
            oli3.UnitPrice=1;
            oli3.Action__c='ADD';
        insert oli3;

        Equipment__c et = new Equipment__c();
            et.Opportunity__c = opp1.Id;
            et.Name = 'R123456789';
            et.Receiver_S__c = 'S123456789';
            et.Location__c = 'C';
            et.Action__c = 'ADD';
        insert et;
        
        Equipment__c et2 = new Equipment__c();
            et2.Opportunity__c = opp1.Id;
            et2.Name = 'RABC456789';
            et2.Receiver_S__c = 'S123456789';
            et2.Location__c = 'C';
            et2.Action__c = 'SWAP PENDING';
        insert et2;
        
       Tenant_Equipment__c te = new Tenant_Equipment__c();
            te.Opportunity__c = opp1.Id;
            te.Name = 'R234567819';
            te.Smart_Card__c = 'S1234567891';
            te.Location__c = 'D';
            te.Action__c = 'ADD';
            te.Address__c ='1011 Coliie Path';
            te.City__c = 'Round Rock';
            te.State__c = 'TX';
            te.Zip_Code__c = '78664';
            te.Unit__c = '1A';
            te.Phone_Number__c = '5123835201';
            te.Email_Address__c = 'jerry.clifft@dish.com';
            te.Customer_First_Name__c = 'Jerry';
            te.Customer_Last_Name__c = 'Clifft';
        insert te;

        Smartbox__c sb = new Smartbox__c();
            sb.Opportunity__c = opp1.Id;
            sb.CAID__c = 'R345678912';
            sb.SmartCard__c = 'S345678912';
            sb.Location__c = 'C';
            sb.Action__c = 'ADD';
            sb.Serial_Number__c = 'LALPSC011642';
            sb.Chassis_Serial__c = 'LALPFB001327';
            sb.AmDocs_No_Send__c = false;
            sb.NoTrigger__c = true;
        insert sb;

        Smartbox__c sb1 = new Smartbox__c();
            sb1.Opportunity__c = opp1.Id;
            sb1.Location__c = 'C';
            sb1.Action__c = 'ADD';
            sb1.Serial_Number__c = 'LALPSC011642';
            sb1.Chassis_Serial__c = 'LALPFB001327';
            sb1.AmDocs_No_Send__c = false;
            sb1.NoTrigger__c = true;
        insert sb1;
        
        Smartbox__c sb3 = new Smartbox__c();
            sb3.Opportunity__c = opp1.Id;
            sb3.Location__c = 'C';
            sb3.Action__c = 'ADD';
            sb3.Serial_Number__c = 'LALPSC011642';
            sb3.Chassis_Serial__c = 'LALPFB001327';
            sb3.AmDocs_No_Send__c = false;
            sb3.NoTrigger__c = true;
        insert sb3;

        Smartbox__c sb2 = new Smartbox__c();
            sb2.Opportunity__c = opp1.Id;
            sb2.Location__c = 'C';
            sb2.Action__c = 'ADD';
            sb2.Serial_Number__c = 'LALPFB001327';
            sb2.Chassis_Serial__c = 'LALPFB001327';
            sb2.AmDocs_No_Send__c = false;
            sb2.NoTrigger__c = true;
        insert sb2;            
  }
  
  
 @isTest
  static void Acct1(){
    Equipment__c opp = [Select Id FROM Equipment__c WHERE Name = 'RABC456789' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp); System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
        String sendThisID = opp.Id;
        Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());       // Set mock callout class
        Test.startTest();    // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
            AmDocs_CalloutClass_BulkHESCSwap.AmDocsMakeCalloutBulkHESCSwap(sendThisID);
        Test.stopTest();    
      
//        opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Opportunity where id =: opp.Id];       // Verify that the response received contains fake values        
//             System.assertEquals('123',opp.AmDocs_CustomerID__c); 
             
//        opr = [select Id, PriceBookEntryId from OpportunityLineItem where OpportunityId =: opp.Id];       // Verify that the response received contains fake values        
//             System.assertEquals('01t600000039UwJ',oli.PriceBookEntryId); 
     
  }
}