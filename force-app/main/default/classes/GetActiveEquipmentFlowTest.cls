@isTEst public class GetActiveEquipmentFlowTest {
    
    @isTEst  public static void ActiveEquips(){
        Opportunity o = TestDataFactoryforInternal.createOpportunity();
        
        List <Tenant_Equipment__c> myEq = TestDataFactoryforInternal.createTenantEquipmentBulk(o);
        if(myEq.size()>0){
            myEq[0].Customer_First_Name__c = 'Test';
            myEq[0].Customer_Last_Name__c = 'Test';
            myEq[0].Amdocs_Tenant_ID__c = 'Test123';
            myEq[0].Phone_Number__c = '1231231234';
            myEq[0].Unit__c = '123';
        }
        Update myEq;
        Set<Id> myEqIds = (new Map<Id,Tenant_Equipment__c>(myEq)).keySet();
        List<Id> TEquipId=new List<Id>();
        TEquipId.addAll(myEqIds);
        
        List <Equipment__c> myEquip = TestDataFactoryforInternal.createHeEquipmentBulk(o);
        Set<Id> myEquipIds = (new Map<Id,Equipment__c>(myEquip)).keySet();
        List<Id> EquipId=new List<Id>();
        EquipId.addAll(myEquipIds);
        
        List <Smartbox__c> mySmt = TestDataFactoryforInternal.createSBEquipmentBulk(o);
        Set<Id> mySmts = (new Map<Id,Smartbox__c>(mySmt)).keySet();
        List<Id> SmtId=new List<Id>();
        SmtId.addAll(mySmts);
        
        Map<String, Object> obj = new Map<String, Object>();
        obj.put('receiverNumber', 'dummy number');
        obj.put('activeOppId', o.Id);
        obj.put('oppID',Equipid);
        obj.put('isTE',true);
        String jsonValue = JSON.Serialize(obj);
        
        Map<String, Object> objs = new Map<String, Object>();
        objs.put('receiverNumber', 'dummy number');
        objs.put('activeOppId', o.Id);
        objs.put('oppID',Equipid);
        String jsonValues = JSON.Serialize(objs);
        
        //Set<String> idStrs = (Set<String>)JSON.deserialize(JSON.serialize(myEqIds), Set<String>.class);
        List<GetActiveEquipmentFlow.EquipData> testEquipData = GetActiveEquipmentFlow.getActiveEquips(JSON.Serialize(TEquipId), JSON.Serialize(EquipId), JSON.Serialize(SmtId));
        testEquipData = GetActiveEquipmentFlow.getActiveTEEquips(TEquipId,testEquipData);
        testEquipData = GetActiveEquipmentFlow.getActiveHEEquips(EquipId,testEquipData);
        GetActiveEquipmentFlow.proceedDisconnect(jsonValue);
        GetActiveEquipmentFlow.loadRecord(jsonValues);
        
        GetActiveEquipmentFlow.EquipData eData=(GetActiveEquipmentFlow.EquipData)JSON.deserialize(jsonValue, GetActiveEquipmentFlow.EquipData.class);
        eData.isHE = false;
        eData.isSB = false;
        eData.TenantID = '1234';
        eData.action = 'Test';
        eData.disconnect = True;
        eData.ServiceID = '1234';
        system.debug('eData-'+eData);
        GetActiveEquipmentFlow.disconnectHESB(eData);
    }
    
    //Milestone: MS-000022 - Mahim - Start    
    public static AmDocs_Login__c aml = new AmDocs_Login__c();
    public static Account acc = new Account();
    public static Opportunity opp = new Opportunity();
    public static Tenant_Account__c ta = new Tenant_Account__c();
    public static Tenant_Equipment__c te =  new Tenant_Equipment__c();
    public static Tenant_Equipment__c te6 =  new Tenant_Equipment__c();
    public static Tenant_Equipment__c te5 =  new Tenant_Equipment__c();
    public static Equipment__c he = new Equipment__c();
    public static Smartbox__c sb = new Smartbox__c();
    public static Equipment__c he2 = new Equipment__c();
    public static Smartbox__c sb2 = new Smartbox__c();
    public static String inputAction;
    
    Public static void testSetupdata(){
        aml = TestDataFactoryForGuideMe.createAmDocsLogin();
        acc = TestDataFactoryForGuideMe.createAccount();
        opp = TestDataFactoryForGuideMe.createOpportunity(acc.Id);
        ta = TestDataFactoryForGuideMe.createTenantAccount(opp.Id);
        te =  TestDataFactoryForGuideMe.createTenantEquipment(opp.Id, ta.Id);
        te6 =  TestDataFactoryForGuideMe.createTenantEquipment6(opp.Id, ta.Id);
        te5 =  TestDataFactoryForGuideMe.createTenantEquipment5(opp.Id, ta.Id);
        he = TestDataFactoryForGuideMe.createHeadEndEquipment(opp.Id);
        sb = TestDataFactoryForGuideMe.createSmartBoxEquipment(opp.Id);
        he2 = TestDataFactoryForGuideMe.createHeadEndEquipment2(opp.Id);
        sb2 = TestDataFactoryForGuideMe.createSmartBoxEquipment2(opp.Id);
        inputAction = '[{"activeOppId":"0066000000M6bCZAAZ","equipId":"a0Rq0000005oJmHEAU","receiverNumber":"R123456789"}]';
        SingleRequestMock fakeResponse = new SingleRequestMock(200, 'Complete',
        	'{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
            null);
        Test.setMock(HttpCalloutMock.class, fakeResponse); 
    }
    
    @isTEst  public static void testMethod2(){
        testSetupdata();
        Test.startTest();               
        GetActiveEquipmentFlow.setActionforTE(inputAction);
        GetActiveEquipmentFlow.setActionforHESB(inputAction);
        GetActiveEquipmentFlow.disconnectTENew(te.Amdocs_Tenant_ID__c, null);
        GetActiveEquipmentFlow.isFirstTime = true;
        Test.stopTest(); 
    }
    
    @isTEst  public static void testMethod3(){
        testSetupdata();
        Test.startTest();       
        GetActiveEquipmentFlow.setActionforTE(inputAction);
        GetActiveEquipmentFlow.setActionforHESB(inputAction);
        GetActiveEquipmentFlow.disconnectTENew(te6.Amdocs_Tenant_ID__c, null);
        Test.stopTest(); 
    }
    
    @isTEst  public static void testMethod4(){
        testSetupdata();
        Test.startTest();      
        GetActiveEquipmentFlow.setActionforTE(inputAction);
        GetActiveEquipmentFlow.setActionforHESB(inputAction);
        GetActiveEquipmentFlow.disconnectTENew(null, he.name);
        Test.stopTest(); 
    }
    
    @isTEst  public static void testMethod5(){
        testSetupdata();
        Test.startTest();
        GetActiveEquipmentFlow.setActionforTE(inputAction);
        GetActiveEquipmentFlow.setActionforHESB(inputAction);        
        GetActiveEquipmentFlow.disconnectHESBNew(he.Opportunity__r.AmDocs_ServiceID__c, null);
        Test.stopTest(); 
    }
    
    @isTEst  public static void testMethod6(){
        testSetupdata();
        Test.startTest();
        GetActiveEquipmentFlow.setActionforTE(inputAction);
        GetActiveEquipmentFlow.setActionforHESB(inputAction);        
        GetActiveEquipmentFlow.disconnectHESBNew(null, te.Name);
        Test.stopTest(); 
    }
    //Milestone: MS-000022 - Mahim - End
}