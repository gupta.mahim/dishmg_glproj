public class AmDocs_CalloutClass_BulkUpdateNONE {

public class person {
    public String serviceID;
    public String ownerServiceId;
    public String orderID;
    public String responseStatus;
    public cls_equipmentList[] equipmentList;
    public cls_componentList[] componentList;
    public cls_spsList[] spsList;
    public cls_informationMessages[] informationMessages;
    public cls_ImplUpdateBulkRestOutput [] ImplUpdateBulkRestOutput;
}

class cls_ImplUpdateBulkRestOutput{
    public String serviceID;
    public String orderID;
    public String responseStatus;
}

class cls_equipmentList {
    public String action;
    public String receiverID;
    public String location;
    public String smartCardID;
    public String leaseInd;
    public String chassisSerialNumber;
    public String serialNumber;
}

class cls_componentList {
    public String action;
    public String caption;
}

class cls_spsList {
    public String action;
    public String caption;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}


    @future(callout=true)

    public static void AmDocsMakeCalloutBulkUpdateNONE(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

 list<Opportunity> O = [select id, Name, No_Sports__c, NLOS_Locals__c, AmDocs_tenantType__c, Number_of_Units__c, Smartbox_Leased__c, AmDocs_SiteID__c, AmDocs_ServiceID__c, Amdocs_CustomerID__c, AmDocs_BarID__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Category__c, AmDocs_Site_Id__c from Opportunity where Id = :id ];
     System.debug('RESULTS of the LIST lookup to the Opp object' +O);
     
 list<Equipment__c> E = [select id, Receiver_S__c, Name, Location__c, Action__c, Opportunity__c from Equipment__c where Opportunity__c = :id  AND ( Action__c = 'ADD' OR Action__c = 'REMOVE' OR Action__c = 'SWAP' OR Action__c = 'Enable' OR Action__c = 'Disable' ) AND Location__c = 'C'];
     System.debug('RESULTS of the LIST lookup to the Equipment object' +E);
     
 list<Tenant_Equipment__c> T = [select id, Smart_Card__c, Name, Address__c, City__c, State__c, Zip_Code__c, Unit__c, Phone_Number__c, Email_Address__c, Customer_First_Name__c, Customer_Last_Name__c, Location__c, Action__c, Opportunity__c from Tenant_Equipment__c where Opportunity__c = :id  AND Action__c = 'ADD' AND Location__c = 'D'];
     System.debug('RESULTS of the LIST lookup to the Tenant Equipment object' +T);     
     
list<Smartbox__c> S1 = [select id, Action__c, Location__c, CAID__c, Opportunity__c, Type_of_Equipment__c, AmDocs_No_Send__c, Second_ProCam__c, Serial_Number__c,  SmartCard__c, Chassis_Serial__c, Smartbox_Leased2__c, id__c from Smartbox__c where Opportunity__c = :id AND  ( Action__c = 'ADD' OR Action__c = 'REMOVE' OR Action__c = 'SWAP' OR Action__c = 'Enable' OR Action__c = 'Disable' ) AND AmDocs_No_Send__c  = false ];
     System.debug('RESULTS of the LIST lookup to the Smartbox object' +S1);

list<OpportunityLineItem> B1 = [select id, Action__c, Quantity, IsPromotion__c, Sales_Price2__c, Amdocs_Caption2__c, Family__c, Opportunity.Id from OpportunityLineItem where Opportunity.Id = :id AND ( Action__c = 'ADD' OR Action__c = 'REMOVE' ) AND (Family__c != 'Core' AND Family__c != 'Promotion' ) ];
     System.debug('RESULTS of the LIST lookup to the non-Core & non-Promotional OpportunityLineItem records' +B1);
     
list<OpportunityLineItem> C = [select id, Action__c, Quantity, IsPromotion__c, Sales_Price2__c, Amdocs_ProductID__c, Family__c, Opportunity.Id from OpportunityLineItem where Opportunity.Id = :id AND Action__c = 'ADD' AND Family__c = 'Core' Order By CreatedDate DESC LIMIT 1];
     System.debug('RESULTS of the LIST lookup to the Core OpportunityLineItem records' +C);
     
list<OpportunityLineItem> P = [select id, Action__c, Quantity, IsPromotion__c, Sales_Price2__c, Amdocs_Caption2__c, Family__c, Opportunity.Id from OpportunityLineItem where Opportunity.Id = :id AND ( Action__c = 'ADD' OR Action__c = 'REMOVE' ) AND Family__c = 'Promotion' ];
     System.debug('RESULTS of the LIST lookup to the Promotional OpportunityLineItem records' +P);

list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
     
     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        jsonObj.writeFieldName('ImplUpdateBulkRestInput');
        jsonObj.writeStartObject();
            jsonObj.writeStringField('orderActionType', 'CH');
            jsonObj.writeStringField('reasonCode', 'CREQ');

            // This is EQUIPMENT - Centeralized ONLY
            jsonObj.writeFieldName('equipmentList');
            jsonObj.writeStartArray();   
            jsonObj.writeEndArray();

            // This is a list of all non-promos and non-core (I guess that leaves add-on)   
            jsonObj.writeFieldName('spsList');
            jsonObj.writeStartArray();
                jsonObj.writeStartObject();
                    jsonObj.writeStringField('action','REMOVE');
                    jsonObj.writeStringField('caption', 'Showtime_WC_FTG');
                jsonObj.writeEndObject();
                jsonObj.writeStartObject();
                    jsonObj.writeStringField('action','ADD');
                    jsonObj.writeStringField('caption', 'Showtime');
                jsonObj.writeEndObject();
//                jsonObj.writeStartObject();
//                    jsonObj.writeStringField('action','ADD');
//                    jsonObj.writeStringField('caption', 'Spotx_Live_253659');
//                jsonObj.writeEndObject();                
            jsonObj.writeEndArray();
                    
            // This should include all promotional things - such as the discount for HBO FTG 3 year.
            jsonObj.writeFieldName('componentList');
            jsonObj.writeStartArray();
                
//                jsonObj.writeStartObject(); 
//                    jsonObj.writeStringField('action', 'ADD'); 
//                    jsonObj.writeStringField('caption', 'Promo_Bulk_Takedown1'); 
//                    jsonObj.writeBooleanField('isPromotion', false); 
//                jsonObj.writeEndObject();
                        
            jsonObj.writeEndArray();
        jsonObj.writeEndObject();
    jsonObj.writeEndObject();
String finalJSON = jsonObj.getAsString();
    System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));

if (!Test.isRunningTest()){ HttpRequest request = new HttpRequest(); String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/updateBulk?sc=SS&lo=EN&ca=SF'; request.setHeader('User-Agent', 'SFDC-Callout/45.0'); request.setEndPoint(endpoint); request.setBody(jsonObj.getAsString()); request.setTimeout(101000); request.setHeader('Content-Type', 'application/json'); request.setMethod('POST'); String authorizationHeader = A[0].UXF_Token__c; request.setHeader('Authorization', authorizationHeader); HttpResponse response = new HTTP().send(request); if (response.getStatusCode() >= 200 && response.getStatusCode() <= 299) { String strjson = response.getbody(); System.debug('DEBUG PRIOR to JSON DeSerialization ======== 1st STRING: ' +strjson); JSONParser parser = JSON.createParser(strjson); parser.nextToken(); parser.nextToken(); parser.nextToken(); person obj = (person)parser.readValueAs( person.class);
    System.debug(response.toString());
    System.debug('STATUS:'+response.getStatus());
    System.debug('STATUS_CODE:'+response.getStatusCode());
    System.debug('DEBUG Respnse Body: ' +response.getBody());
    System.debug('DEBUG Get Respnose to string: ' +response.toString());      

    
    Opportunity sbc = new Opportunity(); sbc.id=id; sbc.AmDocs_Order_ID__c=obj.orderID; sbc.API_Status__c=String.valueOf(response.getStatusCode());  if(obj.informationMessages != Null) { sbc.AmDocs_Transaction_Description__c='UPDATE BULK NONE' +obj.responseStatus + ' ' +obj.informationMessages[0].errorDescription; sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; }         else if(obj.informationMessages == Null) { sbc.AmDocs_Transaction_Description__c='UPDATE BULK ' +obj.responseStatus; sbc.AmDocs_Transaction_Code__c=obj.responseStatus; } else if(obj.responseStatus == Null) { sbc.AmDocs_Transaction_Description__c='UPDATE BULK ERROR Request Rejected'; sbc.AmDocs_Transaction_Code__c='ERROR';} else if ( obj.informationMessages != null) { if (obj.informationMessages.size() > 1) {sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode + ' ' +obj.informationMessages[1].errorCode; sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription + '\n\n' +obj.informationMessages[1].errorDescription +'\n\n';} else if (obj.informationMessages.size() < 2) {sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode; sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription;}    } update sbc;
        System.debug('sbc'+sbc); 
    }
    else if (response.getStatusCode() <= 1990 || response.getStatusCode() >= 300) { Opportunity sbc = new Opportunity(); sbc.id=id; sbc.API_Status__c='API ERROR - UPDATE BULK NONE' +String.valueOf(response.getStatusCode()); update sbc; }
        }
    }
}