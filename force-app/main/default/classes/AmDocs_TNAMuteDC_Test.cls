@isTest
private class AmDocs_TNAMuteDC_Test {
  //Implement mock callout tests here
  
   @testSetup static void testSetupdata(){

    // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'PR';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

        // Create a U.S. based contact
        Contact ctc1 = New Contact();
            ctc1.FirstName = 'Jerry';
            ctc1.LastName = 'Clifft';
            ctc1.Phone = '512-383-5201';
            ctc1.Email = 'jerry.clifft@dish.com';
            ctc1.Role__c = 'Billing Contact (PR)';
            ctc1.AccountId = acct1.Id;
        insert ctc1;
        
        Opportunity o = new Opportunity();
            o.Name = 'Test Opp 1';
            o.AccountId = acct1.Id;
            o.LeadSource='Test Place';
            o.CloseDate=system.Today();
            o.StageName='Closed Won';
            o.Zip__c='78664';
            o.Smartbox_Leased__c = false;
            o.AmDocs_ServiceID__c = '1111111111';
//            o.Mute_Disconnect__c = 'Non-Pay Disconnect';
        insert o;

        Tenant_Account__c tna1 = new Tenant_Account__c();
            tna1.AmDocs_ServiceID__c = '123';
            tna1.Action__c = 'Pending Mute';
            tna1.Opportunity__c = o.Id;
            tna1.Unit__c = '1';
            tna1.Type__c = 'Incremental';
            tna1.Zip__c = '78664';
        insert tna1;
        
        Tenant_Account__c tna2 = new Tenant_Account__c();
            tna2.Action__c = 'Pending UnMute';
            tna2.Opportunity__c = o.Id;
            tna2.Unit__c = '2';
            tna2.Type__c = 'Incremental';
            tna2.Zip__c = '78664';
        insert tna2;

        Tenant_Account__c tna3 = new Tenant_Account__c();
            tna3.AmDocs_ServiceID__c = '456';
            tna3.Action__c = 'Pending Disconnect';
            tna3.Opportunity__c = o.Id;
            tna3.Unit__c = '3';
            tna3.Type__c = 'Incremental';
            tna3.Zip__c = '78664';
            tna3.Reset__c = 'ChewbaccaIsTesting';
        insert tna3;
        
        Tenant_Equipment__c te = new Tenant_Equipment__c();
            te.Opportunity__c = o.Id;
            te.Name = 'R234567819';
            te.Smart_Card__c = 'S-TEST';
            te.Location__c = 'D';
            te.Action__c = 'Mute';
            te.Address__c ='1011 Coliie Path';
            te.City__c = 'Round Rock';
            te.State__c = 'TX';
            te.Zip_Code__c = '78664';
            te.Unit__c = '1A';
            te.Phone_Number__c = '5123835201';
            te.Email_Address__c = 'jerry.clifft@dish.com';
            te.Customer_First_Name__c = 'Jerry';
            te.Customer_Last_Name__c = 'Clifft';
            te.Tenant_Account__c=tna1.Id;
        insert te;
        
         Tenant_Pricebook__c tb = new Tenant_Pricebook__c();
            tb.Name = 'Prcebook Name';
            tb.Pricebook_Name__c = 'Prcebook Name';
        insert tb;

        Tenant_Product__c tp = new Tenant_Product__c();
          tp.Active__c = True;
          tp.AmDocs_Incremental_Caption__c = '123';
          tp.AmDocs_Incremental_ProductID__c = '123';
          tp.AmDocs_Individual_Caption__c = '123';
          tp.AmDocs_Individual_ProductID__c = '123';
          tp.Family__c = 'Core';
          tp.Product_Name__c = 'Test Product 1';
          tp.Name = 'Test Product 1';
        insert tp;

        Tenant_ProductEntry__c tpe = new Tenant_ProductEntry__c();
            tpe.Name = 'Test Product 1';
            tpe.Active__c = True;
            tpe.Tenant_Pricebook__c = tb.id;
            tpe.Tenant_Product__c = tp.id;
        insert tpe;
        
        Tenant_Product_Line_Item__c TOli = new Tenant_Product_Line_Item__c ();
            TOli.Tenant_Account__c = tna1.id;
            TOli.Action__c = 'Active';
            TOli.Tenant_ProductEntry__c = tpe.id;
        insert TOli;
    }
  
  
 static testMethod void AmDocs_CalloutClass_TNAMuteDC_Test1(){
    Tenant_Account__c con1 = [Select Id FROM Tenant_Account__c WHERE Unit__c = '1' Limit 1];
    Tenant_Account__c con2 = [Select Id FROM Tenant_Account__c WHERE Unit__c = '2' Limit 1];
    Tenant_Account__c con3 = [Select Id FROM Tenant_Account__c WHERE Unit__c = '3' Limit 1];
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_TNAMuteDC.AmDocsMakeCalloutTNAMuteDC(con1.Id); 
            AmDocs_CalloutClass_TNAMuteDC.AmDocsMakeCalloutTNAMuteDC(con2.Id); 
            AmDocs_CalloutClass_TNAMuteDC.AmDocsMakeCalloutTNAMuteDC(con3.Id); 
        Test.stopTest(); 
    }

 static testMethod void AmDocs_CalloutClass_TNAMuteDC_Test2(){
    Tenant_Account__c con1 = [Select Id FROM Tenant_Account__c WHERE Unit__c = '1' Limit 1];
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(400,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_TNAMuteDC.AmDocsMakeCalloutTNAMuteDC(con1.Id); 
        Test.stopTest(); 
    }    
}