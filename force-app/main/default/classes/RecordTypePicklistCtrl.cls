public class RecordTypePicklistCtrl {
	
    //Wrapper class to handle the JSON response
    public class picklistWrapper{
		@AuraEnabled public piclistValues[] values;
	}
    //Wrapper class to handle the JSON response
    public class piclistValues {
        @AuraEnabled public String label;	
		@AuraEnabled public String value;
    }
    
    /* Making callout to the UI API to get the list of available picklist values based on the selected recordtype  */
    @AuraEnabled
    public static piclistValues[] getPicklistValues(string recordTypeId, string ObjectApiName,string fieldApiName){
        system.debug('fn1');
        string[] piclistValues=new string[]{};
        string BaseUrl=system.Url.getOrgDomainUrl().toExternalForm();
        
        httprequest request= new httprequest();
        request.setMethod('GET');
        request.setEndpoint(BaseUrl+'/services/data/v44.0/ui-api/object-info/'+ObjectApiName+'/picklist-values/'+recordTypeId+'/'+fieldApiName);
        request.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionID());
        request.setHeader('Content-Type','application/json; charset=UTF-8');
		request.setHeader('Accept','application/json');
        
        system.debug('reqvalue'+request);
        http http = new http();
        httpresponse response=http.send(request);
        system.debug('ResponseBody::'+response.getBody());
        picklistWrapper obj=new picklistWrapper();
        obj=(picklistWrapper)JSON.deserialize(response.getBody(), picklistWrapper.class);
        
        for(piclistValues pickVal:obj.values){
            piclistValues.add(pickVal.value);
        }
        return obj.values;
    }
}