/*-------------------------------------------------------------
Author: Jerry Clifft
Created on: 10th March 2020
Created for: Milestone# MS-000022
Description: For GuideME Bulk REMOVE IsActiveElseWhere Head End Equipment and Smartbox Equipment.
-------------------------------------------------------------*/

public class AmDocs_CalloutClass_BulkUpdateGMDupeRmv {
    
    public class person {
        public String serviceID;
        public String ownerServiceId;
        public String orderID;
        public String responseStatus;
        public cls_equipmentList[] equipmentList;
        public cls_informationMessages[] informationMessages;
        public cls_ImplUpdateBulkRestOutput [] ImplUpdateBulkRestOutput;
    }
    
    class cls_ImplUpdateBulkRestOutput{
        public String serviceID;
        public String orderID;
        public String responseStatus;
    }
    
    class cls_equipmentList {
        public String action;
        public String receiverID;
        public String location;
        public String smartCardID;
        public String leaseInd;
        public String chassisSerialNumber;
        public String serialNumber;
    }
    
    class cls_informationMessages {
        public String errorCode;
        public String errorDescription;
    }
    
    
    @AuraEnabled
    
    public static string TellAmdocstoRemoveThisEquipmentFromServiceID(String Id) {
        String returnStatus = 'Success';
        try{
            list<Opportunity> O = [select id, Name, No_Sports__c, NLOS_Locals__c, AmDocs_tenantType__c, Number_of_Units__c, Smartbox_Leased__c, 
                                   AmDocs_SiteID__c, AmDocs_ServiceID__c, Amdocs_CustomerID__c, AmDocs_BarID__c, AmDocs_Property_Sub_Type__c, 
                                   AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Category__c, AmDocs_Site_Id__c from Opportunity 
                                   where AmDocs_ServiceID__c = :id ];
            System.debug('RESULTS of the LIST lookup to the Opp object' +O);
            
            //Updated by Mahim ------------------------ Start
            //list<Equipment__c> E = [select id, Receiver_S__c, Name, Location__c, Action__c, Opportunity__c from Equipment__c where 
            //                        Opportunity__c = :id  AND Action__c = 'DupeREMOVE' AND Location__c = 'C'];
            list<Equipment__c> E = [select id, Receiver_S__c, Name, Location__c, Action__c, Opportunity__c from Equipment__c where 
                                    Opportunity__c = :O[0].id  AND Action__c = 'DupeREMOVE' AND Location__c = 'C'];
            System.debug('RESULTS of the LIST lookup to the Equipment object' +E);
            
            //list<Smartbox__c> S1 = [select id, Action__c, Location__c, CAID__c, Opportunity__c, Type_of_Equipment__c, AmDocs_No_Send__c, 
            //                        Second_ProCam__c, Serial_Number__c, SmartCard__c, Chassis_Serial__c, Smartbox_Leased__c, id__c 
            //                        from Smartbox__c where Opportunity__c = :id AND  Action__c = 'DupeREMOVE' AND AmDocs_No_Send__c  = false ];
            list<Smartbox__c> S1 = [select id, Action__c, Location__c, CAID__c, Opportunity__c, Type_of_Equipment__c, AmDocs_No_Send__c, 
                                    Second_ProCam__c, Serial_Number__c, SmartCard__c, Chassis_Serial__c, Smartbox_Leased__c, id__c 
                                    from Smartbox__c where Opportunity__c = :O[0].id AND  Action__c = 'DupeREMOVE' AND AmDocs_No_Send__c  = false ];
            //Updated by Mahim ------------------------ End
            System.debug('RESULTS of the LIST lookup to the Smartbox object' +S1);
            
            list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c 
                                       Order By CreatedDate DESC Limit 1 ];
            System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
            
            
            
            JSONGenerator jsonObj = JSON.createGenerator(true);
            if(O.size() > 0) {
                jsonObj.writeStartObject();
                jsonObj.writeFieldName('ImplUpdateBulkRestInput');
                jsonObj.writeStartObject();
                jsonObj.writeStringField('orderActionType', 'CH');
                jsonObj.writeStringField('reasonCode', 'CREQ');
                
                // This is EQUIPMENT - Centeralized ONLY
                jsonObj.writeFieldName('equipmentList');
                jsonObj.writeStartArray();   
                for(Integer i = 0; i < E.size(); i++){
                    jsonObj.writeStartObject();
                    jsonObj.writeStringField('action', 'REMOVE');
                    system.debug( 'DEBUG JSON 2 :' +E[i].Action__c);
                    jsonObj.writeStringField('receiverID', E[i].Name);
                    system.debug( 'DEBUG JSON 3 :' +E[i].Name);
                    jsonObj.writeStringField('smartCardID', E[i].Receiver_S__c);
                    system.debug( 'DEBUG JSON 4 :' +E[i].Receiver_S__c);
                    jsonObj.writeStringField('location', E[i].Location__c);
                    system.debug( 'DEBUG JSON 4 :' +E[i].Location__c);
                    jsonObj.writeEndObject();
                }
                
                // This is SMARTBOX Equipment (CAIDS/Smartbox)
                for(Integer i = 0; i < S1.size(); i++){
                    if(S1[i].CAID__c != Null) {
                        jsonObj.writeStartObject();
                        jsonObj.writeStringField('action', 'REMOVE');
                        system.debug( 'DEBUG JSON 17 :' +S1[i].Action__c);
                        jsonObj.writeStringField('location', S1[i].Location__c);
                        system.debug( 'DEBUG JSON 18 :' +S1[i].Location__c);
                        jsonObj.writeStringField('receiverID', S1[i].CAID__c);
                        system.debug( 'DEBUG JSON 19 :' +S1[i].CAID__c);
                        jsonObj.writeStringField('smartCardID', S1[i].SmartCard__c);
                        system.debug( 'DEBUG JSON 20 :' +S1[i].SmartCard__c);
                        jsonObj.writeEndObject();
                    }
                }
                
                // This is SMARTBOX Equipment (NON-CAIDS/Smartbox)
                for(Integer i = 0; i < S1.size(); i++){
                    if(S1[i].Serial_Number__c != Null && S1[i].AmDocs_No_Send__c == false && S1[i].Second_ProCam__c == false && S1[i].Action__c != 'SWAP' ) {
                        jsonObj.writeStartObject();
                        jsonObj.writeStringField('action', 'REMOVE');
                        jsonObj.writeStringField('location', S1[i].Location__c);
                        jsonObj.writeStringField('chassisSerialNumber', S1[i].Chassis_Serial__c);
// Removed 4-15-2020            if(S1[i].Type_Of_Equipment__c != '16-slot chassis')
                                if(!S1[i].Type_Of_Equipment__c.contains ('hassi'))  // Added 5-15-2020
                        {
                            jsonObj.writeStringField('serialNumber', S1[i].Serial_Number__c);
                        }
                        if(S1[i].Action__c != 'REMOVE') {
                            if(S1[i].Smartbox_Leased__c == true && S1[i].Type_Of_Equipment__c.contains ('hassi')) { jsonObj.writeBooleanField('leaseInd', true); }
                            if(S1[i].Smartbox_Leased__c == false && S1[i].Type_Of_Equipment__c.contains ('hassi')) {
                                jsonObj.writeBooleanField('leaseInd', false);
                            }
                        }
                        jsonObj.writeEndObject();
                    }
                }
                jsonObj.writeEndArray();
                
                HttpRequest request = new HttpRequest(); 
                String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/updateBulk?sc=SS&lo=EN&ca=SF'; 
                request.setEndPoint(endpoint); 
                request.setBody(jsonObj.getAsString()); 
                request.setTimeout(101000);
                request.setHeader('Content-Type', 'application/json');
                request.setHeader('Accept', 'application/json'); request.setHeader('User-Agent', 'SFDC-Callout/45.0');
                request.setMethod('POST'); 
                String authorizationHeader = A[0].UXF_Token__c; request.setHeader('Authorization', authorizationHeader); 
                
                HttpResponse response = new HTTP().send(request); 
                if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { 
                    String strjson = response.getbody();
                    system.debug('strjson - '+ strjson);
                    JSONParser parser = JSON.createParser(strjson); 
                    parser.nextToken(); 
                    parser.nextToken(); 
                    parser.nextToken(); 
                    person obj = (person)parser.readValueAs( person.class); 
                    if( obj.equipmentList != null ) { 
                        for(Integer i = 0; i < obj.equipmentList.size(); i++){
                            System.debug(response.toString());
                            System.debug('STATUS:'+response.getStatus());
                            System.debug('STATUS_CODE:'+response.getStatusCode());
                            System.debug(response.getBody());    
                            System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                            System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID); 
                            System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus); 
                            System.debug('DEBUG 3 ======== obj.equipmentList: ' + obj.equipmentList);
                            System.debug('DEBUG 3.5 ======== obj.equipmentList: ' + obj.equipmentList[i].receiverId);
                        }
                    }
                    System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages); 
                    System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
                    System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus); 
                    
                    // Head-End - Ceteral Equipment (No Smartbox)        
                    if(obj.responseStatus == 'SUCCESS' && E.size() > 0 ){ 
                        
                        if(obj.equipmentList[0].receiverID != null){ 
                            list<string> receverIds = new list<string>(); 
                            for(cls_equipmentList cc0 : obj.equipmentList){ 
                                if(cc0.receiverID != null){ 
                                    receverIds.add(cc0.receiverID);
                                }
                            }
                            if(receverIds != null) { 
                                List<Equipment__c> allEQ = [SELECT Id, Action__c, Name FROM Equipment__C WHERE Name in :receverIds ]; 
                                if( allEQ.Size() > 0 ) { 
                                    for(Equipment__c currentEQ : allEQ){ 
                                        if( currentEQ.Action__c == 'DupeREMOVE'){ 
                                            currentEQ.Action__c = 'Removed'; 
                                        }
                                    } 
                                    update allEQ;
                                }
                            }    
                        } // End of " if(obj.equipmentList[0].receiverID != null) "
                    } // End of " if(obj.responseStatus == 'SUCCESS' && E.size() > 0 ) "
                    
                    
                    // Smartbox Equipment
                    if(obj.responseStatus == 'SUCCESS' && S1.size() > 0 ){ 
                        if(obj.equipmentList[0].chassisSerialNumber != null || obj.equipmentList[0].receiverID != null || obj.equipmentList[0].serialNumber != null ){    
                            list<string> SBoxCHIds = new list<string>(); 
                            list<string> SBoxBLIds = new list<string>(); 
                            list<string> SBoxRVIds = new list<string>(); 
                            for(cls_equipmentList cc : obj.equipmentList){ 
                                if(cc.chassisSerialNumber != null && cc.serialNumber == null) { 
                                    SBoxCHIds.add(cc.chassisSerialNumber); 
                                }
                                if(cc.serialNumber != null) { 
                                    SBoxBLIds.add(cc.serialNumber); 
                                } 
                                if(cc.receiverID != null) { 
                                    SBoxRVIds.add(cc.receiverID );
                                }
                            }
                            List<Smartbox__c> allCHEQ = [SELECT Id, Action__c, Name, Chassis_Serial__c, Serial_Number__c, CAID__c FROM Smartbox__c WHERE  (Chassis_Serial__c in :SBoxCHIds OR Serial_Number__c in :SBoxBLIds OR CAID__c in :SBoxRVIds )]; 
                            if( allCHEQ.Size() > 0 ) { 
                                for(Smartbox__c currentCHEQ : allCHEQ){ 
                                    if( currentCHEQ.Action__c == 'DupeREMOVE' ){ 
                                        currentCHEQ.Action__c = 'Removed'; 
                                    }
                                }  
                                update allCHEQ;
                            }
                        } // End of " if(obj.equipmentList[0].receiverID != null) " 
                    } // End of " if(obj.responseStatus == 'SUCCESS' && T "
                    
                    Opportunity sbc = new Opportunity(); 
                    sbc.id=O[0].id; //Updated by Mahim - Added O[0]. before id
                    sbc.AmDocs_Order_ID__c=obj.orderID; 
                    sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
                    if(obj.informationMessages != Null) { 
                        sbc.AmDocs_Transaction_Description__c='UPDATE BULK ' +obj.responseStatus + ' ' +obj.informationMessages[0].errorDescription; 
                        sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; 
                    }        
                    else if(obj.informationMessages == Null) { 
                        sbc.AmDocs_Transaction_Description__c='UPDATE BULK ' +obj.responseStatus; 
                        sbc.AmDocs_Transaction_Code__c=obj.responseStatus; 
                    }        
                    else if(obj.responseStatus == Null) { 
                        sbc.AmDocs_Transaction_Description__c='UPDATE BULK ERROR Request Rejected'; 
                        sbc.AmDocs_Transaction_Code__c='ERROR';
                    }         
                    else if ( obj.informationMessages != null) { 
                        if (obj.informationMessages.size() > 1)            {
                            sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode + ' ' +obj.informationMessages[1].errorCode; 
                            sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription + '\n\n' +obj.informationMessages[1].errorDescription +'\n\n';
                        }        
                        else if (obj.informationMessages.size() < 2) {
                            sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode; 
                            sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription;
                        }  
                    }  
                    update sbc; 
                    
                    System.debug('sbc'+sbc);
                    
                    API_Log__c apil2 = new API_Log__c();{ 
                        apil2.Record__c=O[0].id;       //Updated by Mahim - Added O[0]. before id
                        apil2.Object__c='Opportunity';       
                        apil2.Status__c='SUCCESS';       
                        apil2.Results__c=strjson;       
                        apil2.API__c='GM Bulk Update Dupe Remove';       
                        apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;       
                        apil2.User__c=UserInfo.getUsername();   
                        insert apil2;  }
                }
                else if (response.getStatusCode() <= 200 || response.getStatusCode() >= 600) {
                    String strjson = response.getbody();
                    Opportunity sbc = new Opportunity();{ 
                        sbc.id=O[0].id;     //Updated by Mahim - Added O[0]. before id
                        sbc.API_Status__c='API ERROR - UPDATE BULK ' +String.valueOf(response.getStatusCode()); update sbc; 
                    }
                    API_Log__c apil2 = new API_Log__c();{ 
                        apil2.Record__c=O[0].id;       //Updated by Mahim - Added O[0]. before id
                        apil2.Object__c='Opportunity';       
                        apil2.Status__c='ERROR';       
                        apil2.Results__c=strjson;       
                        apil2.API__c='GM Bulk Update Dupe Remove';       
                        apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;       
                        apil2.User__c=UserInfo.getUsername();   
                        insert apil2;  }
                }
                
            }
            
            //Incase the Action is not updated to Removed, reverting the Action back to Active - Added by Mahim
            list<Equipment__c> RevertHEAction = new list<Equipment__c>();
            list<Smartbox__c> RevertSBAction = new list<Smartbox__c>();
            List<Equipment__c > checkHEData = [select Id, Action__c from Equipment__c where Opportunity__r.AmDocs_ServiceID__c =: Id 
                                               and Action__c='DupeREMOVE'];
            for(Equipment__c HE: checkHEData){
                HE.Action__c = 'Active';
                RevertHEAction.add(HE);
            }
            if(RevertHEAction.size()>0)
                Update RevertHEAction;
            
            List<Smartbox__c> checkSBData = [select Id, Action__c from Smartbox__c where Opportunity__r.AmDocs_ServiceID__c =: Id 
                                             and Action__c='DupeREMOVE'];
            for(Smartbox__c SB: checkSBData){
                SB.Action__c = 'Active';
                RevertSBAction.add(SB);
            }
            if(RevertSBAction.size()>0)
                Update RevertSBAction;
            
            return returnStatus;
        }
        catch(Exception e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            returnStatus = 'The following exception has occurred: ' + e.getMessage();
            return returnStatus;
        }
    }
}