@isTest
public class OLIChangeLogBD_Test{
    @isTest
    static void tesMethodss()
    {
        account ac=new account(name='test');
        insert ac;

        Pricebook2 pb2 = new Pricebook2(
            Name='Test PB2',
            IsActive=true);
        insert pb2;
        
        pb2.IsActive=true;
        
        Product2 p2 = new Product2(
            Name='AT120 Test',
            IsActive=true,
            Family='Core',
            Category__c='Core',
            Amdocs_ProductID__c = '123456789');
        insert p2;
        
        //add to standrd pricebook        
        Pricebookentry pbe3 = new pricebookentry();
            pbe3.Product2Id=p2.Id;
            pbe3.Pricebook2Id='01s60000000EIELAA4';
            pbe3.UnitPrice=100;
            pbe3.IsActive=true;
        insert pbe3;
        
        Pricebookentry pbe1 = new pricebookentry(
            Product2Id=p2.Id,
//            Pricebook2Id='01s60000000EIELAA4',
            Pricebook2Id=pb2.Id,
            UnitPrice=100,
            IsActive=true);
        insert pbe1;

        Opportunity OD =new Opportunity (
                Name = 'Test',
                BTVNA_Request_Type__c = 'Site Survey',
                LeadSource='Test',
                Pricebook2Id=pb2.id,
                CloseDate=system.Today(),
                StageName='Closed Won');
           insert OD;
           
        OpportunityLineItem oli1 = New OpportunityLineItem(
            OpportunityId = OD.id,
            PriceBookEntryId=pbe1.id,
            Quantity=1,
            UnitPrice=1,
            Action__c='ADD');
        insert oli1;
        
        Equipment__c et = new Equipment__c(
            Opportunity__c = OD.Id,
            Name = 'R123456789',
            Receiver_S__c = 'S123456789',
            Location__c = 'C',
            Action__c = 'ADD');
        insert et;
        
       Tenant_Equipment__c te = new Tenant_Equipment__c(
            Opportunity__c = OD.Id,
            Name = 'R234567819',
            Smart_Card__c = 'S1234567891',
            Location__c = 'D',
            Action__c = 'ADD',
            Address__c ='1011 Coliie Path',
            City__c = 'Round Rock',
            State__c = 'TX',
            Zip_Code__c = '78664',
            Unit__c = '1A',
            Phone_Number__c = '5123835201',
            Email_Address__c = 'jerry.clifft@dish.com',
            Customer_First_Name__c = 'Jerry',
            Customer_Last_Name__c = 'Clifft');
        insert te;

        Tenant_Pricebook__c pb1 = new Tenant_Pricebook__c(
            Name = 'Test PB1',
            Pricebook_Name__c = 'Test PB1');
        insert pb1;
                
        Tenant_Product__c p1 = new Tenant_Product__c(
            Name='AT120 Test',
//            Active=true,
//            Family='Core',
            AmDocs_Incremental_ProductID__c = '123456789',
            AmDocs_Individual_ProductID__c = '123456789' );
        insert p1;
                
        Tenant_ProductEntry__c tpbe1 = new Tenant_ProductEntry__c(
            Name = 'AT120 Test',
            Tenant_Pricebook__c = pb1.Id,
            Tenant_Product__c = p1.Id );
        insert tpbe1;
        
        Tenant_Account__c ta1 = new Tenant_Account__c(
            Opportunity__c = OD.Id,
            Address__c ='1011 Coliie Path',
            City__c = 'Round Rock',
            State__c = 'TX',
            Zip__c = '78664',
            Unit__c = '1A',
            Type__c = 'Incremental',
            Phone_Number__c = '5123835201',
            Email_Address__c = 'jerry.clifft@dish.com',
            First_Name__c = 'Jerry',
            Last_Name__c = 'Clifft');
        insert ta1;
               
        Tenant_Product_Line_Item__c POli1 = new Tenant_Product_Line_Item__c(
            Tenant_ProductEntry__c = tpbe1.Id,
            Tenant_Account__c = ta1.Id,
            Quantity__c = 1 );
        insert POli1;
        
        Tenant_Product_Line_Item__c DPOli1 = [SELECT Id, Price__c FROM Tenant_Product_Line_Item__c WHERE Quantity__c  = 1];
        delete DPOli1;
                
        OpportunityLineItem DOli1 = [SELECT Id, Quantity from OpportunityLineItem WHERE Quantity = 1];
        delete DOli1;
        
        Equipment__c Det = [SELECT Id, Name from Equipment__c WHERE Name = 'R123456789'];
        delete Det;
        
        Tenant_Equipment__c Dte2 = [SELECT Id, Name from Tenant_Equipment__c WHERE Name = 'R234567819'];
        delete Dte2;
       
        Account acc =[SELECT Id,Name from Account where name = 'test'];
        delete acc;
        
        
    }
}