@isTest
//test Data Factory for SIMP_IDFANDBuildingTestClass
//Fiber_Building and Fiber_IDF lightning components
//Christian Comia - 20190719 - initial version 
public class SIMP_TestDataFactory {
    public static void createQuote(Integer numRelated, String objrel, String scenario, Integer numOfClones) {
        
        SBQQ__Quote__c newQuote = new SBQQ__Quote__c();
        newQuote.Contract_Term__c = 12;
        
        insert newQuote;
        newQuote = [SELECT ID, Name FROM SBQQ__Quote__c WHERE ID =: newQuote.ID];
        
        if(objrel == 'BLDG'){
            
            if(numOfClones != 0){
                createBLDGClone(numRelated,newQuote.id,scenario,newQuote.Name);
            }
            else if(scenario == 'ChangeQuoteName'){
                createBLDG(numRelated,newQuote.id,scenario,'dummy');
            }
            else{
                
                createBLDG(numRelated,newQuote.id,scenario,newQuote.Name);
                
            }
        }
        else if(objrel == 'IDF'){
            
            if(numOfClones != 0){
                createIDFClone(numRelated,newQuote.id,scenario,newQuote.Name);
            }
            else if(scenario == 'ChangeQuoteName'){
                createIDF(numRelated,newQuote.id,scenario,'dummy');
            }
            else{
                
                createIDF(numRelated,newQuote.id,scenario,newQuote.Name);
            }
        }
    }
    
    public static List<SBQQ__QuoteLine__c> createQuoteWithQuoteLines(Integer numQuote, Integer numOfQls, String typeOfu) {
        
        Product2 sbqpc = [SELECT Id, Name FROM Product2 WHERE Name = 'DISH Fiber Solution'];
        List<SBQQ__Quote__c> newQuote = new List<SBQQ__Quote__c>();
        for(Integer x = 0;x<numQuote;x++){
            SBQQ__Quote__c q = new SBQQ__Quote__c();
            q.Contract_Term__c = 50;
            q.Annual_ARPU_Increase__c = 0.04;
            q.Monthly_Cash_Flow__c = 0;
            q.No_of_Units__c = 76;
            q.No_of_Ports_per_Unit__c = 1;
            q.Year_1__c = 21000;
            q.Year_2__c = 21313;
            q.Year_3__c = 21313;
            q.Year_4__c = 21313;
            q.Year_5__c = 3813;
            q.Year_6__c = 0;
            q.Year_7__c = 0;
            q.Year_8__c = 0;
            q.Year_9__c = 0;
            q.Year_10__c = 0;
            newQuote.add(q);
        }
        insert newQuote;
        
        List<SBQQ__QuoteLine__c> sbql = new List<SBQQ__QuoteLine__c>();
        List<SBQQ__QuoteLine__c> sbqli = new List<SBQQ__QuoteLine__c>();
        for(Integer i = 0;i<numQuote;i++){
            SBQQ__Quote__c newQuote1 = newQuote[i];
            
            for(Integer y = 0;y<numOfQls;y++){
                sbqli.add(new SBQQ__QuoteLine__c(
                    SBQQ__OptionLevel__c = null,
                    
                    SBQQ__ComponentCost__c = 70338.94,
                    SBQQ__ComponentListTotal__c = 156088.94,
                    SBQQ__ComponentTotal__c = 156088.94,
                    SBQQ__SubscriptionBase__c = 'List',
                    SBQQ__SubscriptionScope__c = 'Quote',
                    SBQQ__Product__c = sbqpc.id,
                    SBQQ__Quote__c = newQuote1.id)
                         );
            }
            
            
        }
        
        insert sbqli;
        if(typeOfu == 'update'){
            SBQQ__QuoteLine__c cc = sbqli[0];
            cc.ARPU__c = 50;
            update cc;
            
            sbql = [SELECT ID, Name,ARPU__c, SBQQ__OptionLevel__c,SBQQ__Quote__c,SBQQ__PackageTotal__c, Contract_Term__c FROM SBQQ__QuoteLine__c WHERE ID =: cc.id]; 
            
        }
        else{
            sbql.add(sbqli[0]);
        }
        
        return sbql;
        
        
    }
    
    public static void updateQuote(Integer numRelated, String objrel, String scenario, Integer numOfClones) {
        SBQQ__Quote__c newQuote = new SBQQ__Quote__c();
        newQuote.Contract_Term__c = 12;
        newQuote.No_of_Ports_per_Unit__c = 1;
        insert newQuote;
        newQuote = [SELECT ID, Name FROM SBQQ__Quote__c WHERE ID =: newQuote.ID];
        
        if(objrel == 'BLDG'){
            if(scenario.contains('BLDG#2')){
                updateBldngMult(newQuote.ID,newQuote.Name,scenario);
            }
            else if(scenario == 'less'){
                updateBldngMult(newQuote.ID,newQuote.Name,'BLDG#2');
            }
            else if(scenario == 'more'){
                updateBldngMult(newQuote.ID,newQuote.Name,'BLDG#3');
            }
            else if(scenario == 'nomatch'){
                updateBldng(newQuote.ID,newQuote.Name,'33-BLDG#1');
            }
            else if(scenario.contains('Q-99999')){
                updateBldng(newQuote.ID,newQuote.Name,scenario);
            }
            else if(scenario != ''){
                updateBldng(newQuote.ID,newQuote.Name,scenario);
            }
            else{
                updateBldng(newQuote.ID,newQuote.Name,'');  
            }
        }
        else if(objrel == 'IDF'){
            if(scenario.contains('IDF#2')){
                updateIDFMult(newQuote.ID,newQuote.Name,scenario);
            }
            else if(scenario == 'less'){
                updateIDFMult(newQuote.ID,newQuote.Name,'IDF#2');
            }
            else if(scenario == 'more'){
                updateIDFMult(newQuote.ID,newQuote.Name,'IDF#3');
            }
            else if(scenario == 'nomatch'){
                updateIDF(newQuote.ID,newQuote.Name,'33-IDF#1');
            }
            else if(scenario.contains('Q-99999')){
                updateIDF(newQuote.ID,newQuote.Name,scenario);
            }
            else if(scenario != ''){
                updateIDF(newQuote.ID,newQuote.Name,scenario);
            }
            else{
                updateIDF(newQuote.ID,newQuote.Name,'');
            }
        }
    }
    
    public static void updateBldng(ID qID, String qName,String modval){
        Fiber_Building_Detail__c fbdc = new Fiber_Building_Detail__c();
        fbdc.Quote__c = qid;
        fbdc.Name = qName;  
        insert fbdc;
        
        fbdc= [select Id, Name from Fiber_Building_Detail__c where id = : fbdc.id];
        
        if(modval != '' && !modval.contains('BLDG#0') && !modval.contains('33-BLDG#1')){
            fbdc.Name = modval;
        }
        if(modval.contains('BLDG#0') || modval.contains('33-BLDG#1')){
            fbdc.Name = qName + modval;
            system.debug('ZERO' + fbdc.Name);
        }
        update fbdc;
    }
    
    public static void updateBldngMult(ID qID, String qName,String modval){
        Fiber_Building_Detail__c fbdc = new Fiber_Building_Detail__c();
        fbdc.Quote__c = qid;
        fbdc.Name = qName;  
        insert fbdc;
        
        Fiber_Building_Detail__c fbdc2 = new Fiber_Building_Detail__c();
        fbdc2.Quote__c = qid;
        fbdc2.Name = qName;  
        insert fbdc2;
        
        Fiber_Building_Detail__c fbdc3 = new Fiber_Building_Detail__c();
        fbdc3.Quote__c = qid;
        fbdc3.Name = qName;  
        insert fbdc3;
        
        
        if(modval == 'BLDG#3' ){
            fbdc= [select Id, Name from Fiber_Building_Detail__c where id = : fbdc.id];
            fbdc.Name = qName + modval;
            
            update fbdc;  
        }
        else if(modval != 'BLDG#2' ){
            fbdc= [select Id, Name from Fiber_Building_Detail__c where id = : fbdc.id];
            fbdc.Name = qName + modval;
            
            update fbdc;  
        }
        else{
            
            fbdc3= [select Id, Name from Fiber_Building_Detail__c where id = : fbdc3.id];
            fbdc3.Name = qName + modval;  
            
            update fbdc3;   
        }
        
    }
    
    public static void updateIDF(ID qID, String qName,String modval){
        Fiber_IDF_Detail__c fic = new Fiber_IDF_Detail__c();
        fic.No_of_Units_Connected_to_IDF__c = 1;
        fic.IDF_Location__c = 'Indoor';  
        fic.Quote__c = qID;    
        fic.Name = qName;
        fic.No_of_Ports_per_Unit__c = 1;
        fic.Install_RF__c = 'Yes';
                fic.Usable_Pathway_IDF_to_MDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_Units__c = 'Yes';
        insert fic;
        
        fic= [select Id,Quote__c,Name FROM Fiber_IDF_Detail__c where id = : fic.id];
        
        if(modval != '' && !modval.contains('IDF#0') && !modval.contains('33-IDF#1')){
            fic.Name = modval;
        }
        if(modval.contains('IDF#0') || modval.contains('33-IDF#1')){
            fic.Name = qName + modval;
            system.debug('ZERO' + fic.Name);
        }
        
        update fic;
    }
    
    public static void updateIDFMult(ID qID, String qName,String modval){
        Fiber_IDF_Detail__c fic = new Fiber_IDF_Detail__c();
        fic.No_of_Units_Connected_to_IDF__c = 1;
        fic.IDF_Location__c = 'Indoor';  
        fic.Quote__c = qID;    
        fic.Name = qName;
        fic.No_of_Ports_per_Unit__c = 1;
        fic.Install_RF__c = 'Yes';
        fic.Usable_Pathway_Common_Area_to_IDF__c = 'Yes';
                fic.Usable_Pathway_IDF_to_MDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_Units__c = 'Yes';
        insert fic;
        
        Fiber_IDF_Detail__c fic2 = new Fiber_IDF_Detail__c();
        fic2.No_of_Units_Connected_to_IDF__c = 1;
        fic2.IDF_Location__c = 'Indoor';  
        fic2.Quote__c = qID;    
        fic2.Name = qName;
        fic2.No_of_Ports_per_Unit__c = 1;
        fic2.Install_RF__c = 'Yes';
        fic.No_of_Ports_per_Unit__c = 1;
        fic.Install_RF__c = 'Yes';
        fic.Usable_Pathway_Common_Area_to_IDF__c = 'Yes';
                fic.Usable_Pathway_IDF_to_MDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_Units__c = 'Yes';
        insert fic2;
        
        Fiber_IDF_Detail__c fic3 = new Fiber_IDF_Detail__c();
        fic3.No_of_Units_Connected_to_IDF__c = 1;
        fic3.IDF_Location__c = 'Indoor';  
        fic3.Quote__c = qID;    
        fic3.Name = qName;
        fic3.No_of_Ports_per_Unit__c = 1;
        fic3.Install_RF__c = 'Yes';
        fic.Install_RF__c = 'Yes';
        fic.Usable_Pathway_Common_Area_to_IDF__c = 'Yes';
                fic.Usable_Pathway_IDF_to_MDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_Units__c = 'Yes';
        fic.No_of_Ports_per_Unit__c = 1;
        insert fic3;
        
        
        if(modval == 'IDF#3'){
            fic= [select Id,Quote__c,Name FROM Fiber_IDF_Detail__c where id = : fic.id];
            fic.Name = qName + modval;
            fic.Install_RF__c = 'Yes';
            fic.Usable_Pathway_Common_Area_to_IDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_MDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_Units__c = 'Yes';            
            fic.No_of_Ports_per_Unit__c = 1;
            update fic;  
        }
        
        else if(modval != 'IDF#2'){
            fic= [select Id,Quote__c,Name FROM Fiber_IDF_Detail__c where id = : fic.id];
            fic.Name = qName + modval;
            fic.Install_RF__c = 'Yes';
            fic.Usable_Pathway_Common_Area_to_IDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_MDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_Units__c = 'Yes';
            fic.No_of_Ports_per_Unit__c = 1;
            update fic;  
        }
        else{
            fic3= [select Id,Quote__c,Name FROM Fiber_IDF_Detail__c where id = : fic3.id];
            fic3.Name = qName + modval;
            fic3.Install_RF__c = 'Yes';             
            fic.Usable_Pathway_Common_Area_to_IDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_MDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_Units__c = 'Yes';
            fic.No_of_Ports_per_Unit__c = 1;
            
            update fic3;   
        }
    }
    
    public static void createsingleBLDG(ID qid, String qname, String toUpdate){
        Fiber_Building_Detail__c fbdc = new Fiber_Building_Detail__c();
        fbdc.Quote__c = qid;
        fbdc.Name = qname;  
        insert fbdc;
    }
    
    public static void createsingleIDF(ID qid, String qname, String toUpdate){
        Fiber_IDF_Detail__c fic = new Fiber_IDF_Detail__c();
        fic.No_of_Units_Connected_to_IDF__c = 1;
        fic.IDF_Location__c = 'Indoor';  
        fic.Quote__c = qID;    
        fic.Name = qname;
        fic.No_of_Ports_per_Unit__c = 1;
        fic.Install_RF__c = 'Yes';
        fic.Usable_Pathway_Common_Area_to_IDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_MDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_Units__c = 'Yes';
        fic.No_of_Ports_per_Unit__c = 1;
        insert fic;
    }
    
    public static void createBLDG(Integer count, ID qID, String sce, String qname){
        List<Fiber_Building_Detail__c> fbcList = new List<Fiber_Building_Detail__c>();
        
        if(count == 1 && sce != 'ChangeQuoteName' && sce != 'ChangeQuoteName1'){
            createsingleBLDG(qID,qname,'');
        }
        
        if(sce == 'ChangeQuoteName'){
            system.debug('TESTME' + qname +' '+ count);
            sce = qname;
        }
        else if(sce == 'ChangeQuoteName1'){
            sce = qname;
            createsingleBLDG(qID,qname,'');
        }
        
        for(Integer i = 1; i<=count; i++){
            Fiber_Building_Detail__c fbdcLoop = new Fiber_Building_Detail__c();
            fbdcLoop.Quote__c = qID;
            fbdcLoop.Name = sce;  
            fbcList.add(fbdcLoop);
        }
        insert fbcList;
    }
    
    public static void createIDF(Integer count, ID qID, String sce,String qname){
        List<Fiber_IDF_Detail__c> ficList = new List<Fiber_IDF_Detail__c>();
        
        if(count == 1 && sce != 'ChangeQuoteName' && sce != 'ChangeQuoteName1'){
            createsingleIDF(qID,qname,'');
        }
        
        if(sce == 'ChangeQuoteName'){
            sce = qname;
        }
        else if(sce == 'ChangeQuoteName1'){
            sce = qname;
            createsingleIDF(qID,qname,'');
        }
        
        for(Integer i = 1; i<=count; i++){
            Fiber_IDF_Detail__c ficLoop = new Fiber_IDF_Detail__c();  
            ficLoop.No_of_Ports_per_Unit__c = 1;
            ficLoop.No_of_Units_Connected_to_IDF__c = 1;
            ficLoop.IDF_Location__c = 'Indoor';  
            ficLoop.Quote__c = qID;    
            ficLoop.Name = sce;
            ficLoop.No_of_Ports_per_Unit__c = 1;
            ficLoop.Install_RF__c = 'Yes';
            ficLoop.Usable_Pathway_Common_Area_to_IDF__c = 'Yes';
            ficLoop.Usable_Pathway_IDF_to_MDF__c = 'Yes';
            ficLoop.Usable_Pathway_IDF_to_Units__c = 'Yes';
            ficList.add(ficLoop);
        }
        insert ficList;
        
    }
    
    public static void createBLDGClone(Integer count, ID qID, String sce,String qname){
        
        Fiber_Building_Detail__c fbdc = new Fiber_Building_Detail__c();
        fbdc.Quote__c = qid;
        fbdc.Name = qname;  
        insert fbdc;
        
        fbdc = [select Id, Name, Quote__c from Fiber_Building_Detail__c where id = : fbdc.id];
        
        Fiber_Building_Detail__c clonefb = fbdc.clone(false,true,false,false);
        insert clonefb;
        
    }
    
    public static void createIDFClone(Integer count, ID qID, String sce,String qname){
        
        Fiber_IDF_Detail__c fic = new Fiber_IDF_Detail__c();
        fic.No_of_Units_Connected_to_IDF__c = 1;
        fic.IDF_Location__c = 'Indoor';  
        fic.Quote__c = qID;    
        fic.Name = qname;
        fic.No_of_Ports_per_Unit__c = 1;
        fic.Install_RF__c = 'Yes';
        fic.Usable_Pathway_Common_Area_to_IDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_MDF__c = 'Yes';
        fic.Usable_Pathway_IDF_to_Units__c = 'Yes';
        fic.No_of_Ports_per_Unit__c = 1;
        insert fic; 
        
        fic = [select Id, Name, Quote__c ,Install_RF__c, No_of_Ports_per_Unit__c from Fiber_IDF_Detail__c where id = : fic.id];
        
        Fiber_IDF_Detail__c clonefi = fic.clone(false,true,false,false);
        insert clonefi;
        
    }
    
}