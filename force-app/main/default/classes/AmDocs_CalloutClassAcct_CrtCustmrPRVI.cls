public class AmDocs_CalloutClassAcct_CrtCustmrPRVI {

public class person {
    public String customerID;
    public String faID;
    public String barID;
    public String errorCode;
    public String backendErrorMessage;
    }


    @future(callout=true)

    public static void AmDocsMakeCalloutCreateCustomer(String Id) {
    
list<Account> O = [select id, Name, Distributor__c, OE_AR__c, ToP__c, AmDocs_Customer_Sub_Type2__c, Pyscical_Address__c, City__c, State__c, Zip__c from Account where Id = :id AND State__c = 'PR' AND OE_AR__c != NULL ];
   System.debug('RESULTS of the LIST lookup to the Account object' +O);

list<Contact> C = [select id, FirstName, LastName, Phone, Email, Role__c, AccountID FROM Contact WHERE AccountID = :Id AND Role__c = 'Billing Contact (PR)' Order By CreatedDate DESC Limit 1];
   System.debug('RESULTS of the LIST lookup to the Contact object' +C);     
   
list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        jsonObj.writeFieldName('ImplCreateFullCustomerSetUpInput');
            jsonObj.writeStartObject();
                 jsonObj.writeFieldName('customerDetails');
                     for(Integer i = 0; i < O.size(); i++){
                         jsonObj.writeStartObject();
                             jsonObj.writeStringField('name', O[i].Name);
                             if(O[i].ToP__c == 'Integrator'){
                                  jsonObj.writeStringField('type', 'I');
                            }
                            else
                            if(O[i].ToP__c == 'PCO'){
                                jsonObj.writeStringField('type', 'P');
                            }
                            else
                            if(O[i].ToP__c == 'Direct'){
                               jsonObj.writeStringField('type', 'D');
                            }
                            else
                            if(O[i].ToP__c == 'Retailer'){
                                jsonObj.writeStringField('type', 'T');
                            }
                            jsonObj.writeStringField('subType', O[i].AmDocs_Customer_Sub_Type2__c);
//                            if(O[i].OE_AR__c != '' && O[i].OE_AR__c != null ){
//                                jsonObj.writeStringField('dealerCode', O[i].OE_AR__c);
//                            }                      
                           if(O[i].Distributor__c != '' && O[i].Distributor__c != null ){if(O[i].Distributor__c == 'PACE' ){ jsonObj.writeStringField('distributorCode', '21493339'); } else if(O[i].Distributor__c == 'SMS' ){ jsonObj.writeStringField('distributorCode', '9325'); }else if(O[i].Distributor__c == '4COM' ){ jsonObj.writeStringField('distributorCode', '71701'); }else if(O[i].Distributor__c == 'CVS Systems Incorporated' ){jsonObj.writeStringField('distributorCode', '1001');}else if(O[i].Distributor__c == 'All Systems Satellite' ){ jsonObj.writeStringField('distributorCode', '21844'); }else if(O[i].Distributor__c == 'DOW Electronics' ){ jsonObj.writeStringField('distributorCode', '1007'); }else if(O[i].Distributor__c == 'RS&I' ){ jsonObj.writeStringField('distributorCode', '1024'); }else if(O[i].Distributor__c == 'Mid-State Distributing' ){ jsonObj.writeStringField('distributorCode', '23439'); }
              }
                           jsonObj.writeEndObject();
                        }
                
                   jsonObj.writeFieldName('customerAddress');
                       for(Integer i = 0; i < O.size(); i++){
                           jsonObj.writeStartObject();
                               jsonObj.writeStringField('state', O[i].State__c);
                               jsonObj.writeStringField('country', 'US');
                               jsonObj.writeStringField('city', O[i].City__c);            
                               jsonObj.writeStringField('addressLine1', O[i].Pyscical_Address__c);            
                               // address 2
                               // address format
                               // geocode
                               jsonObj.writeStringField('zipCode', O[i].Zip__c);
                               // zip +4
                           jsonObj.writeEndObject();
                       }
            
                   jsonObj.writeFieldName('customerAccount');
                       for(Integer i = 0; i < O.size(); i++){
                           jsonObj.writeStartObject();
                                 jsonObj.writeStringField('name', O[i].Name);
                           jsonObj.writeEndObject();
                       }

                   jsonObj.writeFieldName('customerBillingData');
                       for(Integer i = 0; i < O.size(); i++){
                           jsonObj.writeStartObject();
                               jsonObj.writeStringField('faName', O[i].Name + ' FAR');
                               jsonObj.writeStringField('barName', O[i].Name + ' BAR');
                               jsonObj.writeStringField('billFormat', 'CSV');
                           jsonObj.writeEndObject();
                       }

                   jsonObj.writeFieldName('customerContact');
                       for(Integer i = 0; i < O.size(); i++){
                           jsonObj.writeStartObject();
                               // Salutation
                               jsonObj.writeStringField('firstName', C[i].FirstName);
                               // Middle Name
                               jsonObj.writeStringField('lastName', C[i].LastName);
                               jsonObj.writeStringField('phone', C[i].Phone);
                               jsonObj.writeStringField('email', C[i].Email);
                           jsonObj.writeEndObject();
                   }
           jsonObj.writeEndObject();
    

String finalJSON = jsonObj.getAsString();
System.debug('Jerry Debug 0 === json string: '   + finalJSON);
   
 if (!Test.isRunningTest()){ HttpRequest request = new HttpRequest(); String endpoint = A[0].End_Point_Environment__c+'/commerce/createCustomer?lo=EN'; request.setEndPoint(endpoint); request.setBody(jsonObj.getAsString()); request.setHeader('Content-Type', 'application/json');request.setHeader('Accept', 'application/json');request.setHeader('User-Agent', 'SFDC-Callout/45.0'); request.setMethod('POST');  String authorizationHeader = A[0].UXF_Token__c ; request.setHeader('Authorization', authorizationHeader); HttpResponse response = new HTTP().send(request);  System.debug(response.getBody());request.setTimeout(101000); if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { String strjson = response.getbody(); JSONParser parser = JSON.createParser(strjson); parser.nextToken(); parser.nextToken(); parser.nextToken(); person obj = (person)parser.readValueAs( person.class);
                         System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
                         System.debug('DEBUG 1 ======== obj.customerID: ' + obj.customerID);
                         System.debug('DEBUG 2 ======== obj.faID: ' + obj.faID);
                         System.debug('DEBUG 3 ======== obj.barID: ' + obj.barID);
                   
                    Account sbc = new Account(); sbc.id=id; sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); sbc.Amdocs_CustomerID_PRVI__c=obj.customerID; sbc.AmDocs_FAID_PRVI__c=obj.faID; sbc.AmDocs_barID_PRVI__c=obj.barID; sbc.API_Status__c=String.valueOf(response.getStatusCode()); if(obj.customerID != null){ sbc.AmDocs_Transaction_Code__c='SUCCESS'; sbc.AmDocs_Transaction_Description__c='CUSTOMER (PR) ID, FINANCIAL ACCOUNT (PR) ID, and  BILLING ARRANGEMENT (PR) ID CREATED'; } if(obj.backendErrorMessage != null){ sbc.AmDocs_Transaction_Code__c='FAILED'; sbc.AmDocs_Transaction_Description__c=obj.backendErrorMessage; } update sbc;
                           System.debug('sbc'+sbc);
                }
                else if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { Account sbc = new Account(); sbc.id=id; sbc.API_Status__c=String.valueOf(response.getStatusCode()); sbc.AmDocs_FullString_Return__c=String.valueOf(response.getBody()) + ' ' + system.now(); update sbc;
             }
        }
    }
}