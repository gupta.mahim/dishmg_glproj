@isTEST
private class Smartbox_Accounting_Test{

    private static testmethod void testTriggerForCase(){
        Account a = new Account();
            a.Name = 'Test Account';
  //          a.Programming__c = 'Starter';
  //          a.phone = '(303) 555-5555';
        insert a;
        
        Opportunity o = new Opportunity();
            o.Name = 'Test';
            o.AccountId = a.Id;
            o.Smartbox_Leased__c = true;
    //        o.BTVNA_Request_Type__c = 'Site Survey';
    //        o.LeadSource='BTVNA Portal';
    //        o.Property_Type__c='Private';
    //        o.misc_code_siu__c='N999';
            o.CloseDate=system.Today();
            o.StageName='Closed Won';
    //        o.Restricted_Programming__c = 'True';
        insert o;
       
       Smartbox__c s = new Smartbox__c();
           s.Chassis_Serial__c = 'LALPFY00203M';
           s.CAID__c = 'R121212123';
           s.CSG_Sub_Account__c = '825500000000000000';
           s.MEID__c = '1234';
           s.Opportunity__c = o.id;
           s.Part_Number__c = 'DN123';
           s.Serial_Number__c = 'ABCD123';
           s.SmartCard__c = 'S000000000000';
//           s.Opportunity_Status__c = '';
//           s.SF_Status__c = 'Active';
           s.CSG_Status__c = 'Active';
//           s.CaseRequestedCompletionDate__c = 
//           s.CaseRequestType__c = SB.CaseRequestType__c,
//           s.Case_Number__c = SB.Case_Number__c,
           s.Type_of_Equipment__c = '24-Channel analog NTSC TV blade';

      insert s;
         }
}