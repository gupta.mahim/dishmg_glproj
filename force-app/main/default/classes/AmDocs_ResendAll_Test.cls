@isTest
private class AmDocs_ResendAll_Test {
  //Implement mock callout tests here
  
   @testSetup static void testSetupdata(){

    // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'PR';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

        // Create a U.S. based contact
        Contact ctc1 = New Contact();
            ctc1.FirstName = 'Jerry';
            ctc1.LastName = 'Clifft';
            ctc1.Phone = '512-383-5201';
            ctc1.Email = 'jerry.clifft@dish.com';
            ctc1.Role__c = 'Billing Contact (PR)';
            ctc1.AccountId = acct1.Id;
        insert ctc1;
        
        Opportunity o = new Opportunity();
            o.Name = 'Test Opp 1';
            o.AccountId = acct1.Id;
            o.LeadSource='Test Place';
            o.Zip__c='78664';
            o.CloseDate=system.Today();
            o.StageName='Closed Won';
            o.Smartbox_Leased__c = false;
            o.AmDocs_ServiceID__c = '1111111111';
        insert o;

        Smartbox__c s = new Smartbox__c();
            s.Chassis_Serial__c='A1234';
            s.Part_Number__c='DN004343';
            s.Serial_Number__c='L23PR6789ABC';
            s.CAID__c='R1234567';
            s.SmartCard__c='S1234567';
            s.Opportunity__c=o.id;
            s.Smartbox_Leased2__c=false;
            s.RA_Number__c = 'RA123';

            insert s;

        
    }
  
  
  @isTest
  static void Acct1(){
    Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 1' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp);  
        System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
  
        String sendThisID = opp.Id;
        System.Debug('DEBUG 2 sendThisID : ' +sendThisID);
  
      // Set mock callout class
      Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
  
      // This causes a fake response to be sent
      // from the class that implements HttpCalloutMock. 
      Test.startTest();
          AmDocs_CalloutClass_ReSendAll.AmDocsMakeCalloutReSendAll(sendThisID);
      Test.stopTest();    
  
  // Verify that the response received contains fake values        
      opp = [select AmDocs_ServiceID__c from Opportunity where id =: opp.Id];
//        System.assertEquals(null,opp.AmDocs_ServiceID__c);     
//      System.assertEquals('0123456789',opp.AmDocs_ServiceID__c);     
  }
}