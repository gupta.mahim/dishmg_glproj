public class Sites_Controller_SFLCase {
        public Sites_Controller_SFLCase() {}

        public Case SFLC {get; set;}
        public Case cas {get;set;}
        public string error {get; set;}
           
       public Sites_Controller_SFLCase (ApexPages.StandardController controller) 
           {   
               SFLC = new Case
                   (AccountId=ApexPages.currentPage().getParameters().get('acctId'), 
                   Property_Type__c=ApexPages.currentPage().getParameters().get('cat'),
                   Subject=ApexPages.currentPage().getParameters().get('type'),
                   Status='Form Submitted',
                   Origin='Salesforce Lite',
                   RecordTypeId='0126000000017Vk',
                   Description = '* Property/Location Name/ID:' + '\n' + '* Street: \n' + '* City: \n' + '* State: \n' + '* Zipcode: \n'  + '* Description: \n'
                   );
           }     




  public PageReference createSFLCase(){
     if(SFLC.Contact_Name__c==null){error='Error: Please Enter Contact Name'; return null;}
     if(SFLC.Contact_Phone__c==null){error='Error: Please Enter Contact Phone'; return null;}
     if(SFLC.Contact_Email__c==null){error='Error: Please Enter Contact Email'; return null;}
//     if(SFLC.RequestedAction__c!='Activation' || SFLC.RequestedAction__c!='Change' || SFLC.RequestedAction__c!='Disconnection' || SFLC.RequestedAction__c!='Restart' ){error='Error: Please Enter a Requested Action'; return null;}
     if(SFLC.RequestedAction__c==null){error='Error: Please Enter a Requested Action'; return null;}
     if(SFLC.Requested_Actvation_Date_Time__c==null){error='Error: Please Enter Requested Dated'; return null;}
     if(SFLC.Accept_Terms__c==False){error='Error: You must accept the terms before requesting this action.'; return null;}
     //   opp.Password__c=''; 
//   opp.Tax_Status__c=''; 
//   opp.Promotion__c=''; 
//   opp.TVs_Installed__c='No'; 
//   opp.Point_of_Entry__c='No'; 
//   opp.Landlord_permission__c='No'; 
//   opp.Roof_Access__c='No';  
//     SFLC.AccountId='0016000000ev8ZXAAY';
     SFLC.Status='Form Submitted';
     SFLC.Origin='Salesforce Lite';
     SFLC.RecordTypeId='0126000000017LQ';
     SFLC.Bulk_Load__c=True;
     SFLC.Subject=ApexPages.currentPage().getParameters().get('type');
     insert SFLC;
  
  cas = [select Id, CaseNumber, AccountId, Status from Case where (AccountId = :ApexPages.currentPage().getParameters().get('acctId')) and Status = 'Form Submitted' Limit 1 ];  
  if(ApexPages.currentPage().getParameters().get('cat') == 'Public' )   {       return new PageReference('/Salesforcelite/Sites_SFLThanks?casid='+ cas.CaseNumber
                                                            + '&acctId=' + ApexPages.currentPage().getParameters().get('acctId') 
                                                            + '&acp=' + ApexPages.currentPage().getParameters().get('acp') );
  }
        else         {       return new PageReference('/Salesforcelite/Sites_SFLThanks?casid='+ cas.CaseNumber
                                                            + '&acctId=' + ApexPages.currentPage().getParameters().get('acctId') 
                                                            + '&acp=' + ApexPages.currentPage().getParameters().get('acp') );
        }     
   }
    }