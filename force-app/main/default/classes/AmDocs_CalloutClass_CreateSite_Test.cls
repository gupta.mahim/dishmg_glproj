@isTest
private class AmDocs_CalloutClass_CreateSite_Test{
    //Implement mock callout tests here
  
     Public static testMethod void getAcct(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

        Account acct = TestDataFactoryforInternal.CreateAccount();
        Opportunity opp1= TestDataFactoryforInternal.createOppforCallout(acct);
       
        List<AmDocs_CalloutClass_CreateSite.FlowInputs> ListOfFlowInp = new List<AmDocs_CalloutClass_CreateSite.FlowInputs>();
        AmDocs_CalloutClass_CreateSite.FlowInputs FlowInpObj = new AmDocs_CalloutClass_CreateSite.FlowInputs();
        
        
        Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 1' Limit 1];
      
        List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp); System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
        String sendThisID = opp.Id;
        FlowInpObj.id=opp.Id;
        ListOfFlowInp.add(FlowInpObj);
             // Set mock callout class
        Test.startTest();    // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
         Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
          AmDocs_CalloutClass_CreateSite.AmDocsMakeCalloutCreateSite(sendThisID);
         AmDocs_CalloutClass_CreateSite.AmDocsMakeCalloutCreateSitefromFlow(ListOfFlowInp);
        
         Test.stopTest();    
      
       // opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Opportunity where id =: opp.Id];       // Verify that the response received contains fake values        
            // System.assertEquals('0123456789',opp.AmDocs_CustomerID__c); System.assertEquals(null,opp.AmDocs_CustomerID__c); System.assertEquals('0123456789',opp.AmDocs_FAID__c); System.assertEquals('0123456789',opp.AmDocs_BarID__c);     
  }
    Public static testMethod void getAcct1(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

       Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '';
            insert acct1;
            Opportunity opp1= TestDataFactoryforInternal.createOppforCallout(acct1);
       
             List<AmDocs_CalloutClass_CreateSite.FlowInputs> ListOfFlowInp = new List<AmDocs_CalloutClass_CreateSite.FlowInputs>();
             AmDocs_CalloutClass_CreateSite.FlowInputs FlowInpObj = new AmDocs_CalloutClass_CreateSite.FlowInputs();
        
        Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 1' Limit 1];
        List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp); System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
        String sendThisID = opp.Id;
        FlowInpObj.id=opp.Id;
        ListOfFlowInp.add(FlowInpObj);
             // Set mock callout class
        Test.startTest();    // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
         Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());
         AmDocs_CalloutClass_CreateSite.AmDocsMakeCalloutCreateSite(sendThisID);
         AmDocs_CalloutClass_CreateSite.AmDocsMakeCalloutCreateSitefromFlow(ListOfFlowInp);
          
        Test.stopTest();    
      
       // opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Opportunity where id =: opp.Id];       // Verify that the response received contains fake values        
            // System.assertEquals('0123456789',opp.AmDocs_CustomerID__c); System.assertEquals(null,opp.AmDocs_CustomerID__c); System.assertEquals('0123456789',opp.AmDocs_FAID__c); System.assertEquals('0123456789',opp.AmDocs_BarID__c);     
  }
     Public static testMethod void getAcct3(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

        Account acct = TestDataFactoryforInternal.CreateAccount();
        Opportunity opp1= TestDataFactoryforInternal.createOppforCallout(acct);
       
        List<AmDocs_CalloutClass_CreateSite.FlowInputs> ListOfFlowInp = new List<AmDocs_CalloutClass_CreateSite.FlowInputs>();
        AmDocs_CalloutClass_CreateSite.FlowInputs FlowInpObj = new AmDocs_CalloutClass_CreateSite.FlowInputs();
        
        
        Opportunity opp = [Select Id FROM Opportunity WHERE Name = 'Test Opp 1' Limit 1];
      
        List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp); System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
        String sendThisID = opp.Id;
        FlowInpObj.id=opp.Id;
        ListOfFlowInp.add(FlowInpObj);
             // Set mock callout class
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(100,
                                                               'Complete',
                                                               '{"ImplCreateCustomerOutput": {"customerID": "0123456789","barID": "0123456789","faID": "0123456789"}}',
                                                               null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        AmDocs_CalloutClass_CreateSite.AmDocsMakeCalloutCreateSite(sendThisID);
        Test.stopTest();   
      
       // opp = [select AmDocs_CustomerID__c, AmDocs_FAID__c, Amdocs_BarID__c from Opportunity where id =: opp.Id];       // Verify that the response received contains fake values        
            // System.assertEquals('0123456789',opp.AmDocs_CustomerID__c); System.assertEquals(null,opp.AmDocs_CustomerID__c); System.assertEquals('0123456789',opp.AmDocs_FAID__c); System.assertEquals('0123456789',opp.AmDocs_BarID__c);     
  }

}