@isTEST
 private class OppPropertyType_Test{

// Private
    private static testmethod void testCaseOppUpdate1(){
    Account a1 = new Account();
        a1.Name = 'Test Account';
        a1.Programming__c = 'Starter';
        a1.phone = '(303) 555-5555';
        insert a1;
        
            Opportunity o1 = new Opportunity();
                o1.Name = 'Test';
                o1.AccountId = a1.Id;
                o1.BTVNA_Request_Type__c = 'Site Survey';
                o1.LeadSource='Test';
                o1.CloseDate=system.Today();
                o1.StageName='Closed Won';
                o1.CSG_Account_Number__c='8255707080000000';
            
            insert o1;
                   
            o1= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id     =:o1.Id];
       System.debug('Property Type after the trigger fired: ' + o1.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o1.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o1.Agent__c);
       System.debug('Category after the trigger fired: ' + o1.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Private', o1.Property_Type__c);
       // System.assertEquals('7000', o1.Prin_s__c);
       // System.assertEquals('7080', o1.Agent__c);
       // System.assertEquals('P/P', o1.Category__c);
       }

// Private - Puerto Rico
    private static testmethod void testCaseOppUpdate2(){
    Account a2 = new Account();
        a2.Name = 'Test Account';
        a2.Programming__c = 'Starter';
        a2.phone = '(303) 555-5555';
        insert a2;
        
            Opportunity o2 = new Opportunity();
                o2.Name = 'Test';
                o2.AccountId = a2.Id;
                o2.BTVNA_Request_Type__c = 'Site Survey';
                o2.LeadSource='Test';
                o2.CloseDate=system.Today();
                o2.StageName='Closed Won';
                o2.CSG_Account_Number__c='8255490010000000';
            
            insert o2;
                   
            o2= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id     =:o2.Id];
       System.debug('Property Type after the trigger fired: ' + o2.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o2.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o2.Agent__c);
       System.debug('Category after the trigger fired: ' + o2.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Private - Puerto Rico', o2.Property_Type__c);
       // System.assertEquals('4900', o2.Prin_s__c);
       // System.assertEquals('0010', o2.Agent__c);
       // System.assertEquals('P/P', o2.Category__c);
       }
 

// Public - Puerto Rico
    private static testmethod void testCaseOppUpdate3(){
    Account a3 = new Account();
        a3.Name = 'Test Account';
        a3.Programming__c = 'Starter';
        a3.phone = '(303) 555-5555';
        insert a3;
        
            Opportunity o3 = new Opportunity();
                o3.Name = 'Test';
                o3.AccountId = a3.Id;
                o3.BTVNA_Request_Type__c = 'Site Survey';
                o3.LeadSource='Test';
                o3.CloseDate=system.Today();
                o3.StageName='Closed Won';
                o3.CSG_Account_Number__c='8255470010000000';
                o3.EVO2__c=10;
            
            insert o3;
                   
            o3= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id     =:o3.Id];
       System.debug('Property Type after the trigger fired: ' + o3.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o3.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o3.Agent__c);
       System.debug('Category after the trigger fired: ' + o3.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Public - Puerto Rico', o3.Property_Type__c);
       // System.assertEquals('4700', o3.Prin_s__c);
       // System.assertEquals('0010', o3.Agent__c);
       // System.assertEquals('P/P', o3.Category__c);
       }      

 // Public
    private static testmethod void testCaseOppUpdate4(){
    Account a4 = new Account();
        a4.Name = 'Test Account';
        a4.Programming__c = 'Starter';
        a4.phone = '(303) 555-5555';
        insert a4;
        
            Opportunity o4 = new Opportunity();
                o4.Name = 'Test';
                o4.AccountId = a4.Id;
                o4.BTVNA_Request_Type__c = 'Site Survey';
                o4.LeadSource='Test';
                o4.CloseDate=system.Today();
                o4.StageName='Closed Won';
                o4.CSG_Account_Number__c='8255707050000000';
                o4.EVO2__c=10;
            
            insert o4;
                   
            o4= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id     =:o4.Id];
       System.debug('Property Type after the trigger fired: ' + o4.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o4.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o4.Agent__c);
       System.debug('Category after the trigger fired: ' + o4.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Public', o4.Property_Type__c);
       // System.assertEquals('7000', o4.Prin_s__c);
       // System.assertEquals('7050', o4.Agent__c);
       // System.assertEquals('P/P', o4.Category__c);
       }      
       

// MDU Bulk - Puerto Rico
    private static testmethod void testCaseOppUpdate5(){
    Account a5 = new Account();
        a5.Name = 'Test Account';
        a5.Programming__c = 'Starter';
        a5.phone = '(303) 555-5555';
        insert a5;
        
            Opportunity o5 = new Opportunity();
                o5.Name = 'Test';
                o5.AccountId = a5.Id;
                o5.BTVNA_Request_Type__c = 'Site Survey';
                o5.LeadSource='Test';
                o5.CloseDate=system.Today();
                o5.StageName='Closed Won';
                o5.CSG_Account_Number__c='8255560010000000';
            
            insert o5;
                   
            o5= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id     =:o5.Id];
       System.debug('Property Type after the trigger fired: ' + o5.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o5.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o5.Agent__c);
       System.debug('Category after the trigger fired: ' + o5.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('MDU Bulk - Puerto Rico', o5.Property_Type__c);
       // System.assertEquals('5600', o5.Prin_s__c);
       // System.assertEquals('0010', o5.Agent__c);
       // System.assertEquals('MDU', o5.Category__c);
       }    
       
 
// MDU Bulk
    private static testmethod void testCaseOppUpdate6(){
    Account a6 = new Account();
        a6.Name = 'Test Account';
        a6.Programming__c = 'Starter';
        a6.phone = '(303) 555-5555';
        insert a6;
        
            Opportunity o6 = new Opportunity();
                o6.Name = 'Test';
                o6.AccountId = a6.Id;
                o6.BTVNA_Request_Type__c = 'Site Survey';
                o6.LeadSource='Test';
                o6.CloseDate=system.Today();
                o6.StageName='Closed Won';
                o6.CSG_Account_Number__c='8255707030000000';
            
            insert o6;
                   
            o6= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id     =:o6.Id];
       System.debug('Property Type after the trigger fired: ' + o6.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o6.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o6.Agent__c);
       System.debug('Category after the trigger fired: ' + o6.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('MDU Bulk', o6.Property_Type__c);
       // System.assertEquals('7000', o6.Prin_s__c);
       // System.assertEquals('7030', o6.Agent__c);
       // System.assertEquals('MDU', o6.Category__c);
       }      
  
// FTG Bulk - Puerto Rico
    private static testmethod void testCaseOppUpdate7(){
    Account a7 = new Account();
        a7.Name = 'Test Account';
        a7.Programming__c = 'Starter';
        a7.phone = '(303) 555-5555';
        insert a7;
        
            Opportunity o7 = new Opportunity();
                o7.Name = 'Test';
                o7.AccountId = a7.Id;
                o7.BTVNA_Request_Type__c = 'Site Survey';
                o7.LeadSource='Test';
                o7.CloseDate=system.Today();
                o7.StageName='Closed Won';
                o7.CSG_Account_Number__c='8255540010000000';
            
            insert o7;
                   
            o7= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id     =:o7.Id];
       System.debug('Property Type after the trigger fired: ' + o7.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o7.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o7.Agent__c);
       System.debug('Category after the trigger fired: ' + o7.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('FTG Bulk - Puerto Rico', o7.Property_Type__c);
       // System.assertEquals('5400', o7.Prin_s__c);
       // System.assertEquals('0010', o7.Agent__c);
       // System.assertEquals('FTG', o7.Category__c);
       }
       
// Bulk Hospital - Nursing
    private static testmethod void testCaseOppUpdate8(){
    Account a8 = new Account();
        a8.Name = 'Test Account';
        a8.Programming__c = 'Starter';
        a8.phone = '(303) 555-5555';
        insert a8;
        
            Opportunity o8 = new Opportunity();
                o8.Name = 'Test';
                o8.AccountId = a8.Id;
                o8.BTVNA_Request_Type__c = 'Site Survey';
                o8.LeadSource='Test';
                o8.CloseDate=system.Today();
                o8.StageName='Closed Won';
                o8.CSG_Account_Number__c='8255707010000000';
            
            insert o8;
                   
            o8= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id     =:o8.Id];
       System.debug('Property Type after the trigger fired: ' + o8.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o8.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o8.Agent__c);
       System.debug('Category after the trigger fired: ' + o8.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Bulk Hospital - Nursing', o8.Property_Type__c);
       // System.assertEquals('7000', o8.Prin_s__c);
       // System.assertEquals('7010', o8.Agent__c);
       // System.assertEquals('FTG', o8.Category__c);
       }
    
// Muzak - Puerto Rico
    private static testmethod void testCaseOppUpdate9(){
    Account a9 = new Account();
        a9.Name = 'Test Account';
        a9.Programming__c = 'Starter';
        a9.phone = '(303) 555-5555';
        insert a9;
        
            Opportunity o9 = new Opportunity();
                o9.Name = 'Test';
                o9.AccountId = a9.Id;
                o9.BTVNA_Request_Type__c = 'Site Survey';
                o9.LeadSource='Test';
                o9.CloseDate=system.Today();
                o9.StageName='Closed Won';
                o9.CSG_Account_Number__c='8255620010000000';
            
            insert o9;
                   
            o9= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id     =:o9.Id];
       System.debug('Property Type after the trigger fired: ' + o9.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o9.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o9.Agent__c);
       System.debug('Category after the trigger fired: ' + o9.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Muzak - Puerto Rico', o9.Property_Type__c);
       // System.assertEquals('6200', o9.Prin_s__c);
       // System.assertEquals('0010', o9.Agent__c);
       // System.assertEquals('MUZ', o9.Category__c);
       }   
       
       

// Muzak
    private static testmethod void testCaseOppUpdate10(){
    Account a10 = new Account();
        a10.Name = 'Test Account';
        a10.Programming__c = 'Starter';
        a10.phone = '(303) 555-5555';
        insert a10;
        
            Opportunity o10 = new Opportunity();
                o10.Name = 'Test';
                o10.AccountId = a10.Id;
                o10.BTVNA_Request_Type__c = 'Site Survey';
                o10.LeadSource='Test';
                o10.CloseDate=system.Today();
                o10.StageName='Closed Won';
                o10.CSG_Account_Number__c='8255707070000000';
            
            insert o10;
                   
            o10= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o10.Id];
       System.debug('Property Type after the trigger fired: ' + o10.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o10.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o10.Agent__c);
       System.debug('Category after the trigger fired: ' + o10.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Muzak', o10.Property_Type__c);
       // System.assertEquals('7000', o10.Prin_s__c);
       // System.assertEquals('7070', o10.Agent__c);
       // System.assertEquals('MUZ', o10.Category__c);
       }     
       

// UVTV Transport
    private static testmethod void testCaseOppUpdate11(){
    Account a11 = new Account();
        a11.Name = 'Test Account';
        a11.Programming__c = 'Starter';
        a11.phone = '(303) 555-5555';
        insert a11;
        
            Opportunity o11 = new Opportunity();
                o11.Name = 'Test';
                o11.AccountId = a11.Id;
                o11.BTVNA_Request_Type__c = 'Site Survey';
                o11.LeadSource='Test';
                o11.CloseDate=system.Today();
                o11.StageName='Closed Won';
                o11.CSG_Account_Number__c='8255290010000000';
            
            insert o11;
                   
            o11= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o11.Id];
       System.debug('Property Type after the trigger fired: ' + o11.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o11.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o11.Agent__c);
       System.debug('Category after the trigger fired: ' + o11.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('UVTV', o11.Property_Type__c);
       // System.assertEquals('2900', o11.Prin_s__c);
       // System.assertEquals('0010', o11.Agent__c);
       // System.assertEquals('TRP', o11.Category__c);
       }       
       
// Airline
    private static testmethod void testCaseOppUpdate12(){
    Account a12 = new Account();
        a12.Name = 'Test Account';
        a12.Programming__c = 'Starter';
        a12.phone = '(303) 555-5555';
        insert a12;
        
            Opportunity o12 = new Opportunity();
                o12.Name = 'Test';
                o12.AccountId = a12.Id;
                o12.BTVNA_Request_Type__c = 'Site Survey';
                o12.LeadSource='Test';
                o12.CloseDate=system.Today();
                o12.StageName='Closed Won';
                o12.CSG_Account_Number__c='8255260010000000';
            
            insert o12;
                   
            o12= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o12.Id];
       System.debug('Property Type after the trigger fired: ' + o12.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o12.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o12.Agent__c);
       System.debug('Category after the trigger fired: ' + o12.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Airline', o12.Property_Type__c);
       // System.assertEquals('2600', o12.Prin_s__c);
       // System.assertEquals('0010', o12.Agent__c);
       // System.assertEquals('AIR', o12.Category__c);
       }
   
// PCO - MDU Residential
    private static testmethod void testCaseOppUpdate13(){
    Account a13 = new Account();
        a13.Name = 'Test Account';
        a13.Programming__c = 'Starter';
        a13.phone = '(303) 555-5555';
        insert a13;
        
            Opportunity o13 = new Opportunity();
                o13.Name = 'Test';
                o13.AccountId = a13.Id;
                o13.BTVNA_Request_Type__c = 'Site Survey';
                o13.LeadSource='Test';
                o13.CloseDate=system.Today();
                o13.StageName='Closed Won';
                o13.CSG_Account_Number__c='8255250010000000';
            
            insert o13;
                   
            o13= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o13.Id];
       System.debug('Property Type after the trigger fired: ' + o13.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o13.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o13.Agent__c);
       System.debug('Category after the trigger fired: ' + o13.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('PCO - MDU Residential', o13.Property_Type__c);
       // System.assertEquals('2500', o13.Prin_s__c);
       // System.assertEquals('0010', o13.Agent__c);
       // System.assertEquals('SDS', o13.Category__c);
       }    
     
// PCO - Analog
    private static testmethod void testCaseOppUpdate14(){
    Account a14 = new Account();
        a14.Name = 'Test Account';
        a14.Programming__c = 'Starter';
        a14.phone = '(303) 555-5555';
        insert a14;
        
            Opportunity o14 = new Opportunity();
                o14.Name = 'Test';
                o14.AccountId = a14.Id;
                o14.BTVNA_Request_Type__c = 'Site Survey';
                o14.LeadSource='Test';
                o14.CloseDate=system.Today();
                o14.StageName='Closed Won';
                o14.CSG_Account_Number__c='8255240010000000';
            
            insert o14;
                   
            o14= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o14.Id];
       System.debug('Property Type after the trigger fired: ' + o14.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o14.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o14.Agent__c);
       System.debug('Category after the trigger fired: ' + o14.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('PCO - Analog', o14.Property_Type__c);
       // System.assertEquals('2400', o14.Prin_s__c);
       // System.assertEquals('0010', o14.Agent__c);
       // System.assertEquals('PCO', o14.Category__c);
       }
       
// PCO MDU More DISH
    private static testmethod void testCaseOppUpdate15(){
    Account a15 = new Account();
        a15.Name = 'Test Account';
        a15.Programming__c = 'Starter';
        a15.phone = '(303) 555-5555';
        insert a15;
        
            Opportunity o15 = new Opportunity();
                o15.Name = 'Test';
                o15.AccountId = a15.Id;
                o15.BTVNA_Request_Type__c = 'Site Survey';
                o15.LeadSource='Test';
                o15.CloseDate=system.Today();
                o15.StageName='Closed Won';
                o15.CSG_Account_Number__c='8255909999999999';
            
            insert o15;
                   
            o15= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o15.Id];
       System.debug('Property Type after the trigger fired: ' + o15.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o15.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o15.Agent__c);
       System.debug('Category after the trigger fired: ' + o15.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Residential SDS', o15.Property_Type__c);
       // System.assertEquals('9000', o15.Prin_s__c);
       // System.assertEquals('0000', o15.Agent__c);
       // System.assertEquals('SDS', o15.Category__c);
       }
       
// Hierarchy
    private static testmethod void testCaseOppUpdate16(){
    Account a16 = new Account();
        a16.Name = 'Test Account';
        a16.Programming__c = 'Starter';
        a16.phone = '(303) 555-5555';
        insert a16;
        
            Opportunity o16 = new Opportunity();
                o16.Name = 'Test';
                o16.AccountId = a16.Id;
                o16.BTVNA_Request_Type__c = 'Site Survey';
                o16.LeadSource='Test';
                o16.CloseDate=system.Today();
                o16.StageName='Closed Won';
                o16.CSG_Account_Number__c='8255220010000000';
            
            insert o16;
                   
            o16= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o16.Id];
       System.debug('Property Type after the trigger fired: ' + o16.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o16.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o16.Agent__c);
       System.debug('Category after the trigger fired: ' + o16.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Hierarchy', o16.Property_Type__c);
       // System.assertEquals('2200', o16.Prin_s__c);
       // System.assertEquals('0010', o16.Agent__c);
       // System.assertEquals('HRY', o16.Category__c);
       }
       
// MDU More DISH
    private static testmethod void testCaseOppUpdate17(){
    Account a17 = new Account();
        a17.Name = 'Test Account';
        a17.Programming__c = 'Starter';
        a17.phone = '(303) 555-5555';
        insert a17;
        
            Opportunity o16 = new Opportunity();
                o16.Name = 'Test';
                o16.AccountId = a17.Id;
                o16.BTVNA_Request_Type__c = 'Site Survey';
                o16.LeadSource='Test';
                o16.CloseDate=system.Today();
                o16.StageName='Closed Won';
                o16.CSG_Account_Number__c='8255160010000000';
            
            insert o16;
                   
            o16= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o16.Id];
       System.debug('Property Type after the trigger fired: ' + o16.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o16.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o16.Agent__c);
       System.debug('Category after the trigger fired: ' + o16.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('MDU More DISH', o16.Property_Type__c);
       // System.assertEquals('1600', o16.Prin_s__c);
       // System.assertEquals('0010', o16.Agent__c);
       // System.assertEquals('SDS', o16.Category__c);
       }  
       
    
// SDS - Puerto Rico
    private static testmethod void testCaseOppUpdate18(){
    Account a18 = new Account();
        a18.Name = 'Test Account';
        a18.Programming__c = 'Starter';
        a18.phone = '(303) 555-5555';
        insert a18;
        
            Opportunity o18 = new Opportunity();
                o18.Name = 'Test';
                o18.AccountId = a18.Id;
                o18.BTVNA_Request_Type__c = 'Site Survey';
                o18.LeadSource='Test';
                o18.CloseDate=system.Today();
                o18.StageName='Closed Won';
                o18.CSG_Account_Number__c='8255919999999999';
            
            insert o18;
                   
            o18= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o18.Id];
       System.debug('Property Type after the trigger fired: ' + o18.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o18.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o18.Agent__c);
       System.debug('Category after the trigger fired: ' + o18.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('SDS - Puerto Rico', o18.Property_Type__c);
       // System.assertEquals('9100', o18.Prin_s__c);
       // System.assertEquals('0000', o18.Agent__c);
       // System.assertEquals('SDS', o18.Category__c);
       }
       
// SDS
    private static testmethod void testCaseOppUpdate19(){
    Account a19 = new Account();
        a19.Name = 'Test Account';
        a19.Programming__c = 'Starter';
        a19.phone = '(303) 555-5555';
        insert a19;
        
            Opportunity o19 = new Opportunity();
                o19.Name = 'Test';
                o19.AccountId = a19.Id;
                o19.BTVNA_Request_Type__c = 'Site Survey';
                o19.LeadSource='Test';
                o19.CloseDate=system.Today();
                o19.StageName='Closed Won';
                o19.CSG_Account_Number__c='8255130010000000';
            
            insert o19;
                   
            o19= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o19.Id];
       System.debug('Property Type after the trigger fired: ' + o19.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o19.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o19.Agent__c);
       System.debug('Category after the trigger fired: ' + o19.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('SDS', o19.Property_Type__c);
       // System.assertEquals('1300', o19.Prin_s__c);
       // System.assertEquals('0010', o19.Agent__c);
       // System.assertEquals('SDS', o19.Category__c);
       }
       
       
// Institutional Residential - Puerto Rico
    private static testmethod void testCaseOppUpdate20(){
    Account a20 = new Account();
        a20.Name = 'Test Account';
        a20.Programming__c = 'Starter';
        a20.phone = '(303) 555-5555';
        insert a20;
        
            Opportunity o20 = new Opportunity();
                o20.Name = 'Test';
                o20.AccountId = a20.Id;
                o20.BTVNA_Request_Type__c = 'Site Survey';
                o20.LeadSource='Test';
                o20.CloseDate=system.Today();
                o20.StageName='Closed Won';
                o20.CSG_Account_Number__c='8255420010000000';
            
            insert o20;
                   
            o20= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o20.Id];
       System.debug('Property Type after the trigger fired: ' + o20.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o20.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o20.Agent__c);
       System.debug('Category after the trigger fired: ' + o20.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Institutional Residential - Puerto Rico', o20.Property_Type__c);
       // System.assertEquals('4200', o20.Prin_s__c);
       // System.assertEquals('0010', o20.Agent__c);
       // System.assertEquals('I/R', o20.Category__c);
       }
       
       
// Institutional Residential
    private static testmethod void testCaseOppUpdate21(){
    Account a21 = new Account();
        a21.Name = 'Test Account';
        a21.Programming__c = 'Starter';
        a21.phone = '(303) 555-5555';
        insert a21;
        
            Opportunity o21 = new Opportunity();
                o21.Name = 'Test';
                o21.AccountId = a21.Id;
                o21.BTVNA_Request_Type__c = 'Site Survey';
                o21.LeadSource='Test';
                o21.CloseDate=system.Today();
                o21.StageName='Closed Won';
                o21.CSG_Account_Number__c='8255100010000000';
            
            insert o21;
                   
            o21= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o21.Id];
       System.debug('Property Type after the trigger fired: ' + o21.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o21.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o21.Agent__c);
       System.debug('Category after the trigger fired: ' + o21.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('Institutional Residential', o21.Property_Type__c);
       // System.assertEquals('1000', o21.Prin_s__c);
       // System.assertEquals('0010', o21.Agent__c);
       // System.assertEquals('I/R', o21.Category__c);
       }   
       
 
// Bulk FTG
    private static testmethod void testCaseOppUpdate22(){
    Account a22 = new Account();
        a22.Name = 'Test Account';
        a22.Programming__c = 'Starter';
        a22.phone = '(303) 555-5555';
        insert a22;
        
            Opportunity o22 = new Opportunity();
                o22.Name = 'Test';
                o22.AccountId = a22.Id;
                o22.BTVNA_Request_Type__c = 'Site Survey';
                o22.LeadSource='Test';
                o22.CloseDate=system.Today();
                o22.StageName='Closed Won';
                o22.CSG_Account_Number__c='8255707020000000';
            
            insert o22;
                   
            o22= [SELECT Property_Type__c, Prin_s__c, Agent__c, Category__c FROM Opportunity WHERE Id    =:o22.Id];
       System.debug('Property Type after the trigger fired: ' + o22.Property_Type__c);
       System.debug('Prin after the trigger fired: ' + o22.Prin_s__c);
       System.debug('Agent after the trigger fired: ' + o22.Agent__c);
       System.debug('Category after the trigger fired: ' + o22.Category__c);

       // Test that the trigger correctly updated the fields 
    
       // System.assertEquals('FTG Bulk', o22.Property_Type__c);
       // System.assertEquals('7000', o22.Prin_s__c);
       // System.assertEquals('7020', o22.Agent__c);
       // System.assertEquals('FTG', o22.Category__c);
       }      
       
       
       }