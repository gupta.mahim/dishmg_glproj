public class AllEquipmentUploadCtrl
{
    @AuraEnabled
    public static void saveAll(String leasedSBData,String purchasedSBData,String heData,String teData)
    {
        List<Smartbox__c> leasedSB=new List<Smartbox__c>();
        List<Smartbox__c> purchasedSB=new List<Smartbox__c>();
        List<Equipment__c> he=new List<Equipment__c>();
        List<Tenant_Equipment__c> te=new List<Tenant_Equipment__c>();
        
        if(!String.isEmpty(leasedSBData))leasedSB=(List<Smartbox__c>)JSON.deserialize(leasedSBData, List<Smartbox__c>.class);
        if(!String.isEmpty(purchasedSBData))purchasedSB=(List<Smartbox__c>)JSON.deserialize(purchasedSBData, List<Smartbox__c>.class);
        if(!String.isEmpty(heData))he=(List<Equipment__c>)JSON.deserialize(heData, List<Equipment__c>.class);
        if(!String.isEmpty(teData))te=(List<Tenant_Equipment__c>)JSON.deserialize(teData, List<Tenant_Equipment__c>.class);
        
        try
        {
            if(leasedSB!=null && leasedSB.size()>0){insert leasedSB;}

            if(purchasedSB!=null && purchasedSB.size()>0){insert purchasedSB;}
            
            if(he!=null && he.size()>0){insert he;}

            if(te!=null && te.size()>0){insert te;CSVTenantEquipFileUploaderfromLEX.invokeAddressValidation(te);}
        }
        catch (Exception e){throw new AuraHandledException(e.getMessage());}
    }
}