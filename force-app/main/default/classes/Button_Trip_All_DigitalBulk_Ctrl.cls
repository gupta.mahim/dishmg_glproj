/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 3 Sept 2020
Created for: Milestone# MS-001252
Description: Controller for Button_Trip_All_DigitalBulk2.vfp
-------------------------------------------------------------*/

public class Button_Trip_All_DigitalBulk_Ctrl {
    
    public Boolean userHaveAccess   {get;set;}
    public String noAccessMsg   {get;set;}
    
    public Button_Trip_All_DigitalBulk_Ctrl(ApexPages.StandardController controller) {
        userAccessCheck.userAccessCheckResponse resp = new userAccessCheck.userAccessCheckResponse();
        list<Custom_Button_Access_Detail__mdt> objectFieldDetail = [SELECT Object_Name__c, Field_Name__c FROM Custom_Button_Access_Detail__mdt 
                                                                    WHERE Custom_Button_Name__c='Trip All Digital Bulk Equipment' Limit 1];
        if(objectFieldDetail.size()>0)
            resp=userAccessCheck.getCurrentUserAccess(objectFieldDetail[0].Object_Name__c, objectFieldDetail[0].Field_Name__c, 'Trip All Digital Bulk Equipment');
        else{
            resp.currentUserHaveAccess = false;
            resp.noAccessMsg = System.Label.CustomButtonNoAccessMsg;
        }
        
        userHaveAccess = resp.currentUserHaveAccess;
        noAccessMsg = resp.noAccessMsg;
        system.debug('userHaveAccess-'+userHaveAccess);
        system.debug('noAccessMsg-'+noAccessMsg);
    }   
}