@isTest public class ActivateTenantAccountFlowTest {
    @isTest public static void test1(){
        
        AmDocs_Login__c aml = new AmDocs_Login__c();
        aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;
        Opportunity opp = TestDataFactoryforInternal.createOpportunity();
         System.debug('DEBUG 002 ======== AmDocs_BarID: ' + opp.AmDocs_BarID__c);
        Tenant_Account__c TA = TestDataFactoryforInternal.createTenantAccount(opp);
        ActivateTenantAccountFlow.initAPICalls(TA.Id);
        ActivateTenantAccountFlow.sendCreateUnit(TA.Id);
        ActivateTenantAccountFlow.sendUpdateUnit(TA.Id);
        ActivateTenantAccountFlow.hasActiveTenentServiceId(TA.Id);
        ActivateTenantAccountFlow.hasProgramming(TA.Id);
        ActivateTenantAccountFlow.loadTenantAccount(TA.Id);
    }
}