public class Tenant_Equipment_CSV_FileUploader 
{
    ApexPages.StandardController stdCtrl; public Tenant_Equipment_CSV_FileUploader(ApexPages.StandardController std) {stdCtrl=std;} public PageReference quicksave() {update accstoupload ;return null;} public PageReference fileAccess() {return null;}

public Tenant_Equipment_CSV_FileUploader(){

}

    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    List<Tenant_Equipment__c> accstoupload;
    
    public Pagereference ReadFile()
    {
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        accstoupload = new List<Tenant_Equipment__c>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            
            Tenant_Equipment__c a = new Tenant_Equipment__c();
            a.Name = inputvalues[0];
            a.Smart_Card__c = inputvalues[1];
            a.Unit__c = inputvalues[2];
            a.Address__c = inputvalues[3];
            a.City__c = inputvalues[4];
            a.State__c = inputvalues[5];
            a.Zip_Code__c = inputvalues[6];
            a.Customer_First_Name__c = inputvalues[7];
            a.Customer_Last_Name__c = inputvalues[8];
            a.Phone_Number__c = inputvalues[9];
            a.Email_Address__c = inputvalues[10];
            a.Opportunity__c = ApexPages.currentPage().getParameters().get('oppId');
            
            accstoupload.add(a);         
        }
        try{
              insert accstoupload;
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template');
            ApexPages.addMessage(errormsg);
        }      
                      
        return null ;}  public List<Tenant_Equipment__c> getuploadedEquipment() { if (accstoupload!= NULL) if (accstoupload.size() > 0) return accstoupload; else return null; else return null;}}