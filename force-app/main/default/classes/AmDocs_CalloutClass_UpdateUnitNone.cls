public class AmDocs_CalloutClass_UpdateUnitNone {

public class person {
 public String tenantServiceID;
 public String orderID;
 public String responseStatus;
 public cls_informationMessages[] informationMessages;
 public cls_equipmentList[] equipmentList;
}

class cls_informationMessages {
 public String errorCode;
 public String errorDescription;
}

class cls_equipmentList {
 public string action;
 public string receiverID;
}


 @future(callout=true)

 public static void AmDocsMakeCalloutUpdateUnit(String Id) {
 
list<Tenant_Account__c> TA = [select id, Name, City__c, State__c, Zip__c, Unit__c, Address__c, Action__c, Type__c, Email_Address__c, First_Name__c, Last_Name__c, Phone_Number__c, PPV__c, Opportunity__c, AmDocs_ServiceID__c from Tenant_Account__c where Id = :id ];
  System.debug('DEBUG 1 - RESULTS of the LIST lookup to the Tenant Account object' +TA);
  
list<Tenant_Product_Line_Item__c> TP = [select id, Action__c, Quantity__c, Tenant_Account__c, Standard_Price__c, AmDocs_Incremental_Caption__c, AmDocs_Incremental_ProductID__c, AmDocs_Individual_Caption__c, AmDocs_ProductID__c, isPromotion__c, Family__c from Tenant_Product_Line_Item__c where Tenant_Account__c = :id AND ( Action__c = 'ADD' OR Action__c = 'REMOVE' ) AND Family__c = 'Promotion' ];
  System.debug('DEBUG 8 - RESULTS of the LIST lookup to the Promotional OpportunityLineItem records for TP ' +TP);  
  
  if(TA[0].AmDocs_ServiceID__c  == '' || TA[0].AmDocs_ServiceID__c  == Null ){
  
      Tenant_Account__c sbc = new Tenant_Account__c(); {
          sbc.id=id;
          sbc.API_Status__c='Error';
          sbc.Amdocs_Transaction_Code__c='Error';
          if( (TA[0].AmDocs_ServiceID__c   == '' || TA[0].AmDocs_ServiceID__c  == Null )) {
              sbc.Amdocs_Transaction_Description__c='Update Unit requires a Tenant Service ID. You are missing: Tenant Service ID.';
          }
      update sbc;
      }
      API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;
          apil2.Object__c='Tenant Account';
          apil2.Status__c='ERROR';
          apil2.Results__c='Missing Tenant Service ID';
          apil2.API__c='Update Unit';
          apil2.ServiceID__c=TA[0].AmDocs_ServiceID__c;
          apil2.User__c=UserInfo.getUsername();
      insert apil2;
  }
 
  
    }
    if(TA[0].AmDocs_ServiceID__c  != '' && TA[0].AmDocs_ServiceID__c  != Null ){
        Datetime dt1 = System.Now();
        list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateUpdateTime__c from API_LOG__c where ServiceID__c = :TA[0].Amdocs_ServiceID__c AND NextAvailableActivateUpdateTime__c >= :dt1 Order By NextAvailableActivateUpdateTime__c  DESC Limit 1];
        
        if( (timer.size() > 0 )) {
            system.debug('DEBUG API LOG Timer size : ' +timer.size());
            system.debug('DEBUG API LOG Info' +timer);
            system.debug('DEBUG timer 1 : ' +dt1);
     
            Tenant_Account__c sbc = new Tenant_Account__c(); { sbc.id=id; sbc.API_Status__c='Error'; sbc.Amdocs_Transaction_Code__c='Error'; sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions'; update sbc; }
            API_Log__c apil2 = new API_Log__c();{ apil2.Record__c=id; apil2.Object__c='Tenant Account'; apil2.Status__c='ERROR'; apil2.Results__c='You must wait atleast (2) two minutes between transactions'; apil2.API__c='Update Unit'; apil2.ServiceID__c=TA[0].AmDocs_ServiceID__c; apil2.User__c=UserInfo.getUsername(); insert apil2; }
        }
    
   
        else if(timer.size() < 1) {

list<AmDocs_Login__c> A = [select id, UserName__c, UserPassword__c, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
  System.debug('DEBUG 13 - RESULTS of the LIST lookup to the OpportunityLineItem object' +A);  
  
list<Tenant_Product_Line_Item__c> PPV = [select id, Action__c, Name, AmDocs_Incremental_Caption__c, AmDocs_Incremental_ProductID__c, AmDocs_Individual_Caption__c, AmDocs_ProductID__c, Family__c from Tenant_Product_Line_Item__c where Tenant_Account__c = :id AND Family__c = 'Fee' AND AmDocs_Incremental_Caption__c = 'PPV'];
  System.debug('DEBUG 9 - RESULTS of the LIST lookup to the Core OpportunityLineItem records for PPV ' +PPV); 
  
list<Tenant_Product_Line_Item__c> IFEE = [select id, Action__c, Name, AmDocs_Incremental_Caption__c, AmDocs_Incremental_ProductID__c, AmDocs_Individual_Caption__c, AmDocs_ProductID__c, Family__c from Tenant_Product_Line_Item__c where Tenant_Account__c = :id AND Family__c = 'Fee' AND AmDocs_Incremental_Caption__c = 'IncrementalAccessFeeBO'];
  System.debug('DEBUG 10 - RESULTS of the LIST lookup to the Incremental FEE OpportunityLineItem records for IFEE ' +IFEE);
  System.debug('DEBUG 11 - RESULTS of the LIST lookup to the Incremental FEE OpportunityLineItem records for IFEE SIZE' +IFEE.size());
  
String SO = [select Opportunity__c from Tenant_Account__c where Id = :id LIMIT 1 ].Opportunity__c ;
  System.debug('DEBUG 3 - RESULTS of the LIST SO String to the Opp object ' +SO);

list<Opportunity> O = [select id, Name, AmDocs_tenantType__c from Opportunity where Id = :SO LIMIT 1 ];
  System.debug('DEBUG 4 - RESULTS of the LIST lookup to the Opp object S From SOs ID ' +O);  
  
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        jsonObj.writeFieldName('ImplUpdateTVRestInput');
            jsonObj.writeStartObject();
                jsonObj.writeStringField('orderActionType', 'CH');
                jsonObj.writeStringField('reasonCode', 'CREQ');


                jsonObj.writeFieldName('componentList');
                jsonObj.writeStartArray();
                    if(TP.size() > 0 || (PPV.size() < 1 && TA[0].PPV__c == TRUE) || (IFEE.size() < 1 && TA[0].Type__c == 'Incremental') || (PPV.size() < 1 && TA[0].PPV__c == FALSE)) {
                        if(TP.size() > 0){
                            jsonObj.writeStartObject();
                                for(Integer i = 0; i < TP.size(); i++){
                                    if(O[0].AmDocs_tenantType__c == 'Incremental') {
                                        jsonObj.writeStringField('action', TP[i].Action__c);
                                        jsonObj.writeStringField('caption', TP[i].AmDocs_Incremental_Caption__c);
                                        jsonObj.writeBooleanField('isPromotion', TP[i].isPromotion__c);
                                    }
                                    if(O[0].AmDocs_tenantType__c == 'Individual'){
                                        jsonObj.writeStringField('action', TP[i].Action__c);
                                        jsonObj.writeStringField('caption', TP[i].AmDocs_Individual_Caption__c);
                                        jsonObj.writeBooleanField('isPromotion', TP[i].isPromotion__c);  
                                    }
                                }
                            jsonObj.writeEndObject();       
                        }

                        if(IFEE.size() < 1 && TA[0].Type__c == 'Incremental'){
                            jsonObj.writeStartObject();
                                jsonObj.writeStringField('action', 'UPDATE');
                                jsonObj.writeStringField('caption', 'TV_Main');
                                jsonObj.writeBooleanField('isPromotion', False);
                                jsonObj.writeFieldName('attributes');
                                    jsonObj.writeStartArray();
                                        jsonObj.writeStartObject();
                                            jsonObj.writeStringField('catalogCode', 'Charge_Incremental');
                                            jsonObj.writeStringField('selectedValue', 'Yes');
                                        jsonObj.writeEndObject();
                                    jsonObj.writeEndArray();
                            jsonObj.writeEndObject();
                        }

                        if( TA[0].PPV__c == TRUE) {
                            jsonObj.writeStartObject();
                                jsonObj.writeStringField('action', 'UPDATE');
                                jsonObj.writeStringField('caption', 'PPV');
                                jsonObj.writeBooleanField('isPromotion', False);
                                jsonObj.writeFieldName('attributes');
                                jsonObj.writeStartArray();
                                    jsonObj.writeStartObject();
                                        jsonObj.writeStringField('catalogCode', 'Enable_iPPV_Purchase');
                                        jsonObj.writeStringField('selectedValue', 'Yes');
                                    jsonObj.writeEndObject();
                                jsonObj.writeEndArray();
                            jsonObj.writeEndObject();
                        }
                        if( TA[0].PPV__c != TRUE) {
                            jsonObj.writeStartObject();
                                jsonObj.writeStringField('action', 'UPDATE');
                                jsonObj.writeStringField('caption', 'PPV');
                                jsonObj.writeBooleanField('isPromotion', False);
                                jsonObj.writeFieldName('attributes');
                                jsonObj.writeStartArray();
                                    jsonObj.writeStartObject();
                                        jsonObj.writeStringField('catalogCode', 'Enable_iPPV_Purchase');
                                        jsonObj.writeStringField('selectedValue', 'No');
                                    jsonObj.writeEndObject();
                                jsonObj.writeEndArray();
                            jsonObj.writeEndObject();
                        }

                    }
                    jsonObj.writeEndArray();

            jsonObj.writeEndObject();
        jsonObj.writeEndObject();  
    String finalJSON = jsonObj.getAsString();
        System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));


  HttpRequest request = new HttpRequest();
      String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+TA[0].AmDocs_ServiceID__c+'/updateTV?sc=SS&lo=EN&ca=SF'; 
      request.setTimeout(101000);
      request.setEndPoint(endpoint);
      request.setBody(jsonObj.getAsString());
      request.setHeader('Content-Type', 'application/json');
      request.setHeader('Accept', 'application/json'); 
      request.setHeader('User-Agent', 'SFDC-Callout/45.0'); 
      request.setMethod('POST'); 
      String authorizationHeader = A[0].UXF_Token__c; 
      request.setHeader('Authorization', authorizationHeader);
      
  HttpResponse response = new HTTP().send(request); 
    if (response.getStatusCode() == 200 ) {
        String strjson = response.getbody();
        JSONParser parser = JSON.createParser(strjson);
        parser.nextToken(); parser.nextToken(); parser.nextToken(); person obj = (person)parser.readValueAs( person.class);
         System.debug(response.toString());
         System.debug('STATUS:'+response.getStatus());
         System.debug('STATUS_CODE:'+response.getStatusCode());
         System.debug(response.getBody());
         System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
         System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.tenantServiceID);
         System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
         System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);

 Tenant_Account__c sbc = new Tenant_Account__c();{
     sbc.id=id;
     sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now();
     sbc.AmDocs_Order_ID__c=obj.orderID;
     if (obj.tenantServiceID != Null) { 
         sbc.AmDocs_ServiceID__c=obj.tenantServiceID;
     }
     sbc.API_Status__c=String.valueOf(response.getStatusCode());
     sbc.AmDocs_Transaction_Description__c='UPDATE SUBSCRIBER SUCCESS';
  
     update sbc;
   }
   
   API_Log__c apil2 = new API_Log__c();{
       apil2.Record__c=id;
       apil2.Object__c='Tenant Account';
       apil2.Status__c='SUCCESS';
       apil2.Results__c=strjson;
       apil2.API__c='Update Unit';
       apil2.ServiceID__c=TA[0].AmDocs_ServiceID__c;
       apil2.User__c=UserInfo.getUsername();
   insert apil2;
  }
  
 }
 
 else if (response.getStatusCode() >= 200 && response.getStatusCode() < 499) {          String strjson = response.getbody();                  JSONParser parser = JSON.createParser(strjson);         parser.nextToken(); parser.nextToken(); parser.nextToken(); person obj = (person)parser.readValueAs( person.class);
         System.debug(response.toString());
         System.debug('STATUS:'+response.getStatus());
         System.debug('STATUS_CODE:'+response.getStatusCode());
         System.debug(response.getBody());
         System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
         System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.tenantServiceID);
         System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
         System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
         
     Tenant_Account__c sbc = new Tenant_Account__c();{          sbc.id=id;          sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now();          sbc.AmDocs_Order_ID__c=obj.orderID;         if (obj.tenantServiceID != Null) {              sbc.AmDocs_ServiceID__c=obj.tenantServiceID;          }         sbc.API_Status__c=String.valueOf(response.getStatusCode());
     
         if(obj.informationMessages != Null) { sbc.AmDocs_Transaction_Description__c='UPDATE SUBSCRIBER ERROR ' +obj.informationMessages[0].errorDescription;             sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode;  
         }
         else if(obj.informationMessages == Null) {             if (obj.tenantServiceID != Null) {                  sbc.AmDocs_ServiceID__c=obj.tenantServiceID;                 sbc.Action__c='Active';                 sbc.AmDocs_Transaction_Description__c='UPDATE SUBSCRIBER ' +obj.responseStatus;                 sbc.AmDocs_Transaction_Code__c=obj.responseStatus;
             }
         }
         else if(obj.responseStatus == Null) { sbc.AmDocs_Transaction_Description__c='UPDATE SUBSCRIBER ERROR Request Rejected'; sbc.AmDocs_Transaction_Code__c='ERROR';
         }
         update sbc;
       }
       API_Log__c apil2 = new API_Log__c();{           apil2.Record__c=id;           apil2.Object__c='Tenant Account';           if(obj.responseStatus == Null) {               apil2.Status__c='Error';           }           else if(obj.responseStatus != Null) {               apil2.Status__c=obj.responseStatus;}           apil2.Results__c=strjson;           apil2.API__c='Update Unit';           apil2.ServiceID__c=TA[0].AmDocs_ServiceID__c;           apil2.User__c=UserInfo.getUsername();       insert apil2;       }
     }
 else if (response.getStatusCode() == Null) {         String strjson = response.getbody(); Tenant_Account__c sbc = new Tenant_Account__c(); { sbc.id=id; sbc.API_Status__c='API ERROR - UPDATE SUBSCRIBER - Request Not Processed'; update sbc;
     }
     API_Log__c apil2 = new API_Log__c();{ apil2.Record__c=id; apil2.Object__c='Tenant Account'; apil2.Status__c='ERROR'; apil2.Results__c=strjson; apil2.API__c='Update Unit'; apil2.ServiceID__c=TA[0].AmDocs_ServiceID__c; apil2.User__c=UserInfo.getUsername(); insert apil2;
     }
    }
   }
  }
 }
}