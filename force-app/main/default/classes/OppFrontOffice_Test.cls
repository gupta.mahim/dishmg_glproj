@isTest(SeeAllData=true)
Public class OppFrontOffice_Test{
static testMethod void testOppFrontOffice(){
 
   Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        insert a;
        
               Opportunity o = new Opportunity(
               Name = 'Test Opp',
               LeadSource='Portal', 
                RecordTypeId='0126000000053vu', 
                misc_code_siu__c='N123', 
                Restricted_Programming__c='True', 
                CloseDate=system.Today(), 
                StageName='Closed Won',
                Launch_Date__c=system.Today(),
                Send_to_FO_for_Verification__c=true);
                insert o;
                update o;
 
 }}