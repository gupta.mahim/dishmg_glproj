public class EmailContent {
    
    @AuraEnabled
    public static String getApprovalContent(String execId)
    {
        Executive_Summary__c exec= [select Id,Name,Property_Name__c from Executive_Summary__c where id=:execId] ;  
        
        String url= 'mailto:michael.rudd@dish.com; lee.hirsch@dish.com;Robert.Grosz@dish.com ?';
        url+='SUBJECT=A new Executive Summary is ready for your approval.';
        url+='+&BODY=Executive Summary Number'+exec.Name+' regarding '+exec.Property_Name__c+' is ready for your approval. \n Please use the link below to access the record and add your approval. https://na4.salesforce.com/'+exec.Id;

        return  url;
    }
    @AuraEnabled
    public static String getRecommnedContent(String execId)
    {      
        Executive_Summary__c exec= [select Id,Name,Property_Name__c from Executive_Summary__c where id=:execId] ;  
        
        String url= 'mailto:michael.rudd@dish.com;lee.hirsch@dish.com;rick.hodgskiss@dish.com ?';
        url+='SUBJECT=A new Executive Summary is ready for your recommendation.';
        url+='+&BODY=Executive Summary Number'+exec.Name+' regarding '+exec.Property_Name__c+' is ready for your recommendation. \n Please use the link below to access the record and add your recommendation. https://na4.salesforce.com/'+exec.Id;

        return  url;
       
      
    }
    
     @AuraEnabled
    public static String getChangeRequestContent(String frontId)
    {    
        Front_Office_Request__c req= [select Id,Owner.Email,Name from Front_Office_Request__c where id=:frontId] ;  
        
        String url= 'mailto:'+req.Owner.Email+'?';
        url+='SUBJECT=A change for Request '+req.Name+' has been requested.';
        url+='+&BODY=A change for Request'+req.Name+' has been requested.Changes Requested:https://na4.salesforce.com/'+req.Id;

        return  url;
       
      
    }
    
  @AuraEnabled
    public static String getLeadContent(String leadId)
    {    
       Lead lead = [select Id,Company,City,State,Status,LeadSource,Lead_Source_External__c,Number_of_Units__c,Street,
                    PostalCode,Name,Phone,Email,Type_of_Business__c,Type_of_Roof__c,CreatedDate,
                    Description from Lead where id=:leadId] ;  
        
        String url= 'mailto:?';
        url+='SUBJECT=You have received a new DISH Network lead for I AM SADIA'+lead.Company+' in '+lead.City+','+lead.State+'.';
        url+='+&BODY=Lead Information Lead Status:'+lead.Status+'\n Lead Source:'+lead.LeadSource+'\n Lead Source External: '+lead.Lead_Source_External__c+'\n Company Name:'+lead.Company+' \n Number of Units:'+lead.Number_of_Units__c+'\n Street Address:'+lead.Street+' \n City:'+lead.City+' \n State:'+lead.State+' \n Zip: '+lead.PostalCode+'\n Contact Name:'+lead.Name+' \n Contact Phone:'+lead.Phone+' \n Contact Email Address:'+lead.Email+' \n Type of Business:'+lead.Type_of_Business__c+' \n Type of Roof:'+lead.Type_of_Roof__c+' \n Lead Create Date:'+lead.CreatedDate+' \n Description '+lead.Description+' \n Salesforce RECORD ID: '+lead.Id+'';

        return  url;
    }

    
    
   /* @AuraEnabled
    public static String getTaskContent(String taskId)
    {    
        Milestone1_Task__c task= [select Id,Name,Project_Milestone__c,Due_Date__c,Description__c from Milestone1_Task__c where id=:taskId] ;  
        
        String url= 'mailto:?';
        url+='SUBJECT=New Task:'+task.Name+''; 
        url+='+&BODY=Project Task Name:'+task.Name+' \n Milestone: '+task.Project_Milestone__c+' \n Due Date:'+task.Due_Date__c+' \n Description:'+task.Description__c+''+task.Id;

        return  url;
       
    } */
}