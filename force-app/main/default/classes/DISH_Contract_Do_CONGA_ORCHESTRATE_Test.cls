/*-------------------------------------------------------------
Author: Jerry Clifft
Created on: 24th March 2020
Created for: CONGA Orchestrate - Product BUG Work-Around related to Customer Object with Master-Detail being used as an Initator Object.
Description: Trigger will cause insertion of Intator record.
-------------------------------------------------------------*/

@isTest
private class 
DISH_Contract_Do_CONGA_ORCHESTRATE_Test { 
    private static testMethod void Test_CustomObjectNameTrigger () { 
    // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
        insert acct1;

    // Create Contact
        Contact ctc1 = New Contact();
            ctc1.FirstName = 'Jerry';
            ctc1.LastName = 'Clifft';
            ctc1.Phone = '5123835201';
            ctc1.Email = 'Jerry.Clifft@DISH.com';
            ctc1.Account = acct1;
        insert ctc1;  
    
        Opportunity opp1 = New Opportunity();
            opp1.Account = acct1;
            opp1.Name = 'Test Opp for Conga Orchestrate';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '786664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
        insert opp1;
    
        DISH_Contract__c fw = new DISH_Contract__c();
            fw.Account__c = acct1.Id;
            fw.Opportunity__c = opp1.Id;
            fw.Contact__c = ctc1.Id;
            fw.Contact2__c = ctc1.Id;
            fw.Contact3__c = ctc1.Id;
            fw.RecordTypeId = '012f2000000cbDR';
        insert fw; 
        
        System.assertNotEquals(null, fw.id); 
        update fw; 
        delete fw; 
    }
}