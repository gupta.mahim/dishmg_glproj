@IsTest
public class CSVTenantEquipFileUploaderfromLEXTest {

    public static testMethod void testReadFile() {
        Opportunity newOpp = TestDataFactoryforInternal.createOpportunity();
        
        //Milestone# MS-001252 - 24Aug20 - Removed SeeAllData=True from this test class. Updated the below code accordingly  - Start
        /*list<Document> lstDoc = new list<Document>();
        String fileContent;
        lstDoc = [select id,name,Body from Document where name = 'tenantEquip' Limit 1];
        if(lstDoc.size() > 0)
        	fileContent=lstDoc[0].Body.toString();
        else
            fileContent='Test';
        List<Tenant_Equipment__c> lexAccstoupload1 = CSVTenantEquipFileUploaderfromLEX.ReadFile(fileContent,newOpp.id);
        List<Tenant_Equipment__c> lexAccstoupload = new List<Tenant_Equipment__c>();
        for(Tenant_Equipment__c TE: lexAccstoupload1){
            TE.Phone_Number__c = '1234567890';
            lexAccstoupload.add(TE);
        }
        */
        
        String equipData;
        list<String> equipTemp = new List<String>(2);
        equipTemp[0] = 'Name,Smart_Card__c,Unit__c,Address__c,City__c,State__c,Zip_Code__c,Customer_First_Name__c,Customer_Last_Name__c,Phone_Number__c,Email_Address__c,Opportunity__c';
		equipTemp[1] = 'Buttons,Objects,Type,Assign,Status,,48001,test,abc,1234567890,a@b.com,';			
        
        for(String eD : equipTemp){
            equipData = equipData + eD +  '\n' ;  
        }
        
        List<Tenant_Equipment__c> lexAccstoupload = CSVTenantEquipFileUploaderfromLEX.ReadFile(equipData,newOpp.id);
        //Milestone# MS-001252 - End
        String str = JSON.serialize(lexAccstoupload);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        CSVTenantEquipFileUploaderfromLEX.save(str);
        Test.stopTest();
        
        //CSVTenantEquipFileUploaderfromLEX.invokeAddressValidation();
        
        CSVTenantEquipFileUploaderfromLEX.getStateOptions();
        
        CSVTenantEquipFileUploaderfromLEX.getUserAccess();
    }
    
}