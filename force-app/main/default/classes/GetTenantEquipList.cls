public class GetTenantEquipList {
    
    public class FlowInput
    {
        @Invocablevariable
        public List<Tenant_Equipment__c> TeList = new list <Tenant_Equipment__c>();
        
        @Invocablevariable
        public string taId=null;
        
        @Invocablevariable
        public Integer existingTECount=0;
        
    }
    public class FlowOutput
    {
        @Invocablevariable
        public List<Tenant_Equipment__c> ListOfTE = new list <Tenant_Equipment__c>();

        @Invocablevariable
        Public boolean EquipIsInvalid=false;
    }
    
    @InvocableMethod (label='CheckListEmptyRow')
    public static List<Flowoutput> GetValidRows(List<FlowInput> req)
    {
        List<FlowOutput> result= new list<FlowOutput>();
        FlowOutput output = new FlowOutput();
		result.add(output);
        
        if(req[0].existingTECount>0 && req[0].Telist.size()==1 && String.isEmpty(req[0].Telist[0].Name) && String.isEmpty(req[0].Telist[0].Smart_Card__c) ){ return result;}
        
        List<Tenant_Equipment__c> TEListforOutput= new List<Tenant_Equipment__c>();
        for(Tenant_Equipment__C TE:req[0].Telist)
        {
            if(String.isEmpty(TE.Name))output.EquipIsInvalid=true;
            if(String.isEmpty(TE.Smart_Card__c))output.EquipIsInvalid=true;
            if(!String.isEmpty(TE.Name) && TE.Name.length()<11)output.EquipIsInvalid=true;
            if(!String.isEmpty(TE.Smart_Card__c) && TE.Smart_Card__c.length()<11)output.EquipIsInvalid=true;
            
            output.ListOfTE.add(TE);
        }
        try
        {
            if(!String.isEmpty(req[0].taId))
            {
                List<Tenant_Equipment__c> existingTEs=[Select id from Tenant_Equipment__c where Tenant_Account__c=:req[0].taId];
                if(existingTEs.size()>0){delete existingTEs;}
            }
        }Catch(exception e){}
        
        
       
        
        return result;
    }
    
}