public class Sites_Controler_MuteDC_Choice {

public String nameQuery {get; set;}
public string error {get; set;}

public Sites_Controler_MuteDC_Choice (ApexPages.StandardController controller) {
}
    
public String getNameQuery() {
    return null;
}
    
public String getNameQuery2() {    return null;
}

public List<SelectOption> getItems() {
    List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('-Select-','-Select-'));
        options.add(new SelectOption('Mute1','Mute (Single Account)'));
        options.add(new SelectOption('Mute2','Mute (Multiple Accounts)'));
        options.add(new SelectOption('Disconnect1','Disconnect (Single Account)'));
        options.add(new SelectOption('Disconnect2','Disconnect (Multiple Accounts'));
    return options;
}
    
public PageReference Next(){
    if(nameQuery == '-Select-'){        error='Please select a request type and account type.';      return null;
}
else
if(nameQuery == 'Mute2'){ return new PageReference('/MuteDC/Site_MuteDC_MuteEntry?acctId=' + ApexPages.currentPage().getParameters().get('acctId'));
}
else
if(nameQuery == 'Disconnect2'){  return new PageReference('/MuteDC/Site_MuteDC_NonPayDC_Entry?acctId=' + ApexPages.currentPage().getParameters().get('acctId'));
}
else
if(nameQuery == 'Mute1'){ return new PageReference('/MuteDC/Sites_MuteDC_MutesSingleEntry?acctId=' + ApexPages.currentPage().getParameters().get('acctId'));
}
else
if(nameQuery == 'Disconnect1'){ return new PageReference('/MuteDC/Sites_MuteDC_DCSingleEntry?acctId=' + ApexPages.currentPage().getParameters().get('acctId'));
}
else
{
        error='Please select a request type and account type.';
     return null;
}
}
}