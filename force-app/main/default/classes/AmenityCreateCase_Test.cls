@isTest(SeeAllData=true)
Public class AmenityCreateCase_Test{
static testMethod void testAmenityCreateCase(){
 
   Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        insert a;
        
               Opportunity o = new Opportunity(
               Name = 'Test Opp',
               AccountId=a.Id,
               LeadSource='Portal', 
                RecordTypeId='0126000000053vu', 
                misc_code_siu__c='N123', 
                Restricted_Programming__c='True', 
                CloseDate=system.Today(), 
                StageName='Closed Won',
                Launch_Date__c=system.Today());
                insert o;
                
            Front_Office_Request__c fo = new Front_Office_Request__c(
                     RecordTypeId = '0126000000017k7',   
                     OwnerId = '00G60000001Evwv',
                     Account__c = o.AccountId,
                     Opportunity__c = o.Id,
                     Front_Office_Decision__c = 'approved',
                     Request_Type__c = 'Amenity Incentive Verification');
                     insert fo;
                     update fo;   
                     
                     
               fo = [SELECT RecordTypeId, OwnerId, Front_Office_Decision__c, Request_Type__c  FROM Front_Office_Request__c WHERE Id =:fo.Id];
                   System.debug('Record Type ID after trigger fired: ' + fo.RecordTypeId);
                      System.assertEquals('0126000000017k7', fo.RecordTypeId);
                     
 
 }}