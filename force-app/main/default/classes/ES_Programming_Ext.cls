public with sharing class ES_Programming_Ext {
private List<opportunityLineItem> Prods;
private Executive_Summary__c es;
    public ES_Programming_Ext(ApexPages.StandardController controller) {
    this.es=(Executive_Summary__c)controller.getRecord();
    }

  public List<opportunityLineItem> getprods() {
       Executive_Summary__c ex = [SELECT ID, Property_Name__r.id from Executive_Summary__c where id = :es.id];
       if (ex.Property_Name__c == null)
       return null;

       prods = [select Id, Quantity, TotalPrice, UnitPrice, Description, PriceBookEntryId, PriceBookEntry.Name, Status__c
       FROM opportunityLineItem WHERE OpportunityId = :ex.Property_Name__r.id ];
         


        return prods;

}




}