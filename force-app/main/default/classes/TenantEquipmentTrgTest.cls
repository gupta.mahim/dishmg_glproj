@isTest
public class TenantEquipmentTrgTest 
{ 
    @isTest static  void TestTripAction()  
    {
        
        Opportunity Opp1= TestDataFactoryforInternal.createOpportunity(1);
        Tenant_Account__c TA=TestDataFactoryforInternal.createTenantAccount(opp1);
        Tenant_Equipment__c TECreate= new Tenant_Equipment__c();
        TECreate=TestDataFactoryforInternal.createTenantEquipment(Opp1);
        Opportunity Opp2= TestDataFactoryforInternal.createOpportunity(1);
        Tenant_Equipment__c TECreate2= new Tenant_Equipment__c();
        TECreate2=TestDataFactoryforInternal.createTenantEquipment(Opp2);
        TECreate.Action__c='Send Trip / HIT';
        update TECreate;
        
        
    }
    @isTest static  void TestREMOVEAction()  
    {
       
        Opportunity Opp1= TestDataFactoryforInternal.createOpportunity(1);
        Tenant_Equipment__c TECreate= new Tenant_Equipment__c();
        TECreate=TestDataFactoryforInternal.createTenantEquipment(Opp1);
        TECreate.Action__c='Remove';
        update TECreate;
        
        Test.startTest();
        SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                               'Complete',
                                                               '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                               null);
        Test.setMock(HttpCalloutMock.class, fakeResponse);
        Test.stopTest();
    }
    /*@isTest static void testSwapAction()
    {   
       
        Opportunity Opp1= TestDataFactoryforInternal.createOpportunity(1);
        Tenant_Equipment__c TECreate= new Tenant_Equipment__c();
        TECreate=TestDataFactoryforInternal.createTenantEquipment(Opp1);
        TECreate.Action__c='Swap';
        update TECreate;
    }
    @isTest static void testMuteAction()
    {
       
        Opportunity Opp1= TestDataFactoryforInternal.createOpportunity(1);
        Tenant_Equipment__c TECreate= new Tenant_Equipment__c();
        TECreate=TestDataFactoryforInternal.createTenantEquipment(Opp1);
        TECreate.Action__c='Mute';
        update TECreate;
    } */   
}