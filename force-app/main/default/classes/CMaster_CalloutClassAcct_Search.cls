public class CMaster_CalloutClassAcct_Search {

public class person {
    public String dishCustomerId;
    Public cls_results [] results;
}
class cls_results {
    public String dishCustomerId;
    cls_customerOwner customerOwner;
}
class cls_customerOwner {
    public String Id;
}  

    
    @future(callout=true)
    
    public static void CMasterCalloutSearchCustomer(String Id) {

    list<Contact> C = [select Id, FirstName, LastName, Role__c, Email, Phone, ExtRefNum__c, Pin__c, dishCustomerId__c, partyId__c from Contact where Id =:id Limit 1];    
        if( C.size() < 1 || ((C[0].LastName == '' || C[0].LastName == Null) && (C[0].FirstName == '' || C[0].FirstName == Null )) || (C[0].Email == '' || C[0].Email == Null) || (C[0].Role__c != 'Billing Contact' && C[0].Role__c != 'Billing Contact (PR)' ) || ( C[0].ExtRefNum__c == '' || C[0].ExtRefNum__c == Null)) {
            Contact sbc = new contact();
                sbc.id=id;
                sbc.API_Status__c='';
                sbc.Transaction_Code__c='';
//                sbc.Transaction_Description__c='Search CMaster -Customer-, CM needs requires the Customer ID (ExtRefNum), or First & Last Name, Email and the role must contain Billing Contact. Your data: ExtRefnum = ' +C[0].ExtRefNum__c  + ', First Name = ' + C[0].FirstName + ', LastName = ' +C[0].LastName + ', Email = ' + C[0].Email + ', Phone # = ' + C[0].Phone + ', Contact Role = ' +C[0].Role__c;

        if( (C[0].LastName == '' || C[0].LastName == Null )) {
            sbc.Transaction_Description__c='Search for Registered Customer requires the Customer ID (ExtRefNum), First & Last Name, Email and the role must contain Billing Contact. Your data is missing the Last Name';
        }
        if( (C[0].FirstName == '' || C[0].FirstName == Null )) {
            sbc.Transaction_Description__c='Search for Registered Customer requires the Customer ID (ExtRefNum), First & Last Name, Email and the role must contain Billing Contact. Your data is missing the First Name';
        }
        if( (C[0].Email == '' || C[0].Email == Null )) {
            sbc.Transaction_Description__c='Search for Registered Customer requires the Customer ID (ExtRefNum), First & Last Name, Email and the role must contain Billing Contact. Your data is missing the Email Address';
        }
        if( (C[0].Role__c != 'Billing Contact' && C[0].Role__c != 'Billing Contact (PR)' )) {
            sbc.Transaction_Description__c='Search for Registered Customer requires the Customer ID (ExtRefNum), First & Last Name, Email and the role must contain Billing Contact. Contact Role must be a Billing Contact';
        }
        if( (C[0].ExtRefNum__c == '' || C[0].ExtRefNum__c == Null )) {
            sbc.Transaction_Description__c='Search for Registered Customer requires the Customer ID (ExtRefNum), First & Last Name, Email and the role must contain Billing Contact. Your data is missing the Customer ID (ExtRefNum)';
        }
        update sbc;
        }
        else if( C.size() > 0) {
            System.debug('RESULTS of the LIST lookup to the Contact object' +C);

        list<CustomerMaster__c> CME = [select Id, EndPoint__c, CreatedDate from CustomerMaster__c ORDER BY CreatedDate DESC LIMIT 1];
            System.debug('DEBUG RESULTS of the LIST lookup to the CustomerMaster object' +CME);
            
            System.debug('DEBUG 0 ' +C[0].Id);
            System.debug('DEBUG 1 ' +C[0].FirstName);
            System.debug('DEBUG 2 ' +C[0].LastName);
            System.debug('DEBUG 3 ' +C[0].Email);
            System.debug('DEBUG 4 ' +C[0].Role__c);
            System.debug('DEBUG 5 ' +C[0].Phone);
            System.debug('DEBUG 6 ' +C[0].Pin__c);
            System.debug('DEBUG 7 ' +C[0].ExtRefNum__c);
            System.debug('DEBUG 8 ' +C[0].dishCustomerId__c);
            System.debug('DEBUG 9 ' +C[0].partyId__c);

        HttpRequest request = new HttpRequest();
            String endpoint = CME[0].Endpoint__c+'/cm-search';
            if(C[0].ExtRefNum__c != '' && C[0].ExtRefNum__c != Null){
                request.setEndPoint(endpoint +'/customers?extRefNum='+C[0].ExtRefNum__c);
            }
            else if(C[0].ExtRefNum__c == '' || C[0].ExtRefNum__c != Null){
                request.setEndPoint(endpoint +'/customers?firstName='+C[0].FirstName+'&lastName='+C[0].LastName+'&Phone='+C[0].Phone+'&Email='+C[0].Email+'&maxResultsCount=40');
            }
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Customer-Facing-Tool', 'Salesforce');
            request.setHeader('User-ID', UserInfo.getUsername());
            request.setMethod('GET');
            request.setTimeout(101000);
     
            HttpResponse response = new HTTP().send(request);
                System.debug(response.toString());
                System.debug('STATUS:'+response.getStatus());
                System.debug('STATUS_CODE:'+response.getStatusCode());
                String strjson = response.getbody();
                System.debug('DEBUG Get body:  ' +response.getBody());
                
           if(response.getStatusCode() >= 200 && response.getStatusCode() <= 299){
               System.debug('DEBUG 0 ======== strjson: ' + strjson);

        
           JSONParser parser = JSON.createParser(strjson);
           parser.nextToken();
           person obj = (person)parser.readValueAs( person.class); 
           if(obj.results.size() > 0 ) {
                Contact sbc = new contact(); {
                   sbc.id=id;
                   sbc.API_Status__c='SUCCESS';
                   sbc.Transaction_Description__c='SEARCH has located the Registered Customer!';
                   sbc.dishCustomerId__c=obj.results[0].dishCustomerId;
                   sbc.partyId__c=obj.results[0].customerOwner.Id;
               update sbc;
           }
           }
           else if(obj.results.size() < 1 ) {
                Contact sbc = new contact(); {
                   sbc.id=id;
                   sbc.API_Status__c='SUCCESS';
                   sbc.Transaction_Description__c='SEARCH COMPLETE, no Registered Customer located!';
               update sbc;
               }
           }
               API_Log__c apil = new API_Log__c();{
                   apil.Record__c=id;
                   apil.Object__c='Contact';
                   apil.Status__c='SUCCESS';
                   apil.Results__c=response.getBody();
                   apil.API__c='Search Customer';
                   apil.User__c=UserInfo.getUsername();
               insert apil;
               }
           }
           if (response.getStatusCode() >= 300 && response.getStatusCode() <= 600){
               System.debug('DEBUG Get Respnose to string: ' +response.toString());                          
               System.debug('DEBUG 0 ======== strjson: ' + strjson);
               System.debug('DEBUG Get body:  ' +response.getBody());
               Contact sbc = new contact(); { sbc.id=id; sbc.API_Status__c='ERROR'; sbc.Transaction_Description__c='Registered Customre Search ERROR: ' +response.getBody(); update sbc;
               }
               API_Log__c apil = new API_Log__c();{ apil.Record__c=id; apil.Object__c='Contact'; apil.Status__c='ERROR'; apil.Results__c=response.getBody();  apil.API__c='Search Customer'; apil.User__c=UserInfo.getUsername(); insert apil;
               }
           }
        }
    }
}