public with sharing class prmHomePageComponents {

    User PartnerUser;
    ApprovalRequests[] ApprovalResults = new ApprovalRequests[]{};
    List<Lead> leadInbox = new List<Lead>();    public User getPartnerUser() {         if(PartnerUser==null)        {             PartnerUser= [select u.Contact.AccountId,u.Contact.Account.Owner.Name,u.Contact.Account.Owner.Email from User u where u.id= :UserInfo.getUserId() limit 1];         }                     return PartnerUser;    }        public List<ApprovalRequests> getApprovals() {
/*        Integer i, j;
        List<ProcessInstanceStep> Approvals = new ProcessInstanceStep[0];
        List<Lead> oppRecords = new Lead[0];
        Set<Id> convertedLeads = new Set<Id>();

        if (ApprovalResults.size() == 0) {
            Approvals = [SELECT Id,  ProcessInstance.TargetObject.Name, toLabel(ProcessInstance.TargetObject.Type), ProcessInstance.Status, CreatedDate, ProcessInstance.Id, ProcessInstance.TargetObjectId FROM ProcessInstanceStep WHERE (StepStatus = 'Started') AND (ActorId=:UserInfo.getUserId()) ORDER BY CreatedDate DESC LIMIT 10];

            //Identify the converted leads
            for (i=0; i<Approvals.size(); i++) {
                if (Approvals[i].ProcessInstance.TargetObject == null) {
                    convertedLeads.add(Approvals[i].ProcessInstance.TargetObjectId);
                }
            }
            //Get the Name and OpportunityIds of the converted leads
            oppRecords = [SELECT Id, Name, ConvertedOpportunityId FROM Lead WHERE Id IN :convertedLeads];
        
            for (i=0; i<Approvals.size(); i++) {
                
                //Map the ProcessInstance records to the custom defined object
                ApprovalResults.add(new ApprovalRequests());
                ApprovalResults[i].Status=Approvals[i].ProcessInstance.Status;
                ApprovalResults[i].CreatedDate=Approvals[i].CreatedDate.format();
                
                //If it's a converted lead then set the TargetObjectId to the Opportunity Id and fill in the Name of lead
                //Also default the Type to Lead
                if (Approvals[i].ProcessInstance.TargetObject == null) {
                    for (j=0; j<oppRecords.size(); j++) {
                        if (Approvals[i].ProcessInstance.TargetObjectId == oppRecords[j].Id) {
                            ApprovalResults[i].RelatedTo = oppRecords[j].Name;
                            ApprovalResults[i].RowId = oppRecords[j].ConvertedOpportunityId;
                            ApprovalResults[i].approvalType = 'Lead';
                        }
                    }
                } else {
                    //If it's not a converted lead then map the fields directly
                    ApprovalResults[i].RelatedTo = Approvals[i].ProcessInstance.TargetObject.Name;
                    ApprovalResults[i].approvalType = Approvals[i].ProcessInstance.TargetObject.Type;
                    ApprovalResults[i].RowId = Approvals[i].ProcessInstance.TargetObjectId;
                }
                
            }
        }
*/
        return ApprovalResults;    }        public List<Lead> getLeadInbox() {
        //if (leadInbox.size() == 0) {
            leadInbox = [Select l.Type_of_Roof__c, l.PostalCode,  l.Type_of_Business__c, l.Category__c, l.State, l.RecordType.Name, l.RecordTypeId, 
                l.Number_of_TVs__c, l.City, l.Building_Stories__c, l.CreatedDate 
                FROM Lead l
                Where IsConverted=false AND Owner.Type='Queue' ORDER BY CreatedDate DESC LIMIT 10];
       // }
        return leadInbox;    }        public PageReference accept() {        Lead acceptedLead = new Lead();               Id selectedRow = System.currentPageReference().getParameters().get('selectedRow');
        //system.debug('Selected Id = '+selectedRow);
        acceptedLead = [select Id, ownerid, Retailer_Owner_Accepted_Date_Time__c from Lead where Id = :selectedRow];        acceptedLead.Ownerid = UserInfo.getUserId();        if (acceptedLead.Retailer_Owner_Accepted_Date_Time__c == null) {            acceptedLead.Retailer_Owner_Accepted_Date_Time__c = DateTime.now();        }        Update acceptedLead;              PageReference leadPage = new PageReference('/'+selectedRow);        leadPage.setRedirect(true);               return leadPage; 
    }
    
    
    public PageReference reject() {
        
        Id selectedRow = System.currentPageReference().getParameters().get('selectedRow');
        String reason = System.currentPageReference().getParameters().get('rejectedReason');
        System.debug('Selected Id = ' + selectedRow);
        System.debug('Reject Reason  = ' + reason);
        if (selectedRow == null) {
            return null;
        }
        Lead rejectedLead = [Select Declined_Reason__c, Retailer_Owner_Declined_Date_Time__c from Lead where Id = :selectedRow];        rejectedLead.Ownerid = getRejectedLeadOwner();        rejectedLead.Declined_Reason__c = reason;        if (rejectedLead.Retailer_Owner_Declined_Date_Time__c == null) {            rejectedLead.Retailer_Owner_Declined_Date_Time__c = DateTime.now();        }        update rejectedLead;        return null; 
    }
    
    public List<String> getRejectionReasons() {
        List<String> rejectionReasons = new List<String>();
        Schema.DescribeFieldResult F = Lead.Declined_Reason__c.getDescribe();

        if (F != null){
            transient List<Schema.Picklistentry> plValues = F.getPicklistValues();
            for(Schema.Picklistentry value : plValues){
                rejectionReasons.add(value.getValue());
            }
        }
        return rejectionReasons;
        
    }
    
    public Id getRejectedLeadOwner() {
        String dsName1 = 'Partner Rejections'; //field name
        Map<String, LeadRejection__c> mapLeadRejection = LeadRejection__c.getAll();
        LeadRejection__c leadRejectionOwner = mapLeadRejection.get(dsName1);
        String rejectedLeadOwner = leadRejectionOwner.Rejected_Lead_Owner__c;
        System.debug('rejectedLeadOwner = ' + rejectedLeadOwner);
        return rejectedLeadOwner;
    }
    
     static testMethod void testReject() {
        prmHomePageComponents controller = new prmHomePageComponents();
        controller.reject();
        controller.getRejectedLeadOwner();
        controller.getRejectionReasons();
     }
    
/*    
    static testMethod void testPRMHomePageComponents() {

        
        //Create a new instance of the object
        prmHomePageComponents test = new prmHomePageComponents();
        //Test the approvals, cmname, and leadinbox components
        test.getPartnerUser();              
        test.getApprovals();
        test.getLeadInbox();
        Lead testLead1= null;

        RecordType recType = [select Id from RecordType where SobjectType='Lead' and Name='Residential - PRM TEST' limit 1];

        //Create a new lead and submit it for approval
        testLead1 = new Lead(LastName='testlm1', firstname='testfm1', company='test1', email='test1@test.com',
            ownerid=UserInfo.getUserId(), RecordTypeId=recType.Id, State = 'CA', Phone = '4152223333');
        insert testLead1;
        
        try {

            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(testLead1.id);
            String[] approvers = new List<String>();
            approvers.add(UserInfo.getUserId());
            req1.setNextApproverIds(approvers);
            
            // Submit the approval request for the account
            Approval.ProcessResult result = Approval.process(req1);
            
            // Verify the result
            System.assert(result.isSuccess());
            System.assertEquals('Pending', result.getInstanceStatus(), 'Instance Status'+result.getInstanceStatus());
    
            
            // Approve the submitted request
            
            // First, get the ID of the newly created item
            List<Id> newWorkItemIds = result.getNewWorkitemIds();
            
            // Instantiate the new ProcessWorkitemRequest object and populate it
            Approval.ProcessWorkitemRequest req2 = new Approval.ProcessWorkitemRequest();
            req2.setComments('Approving request.');
            req2.setAction('Approve');
            req2.setNextApproverIds(new Id[] {UserInfo.getUserId()});
            
            // Use the ID from the newly created item to specify the item to be worked
            req2.setWorkitemId(newWorkItemIds.get(0));
            
            // Submit the request for approval
            Approval.ProcessResult result2 =  Approval.process(req2);
            
            // Verify the results
            System.assert(result2.isSuccess(), 'Result Status:'+result2.isSuccess());
            System.assertEquals('Approved', result2.getInstanceStatus(), 'Instance Status'+result2.getInstanceStatus());
        } catch (Exception ex) {
            System.debug('Error: ' + ex);   
        }

        //Test the accept function
        User partner = [Select u.Id, u.Contact.Account.Owner.Email, u.Contact.Account.Name from User u where u.isActive=True and u.UserType='PowerPartner' limit 1];
        Lead testLead2 = new Lead(LastName='testlm2', firstname='testfm2', company='test2', 
            email='test2@test.com',ownerid=partner.Id, State = 'CA', Phone = '4152223333');
        insert testLead2;
        
        System.currentPageReference().getParameters().put('selectedRow',testLead2.Id);
        test.accept();
        Lead acceptedLead = [Select Id, OwnerId from Lead where Id=:testLead2.Id];
        System.assertEquals(UserInfo.getUserId(), acceptedLead.OwnerId, 'The owner field does not equal the partner id');
        
        //Delete the junk leads         
        delete testLead1;
        delete testLead2;
    }
*/    
}