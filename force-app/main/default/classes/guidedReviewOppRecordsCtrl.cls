/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 10 September 2020
Created for: Milestone# MS-000971, MS-001092, MS-003351
Description: To get details of Opportunity related fields for GuideMe Create Property Review screen
-------------------------------------------------------------*/

public class guidedReviewOppRecordsCtrl {
    
    public class wrapperUtility{
        @AuraEnabled
        public String HotelPortfolioOwnerName {get;set;}
        @AuraEnabled
        public String SelectedDistributorValue {get;set;}
        @AuraEnabled
        public String BrandName {get;set;}
        @AuraEnabled
        public String SubBrandName {get;set;}
    }
    
    @AuraEnabled
    public static wrapperUtility getInitDetails(string HotelPFOwnerId,string selDistAPI, string BrandId, string SubBrandId)
    {
        wrapperUtility response = new wrapperUtility();
        
        List<Hotel_Portfolio_Owner__c> ownerNameLst = New List<Hotel_Portfolio_Owner__c>();
        ownerNameLst = [Select Name from Hotel_Portfolio_Owner__c where Id = : HotelPFOwnerId Limit 1];
        if(ownerNameLst.size()>0){
        	response.hotelPortfolioOwnerName = String.valueof(ownerNameLst[0].Name);
        }
               
        Schema.DescribeFieldResult fieldResult = Opportunity.Distributor__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry v : values) {
            if(selDistAPI == v.getValue())
                response.SelectedDistributorValue = v.getLabel();
        }
        
        List<Brands__c> brandNameLst = New List<Brands__c>();
        brandNameLst = [Select Name from Brands__c where Id = : BrandId Limit 1];
        if(brandNameLst.size()>0){
        	response.BrandName = String.valueof(brandNameLst[0].Name);
        }
        
        List<Sub_Brand__c> subBrandNameLst = New List<Sub_Brand__c>();
        subBrandNameLst = [Select Name from Sub_Brand__c where Id = : SubBrandId Limit 1];
        if(subBrandNameLst.size()>0){
        	response.SubBrandName = String.valueof(subBrandNameLst[0].Name);
        }
        return response;
    }
}