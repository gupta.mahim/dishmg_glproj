public with sharing class PartnerConvertLead {
    
    String LeadId = '';
   
    public PartnerConvertLead(ApexPages.StandardController controller){
        LeadId = ApexPages.currentPage().getParameters().get('id');
    }
    
    
    public Pagereference ConvertIt() {
        Lead ThisCase = [Select OwnerID, Sales_Stage__c, Convert_Lead__c from Lead where ID = :LeadID];
                if( ThisCase.Sales_Stage__c == 'Ready to Activate' ){
                        ThisCase.Convert_Lead__c = 'Ready to Activate';
                        update ThisCase;
                        return  new Pagereference('/'+LeadID);}


                        return  new Pagereference('/'+LeadID);
        
    }
    

}