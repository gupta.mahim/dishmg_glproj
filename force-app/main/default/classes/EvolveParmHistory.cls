public class EvolveParmHistory{
    public List<Evolve_History__b> listOfEvolveHistory{get;set;}

    public EvolveParmHistory(ApexPages.StandardController controller){
//        listOfEvolveHistory = [SELECT Id, Evolve__c, Status_Last_Checked__c, Ethernet_MAC__c, Ethernet_IP__c, Software_Version__c, Connection_Type__c, Room__c, Status__c, WiFi_MAC__c, WiFi_IP__c, CM_IP__c, CM_MAC__c, HOST_MAC__c, HOST_IP__c,Serial_Number__c FROM Evolve_History__b WHERE Evolve__c = 'a4if2000000tLGNAA2'];
        listOfEvolveHistory = [SELECT Id, Evolve__c, Status_Last_Checked__c, Ethernet_MAC__c, Ethernet_IP__c, Software_Version__c, Connection_Type__c, Room__c, Status__c, WiFi_MAC__c, WiFi_IP__c, CM_IP__c, CM_MAC__c, HOST_MAC__c, HOST_IP__c,Serial_Number__c FROM Evolve_History__b WHERE Evolve__c =:ApexPages.currentPage().getParameters().get('Id') ];
        if(listOfEvolveHistory == null){ listOfEvolveHistory = new List<Evolve_History__b>(); }
    }
}