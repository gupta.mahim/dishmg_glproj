@isTest
Public class FiberReferralTests{
    public static testMethod void testFiberReferralForm(){
    
    PageReference pageRef = Page.FiberReferral;
    Test.setCurrentPageReference(pageRef);
    
    Lead newlead = new Lead (INS_Lead_Hold__c=true,
    lastname='Fiber Referral Lead',
    OwnerId='00G600000016h6A',
    RecordTypeId='012600000005D4m',
    LeadSource='Referral',
    Lead_Source_External__c='FRRP',
    Billing_Name__c ='Business Name',
    Billing_Address__c='Business Address',
    Contact_Address__c='Contact Address',
    Billing_City__c='Business City',
    Contact_City__c='Contact City',
    Billing_State__c='TX',
    Contact_Name__c='TX',
    Billing_Zip__c='78759',
    Contact_Zip_Code__c='78664',
    Billing_Phone__c='5123832201',
    Contact_Phone__c='5122965581',
    Billing_Email__c='Business@Email.com',
    Contact_Email__c='Contact@Email.com',
    Contact_Role__c='Contact Role',
    Company='Property Name',
    Street='Property Address',
    Connectivity_between_buildings__c='Yes',
    City='Property City',
    State='TX',
    Postalcode='78664',
    Existing_Provider__c='New',
    Phone='5122965581',
    Number_of_Units__c=100,
    of_Buildings__c=12,
    Referrer_OE__c='1',
    Referrer_Contact_Name__c='Referrer Name',
    Referrer_Contact_Email__c='Referrer@Email.com',
    Referrer_Contact_Phone__c='5122965581',
    Existing_Service_Model__c='Existing Service Model',
    Type_of_vertical_wiring__c='CAT6E',
    Type_of_horizontal_wiring__c='CAT6E',
    Construction_Type__c='Mew',
    Single_or_Multi_Site__c='Single',
    Contact_State__c='TX',
    Distributor__c='DOW'
    );

  ApexPages.StandardController sc = new ApexPages.standardController(newlead);  
FiberReferral myPageCon = new FiberReferral(sc);
myPageCon.onsave();
}


}