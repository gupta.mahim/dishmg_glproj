/*-------------------------------------------------------------------------------------------------
Author:             Khateeb Ibrahim
Company:            Tectonic
Description:        Test class for CSBoxProCam1_2_SNFix trigger
Created Date   :    06/08/2018               
----------------------------------------------------------------------------------------------------*/
@IsTest
public class SBoxProCam1_2_SNFix_Test {
    public static testmethod void testSmartBoxEquipment(){
        
        Test.startTest();
        
        Account a1 = new Account();
        a1.Name = 'Test Account';
        a1.Programming__c = 'Starter';
        a1.phone = '(303) 555-5555';
        insert a1;
        
        Opportunity o1 = new Opportunity();
        o1.Name = 'Test';
        o1.AccountId = a1.Id;
        o1.BTVNA_Request_Type__c = 'Site Survey';
        o1.LeadSource='BTVNA Portal';
        o1.Property_Type__c='Private';
        o1.misc_code_siu__c='N999';
        o1.CloseDate=system.Today();
        o1.StageName='Closed Won';
        o1.Equipment_Model__c = '311';
        o1.Amenity_tier__c = '30-49 (PCO 2013 - AI 36)';
        
        insert o1;
        
        List < Smartbox__c > listSmartBox = new List < Smartbox__c > {
            new Smartbox__c (Serial_Number__c='LALPSC01365A', Second_ProCam__c=FALSE, Chassis_Serial__c='LALPFC00024C',Opportunity__c=o1.id),
            new Smartbox__c (Serial_Number__c='LALPHEA23741', Second_ProCam__c=FALSE, Chassis_Serial__c='LALPFC00055A',Opportunity__c=o1.id)
        };
		insert listSmartBox;
        
        
        
        listSmartBox[0].Serial_Number__c='LALPRC02063B';
        listSmartBox[0].Second_ProCam__c=FALSE;
        listSmartBox[1].Serial_Number__c='LALPRC02063B';
        listSmartBox[1].Second_ProCam__c=FALSE;
        update listSmartBox;    
        
        
        /*Smartbox__c sb = new Smartbox__c (Serial_Number__c='LALPSC01365A', Second_ProCam__c=FALSE, Chassis_Serial__c='LALPFC00024C',Opportunity__c=o1.id);
        insert sb;
        
		sb.Second_ProCam__c=TRUE;     
        sb.Serial_Number__c='LALPRB03162K';
        update sb;*/
        
        Test.stopTest();
        
    }
}