//for use with tenant account to create and update TV

public with sharing class AmdocsActivateUpdateButton2 {
    
    String TenantId = '';
   
    public AmdocsActivateUpdateButton2(ApexPages.StandardController controller){
        TenantId = ApexPages.currentPage().getParameters().get('id');
    }
    
    
     public Pagereference ActivateUpdate(){
        Tenant_Account__c ThisTAccnt = [Select Id, AmDocs_ServiceID__c FROM Tenant_Account__c WHERE ID = :TenantID];    
        
        String TA = [select Id, Opportunity__c from Tenant_Account__c where Id = :TenantID LIMIT 1 ].Opportunity__c ;
        
        Opportunity ThisOpp = [Select Id, IsActiveAmdocs__c, HasSingedContract__c, Amdocs_CustomerID__c, AmDocs_SiteID__c, AmDocs_ServiceID__c, RecordTypeId, AmDocs_tenantType__c from Opportunity where ID = :TA];
        
    // update tv if tenant type is individual/incremental and amdocs is active and contract is signed 
        if( ThisTAccnt.AmDocs_ServiceID__c != Null && ThisOpp.Amdocs_SiteID__c != NULL && ThisOpp.IsActiveAmdocs__c == TRUE && ThisOpp.HasSingedContract__c == TRUE && (ThisOpp.AmDocs_tenantType__c == 'Incremental' || ThisOpp.AmDocs_tenantType__c == 'Individual')){
                ThisTAccnt.AmDocs_UpdateUnit__c = TRUE;
                update ThisTAccnt;
                return  new Pagereference('/'+TenantID);}
    
    // create tv if tenant type is individual and amdocs is active and contract is signed 
        if( ThisTAccnt.AmDocs_ServiceID__c == Null && ThisOpp.Amdocs_SiteID__c != NULL && ThisOpp.IsActiveAmdocs__c == TRUE && ThisOpp.HasSingedContract__c == TRUE && ThisOpp.AmDocs_tenantType__c == 'Individual'){
                ThisTAccnt.AmDocs_CreateUnit__c = TRUE;
                update ThisTAccnt;
                return  new Pagereference('/'+TenantID);}               
            return  new Pagereference('/'+TenantId);
        
    }
}