@isTest
Public class Smartbox_Equipment_EditTests{
    public static testMethod void Smartbox_testEquipment_Edit(){

//Test converage for the myPage visualforce page
PageReference pageRef = Page.Smartbox_equipment_edit_smatv;
Test.setCurrentPageReference(pageRef);
Opportunity newOpp = new Opportunity (name='XYZ Organization', StageName='Closed Won', CloseDate=Date.Today());
insert newOpp;

Smartbox__c myEq = new Smartbox__c (
CAID__c = '10251973',
Chassis_Serial__c = '07112006',
Serial_Number__c = '07252010',
SmartCard__c = '12250000',
Status__c = 'Activation Requested',
Opportunity__c=newopp.id);
insert myEq;
ApexPages.StandardController sc = new ApexPages.standardController(newOpp);

// create an instance of the controller
Smartbox_Equipment_Edit myPageCon = new Smartbox_Equipment_Edit(sc);

//try calling methods/properties of the controller in all possible scenarios
// to get the best coverage.
myPageCon.GetEquip();
myPageCon.onsave();
myPageCon.onclose();
myPageCon.onadd();
}
}