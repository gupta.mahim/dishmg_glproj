public class QuoteLineTriggerHandler {
    private static final Integer MONTHS_IN_A_YEAR = 12;
    private static final Integer MONTHLY_CAPITAL_CONTRIBUTION_MONTHS_BEFORE_START = 4;
    private static final Integer LUMPSUM_CAPITAL_CONTRIBUTION_MONTHS_BEFORE_START = 2;
    private static final Integer MONTHLY_DOOR_FEES_MONTHS_BEFORE_START = 4;
    private static final Integer LUMPSUM_DOOR_FEES_MONTHS_BEFORE_START = 2;
    
    public static Boolean flag = true; 
    
    public static void handleQuoteLines(List<SBQQ__QuoteLine__c> quoteLineItems){
        SBQQ__QuoteLine__c quotelineBundle = new SBQQ__QuoteLine__c();
        for(SBQQ__QuoteLine__c quoteLine : quoteLineItems){
            //added by tin  
            //verified that DISH FIBER has no ProductCode 
            //temporarily used != 0 to ARPU
            if(quoteLine.SBQQ__OptionLevel__c == null && quoteLine.ARPU__c != 0 && quoteLine.ARPU__c != null){
                quotelineBundle = quoteLine;
            }
            else if(quoteLine.SBQQ__OptionLevel__c == null && (quoteLine.ARPU__c == 0 || quoteLine.ARPU__c == null)){
                System.debug('Inside Else If');
                quoteLine.NPV2__c = 0;
                quoteLine.IRR2__c = 0;
                quotelineBundle = null;
            }
            else{
                quotelineBundle = null;
            }
        }
        System.debug('quotelineBundle: ' + quotelineBundle);
        if(quotelineBundle != null){
            handleBundleQuoteLine(quotelineBundle);
        }
    }

    public static void handleBundleQuoteLine(SBQQ__QuoteLine__c quoteLineItem){
        SBQQ__Quote__c quote = [SELECT Id, No_of_Units__c , Contract_Term__c, Upfront_Door_Fees__c, Door_Fees_of_Months__c, Monthly_Door_Fees__c,
                                Discount_for_Ramp_Pricing__c, No_of_Months_Ramp_Pricing__c, Lump_Sum_Upfront_Payments__c,
                                No_of_Months_Upfront_Payments__c, Monthly_Upfront_Payments__c, Correct_Roof_Requirements__c,
                                Correct_MDF_Requirements__c, Correct_IDF_Requirements__c, Correct_In_Unit_Requirements__c, 
                                Miscellaneous_Adjustments__c, No_Days_Door_Money_Payment_Due__c, Contract_Start_Date__c,
                                Month_1__c, Month_2__c, Month_3__c, Month_4__c, Month_5__c, Month_6__c, Month_7__c, Month_8__c, Month_9__c,
                                Month_10__c, Month_11__c, Month_12__c, Annual_ARPU_Increase__c, Year_1__c, Year_2__c, Year_3__c,
                                Year_4__c, Year_5__c, Year_6__c, Year_7__c, Year_8__c, Year_9__c,
                                Year_10__c, Ramp_Pricing__c, Monthly_Cash_Flow__c,RecordTypeId
                                FROM SBQQ__Quote__c 
                                WHERE ID=:quoteLineItem.SBQQ__Quote__c]; 
        //Get needed RecordTypeId for Quote
        Id NO_APPROVER_IDF_RECORDTYPEID = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('No Approver Quote IDF').getRecordTypeId();
        Id FIBER_IDF_PROPERTY_RECORDTYPEID = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('Fiber IDF Property').getRecordTypeId();
        Id NO_APPROVER_MDF_RECORDTYPEID = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('No Approver Quote MDF').getRecordTypeId();
        Id FIBER_MDF_RECORDTYPEID = Schema.SObjectType.SBQQ__Quote__c.getRecordTypeInfosByName().get('Fiber MDF Only Property').getRecordTypeId();
        
        Integer contractTerms = (Integer)quote.Contract_Term__c; // Contract Terms
        Integer numberOfUnits = (Integer)quote.No_of_Units__c ; // Number of Units
        Integer contractStartMonth = 0; // Contract Start Month - Can be modified to be month dependent
        Date contractStartDate = Date.newInstance(Date.today().year(), 1, 1); // Contract Start Date - Can be Modified to be month dependent
        System.debug('contractTerms: ' + contractTerms);
        System.debug('$ Monthly Door Money: ' + quote.Monthly_Door_Fees__c);
        System.debug('# Months Door Money: ' + quote.Door_Fees_of_Months__c);
        System.debug('$ Lump Sum Door Money: ' + quote.Upfront_Door_Fees__c);
        System.debug('$ Annual % Increase: ' + quote.Annual_ARPU_Increase__c);
        System.debug('numberOfUnits: ' + numberOfUnits);
        System.debug('quoteLineItem.ARPU__c: ' + quoteLineItem.ARPU__c);
        if(contractTerms == 0 || numberOfUnits == 0){
            System.debug('Contract Term or Number of Units is 0');
            return;
        }
        
        Decimal totalAnnualRecurringProducts = 0; // Get year 1 - 10
        system.debug(quote.Year_1__c);
        system.debug(quote.Year_2__c);
        system.debug(quote.Year_3__c);
        system.debug(quote.Year_4__c);
        system.debug(quote.Year_5__c);
        system.debug(quote.Year_6__c);
        system.debug(quote.Year_7__c);
        system.debug(quote.Year_8__c);
        system.debug(quote.Year_9__c);
        system.debug(quote.Year_10__c);
        Map<Integer, Decimal> annualRecurringProductMap = SIMP_RecordController.getAnnualRecurringProductMap(10, (sObject)quote); // Maximum Years 10
        for(Decimal yearVal : annualRecurringProductMap.values()){
            system.debug('yearVal:' + yearVal);
            if(yearVal != null){
            totalAnnualRecurringProducts += yearVal;
            }
        }
        Decimal totalMonthlyRecurringProducts = 0;
        system.debug('yearValx:' + totalMonthlyRecurringProducts);
        if(quote.Monthly_Cash_Flow__c != null){
          system.debug('yearValx:' + totalMonthlyRecurringProducts);  
          totalMonthlyRecurringProducts = quote.Monthly_Cash_Flow__c;
        }
        system.debug('yearValx:' + quote.Monthly_Cash_Flow__c);
        //Decimal totalMonthlyRecurringProducts = quote.Monthly_Cash_Flow__c; // GET Monthly Recurring Products value field and multiply by Contract Term
        Decimal initialOutlay = quoteLineItem.SBQQ__PackageTotal__c - totalAnnualRecurringProducts -  totalMonthlyRecurringProducts + 
            quote.Correct_Roof_Requirements__c + quote.Correct_MDF_Requirements__c + quote.Correct_IDF_Requirements__c + 
            quote.Correct_In_Unit_Requirements__c + quote.Miscellaneous_Adjustments__c; // Initial Outlay
        System.debug('initialOutlay: ' + initialOutlay);
        System.debug('annualRecurringProductMap: ' + annualRecurringProductMap);
        System.debug('MonthlyRecurringProducts: ' + totalMonthlyRecurringProducts);
        System.debug('totalMonthlyRecurringProducts: ' + quote.Monthly_Cash_Flow__c);
        //Get Monthly CashInflow
        Decimal arpu =  quoteLineItem.ARPU__c; // ARPU
        Decimal annualArpuIncrease = quote.Annual_ARPU_Increase__c; // Annual ARPU Increase
        Map<Integer,Integer> rampUnitsPerMonth = SIMP_RecordController.getRampUnitsPerMonth(MONTHS_IN_A_YEAR, (sObject)quote);// Create Monthly Map Value for Ramp Units Month 1 - 12 
        Integer currentNumberOfUnits = 0; // Placeholder for current number of units for Ramp
        Integer currentMonth = 1; // Placeholder for current month
        Integer yearMonthLimit = 0;
        //CAPITAL CONTRIBUTION
        Decimal capitalContributionLumpSumVal = quote.Lump_Sum_Upfront_Payments__c; // Lump Sum Capital Contribution
        Decimal capitalContributionMonthlyVal = quote.Monthly_Upfront_Payments__c; // Monthly Capital Contribution
        Integer capitalContributionStartMonth = capitalContributionMonthlyVal != 0 ? MONTHLY_CAPITAL_CONTRIBUTION_MONTHS_BEFORE_START : 0; //Monthly Capital Contribution Start Month
        Integer capitalContributionEndMonth = capitalContributionStartMonth != 0 ? capitalContributionStartMonth + (Integer)quote.No_of_Months_Upfront_Payments__c : 0; // Monthly Capital Contribution End Month
        Integer capitalContributionLumpSumStartMonth = capitalContributionLumpSumVal != 0 ? LUMPSUM_CAPITAL_CONTRIBUTION_MONTHS_BEFORE_START : 0; // Lump Sum Capital Contribution Start Month
        //DOOR MONEY
        Decimal doorMoneyLumpSumVal = quote.Upfront_Door_Fees__c; // Lump Sum Door Fees
        Decimal doorMoneyMonthlyVal = quote.Monthly_Door_Fees__c; // Monthly Door Fees
        Integer doorMoneyStartMonth = doorMoneyMonthlyVal != 0 ? MONTHLY_DOOR_FEES_MONTHS_BEFORE_START : 0; //Monthly Door Fees Start Month
        Integer doorMoneyEndMonth = doorMoneyStartMonth != 0 ? doorMoneyStartMonth + (Integer)quote.Door_Fees_of_Months__c : 0; // Monthly Door Fees End Month
        Integer doorMoneyLumpSumStartMonth = doorMoneyLumpSumVal != 0 ? LUMPSUM_DOOR_FEES_MONTHS_BEFORE_START : 0; // Lump Sum Door Fees Start Month
        
        Map<Integer, Integer> monthsPerYear = SIMP_RecordController.getMonthsInYear(contractTerms, contractStartMonth); // get Months per year
        Map<Integer, Decimal> monthlyNetCashflowMap = new Map<Integer, Decimal>(); 
        
        for(Integer year : monthsPerYear.keySet()){
            System.debug('year: ' + year);
            yearMonthLimit += monthsPerYear.get(year);
            for(Integer month = currentMonth ; month <= yearMonthLimit; month++){
                System.debug('month: ' + month);
                Decimal baseCashInflow = 0;
                //For Ramp Unit
                if(year == 1 && quote.Ramp_Pricing__c){
                    currentNumberOfUnits = rampUnitsPerMonth.get(month); // Check if there is a new value for Ramp Pricing. If 0 then use currentNumber
                    System.debug('currentNumberOfUnits: ' + currentNumberOfUnits);
                    baseCashInflow = arpu * currentNumberOfUnits; // Todo : Get monthly value for ramp units
                }
                else{
                    baseCashInflow = arpu * numberOfUnits; // 
                }
                System.debug('baseCashInflow: ' + baseCashInflow);
                Decimal monthlyCashInflow = baseCashInflow;//Add other factors monthly
                if(month == (doorMoneyLumpSumStartMonth + 1)){
                    monthlyCashInflow -= doorMoneyLumpSumVal; //If Door fees Month = start of lump sum payment add to month cash inflow
                }
                if(month > doorMoneyStartMonth && month <= doorMoneyEndMonth){
                    monthlyCashInflow -= doorMoneyMonthlyVal; // If Door Fees monthly start month period add to month cash inflow
                }
                if(month == (capitalContributionLumpSumStartMonth + 1)){
                    monthlyCashInflow += capitalContributionLumpSumVal; //If Capital Contributions Month = start of lump sum payment add to month cash inflow
                }
                if(month > capitalContributionStartMonth && month <= capitalContributionEndMonth){
                    monthlyCashInflow += capitalContributionMonthlyVal; // If capital contribution monthly start month period add to month cash inflow
                }
                //Get Annual Value if the month is the first month of the year
                if(math.mod(month, 12) == 1){
                    system.debug('getAnnualRecurringProductMap' + annualRecurringProductMap);
                    if(annualRecurringProductMap.get((month/12)+1) != null){
                    monthlyCashInflow -= annualRecurringProductMap.get((month/12)+1);
                    }
                    system.debug('getAnnualRecurringProductMap' + annualRecurringProductMap);
                }
                // Add Monthly recurring product Val
                if(quote.Monthly_Cash_Flow__c != null){
                    monthlyCashInflow -= quote.Monthly_Cash_Flow__c;
                    system.debug('getAnnualRecurringProductMap' + quote.Monthly_Cash_Flow__c);
                }
                System.debug('monthlyCashInflow: ' + monthlyCashInflow);
                monthlyNetCashflowMap.put(month, monthlyCashInflow);
                currentMonth += 1;
            }
            arpu = arpu * (1+(annualArpuIncrease/100)); //Updates ARPU Value based on Annual Increase
        }
        //set (1 + 0.8333) fixed discount price
        Decimal yearlyDiscount = 0.10;
        Decimal monthlyDiscount = yearlyDiscount/12;
        Decimal discountRate = 1 + monthlyDiscount;
        System.debug('discountRate: ' + discountRate);
        Decimal sumOfPVs = SIMP_RecordController.getSumOfPVs(discountRate, monthlyNetCashflowMap);
        Decimal NPV;
        
        NPV = sumOfPVs - initialOutlay;
        System.debug('NPV: ' + NPV);
        quoteLineItem.NPV2__c = NPV;
        
        List<Decimal> cashflowList = new List<Decimal>();
        cashflowList.add(initialOutlay * -1);
        cashflowList.addAll(monthlyNetCashflowMap.values());
        quoteLineItem.IRR2__c  = SIMP_RecordController.calculateIRR(cashflowList) * 12;
        System.debug('IRR: ' + quoteLineItem.IRR2__c);
        quoteLineItem.Payback_Period__c = SIMP_RecordController.calculatePaybackPeriod(initialOutlay, monthlyNetCashflowMap);
        //tin added 09/05/2019 
        //
        //
        system.debug('PaybackPercent1 ' + ((((quoteLineItem.Payback_Period__c - (quoteLineItem.Contract_Term__c / 2))) /  (quoteLineItem.Contract_Term__c / 2))) * 100);
        quoteLineItem.PaybackPercent__c = ((((quoteLineItem.Payback_Period__c - (quoteLineItem.Contract_Term__c / 2))) /  (quoteLineItem.Contract_Term__c / 2))) * 100;
        System.debug('(' + quoteLineItem.Payback_Period__c + '-  (' + quoteLineItem.Contract_Term__c + '/2))/'+ quoteLineItem.Contract_Term__c+ '/ + 2 ) * 100');
        System.debug('Payback_Period__c: ' + quoteLineItem.Payback_Period__c);
        System.debug('PaybackPercent__c: ' + quoteLineItem.PaybackPercent__c);
        
        List<String> retStr = new List<String>();
        //system.debug('RETURN' + checkApprover(quoteLineItem.IRR2__c, quoteLineItem.PaybackPercent__c, quoteLineItem.Payback_Period__c));
        retStr = checkApprover(quoteLineItem.IRR2__c, quoteLineItem.PaybackPercent__c, quoteLineItem.Payback_Period__c,contractTerms);
        
        if(retStr.size() > 0){
          quoteLineItem.ApproverLevel__c = retStr[0];
          quoteLineItem.ApprovalText__c = retStr[1];
        }
        else{
          quoteLineItem.ApproverLevel__c = 'None';
          quoteLineItem.ApprovalText__c = 'None';
        }
        
        if(quoteLineItem.ApproverLevel__c != 'None'){
          quote.ApprovalTextFromql__c = quoteLineItem.ApprovalText__c;
          quote.ApprovalLevelFromql__c = quoteLineItem.ApproverLevel__c;
        }
        else{
          quote.ApprovalTextFromql__c = '';
          quote.ApprovalLevelFromql__c = '';
        }
        
        if(quote.RecordTypeId == NO_APPROVER_IDF_RECORDTYPEID){
           quote.RecordTypeId = FIBER_IDF_PROPERTY_RECORDTYPEID;
        }
        if(quote.RecordTypeId == NO_APPROVER_MDF_RECORDTYPEID){
           quote.RecordTypeId = FIBER_MDF_RECORDTYPEID;
        }
        system.debug('QUOTEEEE' + quote.ApprovalTextFromql__c + ' '+ quote.ApprovalTextFromql__c);
       
        update quote;
        
    }
    
    public static List<String> checkApprover(Decimal irrval, Decimal pbval, Decimal prval, Integer cterm){
        List<String> retApp = new List<String>();
        Integer levelirr = 0, levelpbval = 0;
        String retAppStr;
        String retApp1;
        system.debug('TESTINGING ' + irrval +' ' + pbval +' '+ prval);
        if(irrval>= 24 && irrval < 25){
            levelirr = 1;
        }
        else if(irrval>= 23 && irrval < 24){
            levelirr = 2;
        }
        else if(irrval>= 20 && irrval < 23){
            levelirr = 3;
        }
        else if(irrval>= 15 && irrval < 20){
            levelirr = 4;
        }
        else if(irrval < 15){
            levelirr = 5;
        }
        
        system.debug('TESTINGINGIRR ' + levelirr + ' ' + pbval + ' ' + cterm);	
        
        Decimal hpb = cterm / 2;
        if(prval <= hpb){
        }
        else if(pbval<= 5){
            levelpbval = 1;
        }
        else if(pbval > 5 && pbval <= 10){
            levelpbval = 2;
        }
        else if(pbval > 10 && pbval <= 15){
            levelpbval = 3;
        }
        else if(pbval > 15 && pbval < 25){
            levelpbval = 4;
        }        
        else if(((pbval >= 25) || cterm >= 55)){
            levelpbval = 5;
        } 
        
        system.debug('TESTINGINGNPV ' + levelpbval);
        
        if(levelpbval > levelirr){
            system.debug('XXXX');
            retApp.add('Payback Period');
            retApp.add(retAppcheckequiapp(levelpbval));
        }
        else if(levelpbval < levelirr){
            system.debug('YYYY');
            retApp.add('IRR');
            retApp.add(retAppcheckequiapp(levelirr));
        }
        else if(levelpbval == levelirr && levelpbval != 0 && levelpbval != 0){
            retApp.add('IRR');
            retApp.add(retAppcheckequiapp(levelirr));
        }
        return retApp;
    }
    
    public static String retAppcheckequiapp(Integer x){
      String retStr;
        
        if(x == 1){
           retStr = 'Regional Manager';
        }  
        else if(x == 2){
           retStr = 'Sales Manager'; 
        }
        else if(x == 3){
            retStr = 'General Manager';
        }
        else if(x == 4){
            retStr = 'Business Manager';
        }
        else if(x == 5){
            retStr = 'Head of Business Unit';
        }
        
      
      return retStr;
    }
                       
                       
    /*
    public static void handleBundleQuoteLine2(SBQQ__QuoteLine__c quoteLineItem){
        System.debug('quoteLineItem: ' + quoteLineItem);
        SBQQ__Quote__c quote = [SELECT Id, No_of_Units__c , Contract_Term__c, Upfront_Door_Fees__c, Door_Fees_of_Months__c, Monthly_Door_Fees__c,
                                Discount_for_Ramp_Pricing__c, No_of_Months_Ramp_Pricing__c, Lump_Sum_Upfront_Payments__c,
                                No_of_Months_Upfront_Payments__c, Monthly_Upfront_Payments__c, Correct_Roof_Requirements__c,
                                Correct_MDF_Requirements__c, Correct_IDF_Requirements__c, Correct_In_Unit_Requirements__c, 
                                Miscellaneous_Adjustments__c, No_Days_Door_Money_Payment_Due__c, Contract_Start_Date__c,
                                Month_1__c, Month_2__c, Month_3__c, Month_4__c, Month_5__c, Month_6__c, Month_7__c, Month_8__c, Month_9__c,
                                Month_10__c, Month_11__c, Month_12__c, Annual_ARPU_Increase__c, Year_1__c, Year_2__c, Year_3__c,
                                Year_4__c, Year_5__c, Year_6__c, Year_7__c, Year_8__c, Year_9__c,
                                Year_10__c, Ramp_Pricing__c
                                FROM SBQQ__Quote__c 
                                WHERE ID=:quoteLineItem.SBQQ__Quote__c];
        Integer contractTerms = (Integer)quote.Contract_Term__c;
        Integer numberOfUnits = (Integer)quote.No_of_Units__c ;
        System.debug('contractTerms: ' + contractTerms);
        System.debug('numberOfUnits: ' + numberOfUnits);
        System.debug('quoteLineItem.ARPU__c: ' + quoteLineItem.ARPU__c);
        if(contractTerms == 0 || numberOfUnits == 0){
            System.debug('Contract Term or Number of Units is 0');
            return;
        }
        //Initial Outlay - Package Cost + Upfront Door Fees + ... + .. + ...
        //Update 09/04/2019 - TODO: Subtract total Recurring Cost Amount
        Decimal totalRecurringCostAmount = quote.Year_2__c + quote.Year_3__c + quote.Year_4__c + quote.Year_5__c + quote.Year_6__c + quote.Year_7__c + quote.Year_8__c + quote.Year_9__c + quote.Year_10__c;
        System.debug('totalRecurringCostAmount: ' + totalRecurringCostAmount);
        Decimal initialOutlay = quoteLineItem.SBQQ__PackageTotal__c - totalRecurringCostAmount + 
            quote.Correct_Roof_Requirements__c + quote.Correct_MDF_Requirements__c + quote.Correct_IDF_Requirements__c + 
            quote.Correct_In_Unit_Requirements__c + quote.Miscellaneous_Adjustments__c /* - quote.Lump_Sum_Upfront_Payments__c;
        Integer contractStartMonth = 0;
        System.debug('quoteLineItem.SBQQ__PackageTotal__c: ' + quoteLineItem.SBQQ__PackageTotal__c);
        System.debug('quote.Contract_Start_Date__c: ' + quote.Contract_Start_Date__c );
        Date contractStartDate = Date.newInstance(Date.today().year(), 1, 1);
        //Door Fees Values
        Decimal upfrontDoorFees = quote.Upfront_Door_Fees__c;
        Integer upfrontDoorFeeStartYear = upfrontDoorFees != 0 ? (contractStartDate.addDays(60).year() - contractStartDate.year()) + 1 : 0;
        Integer doorFeesMonths = (Integer)quote.Door_Fees_of_Months__c;
        Integer doorFeesStartMonth = contractStartDate.addDays(120).month() - 1;
        Decimal doorFeesValue = quote.Monthly_Door_Fees__c;
        //Ramp Pricing Values
        Integer rampPriceMonths = 12;
        Integer rampPriceStartMonth = 0;
        //Upfront Payment Values
        Integer upfrontMonths = (Integer)quote.No_of_Months_Upfront_Payments__c;
        Decimal upfrontValue = quote.Monthly_Upfront_Payments__c;
        Integer upfrontCapitalStartMonth = contractStartDate.addDays(120).month() - 1;
        Decimal upfrontCapitalFees = quote.Lump_Sum_Upfront_Payments__c;
        Integer upfrontCapitalStartYear = upfrontCapitalFees != 0 ? (contractStartDate.addDays(60).year() - contractStartDate.year()) + 1 : 0;
        //Getting the months per year where the monthly payments are effective
        Map<Integer, Integer> monthsPerYear = getMonthsInYear(contractTerms, contractStartMonth);
        Map<Integer, Integer> discountMonthsPerYear = getMonthsInYear(doorFeesMonths, doorFeesStartMonth);
        Map<Integer, Integer> upfrontMonthsPerYear = getMonthsInYear(upfrontMonths, upfrontCapitalStartMonth);
        Map<Integer, Integer> rampPricingMonthsPerYear = getMonthsInYear(rampPriceMonths, contractStartMonth);
        //System.debug('monthsPerYear: ' + monthsPerYear);
        // Base Cash Inflow
        Decimal baseCashInflow = quoteLineItem.ARPU__c * numberOfUnits * 12;
        Decimal annualArpuIncrease = quote.Annual_ARPU_Increase__c;
        Integer currentNumberOfUnits = 0;
        Decimal arpu =  quoteLineItem.ARPU__c;
        Decimal rampPricingVal = 0;
        System.debug('annualArpuIncrease: ' + annualArpuIncrease);
        Map<Integer, Decimal> cashOutFlowMap = New Map<Integer, Decimal>();
        Map<Integer, Decimal> yearNetCashflowMap = new Map<Integer, Decimal>();
        
        System.debug('baseCashInflow: ' + baseCashInflow);
        
        for(Integer year : monthsPerYear.keySet()){
            Decimal month = monthsPerYear.get(year);
            Decimal monthValue = month.divide(12, 3);
            Decimal upfrontVal = (upfrontMonthsPerYear.get(year) == null ? 0 : upfrontMonthsPerYear.get(year)*upfrontValue);
            if(upfrontCapitalStartYear == year){
                upfrontVal += upfrontCapitalFees;
            }
            Decimal discountVal = (discountMonthsPerYear.get(year) == null ? 0 : discountMonthsPerYear.get(year)*doorFeesValue);
            if(upfrontDoorFeeStartYear == year ){
                discountVal += upfrontDoorFees;
            }
            Integer rampPricingMonths = rampPricingMonthsPerYear.get(year) != null ? rampPricingMonthsPerYear.get(year) + rampPriceStartMonth : 0;
            System.debug('rampPricingMonths: ' + rampPricingMonths);
            
            sObject quoteRec = (sObject)quote;
            if(rampPriceStartMonth != 12){
                for(Integer i = rampPriceStartMonth ; i < rampPricingMonths ; i++){
                    String fieldAPIName = 'Month_' + (i+1) + '__c';
                    Decimal unitPerMonth = (Decimal)quoteRec.get(fieldAPIName) != 0 ? (Decimal)quoteRec.get(fieldAPIName) + currentNumberOfUnits : 0 + currentNumberOfUnits;
                    System.debug('unitPerMonth: ' + unitPerMonth);
                    rampPricingVal += (arpu * unitPerMonth);
                    rampPriceStartMonth += 1;
                    currentNumberOfUnits = (Integer)unitPerMonth;
                    System.debug('rampPricingVal: ' + rampPricingVal);
                    System.debug('currentNumberOfUnits: ' + currentNumberOfUnits);
                    //NOTE: SHOULD the RAMPPRICING MONTH PROCEED THE FIRST YEAR, WOULD THE NEW ARPU VALUE (+Annual Increase Value)
                    // AFFECT RAMP PRICING for that year.
                } 
            }
            
            System.debug('rampPriceStartMonth: ' + rampPriceStartMonth);
            Decimal baseCashInflowVal = 0.0;
            if(year != 1){
                System.debug('Year: ' + year);
                arpu = arpu + arpu*(annualArpuIncrease/100);
                System.debug('arpu: ' + arpu);
                baseCashInflowVal = (arpu * numberOfUnits * 12) * monthValue;
                System.debug('baseCashInflowVal: ' + baseCashInflowVal);
            }else{
                baseCashInflowVal = baseCashInflow*monthValue;
            }
            rampPricingVal = year == 1 && quote.Ramp_Pricing__c ?  rampPricingVal : baseCashInflowVal;
            System.debug('rampPricingVal: ' + rampPricingVal);
            System.debug('Ramp_Pricing__c: ' + quote.Ramp_Pricing__c);
            System.debug('Value to Deduct to Base Cash Inflow: ' + (baseCashInflowVal - rampPricingVal));
            String recurringYearAPIName = 'Year_' + year + '__c';
            Decimal recurringCashInflow = year == 1 ? 0 : (Decimal)quoteRec.get(recurringYearAPIName);
            System.debug('recurringCashInflow ' + year + ': ' + recurringCashInflow);
            Decimal netCashInflow = baseCashInflowVal + upfrontVal - discountVal - (baseCashInflowVal - rampPricingVal) - recurringCashInflow;//discountYear.get(year);
            yearNetCashflowMap.put(year, netCashInflow);
        }
        System.debug('yearNetCashflowMap: ' + yearNetCashflowMap);
        
        Decimal sumOfPVs = 0; 
        Decimal NPV;
        //set (1 + 0.10) fixed discount price
        Decimal discountRate = 1.10;    
        for (Integer year : yearNetCashflowMap.keySet()) {
            Decimal yearlyCF = yearNetCashflowMap.get(year);
            System.debug('yearlyCF: ' + yearlyCF);
            Decimal divisor = discountRate.pow(year);
            System.debug('divisor: ' + divisor);
            Decimal PV = yearlyCF / divisor;
            System.debug('Year: ' + year + ' PV: ' + PV);
            sumOfPVs+=PV;  
        }
        System.debug('sumOfPVs: ' + sumOfPVs);
        System.debug('initialOutlay: ' + initialOutlay);
        
        NPV = sumOfPVs - initialOutlay;
        System.debug('NPV: ' + NPV);
        quoteLineItem.NPV2__c = NPV;
        
        List<Decimal> cashflowList = new List<Decimal>();
        cashflowList.add(initialOutlay * -1);
        cashflowList.addAll(yearNetCashflowMap.values());
        quoteLineItem.IRR2__c  = calculateIRR(cashflowList);
        
        //tin updated 09/08/2019
        quoteLineItem.Payback_Period__c = calculatePaybackPeriod(initialOutlay, yearNetCashflowMap) * 12;
        //tin added 09/05/2019 
        quoteLineItem.PaybackPercent__c = ((quoteLineItem.Payback_Period__c - (quoteLineItem.Contract_Term__c / 2)) / quoteLineItem.Contract_Term__c / 2) * 100;
        System.debug('Payback_Period__c: ' + quoteLineItem.Payback_Period__c);
        
        
    }
    */
}