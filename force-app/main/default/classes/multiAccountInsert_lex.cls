public class multiAccountInsert_lex{


      Id oppId=null;
    public multiAccountInsert_lex(ApexPages.StandardController controller) {
           oppId=controller.getRecord().Id;
           equip = new List<Equipment__c>();
               equip.add(new Equipment__c(Opportunity__c=oppId, Statis__c='Activation Requested', RecordTypeId = '01260000000Luri'));
        }
    
    
    public List<Equipment__c> equip {get; set;}
    
    public multiAccountInsert_lex(){
        equip = new List<Equipment__c>();
               equip.add(new Equipment__c(Opportunity__c=oppId, Statis__c='Activation Requested', RecordTypeId = '01260000000Luri'));
        
    }
    
    public void addrow(){
        equip.add(new Equipment__c(Opportunity__c=oppId, Statis__c='Activation Requested', RecordTypeId = '01260000000Luri'));
        
    }
    
   
    public PageReference save(){
   
        insert equip;
        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.info,'Records saved successfully.');
            ApexPages.addMessage(errormsg);
        equip = new List<Equipment__c>();
               equip.add(new Equipment__c(Opportunity__c=oppId, Statis__c='Activation Requested', RecordTypeId = '01260000000Luri'));
        return null;
         }
         
             public PageReference onclose(){
   
               return new PageReference('/apex/equipment_edit_smatv?Id=' + ApexPages.currentPage().getParameters().get('Id') + '&st=' + + ApexPages.currentPage().getParameters().get('st'));
         }
               
                

}