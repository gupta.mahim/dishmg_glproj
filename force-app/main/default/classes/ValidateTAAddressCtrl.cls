Public class ValidateTAAddressCtrl
{
    public Id current_TA_Id{get;set;}
    public boolean inProgress{get;set;}
    public boolean callNetQual{get;set;}
    public boolean validation_msg_exist{get;set;}
    
    public ValidateTAAddressCtrl(ApexPages.StandardController sc)
    {
        inProgress=true;
        current_TA_Id=sc.getRecord().Id;
        //Invoke_NetQual__c
        //Override_NetQual__c
        //Valid_Address__c
        
        Tenant_Account__c current_TA=[select id,Invoke_NetQual__c,Override_NetQual__c,Valid_Address__c, Address_Validation_Message__c from Tenant_Account__c where id=:current_TA_Id];
        
        boolean invokeNetQual=current_TA.Invoke_NetQual__c;
        boolean overrideNetQual=current_TA.Override_NetQual__c;
        boolean validAddress=current_TA.Valid_Address__c;
        System.debug('invokeNetQual='+invokeNetQual+', overrideNetQual='+overrideNetQual+', validAddress='+validAddress);
        
        if(overrideNetQual==false && validAddress==false && invokeNetQual==true)
        {
            callNetQual=true;
        }
        else
        {
            callNetQual=false;
        }
        
        String validation_msg = current_TA.Address_Validation_Message__c;
        if (validation_msg != null && validation_msg != ''){
            validation_msg_exist = true;
        }else{
            validation_msg_exist = false;
        }
        
    }
    
    public PageReference invokeNetQual()
    {
        if(callNetQual)
        {
            ValidateAdressCtrl.validateTenantAccount(current_TA_Id);
        }
        return null;
    }
    
    public PageReference removeValidationMsg()
    {
        System.debug('Inside "removeValidationMsg" function');
        System.debug('validation_msg_exist : ' + validation_msg_exist + ' && callNetQual : ' + callNetQual);
        if(validation_msg_exist == true && callNetQual == false)
        {
            Tenant_Account__c current_TA=[Select Id, Address_Validation_Message__c from Tenant_Account__c where Id=:current_TA_Id];
            current_TA.Address_Validation_Message__c = null;
            try{
                update current_TA;
            }catch(Exception e){
                System.debug('Exception message is : ' + e.getMessage());
            }
        }
        return null;
    }
    
}