@isTest
private class AmDocs_CalloutClass_UpdateTVNone_Test{
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.Amdocs_CustomerID__c = '123';
            acct1.Amdocs_BarID__c = '456';
            acct1.Amdocs_FAID__c = '789';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
//            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Incremental';
            opp1.AmDocs_ServiceID__c='012';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.Amdocs_FAID__c = '789';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
        insert opp1;

Opportunity opp2 = New Opportunity();
            opp2.Name = 'Test Opp2 123456789 Testing opp1';
            opp2.First_Name_of_Property_Representative__c = 'Jerry';
            opp2.Name_of_Property_Representative__c = 'Clifft';
            opp2.Business_Address__c = '1011 Collie Path';
            opp2.Business_City__c = 'Round Rock';
            opp2.Business_State__c = 'TX';
            opp2.Business_Zip_Code__c = '78664';
            opp2.Billing_Contact_Phone__c = '512-383-5201';
            opp2.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp2.CloseDate=system.Today();
            opp2.StageName='Closed Won';
            opp2.Smartbox_Leased__c = false;
//            opp1.AmDocs_ServiceID__c = '1111111111';
            opp2.AmDocs_SiteID__c = '22222';
            opp2.AccountId = acct1.Id;
            opp2.AmDocs_tenantType__c = 'Individual';
            opp2.AmDocs_ServiceID__c='012';
            opp2.Amdocs_CustomerID__c = '123';
            opp2.Amdocs_BarID__c = '456';
            opp2.Amdocs_FAID__c = '789';
            opp2.System_Type__c='QAM';
            opp2.Category__c='FTG';
            opp2.Address__c='address';
            opp2.City__c='Austin';
            opp2.State__c='TX';
            opp2.Zip__c='78664';
            opp2.Phone__c='5122965581';
            opp2.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp2.Number_of_Units__c=100;
            opp2.Smartbox_Leased__c=false;
            opp2.RecordTypeId='012600000005ChE';
        insert opp2;

        Tenant_Account__c ta = new Tenant_Account__c();
            ta.Address__c ='1011 Coliie Path';
            ta.City__c = 'Round Rock';
            ta.State__c = 'TX';
            ta.Zip__c = '78664';
            ta.Unit__c = '1A';
            ta.Phone_Number__c = '5123835201';
            ta.Email_Address__c = 'jerry.clifft1111111111111@dish.com';
            ta.First_Name__c = 'Jerry';
            ta.Last_Name__c = 'Clifft';
            ta.Type__c = 'Individual';
            ta.Opportunity__c = opp2.id;
        insert ta;

        Tenant_Account__c ta5 = new Tenant_Account__c();
            ta5.Address__c ='1011 Coliie Path';
            ta5.City__c = 'Round Rock';
            ta5.State__c = 'TX';
            ta5.Zip__c = '78664';
            ta5.Unit__c = '101';
            ta5.Phone_Number__c = '5123835205';
            ta5.Email_Address__c = 'jerry.clifft5555@dish.com';
            ta5.First_Name__c = 'Jerry';
            ta5.Last_Name__c = 'Clifft';
            ta5.Type__c = 'Individual';
            ta5.Opportunity__c = opp2.id;
//            ta5.AmDocs_ServiceID__c = '123';
        insert ta5;
        
        Tenant_Account__c ta6 = new Tenant_Account__c();
            ta6.Address__c ='1011 Coliie Path';
            ta6.City__c = 'Round Rock';
            ta6.State__c = 'TX';
            ta6.Zip__c = '78664';
            ta6.Unit__c = '101';
            ta6.Phone_Number__c = '5123835205';
            ta6.Email_Address__c = 'jerry.clifft5556@dish.com';
            ta6.First_Name__c = 'Jerry';
            ta6.Last_Name__c = 'Clifft';
            ta6.Type__c = 'Individual';
            ta6.Opportunity__c = opp2.id;
//            ta6.AmDocs_ServiceID__c = '123';
            ta6.PPV__c=TRUE;
        insert ta6;

        Tenant_Account__c ta7 = new Tenant_Account__c();
            ta7.Address__c ='1011 Coliie Path';
            ta7.City__c = 'Round Rock';
            ta7.State__c = 'TX';
            ta7.Zip__c = '78664';
            ta7.Unit__c = '101';
            ta7.Phone_Number__c = '5123835205';
            ta7.Email_Address__c = 'jerry.clifft5557@dish.com';
            ta7.First_Name__c = 'Jerry';
            ta7.Last_Name__c = 'Clifft';
            ta7.Type__c = 'Individual';
            ta7.Opportunity__c = opp2.id;
//            ta7.AmDocs_ServiceID__c = '123';
        insert ta7;
        
        Tenant_Account__c ta11 = new Tenant_Account__c();
            ta11.Address__c ='1011 Coliie Path';
            ta11.City__c = 'Round Rock';
            ta11.State__c = 'TX';
            ta11.Zip__c = '78664';
            ta11.Unit__c = '1A';
            ta11.Phone_Number__c = '5123835201';
            ta11.Email_Address__c = 'jerry.clifft@dish.com';
            ta11.First_Name__c = 'Jerry';
            ta11.Last_Name__c = 'Clifft';
            ta11.Type__c = 'Incremental';
            ta11.Opportunity__c = opp1.id;
            ta11.AmDocs_ServiceID__c = '012';
        insert ta11;

        
        Tenant_Account__c ta2 = new Tenant_Account__c();
            ta2.Address__c ='1011 Coliie Path';
            ta2.City__c = 'Round Rock';
            ta2.State__c = 'TX';
            ta2.Zip__c = '78664';
            ta2.Unit__c = '1A';
            ta2.Phone_Number__c = '5123835201';
            ta2.Email_Address__c = 'jerry.clifft2222222222222@dish.com';
            ta2.First_Name__c = 'Jerry';
            ta2.Last_Name__c = 'Clifft';
            ta2.Type__c = 'Incremental';
            ta2.Opportunity__c = opp2.id;
        insert ta2;

        Tenant_Account__c ta3 = new Tenant_Account__c();
            ta3.Address__c ='1011 Coliie Path';
            ta3.City__c = 'Round Rock';
            ta3.State__c = 'TX';
            ta3.Zip__c = '78664';
            ta3.Unit__c = '1A';
            ta3.Phone_Number__c = '5123835201';
            ta3.Email_Address__c = 'jerry.clifftABC@dish.com';
            ta3.First_Name__c = 'Jerry';
            ta3.Last_Name__c = 'Clifft';
            ta3.Type__c = 'Individual';
            ta3.Opportunity__c = opp2.id;
            ta3.PPV__C = TRUE;
            ta3.AmDocs_ServiceID__c = '012';
        insert ta3;
        
    }

  static testMethod void Acct1(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft1111111111111@dish.com' Limit 1];
    Tenant_Account__c opp2 = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft2222222222222@dish.com' Limit 1];
    Tenant_Account__c opp3 = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft5557@dish.com' Limit 1];
    Tenant_Account__c opp4 = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft5556@dish.com' Limit 1];
    Tenant_Account__c opp5 = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft5555@dish.com' Limit 1];
    Tenant_Account__c opp6 = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft@dish.com' Limit 1];
    Tenant_Account__c opp7 = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifftABC@dish.com' Limit 1];

        string id = opp.id;
        string id2 = opp2.id;
        string id3 = opp3.id;
        string id4 = opp4.id;
        string id5 = opp5.id;
        string id6 = opp6.id;
        string id7 = opp7.id;
                
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
              AmDocs_CalloutClass_UpdateUnitNone.AmDocsMakeCalloutUpdateUnit(ID6);
        Test.stopTest(); 
    }

  static testMethod void Acct2(){
    Tenant_Account__c opp6 = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft@dish.com' Limit 1];

        string id6 = opp6.id;
                    
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
          AmDocs_CalloutClass_UpdateUnitNone.AmDocsMakeCalloutUpdateUnit(ID6);
      Test.stopTest(); 
    }

   static testMethod void Acct3(){
    Tenant_Account__c opp6 = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft@dish.com' Limit 1];

        string id6 = opp6.id;
                    
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
          AmDocs_CalloutClass_UpdateUnitNone.AmDocsMakeCalloutUpdateUnit(ID6);
      Test.stopTest(); 
    }
    
    static testMethod void Acct4(){
    Tenant_Account__c opp7 = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifftABC@dish.com' Limit 1];

        string id7 = opp7.id;
                
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
              AmDocs_CalloutClass_UpdateUnit.AmDocsMakeCalloutUpdateUnit(ID7);
        Test.stopTest(); 
    }
    
    static testMethod void Acct5(){
    Tenant_Account__c opp = [Select Id FROM Tenant_Account__c WHERE Email_Address__c = 'jerry.clifft1111111111111@dish.com' Limit 1];

        string id = opp.id;
                
        Test.startTest(); 
          SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"ADD","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
      Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_UpdateUnitNone.AmDocsMakeCalloutUpdateUnit(ID);
      Test.stopTest(); 
    }
}