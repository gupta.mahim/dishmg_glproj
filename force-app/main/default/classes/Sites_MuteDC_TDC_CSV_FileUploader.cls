public class Sites_MuteDC_TDC_CSV_FileUploader 
{
    ApexPages.StandardController stdCtrl; public Sites_MuteDC_TDC_CSV_FileUploader (ApexPages.StandardController std) {stdCtrl=std;} public PageReference quicksave() {update accstoupload ;return null;} public PageReference fileAccess() {return null;}

public Sites_MuteDC_TDC_CSV_FileUploader(){

}

    String[] filelines = new String[]{}; public string nameFile{get;set;}    public Blob contentFile{get;set;}   List<Tenant_Disconnects__c> accstoupload;    public Pagereference ReadFile()    {         nameFile=contentFile.toString();         filelines = nameFile.split('\n');         accstoupload = new List<Tenant_Disconnects__c>();         for (Integer i=1;i<filelines.size();i++) {             String[] inputvalues = new String[]{};            inputvalues = filelines[i].split(',');            Tenant_Disconnects__c a = new Tenant_Disconnects__c();            a.Account_Number__c = inputvalues[0];             a.Account__c = ApexPages.currentPage().getParameters().get('acctId');            accstoupload.add(a);                 }         try{              insert accstoupload;        }         catch (Exception e)        {             ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template');            ApexPages.addMessage(errormsg);        }              return null ;} public List<Tenant_Disconnects__c> getuploadedMutes() { if (accstoupload!= NULL) if (accstoupload.size() > 0) return accstoupload; else return null; else return null;}}