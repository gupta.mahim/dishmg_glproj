@isTest(SeeAllData=true)
Public class multiAccountInsertTests{
    public static testMethod void testmultiAccountInsert(){

PageReference pageRef = Page.case_request;
Test.setCurrentPageReference(pageRef);

 Opportunity newOpp = new Opportunity (name='XYZ Organization', StageName='Closed Won', CloseDate=Date.Today());
 insert newOpp;

 Equipment__c myEq = new Equipment__c (Name='Joe',
 Receiver_S__c='SJoe', Receiver_Model__c='311', Statis__c='Activation Requested', Programming__c='HBO', Opportunity__c= newOpp.id);
 insert myEq;

 Case mycas = new Case (Status='Form Submitted', Origin='PRM', Opportunity__c=newopp.id);
 insert mycas;

ApexPages.currentPage().getParameters().put('oppId', newopp.id);

ApexPages.currentPage().getParameters().put('Id', newopp.id);

multiAccountInsert controller = new multiAccountInsert();

controller.addrow();
controller.save();
}
}