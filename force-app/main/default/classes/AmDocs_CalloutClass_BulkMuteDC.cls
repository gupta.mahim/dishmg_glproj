public class AmDocs_CalloutClass_BulkMuteDC {

public class person {
    public String serviceID;
    public String orderID;
    public String responseStatus;
    public String status;
    public cls_informationMessages[] informationMessages;
}
class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}



    @future(callout=true)

    public static void AmDocsMakeCalloutBulkMuteDC(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

    list<Opportunity> O = [select id, Name, Mute_Disconnect__c, AmDocs_ServiceID__c, Partner_BULK_Mute_UnMute__c, Digital_Status__c, AmDocs_tenantType__c, Property_Status__c from Opportunity where Id = :id AND ( Mute_Disconnect__c = 'Disconnect Pending' OR Mute_Disconnect__c = 'Re-Connect Pending' OR Mute_Disconnect__c = 'Mute Pending' OR Mute_Disconnect__c = 'UnMute Pending' OR Mute_Disconnect__c = 'Non-Pay Disconnect Pending' OR Mute_Disconnect__c = 'Non-Pay Resume Pending' ) ];
        System.debug('RESULTS of the LIST lookup to the Opp object' +O);

    list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
        System.debug('RESULTS of the LIST lookup to the Amdocs Login object' +A);
        System.debug('DEBUG UXF_Token' +A[0].UXF_Token__c);
 
 if(O[0].AmDocs_ServiceID__c  == '' || O[0].AmDocs_ServiceID__c  == Null ){Opportunity sbc = new Opportunity(); {          sbc.id=id;          sbc.API_Status__c='Error';          sbc.Amdocs_Transaction_Code__c='Error';          sbc.Mute_Disconnect__c = 'Error';          sbc.Partner_BULK_Mute_UnMute__c = 'Error';          if( (O[0].AmDocs_ServiceID__c   == '' || O[0].AmDocs_ServiceID__c  == Null )) {              sbc.Amdocs_Transaction_Description__c='Mute - UnMute requires a Service ID. You are missing: Service ID.';          }      update sbc;
      }
      API_Log__c apil2 = new API_Log__c();{          apil2.Record__c=id;          apil2.Object__c='Opportunity';          apil2.Status__c='ERROR';          apil2.Results__c='Missing Service ID';          apil2.API__c='Bulk Mute/DC';          apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;          apil2.User__c=UserInfo.getUsername();      insert apil2;
  }
 
  
    }
    if(O[0].AmDocs_ServiceID__c  != '' && O[0].AmDocs_ServiceID__c  != Null ){
        Datetime dt1 = System.Now();
        list<API_LOG__c> timer = [select Id, ServiceID__c, CreatedDate, NextAvailableActivateUpdateTime__c from API_LOG__c where ServiceID__c = :O[0].Amdocs_ServiceID__c AND NextAvailableActivateUpdateTime__c >= :dt1 Order By NextAvailableActivateUpdateTime__c  DESC Limit 1];
        
        if( (timer.size() > 0 )) {
            system.debug('DEBUG API LOG Timer size : ' +timer.size());
            system.debug('DEBUG API LOG Info' +timer);
            system.debug('DEBUG timer 1 : ' +dt1);
     
            Opportunity sbc = new Opportunity(); { 
                sbc.id=id; 
                sbc.API_Status__c='Error'; 
                sbc.Amdocs_Transaction_Code__c='Error';
                sbc.Mute_Disconnect__c = 'Error';
                sbc.Partner_BULK_Mute_UnMute__c = 'Error';
                sbc.Amdocs_Transaction_Description__c='You must wait atleast (2) two minutes between transactions'; 
            update sbc;
            }
            
            API_Log__c apil2 = new API_Log__c();{ 
                apil2.Record__c=id; 
                apil2.Object__c='Opportunity';
                apil2.Status__c='ERROR'; 
                apil2.Results__c='You must wait atleast (2) two minutes between transactions'; 
                apil2.API__c='Bulk Mute/DC';
                apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
                apil2.User__c=UserInfo.getUsername();
            insert apil2; 
            }
        }
   
        else if(timer.size() < 1) {
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
    jsonObj.writeFieldName('ImplUpdateProductStatusInput');
//    jsonObj.writeStartArray();
        jsonObj.writeStartObject();
      
        for(Integer i = 0; i < O.size(); i++){
            // Cease
            if( O[i].Mute_Disconnect__c == 'Non-Pay Disconnect Pending' || O[i].Partner_BULK_Mute_UnMute__c == 'Non-Pay Disconnect Pending') { jsonObj.writeStringField('type', 'CE'); }
            // Reestablish
            if( O[i].Mute_Disconnect__c == 'Non-Pay Resume Pending' || O[i].Partner_BULK_Mute_UnMute__c =='Non-Pay Resume Pending') { jsonObj.writeStringField('type', 'ES'); }
            // Regular Disconnect
            if( O[i].Mute_Disconnect__c == 'Disconnect Pending' || O[i].Partner_BULK_Mute_UnMute__c == 'Disconnect Pending') { jsonObj.writeStringField('type', 'CE'); }
            // Re-Connect
            if( O[i].Mute_Disconnect__c == 'Re-Connect Pending' || O[i].Partner_BULK_Mute_UnMute__c == 'Re-Connect Pending') { jsonObj.writeStringField('type', 'ES');  }
            // Suspend
            if( O[i].Mute_Disconnect__c == 'Mute Pending' || O[i].Partner_BULK_Mute_UnMute__c == 'Mute Pending') { jsonObj.writeStringField('type', 'SU'); }
            // Resume
            if( O[i].Mute_Disconnect__c == 'UnMute Pending' || O[i].Partner_BULK_Mute_UnMute__c == 'UnMute Pending') { jsonObj.writeStringField('type', 'RS'); }
            jsonObj.writeStringField('reasonCode', 'CREQ');
         }   
        jsonObj.writeEndObject();
//    jsonObj.writeEndArray();
    jsonObj.writeEndObject();
    
String finalJSON = jsonObj.getAsString();
    System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
        
   HttpRequest request = new HttpRequest();
       String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/updateProductStatus?sc=SS&lo=EN&ca=SF';
       request.setEndPoint(endpoint);
       request.setBody(jsonObj.getAsString());
       request.setHeader('Content-Type', 'application/json');
       request.setMethod('POST');
       String authorizationHeader = A[0].UXF_Token__c;
       request.setHeader('Authorization', authorizationHeader);
       request.setTimeout(101000);
       request.setHeader('Accept', 'application/json'); 
       request.setHeader('User-Agent', 'SFDC-Callout/45.0');
       
   HttpResponse response = new HTTP().send(request); 
       if (response.getStatusCode() >= 200 && response.getStatusCode() <= 299) { 
           String strjson = response.getbody(); 
           JSONParser parser = JSON.createParser(strjson);
               parser.nextToken();
               parser.nextToken();
               parser.nextToken();
           person obj = (person)parser.readValueAs( person.class);
               System.debug('DEBUG Authorization Header :' +A[0].UXF_Token__c);
               System.debug(response.toString());
               System.debug('STATUS_CODE:'+response.getStatusCode());
               System.debug(response.getBody());
               System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
               System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID);
               System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
               System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages);
               System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
               System.debug('DEBUG 7 ======== obj.Status: ' + obj.status);

    Opportunity sbc = new Opportunity(); 
        sbc.id=id; 
        sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
        sbc.AmDocs_Order_ID__c=obj.orderID; 
        sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
    //    sbc.Mute_Disconnect__c = O[0].Mute_Disconnect__c+ ' ' + obj.responseStatus;
        if( O[0].Mute_Disconnect__c == 'Non-Pay Disconnect Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Non-Pay Disconnect Pending') 
            {
                sbc.Mute_Disconnect__c = 'Disconnected';
                sbc.Partner_BULK_Mute_UnMute__c = 'Disconnected';
                sbc.Property_Status__c = 'Disconnected';
                if(O[0].AmDocs_tenantType__c == 'Incremental') 
                    {
                        O[0].Digital_Status__c = 'Disconnected';
                    }
            }
        if( O[0].Mute_Disconnect__c == 'Non-Pay Resume Pending' || O[0].Partner_BULK_Mute_UnMute__c =='Non-Pay Resume Pending')  { sbc.Mute_Disconnect__c = 'Non-Pay Resumed';sbc.Partner_BULK_Mute_UnMute__c = 'Non-Pay Resumed';sbc.Property_Status__c = 'Active'; if(O[0].AmDocs_tenantType__c == 'Incremental') { O[0].Digital_Status__c = 'Active'; }}
        if( O[0].Mute_Disconnect__c == 'Disconnect Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Disconnect Pending') { sbc.Mute_Disconnect__c = 'Disconnected';sbc.Partner_BULK_Mute_UnMute__c = 'Disconnected';sbc.Property_Status__c = 'Disconnected';if(O[0].AmDocs_tenantType__c == 'Incremental') { O[0].Digital_Status__c = 'Disconnected'; }}
        if( O[0].Mute_Disconnect__c == 'Re-Connect Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Re-Connect Pending') {sbc.Mute_Disconnect__c = 'Re-Connected';sbc.Partner_BULK_Mute_UnMute__c = 'Re-Connected';sbc.Property_Status__c = 'Active';if(O[0].AmDocs_tenantType__c == 'Incremental') {O[0].Digital_Status__c = 'Active';}}
        if( O[0].Mute_Disconnect__c == 'Mute Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Mute Pending') {sbc.Mute_Disconnect__c = 'Muted';sbc.Partner_BULK_Mute_UnMute__c = 'Muted';sbc.Property_Status__c = 'Muted';if(O[0].AmDocs_tenantType__c == 'Incremental') {O[0].Digital_Status__c = 'Muted';}}
        if( O[0].Mute_Disconnect__c == 'UnMute Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'UnMute Pending') { sbc.Mute_Disconnect__c = 'UnMuted';sbc.Partner_BULK_Mute_UnMute__c = 'UnMuted';sbc.Property_Status__c = 'Active';if(O[0].AmDocs_tenantType__c == 'Incremental') {O[0].Digital_Status__c = 'Active';}}
        if(obj.orderID != null) { sbc.AmDocs_Transaction_Description__c=O[0].Mute_Disconnect__c + obj.responseStatus; sbc.AmDocs_Transaction_Code__c='SUCCESS';  }
//        if(obj.orderID == null) { 
//            sbc.AmDocs_Transaction_Description__c=O[0].Mute_Disconnect__c + ' ERROR something went wrong, please contact your Salesforce Admin';
//            sbc.AmDocs_Transaction_Code__c='ERROR'; 
//        }  
//        if(obj.informationMessages != Null) { 
//            sbc.AmDocs_Transaction_Description__c=obj.informationMessages[0].errorDescription; 
//            sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; sbc.Mute_Disconnect__c = O[0].Mute_Disconnect__c+ ' ' + 'Failed'; 
//        }
        update sbc; 
            API_Log__c apil2 = new API_Log__c(); { 
        apil2.Record__c=id;       
        apil2.Object__c='Opportunity';       
        apil2.Status__c='SUCCESS';       
        apil2.Results__c= response.getbody();
        apil2.API__c='Bulk Mute/DC';
        apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;       
        apil2.User__c=UserInfo.getUsername();   
        insert apil2;  
       } 
    }
    else if (response.getStatusCode() >= 300 && response.getStatusCode() <= 499) { 
           String strjson = response.getbody(); 
           JSONParser parser = JSON.createParser(strjson);
               parser.nextToken();
               parser.nextToken();
               parser.nextToken();
           person obj = (person)parser.readValueAs( person.class);
               System.debug('DEBUG Authorization Header :' +A[0].UXF_Token__c);
               System.debug(response.toString());
               System.debug('STATUS_CODE:'+response.getStatusCode());
               System.debug(response.getBody());
               System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
               System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID);
               System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
               System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages);
               System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
               System.debug('DEBUG 7 ======== obj.Status: ' + obj.status);

    Opportunity sbc = new Opportunity(); 
        sbc.id=id; 
        sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
        sbc.AmDocs_Order_ID__c=obj.orderID; 
        sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
        if( O[0].Mute_Disconnect__c == 'Non-Pay Disconnect Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Non-Pay Disconnect Pending') 
            {
                sbc.Mute_Disconnect__c = 'Disconnect Error';
                sbc.Partner_BULK_Mute_UnMute__c = 'Disconnect Error';

            }
       	if( O[0].Mute_Disconnect__c == 'Non-Pay Resume Pending' || O[0].Partner_BULK_Mute_UnMute__c =='Non-Pay Resume Pending') {sbc.Mute_Disconnect__c = 'Non-Pay Resume Error';sbc.Partner_BULK_Mute_UnMute__c = 'Non-Pay Resume Error'; }
        if( O[0].Mute_Disconnect__c == 'Disconnect Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Disconnect Pending') {sbc.Mute_Disconnect__c = 'Disconnected';sbc.Partner_BULK_Mute_UnMute__c = 'Disconnected';}
        if( O[0].Mute_Disconnect__c == 'Re-Connect Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Re-Connect Pending') {sbc.Mute_Disconnect__c = 'Re-Connected';sbc.Partner_BULK_Mute_UnMute__c = 'Re-Connected';}
        if( O[0].Mute_Disconnect__c == 'Mute Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Mute Pending') {sbc.Mute_Disconnect__c = 'Muted';sbc.Partner_BULK_Mute_UnMute__c = 'Muted';}
        if( O[0].Mute_Disconnect__c == 'UnMute Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'UnMute Pending') {sbc.Mute_Disconnect__c = 'UnMuted';sbc.Partner_BULK_Mute_UnMute__c = 'UnMuted';}
        if(obj.orderID == null && obj.informationMessages == Null) { sbc.AmDocs_Transaction_Description__c=O[0].Mute_Disconnect__c + ' ERROR something went wrong, please contact your Salesforce Admin';sbc.AmDocs_Transaction_Code__c='ERROR'; }  
        if(obj.informationMessages != Null) { 
            sbc.AmDocs_Transaction_Description__c=obj.informationMessages[0].errorDescription; 
            sbc.AmDocs_Transaction_Code__c=obj.informationMessages[0].errorCode; sbc.Mute_Disconnect__c = O[0].Mute_Disconnect__c+ ' ' + 'Failed'; 
        }
        update sbc; 
            API_Log__c apil2 = new API_Log__c(); { 
        apil2.Record__c=id;       
        apil2.Object__c='Opportunity';       
        apil2.Status__c='ERROR';       
        apil2.Results__c= response.getbody();
        apil2.API__c='Bulk Mute/DC';
        apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;       
        apil2.User__c=UserInfo.getUsername();   
        insert apil2;  
       } 
    }
      else if (response.getStatusCode() >= 500) { 
          String strjson = response.getbody(); 
            System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
            
          Opportunity sbc = new Opportunity(); {
              sbc.id=id; 
              sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
              sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
      if( O[0].Mute_Disconnect__c == 'Non-Pay Disconnect Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Non-Pay Disconnect Pending') 
            {
                sbc.Mute_Disconnect__c = 'Disconnect Error';
                sbc.Partner_BULK_Mute_UnMute__c = 'Disconnect Error';

            }
        if( O[0].Mute_Disconnect__c == 'Non-Pay Resume Pending' || O[0].Partner_BULK_Mute_UnMute__c =='Non-Pay Resume Pending') {sbc.Mute_Disconnect__c = 'Non-Pay Resume Error';sbc.Partner_BULK_Mute_UnMute__c = 'Non-Pay Resume Error';}
        if( O[0].Mute_Disconnect__c == 'Disconnect Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Disconnect Pending') {sbc.Mute_Disconnect__c = 'Disconnected';sbc.Partner_BULK_Mute_UnMute__c = 'Disconnected';}
        if( O[0].Mute_Disconnect__c == 'Re-Connect Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Re-Connect Pending') {sbc.Mute_Disconnect__c = 'Re-Connected';sbc.Partner_BULK_Mute_UnMute__c = 'Re-Connected';}
        if( O[0].Mute_Disconnect__c == 'Mute Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'Mute Pending') {sbc.Mute_Disconnect__c = 'Muted';sbc.Partner_BULK_Mute_UnMute__c = 'Muted';}
       if( O[0].Mute_Disconnect__c == 'UnMute Pending' || O[0].Partner_BULK_Mute_UnMute__c == 'UnMute Pending') {sbc.Mute_Disconnect__c = 'UnMuted';sbc.Partner_BULK_Mute_UnMute__c = 'UnMuted';}
              sbc.AmDocs_Transaction_Description__c=O[0].Mute_Disconnect__c + ' ERROR something went wrong, please contact your Salesforce Admin';
            sbc.AmDocs_Transaction_Code__c='ERROR'; 
     }
     update sbc;

    API_Log__c apil2 = new API_Log__c(); { 
        apil2.Record__c=id;       
        apil2.Object__c='Opportunity';       
        apil2.Status__c='ERROR';       
        apil2.Results__c= response.getbody();
        apil2.API__c='Bulk Mute/DC';
        apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;       
        apil2.User__c=UserInfo.getUsername();   
        insert apil2;  
       } 
            
      }

    }
}}}