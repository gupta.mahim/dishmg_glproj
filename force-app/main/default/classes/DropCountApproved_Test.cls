@isTest(SeeAllData=true)
 private class DropCountApproved_Test{

    private static testmethod void testDropCountApproved(){
    Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter';
        a.phone = '(303) 555-5555';
        insert a;
        
            Opportunity o = new Opportunity();
                o.Name = 'Test';
                o.AccountId = a.Id;
                o.BTVNA_Request_Type__c = 'Site Survey';
                o.LeadSource='BTVNA Portal';
                o.Property_Type__c='Private';
                o.misc_code_siu__c='N999';
                o.CloseDate=system.Today();
                o.StageName='Closed Won';
                o.Drop_Count_Approved__c = false;
                 o.Restricted_Programming__c = 'True';
            insert o;

                    Front_Office_Request__c fo = new Front_Office_Request__c ();
                    fo.Opportunity__c = o.Id;
                    fo.Account__c = a.Id;
                    fo.Reported_Drop_Count__c = 150;
                    fo.Front_Office_Decision__c = 'Approved';
                    insert fo;
                    update fo;
                   
                    
 
            }
            }