@isTest
public class GetTenantEquipmentRecordsTest {
    @isTest  private static void getEquipmentRecords()
    {
        List<Tenant_Equipment__c> TElist= new list<Tenant_Equipment__c>();
        Opportunity Opp= testdataFactoryforInternal.createOpportunity();
       TElist=GetTenantEquipmentRecords.getTenantEquipmentRecords(opp.id);
    }
    
    @isTest private static void updateRecords()
    {
        Opportunity Opp= testdataFactoryforInternal.createOpportunity();
        Tenant_Equipment__c TE= testdataFactoryforInternal.createTenantEquipment(opp);
        Tenant_Equipment__c TE1= testdataFactoryforInternal.createTenantEquipment(opp);
		Tenant_Equipment__c TE2= testdataFactoryforInternal.createTenantEquipment(opp);
        
        string testMockjson='[{"Id":"'+TE.ID+'","Name":"RECR090","Address__c":"101 Col","City__c":"Round Rock","EquipmentType__c":"Tenant","State__c":"TX","Zip_Code__c":"78664","Opportunity__c":"'+Te.Opportunity__c+'","Override_NetQual__c":false,"Address_Validation_Message__c":"Address not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.","Account_Name__c":"Fazal Rehman","IsOverrideCaseCreated__c":false},{"Id":"'+TE1.ID+'","Name":"RECR092","Address__c":"101 Col","City__c":"Round Rock","EquipmentType__c":"Tenant","State__c":"TX","Zip_Code__c":"78664","Opportunity__c":"'+Te.Opportunity__c+'","Override_NetQual__c":false,"Address_Validation_Message__c":"Address not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.","Account_Name__c":"Fazal Rehman","IsOverrideCaseCreated__c":true},{"Id":"'+TE2.ID+'","Name":"RECR092","Address__c":"101 Col","City__c":"Round Rock","EquipmentType__c":"Tenant","State__c":"TX","Zip_Code__c":"78664","Opportunity__c":"'+Te.Opportunity__c+'","Override_NetQual__c":true,"Address_Validation_Message__c":"Address not found. Please check your address information and try again or you can also proceed by checking the Override Address checkbox.","Account_Name__c":"Fazal Rehman","IsOverrideCaseCreated__c":false}]';
            
        // String testjsonstr='[{"Id":"'+TE.ID+'","Name":"R0001234","Address__c":"Lahore","City__c":"karachi","State__c":"ID","Zip_Code__c":"10001","Opportunity__c":"'+Te.Opportunity__c+'","Override_NetQual__c":false}]';
        
          Test.startTest();   
          
           Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
           GetTenantEquipmentRecords.saveRecords(testMockjson);

          Test.stopTest();
    
    }
}