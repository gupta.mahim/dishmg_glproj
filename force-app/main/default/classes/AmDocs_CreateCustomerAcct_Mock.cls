@isTest
global class AmDocs_CreateCustomerAcct_Mock implements HttpCalloutMock{
   //Implement http mock callout here
   // Implement this interface method
   global HTTPResponse respond(HTTPRequest request){
       // Create a fake response
       HttpResponse response = new HttpResponse();
       response.setHeader('Content-Type', 'application/json');
       response.setStatus('OK');
       response.setStatusCode(200);
       response.setBody('{"ImplCreateCustomerOutput": {"customerID": "0123456789","barID": "0123456789","faID": "0123456789"}}');
              
       return response;
   }
}