public class Equipment_Case_Request {
  private List<Equipment__c> equip;
  private Case ca;
    public Equipment_Case_Request(ApexPages.StandardController controller) {
    this.ca= (Case)controller.getRecord();

    }

     
  public List<Equipment__c> getequip() {
       Case cas = [SELECT ID, Opportunity__r.id from Case where id = :ca.id];
       if (cas.Opportunity__c == null)       return null;
        
       
       equip = [SELECT ID, Name, CaseID__c, Channel__c, Remove_Equipment__c, Receiver_S__c, Receiver_Model__c, Programming__c, Statis__c 
       FROM Equipment__c WHERE Opportunity__c = :cas.Opportunity__r.id AND CaseID__c = :cas.id AND
        (Statis__c='Activation Requested' OR 
        Statis__c='Drop Requested' OR
        Statis__c='Programming Change Requested' OR
        Statis__c='Receiver Number Change Requested' OR
        Statis__c='Smart Card Number Change Requested' OR
        Statis__c='Model Number Change Requested'
            ) ORDER BY Statis__c];
        
        
             


        return equip;
        
    }

    
}