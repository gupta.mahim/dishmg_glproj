public class GetAssignedRecordTypes {
   @AuraEnabled
   public static List<AssignedRecordTypes> getDishContractRecordTypes() {
        List<AssignedRecordTypes> recordTypes = new List<AssignedRecordTypes>();
        
        for(RecordTypeInfo info: DISH_Contract__c.SObjectType.getDescribe().getRecordTypeInfos()) {if(info.isAvailable() && info.getName()!='Master') {recordTypes.add(new AssignedRecordTypes(info.getRecordTypeId(),info.getName()));}}
        
        return recordTypes;
   }
    @AuraEnabled
   public static List<AssignedRecordTypes> getCaseRecordTypes() {
        List<AssignedRecordTypes> recordTypes = new List<AssignedRecordTypes>();
        
        for(RecordTypeInfo info: Case.SObjectType.getDescribe().getRecordTypeInfos()) {if(info.isAvailable() && info.getName()!='Master') {recordTypes.add(new AssignedRecordTypes(info.getRecordTypeId(),info.getName()));}}
        
        return recordTypes;
   }
    @AuraEnabled
   public static List<AssignedRecordTypes> getContactRecordTypes() {
        List<AssignedRecordTypes> recordTypes = new List<AssignedRecordTypes>();
        
        for(RecordTypeInfo info: Contact.SObjectType.getDescribe().getRecordTypeInfos()) {if(info.isAvailable() && info.getName()!='Master') {recordTypes.add(new AssignedRecordTypes(info.getRecordTypeId(),info.getName()));}}
        
        return recordTypes;
   }
    //added method for BULK opportunity recordtype
    @AuraEnabled
   public static List<AssignedRecordTypes> getOpportunityRecordTypes()
   {
        List<AssignedRecordTypes> recordTypes = new List<AssignedRecordTypes>();
        list <RecordType> RecordTypeid=new list<RecordType>();
        Recordtypeid=[Select id from RecordType where Name='Retailer Bulk' OR Name='PCO' OR Name='Integrator Bulk'];
        Set<id> Recordtypeidset=new set<id>();
        for(RecordType rt:Recordtypeid)
        {
            Recordtypeidset.add(rt.id);
        }
        for(RecordTypeInfo info: Opportunity.SObjectType.getDescribe().getRecordTypeInfos())
        {
          if(info.isAvailable() && info.getName()!='Master'&&RecordTypeidset.contains(info.getRecordTypeid()))
          {
              AssignedRecordTypes Recrdtype = new AssignedRecordTypes(info.getRecordTypeId(),info.getName());
              recordTypes.add(Recrdtype);
              
              // recordTypes.add(new AssignedRecordTypes(info.getRecordTypeId(),info.getName()));
          }
        }
       system.debug('recortypeare'+recordtypes);
           return recordTypes;
   }
    public class AssignedRecordTypes
    {
        @AuraEnabled 
        public string value{get;set;}
        
        @AuraEnabled 
        public string label{get;set;}
        public AssignedRecordTypes(String strId,String strName)
        {
            value=strId;
            label=strName;
           
        }
    }
}