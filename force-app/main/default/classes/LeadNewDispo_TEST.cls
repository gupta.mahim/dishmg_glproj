@IsTest
private class LeadNewDispo_TEST {
    static testMethod void validateLeadNewDispo_TEST() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        
        insert L;
        
       L = [SELECT Status FROM Lead WHERE Id =:L.Id];
       System.debug('Status after trigger fired: ' + L.Status);


       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Open', L.Status);
    }

   static testMethod void validateLeadNewDispo_TEST2() {
        Lead L2 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Lead_Sold__c=True,
        Lead_Lost__c=False,
        CSG_Account_Number__c='8255707012345678',     
        Number_of_TVs__c='1 - 2');

        
        insert L2;
        
       L2 = [SELECT Status FROM Lead WHERE Id =:L2.Id];
       System.debug('Status after trigger fired: ' + L2.Status);


       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Closed', L2.Status);
    }
    
    
   static testMethod void validateLeadNewDispo_TEST3() {
        Lead L3 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Lead_Lost__c=True,
        Lost_Reason__c='Due to the needs to this test',
        Number_of_TVs__c='1 - 2');

        
        insert L3;
        
       L3 = [SELECT Status FROM Lead WHERE Id =:L3.Id];
       System.debug('Status after trigger fired: ' + L3.Status);


       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Closed', L3.Status);
    }
       static testMethod void validateLeadNewDispo_TEST4() {
        Lead L4 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Sales_Stage__c='Customer Contacted',
        Number_of_TVs__c='1 - 2');

        
        insert L4;
        
       L4 = [SELECT Status FROM Lead WHERE Id =:L4.Id];
       System.debug('Status after trigger fired: ' + L4.Status);


       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Open', L4.Status);
    }
       static testMethod void validateLeadNewDispo_TEST5() {
        Lead L5 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Lead_Lost__c=True,
        Status='Closed',
        Sales_Stage__c='Returned to Dish - Invalid Lead',
        Lost_Reason__c='Returned to Dish - Invalid Lead',
        Number_of_TVs__c='1 - 2');

        
        insert L5;
        
       L5 = [SELECT Type_of_Business__c FROM Lead WHERE Id =:L5.Id];
       System.debug('Sales Stage after trigger fired: ' + L5.Type_of_Business__c);


       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Firehouse/Oil Rig/EMT', L5.Type_of_Business__c);
    }
    static testMethod void validateLeadNewDispo_TEST6() {
        Lead L6 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Lead_Lost__c=True,
        Status='Closed',
        Sales_Stage__c='Returned to Dish - Duplicate',
        Lost_Reason__c='Returned to Dish - Duplicate',
        Number_of_TVs__c='1 - 2');

        
        insert L6;
        
       L6 = [SELECT Type_of_Business__c FROM Lead WHERE Id =:L6.Id];
       System.debug('Sales Stage after trigger fired: ' + L6.Type_of_Business__c);


       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Firehouse/Oil Rig/EMT', L6.Type_of_Business__c);
    }
  static testMethod void validateLeadNewDispo_TEST7() {
        Lead L7 = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Firehouse/Oil Rig/EMT',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Sales_Stage__c='Lead Accepted',
        Number_of_TVs__c='1 - 2');

        
        insert L7;
        
       L7 = [SELECT Status FROM Lead WHERE Id =:L7.Id];
       System.debug('Status after trigger fired: ' + L7.Status);


       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Open', L7.Status);
   }
  }