public class CMaster_CalloutClassForgotUsernameOPP {

public class person {
    Public cls_Search [] Search;
}

class cls_Search {
  Public cls_logins [] logins;
  Public cls_securityInfo [] securityinfo;
}
class cls_logins {
    public String userName;
    public String loginId;
}
class cls_securityinfo {
    public String securityPin;
    public String securityPinHint;
}
    
    @future(callout=true)
    
    public static void CMasterCalloutForgotUserNameOPP(String Id) {

 list<Opportunity> C = [select Id,   Amdocs_CustomerID__c, First_Name_of_Property_Representative__c, Name_of_Property_Representative__c, Billing_Contact_Email__c, Billing_Contact_Phone__c, AmDocs_ServiceID__c, PIN__c, dishCustomerId__c, partyId__c from Opportunity where Id =:id Limit 1];    
        if( C.size() < 1 || ((C[0].First_Name_of_Property_Representative__c == '' || C[0].First_Name_of_Property_Representative__c == Null) || (C[0].Name_of_Property_Representative__c == '' || C[0].Name_of_Property_Representative__c == Null )) || (C[0].Billing_Contact_Email__c == '' || C[0].Billing_Contact_Email__c == Null) || ( C[0].Amdocs_CustomerID__c == '' || C[0].Amdocs_CustomerID__c == Null)) {
            Opportunity sbc = new Opportunity();
                sbc.id=id;
                sbc.API_Status__c='';
                sbc.Amdocs_Transaction_Code__c='';
//                sbc.Amdocs_Transaction_Description__c='CMaster -Forgot Username-, CM needs requires the Customer ID (Customer ID) Customer ID = ' +C[0].AmDocs_ServiceID__c;

        if( (C[0].First_Name_of_Property_Representative__c == '' || C[0].First_Name_of_Property_Representative__c == Null )) {
            sbc.Amdocs_Transaction_Description__c='Search for a username requires the Customer ID, First & Last Name of the Billing Contact, and the Billing Contact Email Address. Your data is missing the First Name.';
        }
        if( (C[0].Name_of_Property_Representative__c == '' || C[0].Name_of_Property_Representative__c == Null )){
            sbc.Amdocs_Transaction_Description__c='Search for a username requires the Customer ID, First & Last Name of the Billing Contact, and the Billing Contact Email Address. Your data is missing the Last Name.';
        }
        if( (C[0].Billing_Contact_Email__c == '' || C[0].Billing_Contact_Email__c == Null )) {
            sbc.Amdocs_Transaction_Description__c='Search for a username requires the Customer ID, First & Last Name of the Billing Contact, and the Billing Contact Email Address. Your data is missing the Email Address.';
        }
//        if( (C[0].PIN__c == '' || C[0].PIN__c == Null )) {
//            sbc.Amdocs_Transaction_Description__c='Search for a username requires the Subscriber ID, First & Last Name of the Billing Contact, the Billing Contact Email Address, and the Customer PIN. Your data is missing the Customer PIN.';
//        }
        if( (C[0].Amdocs_CustomerID__c == '' || C[0].Amdocs_CustomerID__c == Null )) {
            sbc.Amdocs_Transaction_Description__c='Search for a username requires the Customer ID, First & Last Name of the Billing Contact, and the Billing Contact Email Address PIN. Your data is missing the Subscriber ID.';
        }

            update sbc;
        }
        else if( C.size() > 0) {
            System.debug('RESULTS of the LIST lookup to the Contact object' +C);

            list<CustomerMaster__c> CME = [select Id, EndPoint__c, CreatedDate from CustomerMaster__c ORDER BY CreatedDate DESC LIMIT 1];
                if( CME.size() < 1 || (CME[0].EndPoint__c == '')) {
                    Opportunity sbc = new Opportunity();
                        sbc.id=id;
                        sbc.API_Status__c='';
                        sbc.Amdocs_Transaction_Code__c='';
                        sbc.Amdocs_Transaction_Description__c='ERROR: CMaster_CalloutClassForgotUserNameOPP is missing a valid ENDPOINT';
                    update sbc;
            }
            else if( CME.size() > 0) {
                System.debug('DEBUG RESULTS of the LIST lookup to the CustomerMaster object' +CME);            

        HttpRequest request = new HttpRequest();
            String endpoint = CME[0].Endpoint__c;
            request.setEndPoint(endpoint +'/cm-search/customerParties?extRefNum='+C[0].Amdocs_CustomerID__c);
            request.setHeader('Content-Type', 'application/json');
            request.setHeader('Customer-Facing-Tool', 'Salesforce');
            request.setHeader('User-ID', UserInfo.getUsername());
            request.setMethod('GET');
            request.setTimeout(101000);
     
            HttpResponse response = new HTTP().send(request);
                System.debug(response.toString());
                System.debug('STATUS:'+response.getStatus());
                System.debug('STATUS_CODE:'+response.getStatusCode());


                String strjson1 = response.getbody();
                String strjson = '{ "Search": '+strjson1 +'}';
//                String strjson = response.getbody();

                System.debug('DEBUG Get body:  ' +response.getBody());
                
           if(response.getStatusCode() >= 200 && response.getStatusCode() <= 299){
               System.debug('DEBUG 0 ======== strjson: ' + strjson);

        
           JSONParser parser = JSON.createParser(strjson);
           parser.nextToken();
           person obj = (person)parser.readValueAs( person.class); 

               System.debug('DEBUG 1 ======== Search: ' +obj.Search);
               System.debug('DEBUG 1 ======== logins: ' +obj.Search[0].logins);
               System.debug('DEBUG 1 ======== logins.loginId: ' +obj.Search[0].logins[0].loginId);

 
                Opportunity sbc = new Opportunity(); {
                   sbc.id=id;
                   sbc.API_Status__c='SUCCESS';
                   sbc.Amdocs_Transaction_Description__c='Search for a lost / forgotten Username was successful!';
                   sbc.Username__c=obj.Search[0].logins[0].userName;
                   sbc.LoginId__c=obj.Search[0].logins[0].loginId;
                   sbc.PinSec__c=obj.Search[0].securityinfo[0].securityPin;
                   sbc.PinSecHint__c=obj.Search[0].securityinfo[0].securityPinHint;
               update sbc;
               }
               API_Log__c apil = new API_Log__c();{
                   apil.Record__c=id;
                   apil.Object__c='Opportunity';
                   apil.Status__c='SUCCESS';
                   apil.Results__c=response.getBody();
                   apil.API__c='Forgot OPP Customer Username';
                   apil.User__c=UserInfo.getUsername();
               insert apil;
               }
           }

           if (response.getStatusCode() >= 300 && response.getStatusCode() <= 600){
               System.debug('DEBUG Get Respnose to string: ' +response.toString());                          
               System.debug('DEBUG 0 ======== strjson: ' + strjson);
               System.debug('DEBUG Get body:  ' +response.getBody());
               Opportunity sbc2 = new Opportunity(); {
                   sbc2.id=id;
                   sbc2.API_Status__c='ERROR';
                   sbc2.Amdocs_Transaction_Description__c='Search for Customer Username failed, the system says: ' +response.getBody();
               update sbc2;
               }
               API_Log__c apil2 = new API_Log__c();{
                   apil2.Record__c=id;
                   apil2.Object__c='Opportunity';
                   apil2.Status__c='ERROR';
                   apil2.Results__c=response.getBody();
                   apil2.API__c='Forgot Opp Customer Username';
                   apil2.User__c=UserInfo.getUsername();
               insert apil2;
               }
            }
        }
    }
}}