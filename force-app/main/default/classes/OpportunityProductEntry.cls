public class OpportunityProductEntry {
    
    static LIST<opportunityLineItem> forDeletion = new LIST<opportunityLineItem>() ;
    static opportunityLineItem[] shoppingCart;
    static boolean multipleCurrencies = UserInfo.isMultiCurrencyOrganization();
    static Opportunity theOpp ;
    static Boolean forcePricebookSelection = false;
    static Pricebook2 theBook;
    static priceBookEntry[] AvailableProducts;
    static Boolean overLimit;
    @AuraEnabled
    public static PricebookEntry[] updateAvailableList(ID oppId) {
        /////////////////////////////////////////////////////////////
        if(multipleCurrencies)            
            theOpp = database.query('select Id, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode from Opportunity where Id = \'' + oppId + '\' limit 1');
        else
            theOpp = [select Id, Pricebook2Id, PriceBook2.Name, weighted_average__c from Opportunity where Id = :oppId limit 1];
        
        // If products were previously selected need to put them in the "selected products" section to start with
        opportunityLineItem[] shoppingCart  = [select Id, Quantity, TotalPrice, UnitPrice, Description, PriceBookEntryId, PriceBookEntry.Name, PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, PriceBookEntry.Product2.Maintenance__c, PriceBookEntry.PriceBook2Id from opportunityLineItem where PriceBookEntry.Product2.Maintenance__c != true AND OpportunityId=:theOpp.Id];
        
        // Check if Opp has a pricebook associated yet
        if(theOpp.Pricebook2Id == null)
        {
            Pricebook2[] activepbs = [select Id, Name from Pricebook2 where Id = '01s60000000dMje'];
            if(activepbs.size() == 2){ forcePricebookSelection = true;    theBook = new Pricebook2();
                                     }
            else{
                theBook = activepbs[0];
            }
        }
        else{
            theBook = theOpp.Pricebook2;
        }
        
        if(!forcePricebookSelection) {
            //updateAvailableList();
            /////////////////////////////////////////////////////////////
            // We dynamically build a query string and exclude items already in the shopping cart
            String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, Product2.Grandfathered__c, UnitPrice from PricebookEntry where Grandfathered__c = False and IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
            //        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
            if(multipleCurrencies)  
                qString += ' and CurrencyIsoCode = \'' + theOpp.get('currencyIsoCode') + '\'';
            
            // note that we are looking for the search string entered by the user in the name OR description
            // modify this to search other fields if desired
            Set<Id> selectedEntries = new Set<Id>();
            for(opportunityLineItem d:shoppingCart){
                selectedEntries.add(d.PricebookEntryId);
            }
            qString+= ' order by Product2.Name';
            qString+= ' limit 350';
            
            //system.debug('qString:' +qString);        
            AvailableProducts = database.query(qString);
            
            // We only display up to 100 results... if there are more than we let the user know (see vf page)
            if(AvailableProducts.size()==351){ AvailableProducts.remove(350); overLimit = true; }
            else{
                overLimit=false;
            }
        }
        return AvailableProducts;
    }
    /*@AuraEnabled
public static void SaveAccounts( PricebookEntry accList){
Insert accList;
} */
    @AuraEnabled
    public static opportunityLineItem[] createOppLineItem(String strEquipData,String OPLITData, String recordToAdd, String theOppId) {
        //List<opportunityLineItem> shoppingCart = new List<opportunityLineItem>();
        List<PricebookEntry> recordsToUpdate=new List<PricebookEntry>(); 
        List<PricebookEntry> AvailableProducts=(List<PricebookEntry>)JSON.deserialize(strEquipData, List<PricebookEntry>.class);
        //List<opportunityLineItem> recordsToUpdate=new List<opportunityLineItem>(); 
        opportunityLineItem[] shoppingCart=(List<opportunityLineItem>)JSON.deserialize(OPLITData, List<opportunityLineItem>.class);
        
        ////System.debug('KA:: AvailableProducts '+AvailableProducts  );
        ////System.debug('KA:: RecordID'+ recordToAdd);
        /////System.debug('KA:: RecordID'+ theOppId);
        ID   oppID =theOppId; 
        Opportunity theOpp = [Select id, Drop_Count_Text__c,weighted_average__c from Opportunity where id=:oppID LIMIT 1];
        ////System.debug('KA:: weighted_average__c: '+theOpp.id);
        //for(PricebookEntry d:AvailableProducts) {
        //   System.debug('KA:: '+d.id+ 'Unit Price: '+d.UnitPrice);
        // }
        //System.debug(recordsToUpdate.size());
        // if(recordsToUpdate.size()>0) {	update recordsToUpdate;}
        for(PricebookEntry d : AvailableProducts)
        {  
            if((String)d.Id==recordToAdd)
            { 
                ////System.debug('KA:: IF is TRUE');
                if(d.Id=='01u60000004TFt1'||d.Id=='01u60000004TF2e'||d.ID=='01u60000004TF2U')
                {   
                    shoppingCart.add(new opportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice, Quantity=1, Status__c='Add Requested')); 
                } 
                else 
                {  
                    if(theOpp.weighted_average__c > 0)
                    { 
                        shoppingCart.add(new opportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice, Quantity = theOpp.weighted_average__c, Status__c='Add Requested'));
                    }  
                    else
                    { ////System.debug('KA:: 3');
                        shoppingCart.add(new opportunityLineItem(OpportunityId=theOpp.Id, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice, Quantity = Decimal.valueOf(theOpp.Drop_Count_Text__c), Status__c='Add Requested')); 
                    } 
                } 
                break;
            }
        }
        //// System.debug('KA:: Shopping Cart Size: '+ shoppingCart.size());
        return shoppingCart;
    }
    @AuraEnabled
    public static opportunityLineItem[] saveRecords(String strOppLineItemData, String itemToRemove) {
        List<opportunityLineItem> recordsToUpdate=new List<opportunityLineItem>(); 
        List<opportunityLineItem> oppLinItm=(List<opportunityLineItem>)JSON.deserialize(strOppLineItemData, List<opportunityLineItem>.class);
        forDeletion=(List<opportunityLineItem>)JSON.deserialize(itemToRemove, List<opportunityLineItem>.class);
        ////System.debug('equipData::'+forDeletion);
        for(opportunityLineItem oppLineItem : oppLinItm) { 
            
            recordsToUpdate.add(oppLineItem);  
        }
        ////System.debug('KA:: Update Record Size'+recordsToUpdate.size());
        if(recordsToUpdate.size()>0) {	upsert recordsToUpdate;}
        ////System.debug('KA:: for deletion in Save '+forDeletion.size());
        
        if(forDeletion.size() > 0) {
            update forDeletion;
            forDeletion.clear();
        }
        return forDeletion;
    }
    @AuraEnabled
    public static opportunityLineItem[] getOppLineItems(id oppId) {
        
        //System.debug('KA:: '+oppId);
        opportunityLineItem[] oppLineitems = [SELECT ID, PricebookEntry.Product2.Name, Quantity, UnitPrice, PricebookEntry.Product2.Description FROM opportunityLineItem WHERE OpportunityId = :oppId/*ApexPages.currentPage().getParameters().get('Id')*/ ];
        return oppLineitems;
    }
    
    @AuraEnabled
    public static opportunityLineItem[] removeFromShoppingCart(String strOppLineItemData, String recordToRemove,String itemToRemove, id oppID ){
        
        // This function runs when a user hits "remove" on an item in the "Selected Products" section
        List<opportunityLineItem> recordsToUpdate=new List<opportunityLineItem>(); 
        opportunityLineItem[] shoppingCart=(List<opportunityLineItem>)JSON.deserialize(strOppLineItemData, List<opportunityLineItem>.class);
        forDeletion=(List<opportunityLineItem>)JSON.deserialize(itemToRemove, List<opportunityLineItem>.class);
        //forDeletion=(List<opportunityLineItem>)JSON.deserialize(itemTORemove, List<opportunityLineItem>.class);
        
        Integer count = 0;
        
        Integer recordNumber=0;
        // if(recordToRemove !=null || recordToRemove !='' )
        //{
        //Integer itemNumber=integer.valueof(recordToRemove);
        for(opportunityLineItem d : shoppingCart){
            ////System.debug('KA:: Record Num '+recordToRemove+ ' OPT ID: '+ d.PriceBookEntryId);
            if((String)d.PriceBookEntryId==recordToRemove){
                
                ////System.debug('KA:: Record ID '+ d.id);
                
                if(d.Id!=null)
                    d.Status__c='Remove Requested';
                d.Action__c='REMOVE';
                forDeletion.add(d);
                //shoppingCart.remove(count);
                break;} 
            count++;
            recordNumber++;
        }  
        ////System.debug('KA:: For Delete Size in Remove Mthod: '+forDeletion.size());
        //}
        
        //updateAvailableList(getTheBook(oppID));
        return forDeletion;
    }
    /* @AuraEnabled
public static void onSave(){

// If previously selected products are now removed, we need to delete them
if(forDeletion.size()>0)
Update (forDeletion);

// Previously selected products may have new quantities and amounts, and we may have new products listed, so we use upsert here
try{
if(shoppingCart.size()>0) upsert(shoppingCart); } catch(Exception e){

}  

// After save return the user to the Opportunity
}
public static void updateAvailableList(Pricebook2 theBook)
{
/////////////////////////////////////////////////////////////
// We dynamically build a query string and exclude items already in the shopping cart
String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, Product2.Grandfathered__c, UnitPrice from PricebookEntry where Grandfathered__c = False and IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
//        String qString = 'select Id, Pricebook2Id, IsActive, Product2.Name, Product2.Family, Product2.IsActive, Product2.Description, UnitPrice from PricebookEntry where IsActive=true and Pricebook2Id = \'' + theBook.Id + '\'';
if(multipleCurrencies)  
qString += ' and CurrencyIsoCode = \'' + theOpp.get('currencyIsoCode') + '\'';

// note that we are looking for the search string entered by the user in the name OR description
// modify this to search other fields if desired
Set<Id> selectedEntries = new Set<Id>();
for(opportunityLineItem d:shoppingCart){
selectedEntries.add(d.PricebookEntryId);
}
qString+= ' order by Product2.Name';
qString+= ' limit 350';

system.debug('qString:' +qString);        
AvailableProducts = database.query(qString);

// We only display up to 100 results... if there are more than we let the user know (see vf page)
if(AvailableProducts.size()==351){ AvailableProducts.remove(350); overLimit = true; }
else{
overLimit=false;
}  
}
public static Pricebook2 getTheBook(ID oppId)
{
if(multipleCurrencies)            
theOpp = database.query('select Id, Pricebook2Id, Pricebook2.Name, CurrencyIsoCode from Opportunity where Id = \'' + oppId + '\' limit 1');
else
theOpp = [select Id, Pricebook2Id, PriceBook2.Name, weighted_average__c from Opportunity where Id = :oppId limit 1];

// If products were previously selected need to put them in the "selected products" section to start with
shoppingCart = [select Id, Quantity, TotalPrice, UnitPrice, Description, PriceBookEntryId, PriceBookEntry.Name, PriceBookEntry.IsActive, PriceBookEntry.Product2Id, PriceBookEntry.Product2.Name, PriceBookEntry.Product2.Maintenance__c, PriceBookEntry.PriceBook2Id from opportunityLineItem where PriceBookEntry.Product2.Maintenance__c != true AND OpportunityId=:theOpp.Id];

// Check if Opp has a pricebook associated yet
if(theOpp.Pricebook2Id == null)
{
Pricebook2[] activepbs = [select Id, Name from Pricebook2 where Id = '01s60000000dMje'];
if(activepbs.size() == 2)
{ 
forcePricebookSelection = true;    
theBook = new Pricebook2();
}
else{
theBook = activepbs[0];
}
}
else{
theBook = theOpp.Pricebook2;
}
return theBook;
} */
    
    //////////////////// This methid is us to check recrod type and send falg to show colums on lightning component
    
    @AuraEnabled
    public static boolean getNoPriceStage(String opporId)
    {
        String url= 'opportunityproductentry';
        boolean isNoPrice = false;
           
                        try 
                    {
                        Opportunity oppRec=[select id,Property_Type__c,AccountId,RecordTypeId,Drop_Count_Text__c,weighted_average__c,FOC__c,EVO__c,NA_Programming_Discount__c from Opportunity where id=:opporId];        
                        
                       
                        if (oppRec.Property_Type__c != 'Public' && oppRec.Property_Type__c != 'Private'&& oppRec.AccountId =='0016000000kGWKW')
                        {
                             
                            isNoPrice = true;
                        }
                        else
                        {
                             
                            if(oppRec.RecordTypeId != '01260000000LuQM'|| oppRec.RecordTypeId != '0126000000053vz' &&  oppRec.AccountId <>'0016000000kGWKW' )
                            {
                                   
                                isNoPrice =true;
                            }
                            if(oppRec.Property_Type__c == 'Public')
                            {
                                 
                                isNoPrice =true;
                            }
                            if(oppRec.Property_Type__c == 'Private')
                            {
                  
                                isNoPrice =true;
                            }
                             
                        }
                        
                        return true ;
                    }
        
        catch (Exception ex)
        {
            return null; 
        }
    }
    
    
    
}