@isTest(SeeAllData=false)
Public class EvolveParmHistory_Test{

    public static testMethod void EvolveParmHistory_Test(){

    PageReference pageRef = Page.EvolveParmHistory;
    
      // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.Amdocs_CustomerID__c = '123';
            acct1.Amdocs_BarID__c = '456';
            acct1.Amdocs_FAID__c = '789';
        insert acct1;

Pricebook2 pb2 = new Pricebook2();
            pb2.Name='Test PB2';
            pb2.IsActive=false;
        insert pb2;
        
        pb2.IsActive=true;
        update pb2;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '786664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
//            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Digital Bulk';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.Amdocs_FAID__c = '789';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Pricebook2Id=pb2.id;
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
            opp1.IsActiveAmdocs2__c=true;
        insert opp1;
        
        Smartbox__c sb = new Smartbox__c();
            sb.Opportunity__c = opp1.Id;
            sb.Location__c = 'C';
            sb.Action__c = 'ADD';
            sb.Serial_Number__c = 'LALPFZ00278B';
            sb.Chassis_Serial__c = 'LALPFZ00278B';
            sb.NoTrigger__c = True;
        insert sb;
        
        Evolve__c ev = new Evolve__c();
            ev.Smartbox__c=sb.id;
            ev.HREF__c='CJQL7yqhqCTG5puV1kKQ8s2SLx4Otest_98:93:CC:A7:58:E2';
            ev.Ethernet_MAC__c='98:93:CC:A7:58:E2';
        insert ev;

    Evolve__c e = [select id from Evolve__c where Ethernet_MAC__c='98:93:CC:A7:58:E2'];
    ApexPages.StandardController sc = new ApexPages.StandardController(e);
    EvolveParmHistory ext = new EvolveParmHistory(sc);

    }
}