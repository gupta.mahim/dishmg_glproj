@IsTest
private class LeadCategorySelectionPrivate_TEST {
    static testMethod void validateLeadCategorySelectionPrivate() {
        Lead L = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Health Care',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

       insert L;
        
       L = [SELECT Category__c FROM Lead WHERE Id =:L.Id];
       System.debug('Category after trigger fired: ' + L.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', L.Category__c);
    }
     static testMethod void validateLeadCategorySelectionPrivate2() {
        Lead LL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Sports Facilities',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LL;
        
       LL = [SELECT Category__c FROM Lead WHERE Id =:LL.Id];
       System.debug('Category after trigger fired: ' + LL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate3() {
        Lead LLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hospitals',
        Location_of_Service__c ='Not Resident Room',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLL;
        
       LLL = [SELECT Category__c FROM Lead WHERE Id =:LLL.Id];
       System.debug('Category after trigger fired: ' + LLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate4() {
        Lead LLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hospitals',
        Location_of_Service__c='Office',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLL;
        
       LLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLL.Id];
       System.debug('Category after trigger fired: ' + LLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate5() {
        Lead LLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Government',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLL;
        
       LLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate6() {
        Lead LLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Health/Fitness',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLL;
        
       LLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate7() {
        Lead LLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Automotive',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLL;
        
       LLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate8() {
        Lead LLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Banks',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLL;
        
       LLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate9() {
        Lead LLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Hair salon',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLL;
        
       LLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate10() {
        Lead LLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Beauty Services',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLL;
        
       LLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate11() {
        Lead LLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Grocery/Health Food Store',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLL;
        
       LLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate12() {
        Lead LLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Church',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLL;
        
       LLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate13() {
        Lead LLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Professional Services',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLL;
        
       LLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate14() {
        Lead LLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Nail Salon',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate15() {
        Lead LLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Stadiums',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLL.Category__c);
}

static testMethod void validateLeadCategorySelectionPrivate17() {
        Lead LLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Military Bases',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate18() {
        Lead LLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Private Club',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate19() {
        Lead LLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Office',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate20() {
        Lead LLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='School',
        Location_of_Service__c='Classroom',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate21() {
        Lead LLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='School',
        Location_of_Service__c='Common Area',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate22() {
        Lead LLLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='School',
        Location_of_Service__c='Not Resident Rooms',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate25() {
        Lead LLLLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='PrivateClubs',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate26() {
        Lead LLLLLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Service Industry',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate27() {
        Lead LLLLLLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retail Store',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate28() {
        Lead LLLLLLLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Retail',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLLLLLLLLLLLLL.Category__c);
}
static testMethod void validateLeadCategorySelectionPrivate29() {
        Lead LLLLLLLLLLLLLLLLLLLLLLLLLLL = new Lead(Company='Jerry is doing some testing',
        LeadSource='Direct Call',
        Lead_Source_External__c='Dish Network Web Site',
        Phone='512-295-5581',
        Type_of_Business__c='Medical / Dental',
        Building_Stories__c='1 - 2',
        FirstName='Jerry',
        LastName='Clifft',
        Number_of_Locations__c=1,
        Number_of_Units__c=1,
        Number_of_TVs__c='1 - 2');

        insert LLLLLLLLLLLLLLLLLLLLLLLLLLL;
        
       LLLLLLLLLLLLLLLLLLLLLLLLLLL = [SELECT Category__c FROM Lead WHERE Id =:LLLLLLLLLLLLLLLLLLLLLLLLLLL.Id];
       System.debug('Category after trigger fired: ' + LLLLLLLLLLLLLLLLLLLLLLLLLLL.Category__c);

       // Test that the trigger correctly updated the price 
    
       System.assertEquals('Private', LLLLLLLLLLLLLLLLLLLLLLLLLLL.Category__c);
}
}