public class NAProductSelection {

    public NAProductSelection() {
    }

public Opportunity opp {get; set;}public Case cas {get;set;}
public List <pricebookEntry> prods;
public String equip {get;set;}public opportunityLineItem[] shoppingCart {get;set;} public priceBookEntry[] prods2 {get;set;}
public string error {get; set;}
public String promo {get;set;}

           
   public NAProductSelection(ApexPages.StandardController controller) {   
   opp = new Opportunity(AccountId=ApexPages.currentPage().getParameters().get('acctId'), LeadSource='BTVNA Portal', Property_Type__c=ApexPages.currentPage().getParameters().get('cat'), RecordTypeId='0126000000017Tj', BTVNA_Request_Type__c=ApexPages.currentPage().getParameters().get('type'), misc_code_siu__c=ApexPages.currentPage().getParameters().get('siu'), Restricted_Programming__c=ApexPages.currentPage().getParameters().get('prog'), CloseDate=system.Today(), StageName='Closed Won');
   account a = [SELECT ID, Name, Promotion__c, Programming__c, Equipment_Options__c  from Account where Id = :ApexPages.currentPage().getParameters().get('acctId')];

equip=a.Equipment_Options__c;
promo=a.Promotion__c;
   }     
      
public List<pricebookEntry> getprods() {
if(ApexPages.currentPage().getParameters().get('prog') == 'True'){
account a = [SELECT ID, Name, Promotion__c, Programming__c, Equipment_Options__c  from Account where Id = :ApexPages.currentPage().getParameters().get('acctId')];

String[] progs = a.programming__c.split(';');

 prods = [select Id, UnitPrice, Name from pricebookEntry where Name = :progs and Pricebook2Id = '01s60000000Ia6UAAS'];
   return prods;  
  }
  else
  {
  account a = [SELECT ID, Name, Programming__c, NA_Programming_Discount__c, Equipment_Options__c  from Account where Id = :ApexPages.currentPage().getParameters().get('acctId')];
 prods = [select Id, UnitPrice, Name from pricebookEntry where Name = ''];
   return prods;
}
}
  public PageReference createOppty(){
  if(opp.Password__c==null){error='Error: Please Enter the properties Security Code'; return null;}  if(opp.Password_Reminder__c==null){error='Error: Please Enter the Password Reminder'; return null;}  if(opp.Contact_Name__c==null){error='Error: Please Enter Contact Name'; return null;}  if(opp.Contact_Phone__c==null){error='Error: Please Enter Contact Phone'; return null;}  if(opp.Contact_Email__c==null){error='Error: Please Enter Contact Email'; return null;}  if(opp.Name==null){error='Error: Please Enter Property Name'; return null;}  if(opp.Phone__c==null){error='Error: Please Enter Property Phone'; return null;}  if(opp.Address__c==null){error='Error: Please Enter Address'; return null;}  if(opp.City__c==null){error='Error: Please Enter City'; return null;}  if(opp.State__c==null){error='Error: Please Enter State'; return null;}  if(opp.Zip__c==null){error='Error: Please Enter Zip Code'; return null;}  if(opp.Tax_Employer_ID_Number__c==null){error='Error: Please Enter the TAX ID'; return null;}  if(opp.EVO2__C==null && ApexPages.currentPage().getParameters().get('cat') == 'Public'){error='Error: Please Enter EVO'; return null;}  if(opp.Password__c==null){error='Error: Please Enter the Security Code'; return null;}  if(opp.Password_Reminder__c==null){error='Error: Please Enter the Password Reminder'; return null;}  if(opp.Number_of_Floors__c==null){error='Error: Please Enter Number of Floors'; return null;}  if(opp.Total_Number_of_TVs__c==null){error='Error: Please Enter Number of TVs'; return null;}   if(opp.Promotion__c==null && ApexPages.currentPage().getParameters().get('pro') == 'null' ){error='Error: Please Enter Promotion'; return null;}   if(ApexPages.currentPage().getParameters().get('pro') == 'DBA') {opp.Promotion__c='DBA';}  if(ApexPages.currentPage().getParameters().get('pro') == 'DBA 24') {opp.Promotion__c='DBA 24';}  if(opp.TVs_Installed__c==null || opp.Point_of_Entry__c==null || opp.Landlord_permission__c==null || opp.Roof_Access__c==null){error='Error: Please Answer All Additional Questions'; return null;}      insert opp;     cas = [select Id, CaseNumber, Opportunity__c, Status from Case where Opportunity__c = :opp.id and Status = 'Form Submitted']; if(ApexPages.currentPage().getParameters().get('prog') == 'True' && ApexPages.currentPage().getParameters().get('siu') != 'N149'){Messaging.reserveSingleEmailCapacity(2);Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); String[] toAddresses = new String[] {opp.Contact_Email__c};  mail.setToAddresses(toAddresses); mail.setReplyTo('salesforceadmin@dish.com'); mail.setSenderDisplayName('Salesforce Support'); mail.setSubject('Your ' + opp.BTVNA_Request_Type__c + ' request has been received for ' + opp.Name +'.'); mail.setBccSender(false); mail.setUseSignature(false); mail.setPlainTextBody('Your ' + opp.BTVNA_Request_Type__c + ' request has been received for ' + opp.Name +'. Your confirmation number is ' + cas.CaseNumber + '. For questions or concerns please contact BTVNA@dish.com' ); mail.setHtmlBody('Your ' + opp.BTVNA_Request_Type__c + ' request has been received for <b>' + opp.Name +'</b>. Your confirmation number is<b> ' + cas.CaseNumber + '.</b> <br>For questions or concerns please contact BTVNA@dish.com');Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });   return new PageReference('/apex/NA_Thank_You');   }    else   {    if(ApexPages.currentPage().getParameters().get('cat') == 'Public' ){        return new PageReference('/apex/NA_Products_Public?id=' + opp.id + '&evo=EVO ' + opp.EVO2__c + '&disc=' + ApexPages.currentPage().getParameters().get('disc') );    }         else        {        return new PageReference('/apex/NA_Products_Private?id=' + opp.id + '&disc=' + ApexPages.currentPage().getParameters().get('disc'));      }     
   }
    }
}