/*   This test class is doing code coverage of the following classes.
*    1-    AddressScrubCallout  
*    2-    AddressScrubRequestInfo
*    3-    AddressScrubResponseInfo
*    4-    ValidateOpptyAddressCtrl
*    5-    ValidateTAAddressCtrl
*    6-    ValidateTEAddressCtrl
*    7-    ValidateAdressCtrl
*    8-    ValidateTEAddressBatch
*    9-    ValidateOpptyAddressCtrlComp
*/


@isTest
public class AddressScrubCalloutTest {
    
    @testSetup static void setup() {
        // Create common test accounts
        Opportunity newOpp = TestDataFactoryforInternal.createOpportunity(); 
        System.debug('New Opp Id=='+newOpp.id);
        system.assert(newOpp.Id!=null);        
    }
    
    public static testMethod void AddressApiReqandRes_Opp(){
        AddressScrubRequestInfo requestInfo = new AddressScrubRequestInfo();
        AddressScrubRequestInfo.address add = new AddressScrubRequestInfo.address();

        add.line1='';
        add.city= 'test';
        add.state='testc';
        add.zipCode='48001';
        requestInfo.address= add;
        requestInfo.checkForPushableAddress=true;   
        //----------------------------------------------------------------------------

        AddressScrubResponseInfo adres= new AddressScrubResponseInfo();
        AddressScrubResponseInfo.addressChangeIndicators aCI= new AddressScrubResponseInfo.addressChangeIndicators();
        aCI.line1= false;
        aCI.line2=false;
        aCI.city=true;
        aCI.zipCode= true;
        aCI.state= true;
        adres.addressChangeIndicators=aCI;
        
        AddressScrubResponseInfo.scrubbedAddress addScrub= new AddressScrubResponseInfo.scrubbedAddress();
        addScrub.line1='Engineers';
        addScrub.line2='Architect Society';
        addScrub.city='test';
        addScrub.county='test';
        addScrub.state='MI';
        addScrub.zipCode='48001';
        addScrub.zipCodeExtension='00124';
        adres.scrubbedAddress= addScrub;
        //--------------------------------------------------------------------
        
        AddressScrubCallout.invoke(requestInfo, true);
        //---------------------------------------------------------------------
        
        
        Opportunity newOpp = [SELECT Id FROM Opportunity WHERE Name='Test' Order By CreatedDate Desc LIMIT 1]; 
        System.debug('New Opp Id=='+newOpp.id);
        system.assert(newOpp.Id!=null);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(newOpp);
        ValidateOpptyAddressCtrl validateOppAdrsCtrl = new ValidateOpptyAddressCtrl(sc);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        validateOppAdrsCtrl.invokeNetQual();
        Test.stopTest();
        
        validateOppAdrsCtrl.callNetQual = false;
        validateOppAdrsCtrl.validation_msg_exist = true;
        validateOppAdrsCtrl.removeValidationMsg();
        //--------------------------------------------------------------------------
        
        /*
        Tenant_Account__c  Rec= TestDataFactoryforInternal.createTenantAccount(newOpp); 
        Tenant_Equipment__c tEquipment = new Tenant_Equipment__c();
        */
    }
    
        public static testMethod void AddressApiReqandRes_OppCom(){

        Opportunity newOpp = [SELECT Id FROM Opportunity WHERE Name='Test' Order By CreatedDate Desc LIMIT 1]; 
        newOpp.Override_NetQual__c = false;
        newOpp.Valid_Address__c = false;
        newOpp.Invoke_NetQual__c = true;
        Update newOpp;
        System.debug('New Opp Id=='+newOpp.id);
        system.assert(newOpp.Id!=null);
        insert new Address_Valid_Msg__c(ValidMessage__c = 'Value', name = newOpp.Id);
      //  ValidateOpptyAddressCtrlComp.callNetQual = true;
      //  ValidateOpptyAddressCtrlComp.checkOpptyValid(newOpp.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        ValidateOpptyAddressCtrlComp.getValidateOpptyAddress(newOpp.Id);
        Test.stopTest();
        ValidateOpptyAddressCtrlComp.validation_msg_exist = true;
        ValidateOpptyAddressCtrlComp.updateValidationMsg(newOpp.Id, false, true);

    }
    public static testMethod void AddressApiReqandRes_OppComp(){

        Opportunity newOpp = [SELECT Id FROM Opportunity WHERE Name='Test' Order By CreatedDate Desc LIMIT 1]; 
        System.debug('New Opp Id=='+newOpp.id);
        system.assert(newOpp.Id!=null);
        newOpp.Override_NetQual__c = true;
        newOpp.Valid_Address__c = true;
        newOpp.Address_Validation_Message__c = 'abc';
        newOpp.Invoke_NetQual__c = true;
        Update newOpp;
       insert new Address_Valid_Msg__c(ValidMessage__c = 'Value', name = 'ABC');
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        ValidateOpptyAddressCtrlComp.getValidateOpptyAddress(newOpp.Id);
        Test.stopTest();
        ValidateOpptyAddressCtrlComp.callBulk = true;
        ValidateOpptyAddressCtrlComp.callBulkAPI();
        ValidateOpptyAddressCtrlComp.validation_msg_exist = true;
        ValidateOpptyAddressCtrlComp.removeValidationMsg(newOpp.Id, true, true);

    }
    
    public static testMethod void AddressApiReqandRes_TA(){
        Opportunity newOpp = [SELECT Id FROM Opportunity WHERE Name='Test' Order By CreatedDate Desc LIMIT 1]; 
        Tenant_Account__c tenantAccount = TestDataFactoryforInternal.createTenantAccount(newOpp);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(tenantAccount);
        ValidateTAAddressCtrl validateTAAdrsCtrl = new ValidateTAAddressCtrl(sc);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        validateTAAdrsCtrl.invokeNetQual();
        Test.stopTest();
        
        validateTAAdrsCtrl.callNetQual = false;
        validateTAAdrsCtrl.validation_msg_exist = true;
        validateTAAdrsCtrl.removeValidationMsg();
    }
    
    public static testMethod void AddressApiReqandRes_TACom(){
        
        Opportunity newOpp = [SELECT Id FROM Opportunity WHERE Name='Test' Order By CreatedDate Desc LIMIT 1]; 
        Tenant_Account__c tenantAccount = TestDataFactoryforInternal.createTenantAccount(newOpp);
        tenantAccount.Override_NetQual__c = false;
        tenantAccount.Valid_Address__c = false;
        tenantAccount.Invoke_NetQual__c = true;
        Update tenantAccount;
        System.debug('New Opp Id=='+tenantAccount.id);
        system.assert(tenantAccount.Id!=null);
        insert new Address_Valid_Msg_TA__c(TAValidMessage__c = 'Value', name = tenantAccount.Id);
        //  ValidateOpptyAddressCtrlComp.callNetQual = true;
        //  ValidateOpptyAddressCtrlComp.checkOpptyValid(newOpp.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        ValidateTAAddressCtrlComp.getValidateTAAddress(tenantAccount.Id);
        Test.stopTest();
        ValidateTAAddressCtrlComp.validation_msg_exist = true;
        ValidateTAAddressCtrlComp.updateValidationMsg(tenantAccount.Id, false, true);
        ValidateTAAddressCtrlComp.removeValidationMsg(tenantAccount.Id, true, true);
    }
    
    public static testMethod void AddressApiReqandRes_TE(){
        Opportunity newOpp = [SELECT Id FROM Opportunity WHERE Name='Test' Order By CreatedDate Desc LIMIT 1]; 
        Tenant_Equipment__c tenantEquipment = TestDataFactoryforInternal.createTenantEquipment(newOpp);
        
        ApexPages.StandardController sc = new ApexPages.StandardController(tenantEquipment);
        ValidateTEAddressCtrl validateTEAdrsCtrl = new ValidateTEAddressCtrl(sc);
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        validateTEAdrsCtrl.invokeNetQual();
        Test.stopTest();
        
        validateTEAdrsCtrl.callNetQual = false;
        validateTEAdrsCtrl.validation_msg_exist = true;
        validateTEAdrsCtrl.removeValidationMsg();
    }
    
    public static testMethod void AddressApiReqandRes_TECom(){
        
        Opportunity newOpp = [SELECT Id FROM Opportunity WHERE Name='Test' Order By CreatedDate Desc LIMIT 1]; 
        Tenant_Equipment__c tenantEquipment = TestDataFactoryforInternal.createTenantEquipment(newOpp);
        tenantEquipment.Override_NetQual__c = false;
        tenantEquipment.Valid_Address__c = false;
        tenantEquipment.Invoke_NetQual__c = true;
        Update tenantEquipment;
        System.debug('New Opp Id=='+tenantEquipment.id);
        system.assert(tenantEquipment.Id!=null);
        insert new Address_Valid_Msg_TE__c(TEValidMessage__c = 'Value', name = tenantEquipment.Id);
        //  ValidateOpptyAddressCtrlComp.callNetQual = true;
        //  ValidateOpptyAddressCtrlComp.checkOpptyValid(newOpp.Id);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        ValidateTEAddressCtrlComp.getValidateTEAddress(tenantEquipment.Id);
        Test.stopTest();
        ValidateTEAddressCtrlComp.validation_msg_exist = true;
        ValidateTEAddressCtrlComp.updateValidationMsg(tenantEquipment.Id, false, true);
        ValidateTEAddressCtrlComp.removeValidationMsg(tenantEquipment.Id, true, true);
    }
    
    /*
    public static testMethod void AddressApiReqandRes_TE(){
        Opportunity newOpp = [SELECT Id FROM Opportunity WHERE Name='Test' Order By CreatedDate Desc LIMIT 1]; 
        Tenant_Equipment__c tenantEquipment = TestDataFactoryforInternal.createTenantEquipment(newOpp);
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        ValidateAdressCtrl.validateTenantEquip(tenantEquipment.Id);
        Test.stopTest();
    }
    */
    
    public static testMethod void AddressApiReqandRes_TE_Batch(){
        Opportunity newOpp = [SELECT Id FROM Opportunity WHERE Name='Test' Order By CreatedDate Desc LIMIT 1]; 
        Tenant_Equipment__c tenantEquipment = TestDataFactoryforInternal.createTenantEquipment(newOpp);
        List<Tenant_Equipment__c> lst_tenantEquipment = new List<Tenant_Equipment__c>();
        lst_tenantEquipment.add(tenantEquipment);
        
        String query = 'Select Id,Address__c,city__c,Valid_Address__c,State__c,Zip_Code__c from Tenant_Equipment__c';
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new ExampleCalloutMock());
        ValidateTEAddressBatch job = new ValidateTEAddressBatch(query);
        database.executebatch(job);
        Test.stopTest();
    }
    
}