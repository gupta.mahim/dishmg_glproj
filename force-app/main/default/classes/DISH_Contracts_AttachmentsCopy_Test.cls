@isTest
public class DISH_Contracts_AttachmentsCopy_Test {
     static Attachments_Classic_Lightning__c obj = new Attachments_Classic_Lightning__c();
    @isTest
    public static void copyAttachmentsLightning()
    {
         obj.Name ='Test';
        obj.Is_File_Upload__c = true;
        insert obj;
        
        Account a = new Account();
        a.Name = 'Test Account';
        //          a.Programming__c = 'Starter';
        //          a.phone = '(303) 555-5555';
        insert a;
        DISH_Contract__c dishconract = new DISH_Contract__c();
        dishconract.Account__c = a.id;
        dishconract.Contract_Start_Date__c =system.today();
        dishconract.Status__c ='Athoring';
        try{
            insert dishconract;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }
        
        
        // Ceation of content document
        ContentVersion contentVersionInsert = new ContentVersion(
            Title = 'Test'+a.Id+'.jpg',
            PathOnClient = 'Test'+a.Id+'.jpg',
            VersionData = Blob.valueOf('Test Content Data'),
            IsMajorVersion = true
        );
        
        try{
            insert contentVersionInsert;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }
        //ContentVersion contentVersionSelect = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionInsert.Id LIMIT 1];
        //List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];
        ContentDocumentLink cdl = new ContentDocumentLink();
        ContentVersion contentDocId = [select ContentDocumentId FROM ContentVersion WHERE Id = :contentVersionInsert.Id LIMIT 1];
        cdl.ContentDocumentId =  contentDocId.ContentDocumentId;
        cdl.LinkedEntityId = dishconract.id; //accontdata.Account__c;
        cdl.ShareType = 'V';
        try{
            insert cdl;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }

        ///// now updaing DISH_Contract__c status to Fully Signed.
        DISH_Contract__c dishconractRead = [select id, status__c from DISH_Contract__c dishconract where id=:dishconract.id LIMIT 1];
        dishconractRead .Status__c = 'Fully Signed';
        Test.startTest();
        try{
            update dishconractRead;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }
        Test.stopTest();
    }
    @isTest
    public static void copyAttachmentsClassic()
    {
         obj.Is_File_Upload__c = false;
        
        Account a = new Account();
        a.Name = 'Test Account Classic';
        //          a.Programming__c = 'Starter';
        //          a.phone = '(303) 555-5555';
        insert a;
        DISH_Contract__c dishconract = new DISH_Contract__c();
        dishconract.Account__c = a.id;
        dishconract.Contract_Start_Date__c =system.today();
        dishconract.Status__c ='Athoring';
        try{
            insert dishconract;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }
        Attachment attachment = new Attachment();
            attachment.body = Blob.valueOf( 'this is an attachment test' );
            attachment.name = 'attachment'+a.id+'.jpg';
            attachment.parentId = dishconract.id;
        try{
            System.debug('KA:: '+ attachment);
             insert attachment;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }
       
         ///// now updaing DISH_Contract__c status to Fully Signed.
        DISH_Contract__c dishconractReadClassic = [select id, status__c from DISH_Contract__c dishconract where id=:dishconract.id LIMIT 1];
        dishconractReadClassic .Status__c = 'Fully Signed';
        Test.startTest();
        
        try{
            update dishconractReadClassic;
        }catch(DmlException ex)
        {
            System.debug('KA:: DmlException ' + ex.getMessage());
        }
        Test.stopTest(); 
    } 
}