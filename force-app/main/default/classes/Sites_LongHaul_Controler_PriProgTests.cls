@isTest (seeAllData=true)
Public class Sites_LongHaul_Controler_PriProgTests{
    public static testMethod void testSites_LongHaul_Controler_PriProg(){
    PageReference pageRef = Page.Sites_LongHaul_PrivateProgram;
Test.setCurrentPageReference(pageRef);

        Account Acc = New Account(Name='Test Account', RecordtypeId='0126000000018Rc', Email__c='dish.business@dish.com', Misc_Code_SIU__c='S100', Phone='(303)555-5555');
        insert Acc;
        
        Pricebook2 standardPB = [select id from Pricebook2 where isStandard=true];

        Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book 2009', Description = 'Price Book 2009 Products', IsActive = true);
        insert pb;
        Product2 prod = new Product2(Name = 'Anti-infectives 2007', Family = 'Best Practices', IsActive = true);
        insert prod;

        PricebookEntry standardPrice = new PricebookEntry(Pricebook2Id = standardPB.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert standardPrice;

        PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
        insert pbe;
        Opportunity opp = new Opportunity(AccountId=Acc.Id, Name = 'Test Syndicated 2010', Type = 'Syndicated - New', StageName = 'Planning', Contact_Email__c = 'm@dish.com', CloseDate = system.today());
        insert opp;
        OpportunityLineItem oli = new OpportunityLineItem(opportunityId = opp.Id, pricebookentryId = pbe.Id, Quantity = 1, UnitPrice = 7500, Description = '2007 CMR #4 - Anti-Infectives');
        insert oli;  

Case mycas = new Case (AccountId=Acc.Id, Status='Form Submitted', Origin='PRM', Opportunity__c=opp.id);
insert mycas;

ApexPages.StandardController sc = new ApexPages.standardController(opp);
System.currentPagereference().getParameters().put('id',opp.Id);
System.currentPagereference().getParameters().put('direct','');
Sites_LongHaul_Controler_PriProg oPEE = new Sites_LongHaul_Controler_PriProg(sc);

   if(UserInfo.isMultiCurrencyOrganization())
            System.assert(oPEE.getChosenCurrency()!='');
        else
            System.assertEquals(oPEE.getChosenCurrency(),'');
        
           
            Integer startCount = oPEE.ShoppingCart.size();
        system.assert(startCount>0);
            
            oPEE.searchString = 'blah blaht';
        oPEE.updateAvailableList();
        system.assert(oPEE.AvailableProducts.size()==0);
        
                oPEE.toUnselect = oli.PricebookEntryId;
        oPEE.removeFromShoppingCart();
        system.assert(oPEE.shoppingCart.size()==startCount-1);
        
                oPEE.searchString = oli.PricebookEntry.Name;
        oPEE.updateAvailableList();
        system.assert(oPEE.AvailableProducts.size()>0); 
        
                oPEE.onSave();
        
                for(OpportunityLineItem o : oPEE.ShoppingCart){
            o.quantity = 5;
            o.unitprice = 300;
        }
        oPEE.onSave();
        oPEE.onCancel();
        
          opportunityLineItem[] oli2 = [select Id from opportunityLineItem where OpportunityId = :oli.OpportunityId];
        system.assert(oli2.size()==startCount);
        



}
}