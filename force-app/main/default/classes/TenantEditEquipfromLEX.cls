public class TenantEditEquipfromLEX {

    public Tenant_Equipment__c[] equip = new Tenant_Equipment__c[0];
    @AuraEnabled
    public static Tenant_Equipment__c[] getEquipments(id oppId) {
        List<Tenant_Equipment__c> recordsToUpdate=new List<Tenant_Equipment__c>();
 		
        Tenant_Equipment__c[] equip = [SELECT ID, Name, Remove_Equipment__c, Action__c, Smart_Card__c, Amdocs_Tenant_ID__c,Tenant_Account_Info__c, Tenant_Account__r.Name,Unit__c, Customer_Last_Name__c,Opportunity__c,Customer_First_Name__c FROM Tenant_Equipment__c WHERE Opportunity__c = :oppId ORDER BY Tenant_Equipment__c.Name ];
        
        return equip;
    }
    @AuraEnabled
    public static List<Tenant_Equipment__c> saveRecords(String strEquipData) {
 		List<Tenant_Equipment__c> recordsToUpdate=new List<Tenant_Equipment__c>(); 
        List<Tenant_Equipment__c> equipData=(List<Tenant_Equipment__c>)JSON.deserialize(strEquipData, List<Tenant_Equipment__c>.class);
        for(Tenant_Equipment__c anEquip:equipData) {  if(anEquip.Remove_Equipment__c==true) anEquip.Action__c='REMOVE';recordsToUpdate.add(anEquip);} 
        if(equipData.size()>0) { update equipData;}
         //recordsToUpdate.clear();
        //for(Tenant_Equipment__c anEquip:equipData) { 
           // if(anEquip.Remove_Equipment__c==true){ anEquip.Remove_Equipment__c=false;}recordsToUpdate.add(anEquip);
        //}
        return equipData;
    }
    @AuraEnabled
    public static String deleteRecords(String strEquipData) {
        
 		List<Tenant_Equipment__c> recordsToDelete=new List<Tenant_Equipment__c>(); 
        List<Tenant_Equipment__c> equipData=(List<Tenant_Equipment__c>)JSON.deserialize(strEquipData, List<Tenant_Equipment__c>.class);
       
        for(Tenant_Equipment__c anEquip:equipData) { if(anEquip.Remove_Equipment__c==true){recordsToDelete.add(anEquip);}}
     
        if(recordsToDelete.size()>0){ delete recordsToDelete; }

        return 'SUCCESS';
    }
    
    //Milestone# MS-001252 - 30Jun20 - Start
    @AuraEnabled
    public static userAccessCheck.userAccessCheckResponse getUserAccess(){
        userAccessCheck.userAccessCheckResponse resp = new userAccessCheck.userAccessCheckResponse();
        list<Custom_Button_Access_Detail__mdt> objectFieldDetail = [SELECT Object_Name__c, Field_Name__c FROM Custom_Button_Access_Detail__mdt 
                                                          WHERE Custom_Button_Name__c='Edit Tenant Equipment' Limit 1];
        
        if(objectFieldDetail.size()>0)
        	resp=userAccessCheck.getCurrentUserAccess(objectFieldDetail[0].Object_Name__c, objectFieldDetail[0].Field_Name__c, 'Edit Tenant Equipment');
        else{
            resp.currentUserHaveAccess = false;
            resp.noAccessMsg = System.Label.CustomButtonNoAccessMsg;
        }
        
        return resp;
    }
    //Milestone# MS-001252 - End
}