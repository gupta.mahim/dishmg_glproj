@isTest (seeAllData=true)
Public class NAProductSelectionTests{

    public static testMethod void testNAProductSelection(){
            Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        insert a;
        
               Opportunity o = new Opportunity(
             
               LeadSource='BTVNA Portal', 
               Property_Type__c='Public', 
               RecordTypeId='0126000000053vu', 
               BTVNA_Request_Type__c='Installation/Activation',
                misc_code_siu__c='N123', 
                Restricted_Programming__c='True', 
                CloseDate=system.Today(), 
                StageName='Closed Won');

    
        
        
        Case c = new Case();
        c.Opportunity__c=o.Id;
        c.Account_Name__c=a.Id;
        c.Status='Form Sumitted';
        c.Origin='BTVNA Portal';
        c.Property_Type__c='Public';
        String equip=a.Equipment_Options__c;
        
    
    PageReference pageRef = Page.NA_Form;
    Test.setCurrentPage(pageRef);
    NAProductSelection controller = new NAProductSelection();
        
    System.currentPagereference().getParameters().put('acctId',a.Id);
    System.currentPagereference().getParameters().put('cat','Public');
    System.currentPagereference().getParameters().put('prog','True');
    System.currentPagereference().getParameters().put('siu','N123');
    System.currentPagereference().getParameters().put('type','Installation/Activation');
        
ApexPages.StandardController sc = new ApexPages.standardController(o);       
NAProductSelection RUSC = new NAProductSelection(sc);

   
  RUSC.getprods();  
  RUSC.createOppty();

  }
  
      public static testMethod void testNAProductSelection2(){
            Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        insert a;
        
               Opportunity o = new Opportunity();
        o.Name='Test Site';
        o.AccountId=a.Id;
        o.LeadSource='BTVNA Portal';
        o.Property_Type__c='Public';
        o.BTVNA_Request_Type__c='SIte Survey';
        o.misc_code_siu__c=a.Misc_Code_SIU__c;
        o.Restricted_Programming__c='True';
        o.CloseDate=system.Today();
        o.StageName='Closed Won';
        o.Contact_Name__c='Matt Test';
        o.Contact_Phone__c='(303) 555-5555';
        o.Contact_Email__c='m@dish.com';
        o.EVO__c='1-50';
        o.FOC__c='1-50';

        
        Case c = new Case();
        c.Opportunity__c=o.Id;
        c.Account_Name__c=a.Id;
        c.Status='Form Sumitted';
        c.Origin='BTVNA Portal';
        c.Property_Type__c='Public';
        String equip=a.Equipment_Options__c;
        
    
    PageReference pageRef = Page.NA_Form;
    Test.setCurrentPage(pageRef);
    NAProductSelection controller = new NAProductSelection();
        
    System.currentPagereference().getParameters().put('acctId',a.Id);
    System.currentPagereference().getParameters().put('cat','Public');
    System.currentPagereference().getParameters().put('prog','False');
    System.currentPagereference().getParameters().put('siu','N123');
    System.currentPagereference().getParameters().put('type','Installation/Activation');
        
ApexPages.StandardController sc = new ApexPages.standardController(o);       
NAProductSelection RUSC = new NAProductSelection(sc);
   
  RUSC.getprods();  
  RUSC.createOppty();

  }

       public static testMethod void testNAProductSelection3(){
            Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        insert a;
        
               Opportunity o = new Opportunity();
        o.Name='Test Site';
        o.AccountId=a.Id;
        o.LeadSource='BTVNA Portal';
        o.Property_Type__c='Public';
        o.BTVNA_Request_Type__c='SIte Survey';
        o.misc_code_siu__c=a.Misc_Code_SIU__c;
        o.Restricted_Programming__c='True';
        o.CloseDate=system.Today();
        o.StageName='Closed Won';
        o.Contact_Name__c='Matt Test';
        o.Contact_Phone__c='(303) 555-5555';
        o.Contact_Email__c='m@dish.com';
       

        
        Case c = new Case();
        c.Opportunity__c=o.Id;
        c.Account_Name__c=a.Id;
        c.Status='Form Sumitted';
        c.Origin='BTVNA Portal';
        c.Property_Type__c='Public';
        String equip=a.Equipment_Options__c;
        
    
    PageReference pageRef = Page.NA_Form;
    Test.setCurrentPage(pageRef);
    NAProductSelection controller = new NAProductSelection();
        
    System.currentPagereference().getParameters().put('acctId',a.Id);
    System.currentPagereference().getParameters().put('cat','Private');
    System.currentPagereference().getParameters().put('prog','False');
    System.currentPagereference().getParameters().put('siu','N123');
    System.currentPagereference().getParameters().put('type','Installation/Activation');
        
ApexPages.StandardController sc = new ApexPages.standardController(o);       
NAProductSelection RUSC = new NAProductSelection(sc);

   
  RUSC.getprods();  
  RUSC.createOppty();

  } 
  
  }