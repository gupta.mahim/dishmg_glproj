public class ActivateTenantAccountFlow
{
    @AuraEnabled
    public static AmDocs_CalloutClass_CreateUnit.ResultMessage initAPICalls(String taId)
    {
        AmDocs_CalloutClass_CreateUnit.ResultMessage result=new AmDocs_CalloutClass_CreateUnit.ResultMessage();
        
        /*Tenant_Account__c taRec=[Select Id,AmDocs_CreateUnit__c,Type__c from Tenant_Account__c where id=:taId];
        if(taRec.Type__c=='Incremental')
        {
           result.isIncremental=true;       
        }
        result.hasActiveTenant=hasActiveTenentServiceId(taId);
        result.hasProgramming=hasProgramming(taId);
        System.debug('isIncremental='+result.isIncremental+', hasActiveTenant'+result.hasActiveTenant+', hasProgramming='+result.hasProgramming);
        if(!result.isIncremental)
        {
            System.debug('Calling create unit for individual...');
            sendCreateUnit(taId);
        }
        else if(!result.hasActiveTenant)
        {
            System.debug('Calling create unit for incremental account with no active tenant...');
            result=sendCreateUnit(taId);
        }
        else if(result.hasActiveTenant)
        {
            System.debug('Calling update unit for incremental with active tenant');
            sendUpdateUnit(taId);
        }*/
        result=sendCreateUnit(taId);
        return result;
    }
    @AuraEnabled
    public static AmDocs_CalloutClass_CreateUnit.ResultMessage sendCreateUnit(String taId)
    {
       //Tenant_Account__c taRec=[Select Id,AmDocs_CreateUnit__c,Type__c from Tenant_Account__c where id=:taId];
       //taRec.AmDocs_CreateUnit__c=true;
          
       //update taRec;
       AmDocs_CalloutClass_CreateUnit.ResultMessage msg=new AmDocs_CalloutClass_CreateUnit.ResultMessage();
       if(!Test.isRunningTest()) msg=AmDocs_CalloutClass_CreateUnit.createUnit(taId); 
         return msg;
    }
    @AuraEnabled
    public static void sendUpdateUnit(String taId)
    {
       Tenant_Account__c taRec=[Select Id,AmDocs_CreateUnit__c,Type__c from Tenant_Account__c where id=:taId];
       taRec.AmDocs_UpdateUnit__c=true;
          
       update taRec;
          
    }
    @AuraEnabled
    public static boolean hasActiveTenentServiceId(String taId)
    {
        boolean hasProgramming=false;
        
        List<Tenant_Product_Line_Item__c> teList=[Select Id from Tenant_Product_Line_Item__c where Tenant_Account__c=:taId];
        
        return hasProgramming;
    }
    @AuraEnabled
    public static boolean hasProgramming(String taId)
    {
        boolean hasActiveTenant=false;
        
        List<Tenant_Equipment__c> teList=[Select Id from Tenant_Equipment__c where Tenant_Account__c=:taId and Action__c='Active' and Amdocs_Tenant_ID__c!=null];
        
        return hasActiveTenant;
    }
    @AuraEnabled
    public static ResponseMessage loadTenantAccount(String taId)
    {
          ResponseMessage result=new ResponseMessage();
          
          Tenant_Account__c taRec=[Select Id,AmDocs_Transaction_Description__c,AmDocs_Transaction_Code__c,API_Status__c from Tenant_Account__c where id=:taId];
          
          result.taId=taRec.Id;
          result.apiStatus=taRec.API_Status__c;
          result.apiCode=taRec.AmDocs_Transaction_Code__c;
          result.apiDescription=taRec.AmDocs_Transaction_Description__c;
          System.debug('apiStatus=['+result.apiStatus+'], apiCode=['+result.apiCode+'], apiDescription=['+result.apiDescription+']');
          return result;
    }
    public Class ResponseMessage
    {
        public ResponseMessage()
        {
            isIncremental=false;
            hasActiveTenant=false;
            hasProgramming=false;
        }
        @AuraEnabled
        public String taId{get;set;}
        
        @AuraEnabled
        public String apiStatus{get;set;}
        
        @AuraEnabled
        public String apiCode{get;set;}
        
        @AuraEnabled
        public String apiDescription{get;set;}
        
        @AuraEnabled
        public boolean isIncremental{get;set;}
        
        @AuraEnabled
        public boolean hasActiveTenant{get;set;}
        
        @AuraEnabled
        public boolean hasProgramming{get;set;}
    }
}