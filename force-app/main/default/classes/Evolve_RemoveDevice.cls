public class Evolve_RemoveDevice {
 
    @future(callout=true)

    public static void makeCallout(String Id) {
    
    list<Evolve__c> E = [select id, MAC_Address__c, Smartbox__r.Opportunity__r.AmDocs_ServiceID__c, Ethernet_MAC__C, Location__c, Smartbox__r.Opportunity__r.UUID__c from Evolve__c where Id =:Id ];
        System.debug('RESULTS of the LIST lookup to the Evolve object' +E);   
        
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        jsonObj.writeStringField('cmd', 'remove');
    jsonObj.writeEndObject();
String finalJSON = jsonObj.getAsString();
    System.debug('DEBUG 0 ======== finalJSON: ' + finalJSON);        
        

    HttpRequest request = new HttpRequest();
    String endpoint = 'https://uxikj70uyh.execute-api.us-west-2.amazonaws.com/dev/v1/devices/'+E[0].Smartbox__r.Opportunity__r.UUID__c+'_'+E[0].Ethernet_MAC__c+'/remove';
//        String endpoint = 'https://i5nquk5eik.execute-api.us-east-2.amazonaws.com/dev/v1/devices/'+E[0].Smartbox__r.Opportunity__r.UUID__c+'_'+E[0].Ethernet_MAC__c+'/commands/sendcommand/';
        request.setEndPoint(endpoint);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json'); 
        request.setTimeout(101000); 
        request.setHeader('Accept', 'application/json'); 
        request.setHeader('User-Agent', 'SFDC-Callout/45.0'); 
        request.setBody(jsonObj.getAsString());
        
        HttpResponse response = new HTTP().send(request); 
            if (response.getStatusCode() == 200) { 
                String strjson = response.getbody();
                    System.debug('DEBUG 1 === RESPONSE json string: '   + strjson);
            
            Evolve__c sbc = new Evolve__c(); {
                sbc.Id=Id;
                sbc.API_Status__c='200';
                sbc.Transaction_Description__c = 'SUCCESS';                
                sbc.Transaction_Description__c = 'Removed Successfully';
            Update sbc; 
        }
        
          API_Log__c apil2 = new API_Log__c();{
              apil2.Record__c=id;       
              apil2.Object__c='Smartbox';
              apil2.Status__c='SUCCESS';       
              apil2.Results__c='SUCCESS: 200 ' +endpoint;
              apil2.API__c='Request Evolve Removal';
              apil2.ServiceID__c=E[0].Smartbox__r.Opportunity__r.AmDocs_ServiceID__c;       
              apil2.User__c=UserInfo.getUsername();   
          insert apil2;  
         }
             System.debug(response.toString());
             System.debug('DEBUG REQUEST: ' + request); 
             System.debug('Full JSON response payload: ' +response.getBody());
             System.debug('Full Status response payload: ' +response.getStatus());
             System.debug('Full Status Code response payload: ' +response.getStatusCode());
             System.debug('DEBUG RESPONSE GET HEADER KEYS: ' +response.getHeaderKeys());
             System.debug('STATUS:'+response.getStatus());
             System.debug('STATUS_CODE:'+response.getStatusCode());
             System.debug(response.getBody());
        }

        if (response.getStatusCode() != 200) { 
            String strjson = response.getbody();
                System.debug('DEBUG 1 === RESPONSE json string: '   + strjson);
            
            Evolve__c sbc = new Evolve__c(); {
                sbc.Id=Id;
                sbc.Transaction_Description__c = 'ERROR';
                sbc.Transaction_Description__c = 'ERROR: Removal Command Sent';
            Update sbc; 
        }
        
          API_Log__c apil2 = new API_Log__c();{
              apil2.Record__c=id;       
              apil2.Object__c='Evolve';
              apil2.Status__c='ERROR';       
              apil2.Results__c=strjson;
              apil2.API__c='Request Evolve Removal';
              apil2.ServiceID__c=E[0].Smartbox__r.Opportunity__r.AmDocs_ServiceID__c;
              apil2.User__c=UserInfo.getUsername();   
          insert apil2;  
         }
        
    }
}}