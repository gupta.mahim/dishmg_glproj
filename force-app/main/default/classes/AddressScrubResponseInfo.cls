/*
* Author: M Fazal Ur Rehman
* Description: JSON response wrapper for NetQual Address Scrub RESTFul 
* 
*/
Public class AddressScrubResponseInfo
{
    public addressChangeIndicators addressChangeIndicators{get;set;}
    public scrubbedAddress scrubbedAddress{get;set;}
    public string addressMatchCode{get;set;}
    public class addressChangeIndicators
    {
        public boolean line1;
        public boolean line2;
        public boolean city;
        public boolean zipCode;
        public boolean state;
        
    }
    public class scrubbedAddress
    {
        public String line1;
        public String line2;
        public String city;
        public String county;
        public String state;
        public String zipCode;
        public String zipCodeExtension;
    }
}