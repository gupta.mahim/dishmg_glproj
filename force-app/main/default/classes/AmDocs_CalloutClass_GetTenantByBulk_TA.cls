public class AmDocs_CalloutClass_GetTenantByBulk_TA {


public class person {
    public String serviceID;
    public String orderID;
    public String responseStatus;
    public String bulkStatus;
    public String status;
    public String errorCode;
    public cls_tenantList[] tenantList;
}

class cls_tenantList {
   public String serviceId;
   public String apId;
   public String productStatus;
   public cls_receiverList[] receiverList;
}

class cls_receiverList {
    public String receiverID;
    public String apId;
    public String smartCardID;
}

    @future(callout=true)

    public static void AmDocsMakeCalloutGetTenantByBulkTA(String Id) {
    
    list<Tenant_Account__c> TA = [select Id, AmDocs_ServiceID__c, Bulk_Service_ID__c from Tenant_Account__c where id = :id Limit 1];
        system.debug('DEBUG TA Results: ' +TA);

    if(TA.size() < 1 || (TA[0].Amdocs_ServiceID__c == '' || TA[0].Amdocs_ServiceID__c == NULL) || (TA[0].Bulk_Service_ID__c == '' || TA[0].Bulk_Service_ID__c == NULL)) {
        
        Tenant_Account__c TANew2 = new Tenant_Account__c(); {
            TANew2.Id=Id;
            TANew2.Amdocs_Transaction_Description__c = 'Missing Tenant Service ID';
            if( (TA[0].Amdocs_ServiceID__c == '' || TA[0].Amdocs_ServiceID__c == NULL )) {
                TANew2.Amdocs_Transaction_Description__c = 'Locate APID requires Tenant Service ID and a BULK Service ID. You data is missing the Tenant Service ID';
            }
            if( (TA[0].Bulk_Service_ID__c == '' || TA[0].Bulk_Service_ID__c == NULL )) {
                TANew2.Amdocs_Transaction_Description__c = 'Locate APID requires Tenant Service ID and a BULK Service ID. You data is missing the Bulk Service ID';
            }
    
        update TANew2;
        }

        API_Log__c apil2 = new API_Log__c();{ 
            apil2.Record__c=id;
            apil2.Object__c='Tenant Account'; 
            apil2.Status__c='SUCCESS'; 
            apil2.Results__c='User Error: Missing Tenant or Bulk Service ID';
            apil2.API__c='Get Tenant By Bulk';
            apil2.User__c=UserInfo.getUsername();
        insert apil2;
        }
    }
    else if(TA.size() > 0 && (TA[0].Amdocs_ServiceID__c != '' && TA[0].Amdocs_ServiceID__c != NULL)) {

    list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
        System.debug('RESULTS of the LIST lookup to the Amdocs Login object' +A);  
     
    HttpRequest request = new HttpRequest(); 
    String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+TA[0].Bulk_Service_ID__c+'/getTenantByBulk?sc=SS&lo=EN&ca=SF';
//    String endpoint = A[0].End_Point_Environment__c+'/commerce/service/1369463/getTenantByBulk?sc=SS&lo=EN&ca=SF';
    request.setEndPoint(endpoint); request.setMethod('GET');
    request.setTimeout(101000); 
    for(Integer i = 0; i < A.size(); i++) {
        String authorizationHeader = A[i].UXF_Token__c ; request.setHeader('Authorization', authorizationHeader);
    }
    HttpResponse response = new HTTP().send(request);
        System.debug('DEBUG ENDPOINT: ' +endpoint);
        System.debug(response.toString());
        System.debug('STATUS:'+response.getStatus());
        System.debug('STATUS_CODE:'+response.getStatusCode());
        System.debug(response.getBody());
                
    if(response.getStatusCode() == 200) {
        String strjson = response.getbody();
        JSONParser parser = JSON.createParser(strjson);
        parser.nextToken(); parser.nextToken(); parser.nextToken();
        person obj = (person)parser.readValueAs( person.class);
            System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
            System.debug('DEBUG 1 ======== obj.serviceID: ' + obj.serviceID);
            System.debug('DEBUG 2 ======== obj.responseStatus: ' + obj.responseStatus);
            System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);
            System.debug('DEBUG 7 ======== obj.tenantList.ServiceId: ' + obj.tenantList);
      
         if(obj.tenantList != Null) {
            list<string> serviceIds = new list<string>();
            for(cls_tenantList cc0 : obj.tenantList) { 
                if(cc0.serviceId == TA[0].Amdocs_ServiceID__c){ 
                    serviceIds.add(cc0.serviceId);
                        system.debug('DEBUG 8 - List of Service Ids  from Amdocs: ' +serviceIds);
                        system.debug('DEBUG 9 - Match of Service Id from Amdocs: ' +cc0.serviceId);
                }
            }

            list<string> apIds = new list<string>();
            for(cls_tenantList cc1 : obj.tenantList) {
                String si = serviceIds[0];
                if(cc1.serviceId == si){ 
                    apIds.add(cc1.apId);
                    system.debug('DEBUG 10 - List of apIds  from Amdocs: ' +apIds);
                    system.debug('DEBUG 11 - Match of APID from Amdocs: ' +cc1.apId);
                }
            }
            
            list<string> pss = new list<string>();
            for(cls_tenantList cc2 : obj.tenantList) {
//                String psa = productStatus[0];
                String si = serviceIds[0];
                if(cc2.serviceId == si){ 
                    pss.add(cc2.productStatus);
                    system.debug('DEBUG 12 - List of apIds  from Amdocs: ' +pss);
                    system.debug('DEBUG 13 - Match of APID from Amdocs: ' +cc2.productStatus);
                }
            }
            

            if(serviceIds != null) {
                List<Tenant_Account__c> allTQ = [SELECT Id, Amdocs_ServiceID__c, Name FROM Tenant_Account__c WHERE Amdocs_ServiceID__c in :serviceIds ];
                if( allTQ.Size() > 0 ) {
                    for(Tenant_Account__c currentEQ : allTQ){ 
                        currentEQ.API_ID__c = apIds[0]; 
                        currentEQ.Action__c = pss[0];
                        currentEQ.API_Status__c = 'Success';
                        currentEQ.Amdocs_Transaction_Description__c = 'Success! APID located / verified and Tenant Account ACTION upated.';
                    }
                    update allTQ;
                }
            }
        
          API_Log__c apil3 = new API_Log__c();{ 
              apil3.Record__c=id;
              apil3.Object__c='Tenant Account'; 
              apil3.Status__c='SUCCESS'; 
              apil3.API__c='Get Tenant By Bulk';
              apil3.User__c=UserInfo.getUsername();
              insert apil3;
              }
          }
    
    if(response.getStatusCode() == 200 && (obj.tenantList == Null)) {
            
        Tenant_Account__c TANew4 = new Tenant_Account__c(); {
            TANew4.Id=Id;
            TANew4.API_Status__c = 'SUCCESS';
            TANew4.Amdocs_Transaction_Description__c = 'Search for Tenant APID restuls: No APID found.';
            }
            update TANew4;
        
        API_Log__c apil3 = new API_Log__c();{ 
            apil3.Record__c=id;
            apil3.Object__c='Tenant Account';
            apil3.Status__c = 'SUCCESS';
            apil3.Results__c='Get Tenant By Bulk did not find an APID ' +response.getBody();
            apil3.API__c='Get Tenant By Bulk';
            apil3.User__c=UserInfo.getUsername();
        insert apil3;
        }
        }
    }        
        if(response.getStatusCode() >= 300) {
        Tenant_Account__c TANew3 = new Tenant_Account__c(); {
            TANew3.Id=Id;
            TANew3.API_Status__c = 'response.getStatusCode()';
            TANew3.Amdocs_Transaction_Description__c = 'Search for Tenant APID FAILED. ' +response.getBody();
            }
            update TANew3;
        
        API_Log__c apil3 = new API_Log__c();{ 
            apil3.Record__c=id;
            apil3.Object__c='Tenant Account';
            apil3.Status__c = 'response.getStatusCode()';
            apil3.Results__c='Get Tenant By Bulk FAILED'  +response.getBody();
            apil3.API__c='Get Tenant By Bulk';
            apil3.User__c=UserInfo.getUsername();
        insert apil3;
        }
        }
          
//        }
       } 
  }
}