/*-------------------------------------------------------------
Author: Mahim Gupta
Created on: 6th May 2020
Created for: Milestone# MS-001631
Description: To get list of Opportunities while creating New Tenant Account through GuideMe Flow
-------------------------------------------------------------*/

public with sharing class guideMeGetOpps {
    
    public class FlowOutput{
        @Invocablevariable
        public List<Opportunity> ListOfOpps = new list <Opportunity>();
    }
    
    @InvocableMethod (label='GetOpportunities')
    public static List<Flowoutput> GetOppsData(){
        List<FlowOutput> result= new list<FlowOutput>();
        FlowOutput output = new FlowOutput();
        result.add(output);
        
         //Milsestone: MS-003348 - 15 Sept 20 - Added RecordType.Name IN ('PCO', 'Retailer Bulk', 'Lab Site') filter in below query
        list<Opportunity> opps = [SELECT Id, Name FROM Opportunity WHERE 
                                  RecordType.Name IN ('PCO', 'Retailer Bulk', 'Lab Site') AND
                                  (Property_Status__c IN ('Active', 'Pre-Activation') OR Digital_Status__c IN ('Active')) AND
                                  AmDocs_tenantType__c IN ('Individual','Incremental')]; //AND AmDocs_SiteID_Unique__c != ''
        
        output.ListofOpps.addAll(opps);
        return result;
    }
}