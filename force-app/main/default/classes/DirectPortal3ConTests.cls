@isTest (seeAllData=true)
Public class DirectPortal3ConTests{

    public static testMethod void testDirectPortal3Con(){
            Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        insert a;
        
               Opportunity o = new Opportunity(
             
               LeadSource='BTVNA Portal', 
               Property_Type__c='Public', 
               RecordTypeId='0126000000053vu', 
               BTVNA_Request_Type__c='Installation/Activation',
                misc_code_siu__c='N123', 
                Restricted_Programming__c='True', 
                CloseDate=system.Today(), 
                StageName='Closed Won');

    
        
        
        Case c = new Case();
        c.Opportunity__c=o.Id;
        c.Account_Name__c=a.Id;
        c.Status='Form Sumitted';
        c.Origin='BTVNA Portal';
        c.Property_Type__c='Public';
        String equip=a.Equipment_Options__c;
        
    
    PageReference pageRef = Page.DirectPortal3;
    Test.setCurrentPage(pageRef);
    NAProductSelection controller = new NAProductSelection();
        
    System.currentPagereference().getParameters().put('acctId',a.Id);
    System.currentPagereference().getParameters().put('cat','Public');
    System.currentPagereference().getParameters().put('prog','True');
    System.currentPagereference().getParameters().put('siu','N123');
    System.currentPagereference().getParameters().put('type','Installation/Activation');
        
ApexPages.StandardController sc = new ApexPages.standardController(o);       
DirectPortal3Con RUSC = new DirectPortal3Con(sc);

   
  RUSC.getprods();  
  RUSC.createOppty();

  }
  
      public static testMethod void testDirectPortal3Con2(){
            Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        insert a;
        
               Opportunity o = new Opportunity();
        o.Name='Test Site';
        o.AccountId=a.Id;
        o.LeadSource='BTVNA Portal';
        o.Property_Type__c='Public';
        o.BTVNA_Request_Type__c='SIte Survey';
        o.misc_code_siu__c=a.Misc_Code_SIU__c;
        o.Restricted_Programming__c='True';
        o.CloseDate=system.Today();
        o.StageName='Closed Won';
        o.Contact_Name__c='Matt Test';
        o.Contact_Phone__c='(303) 555-5555';
        o.Contact_Email__c='m@dish.com';
        o.EVO__c='1-50';
        o.FOC__c='1-50';

        
        Case c = new Case();
        c.Opportunity__c=o.Id;
        c.Account_Name__c=a.Id;
        c.Status='Form Sumitted';
        c.Origin='BTVNA Portal';
        c.Property_Type__c='Public';
        String equip=a.Equipment_Options__c;
        
    
    PageReference pageRef = Page.DirectPortal3;
    Test.setCurrentPage(pageRef);
    DirectPortal3Con controller = new DirectPortal3Con();
        
    System.currentPagereference().getParameters().put('acctId',a.Id);
    System.currentPagereference().getParameters().put('cat','Public');
    System.currentPagereference().getParameters().put('prog','False');
    System.currentPagereference().getParameters().put('siu','N123');
    System.currentPagereference().getParameters().put('type','Installation/Activation');
        
ApexPages.StandardController sc = new ApexPages.standardController(o);       
DirectPortal3Con RUSC = new DirectPortal3Con(sc);
   
  RUSC.getprods();  
  RUSC.createOppty();

  }

       public static testMethod void testDirectPortal3Con3(){
            Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        insert a;
        
               Opportunity o = new Opportunity();
        o.Name='Test Site';
        o.AccountId=a.Id;
        o.LeadSource='BTVNA Portal';
        o.Property_Type__c='Public';
        o.BTVNA_Request_Type__c='SIte Survey';
        o.misc_code_siu__c=a.Misc_Code_SIU__c;
        o.Restricted_Programming__c='True';
        o.CloseDate=system.Today();
        o.StageName='Closed Won';
        o.Contact_Name__c= null;
        o.Contact_Phone__c= null;
        o.Contact_Email__c= null;
        o.Phone__c = null;
        o.Address__c = null;
        o.City__c = null;
        o.State__c = null;
        o.Zip__c = null;
        o.EVO__c = null;
        o.FOC__c = null;
        o.Number_of_Floors__c = null;
        o.Total_Number_of_TVs__c = null;
        o.Promotion__c = null;
        
       
       

        
        Case c = new Case();
        c.Opportunity__c=o.Id;
        c.Account_Name__c=a.Id;
        c.Status='Form Sumitted';
        c.Origin='BTVNA Portal';
        c.Property_Type__c='Public';
        String equip=a.Equipment_Options__c;
        
    
    PageReference pageRef = Page.DirectPortal3;
    Test.setCurrentPage(pageRef);
    DirectPortal3Con controller = new DirectPortal3Con();
        
    System.currentPagereference().getParameters().put('acctId',a.Id);
    System.currentPagereference().getParameters().put('cat','Private');
    System.currentPagereference().getParameters().put('prog','False');
    System.currentPagereference().getParameters().put('siu','N123');
    System.currentPagereference().getParameters().put('type','Installation/Activation');
    System.currentPagereference().getParameters().put('direct','true');
        
ApexPages.StandardController sc = new ApexPages.standardController(o);       
DirectPortal3Con RUSC = new DirectPortal3Con(sc);

   
  RUSC.getprods();  
  RUSC.createOppty();

  } 
  
      public static testMethod void testDirectPortal3Con4(){
            Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        insert a;
        
               Opportunity o = new Opportunity(
             
               LeadSource='BTVNA Portal', 
               Property_Type__c='Public', 
               RecordTypeId='0126000000053vu', 
               BTVNA_Request_Type__c='Installation/Activation',
                misc_code_siu__c='N123', 
                Restricted_Programming__c='True', 
                CloseDate=system.Today(), 
                StageName='Closed Won');

    
        
        
        Case c = new Case();
        c.Opportunity__c=o.Id;
        c.Account_Name__c=a.Id;
        c.Status='Form Sumitted';
        c.Origin='BTVNA Portal';
        c.Property_Type__c='Public';
        String equip=a.Equipment_Options__c;
        
    
    PageReference pageRef = Page.DirectPortal3;
    Test.setCurrentPage(pageRef);
    NAProductSelection controller = new NAProductSelection();
        
    System.currentPagereference().getParameters().put('acctId',a.Id);
    System.currentPagereference().getParameters().put('cat','Public');
    System.currentPagereference().getParameters().put('prog','True');
    System.currentPagereference().getParameters().put('siu','N123');
    System.currentPagereference().getParameters().put('type','Installation/Activation');
        
ApexPages.StandardController sc = new ApexPages.standardController(o);       
DirectPortal3Con RUSC = new DirectPortal3Con(sc);

   
  RUSC.getprods();  
  RUSC.createOppty();

  }
  
  }