/*
Name        : CMaster_CalloutClassAcct_Search_Test Apex class 
Author      : Khateeb Ibrahim
Date        : 08/28/2018
Version     : 1.0
Description : Test Class for CMaster_CalloutClassAcct_Search Class
*/
@isTest
private class CMaster_CalloutClassAcct_Search_Test {

    @testSetup static void testSetupdata(){
   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.AmDocs_CustomerID__c = 'Chewbacca101';
        insert acct1;

        // Create a U.S. based contact
        Contact ctc1 = New Contact();
            ctc1.FirstName = 'Jerry';
            ctc1.LastName = 'Clifft';
            ctc1.Phone = '512-383-5201';
            ctc1.Email = '';
            ctc1.Role__c = 'Billing Contact';
            ctc1.AccountId = acct1.Id;
            ctc1.Password__c = 'Sling11!@';
        insert ctc1;
        
        // Create a U.S. based contact
        Contact ctc2 = New Contact();
            ctc2.FirstName = 'Brad';
            ctc2.LastName = 'Clawson';
            ctc2.Phone = '512-383-5201';
            ctc2.Email = 'jerry.test@dish.com';
            ctc2.Role__c = 'Billing Contact';
            ctc2.AccountId = acct1.Id;
            ctc2.Password__c = 'Sling11!@';
            ctc2.dishCustomerId__c = 'DISH775768547075';
            ctc2.Username__c = 'jerry.test@dish.com';
            ctc2.partyId__c = '5b8436484f94675bdd9c6fbf';
            //ctc2.ExtRefNum__c  = 'Chewbacca101';
        insert ctc2;
        
        // Calout Trigger Records
        Contact ctc3 = New Contact();
            ctc3.FirstName = 'Jerry';
            ctc3.LastName = 'Clifft';
            ctc3.Phone = '512-383-5201';
            ctc3.Email = 'jerry.test@dish.com';
            ctc3.Role__c = 'Billing Contact';
            ctc3.AccountId = acct1.Id;
            ctc3.Password__c = 'Sling11!@';
            ctc3.dishCustomerId__c = 'DISH775768547075';
            ctc3.Username__c = 'jerry.test@dish.com';
            ctc3.partyId__c = 'Chewbacca1';
            ctc3.Update_Password__c = false;
            ctc3.CM_Forgot_Username__c = false;
            ctc3.AddSecurityPin__c = false;
            ctc3.CreateLogin__c = false;
            ctc3.UpdateFromCustomerMaster__c = false;
            ctc3.CM_CreateCustomer__c = false;
        insert ctc3;
        
   
        CustomerMaster__c cm =new CustomerMaster__c();
            cm.EndPoint__c = 'https://test-api-gateway.dish.com/amdocs-api-gateway';
        insert cm;
        
    }
    
    static testMethod void testCreateCustomerLogin(){
        Contact con1 = [select id from Contact where firstName ='Jerry' limit 1];
        Contact con2 = [select id from Contact where firstName ='Brad' limit 1];
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"dishCustomerId": "DISH775768547075","results": [{"customerOwner": {"id": "test"} } ]}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            CMaster_CalloutClassAcct_Search.CMasterCalloutSearchCustomer(con1.Id); 
            CMaster_CalloutClassAcct_Search.CMasterCalloutSearchCustomer(con2.Id); 
        Test.stopTest(); 
    }

}