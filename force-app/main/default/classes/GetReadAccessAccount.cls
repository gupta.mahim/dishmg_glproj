public class GetReadAccessAccount
{
	public class FlowOutput{
    	@Invocablevariable
        Public string Accountid;
        @Invocablevariable
        Public integer count=0;
    }
    @InvocableMethod (label='GetAccessAccount')
    Public static List<Flowoutput> getAccountId()
    {
        List<FlowOutput>result= new LIST <FlowOutput>();
        flowoutput output = new FlowOutput();
        list<Account> AcctList= new list<Account>([Select id from Account limit 15]);
        list<id> idlst = new list<Id>();
        for(Account acct: AcctList)
        {
            idlst.add(acct.id);
            
        }
        List<UserRecordAccess> RecList= new list<UserRecordAccess>([SELECT RecordId FROM UserRecordAccess WHERE UserId=:UserInfo.getUserId() AND HasReadAccess = true AND RecordId IN : idlst]);
  for(UserRecordAccess Ur:RecList)
  {
      if(RecList.size()>0)
          
      {
          
         if(output.count>1)
         {
             break;
         }
          output.Accountid=Ur.Recordid;
          output.count+=1;
      }
      
   }
        
        result.add(output);
        return result;
    
    }
    
}