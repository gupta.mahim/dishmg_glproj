@isTest(SeeAllData=true)
 private class CaseProductionTrackerBTVNA_Test{
    private static testmethod void testTriggerForCase(){
  Account a = new Account();
        a.Name = 'Test Account';
        a.Programming__c = 'Starter Pack';
        a.phone = '(303) 555-5555';
        a.Misc_Code_SIU__c = 'N123';
        a.Zip__c = '80221';
        a.Equipment_Options__c = '1-722k';
        insert a;
        
               Opportunity o = new Opportunity(
               Name = 'Test Opp',
               LeadSource='Portal', 
                RecordTypeId='0126000000053vu', 
                misc_code_siu__c='N123', 
                Restricted_Programming__c='True', 
                CloseDate=system.Today(), 
                StageName='Closed Won');
                insert o;
              
                    Case c = new Case();
                    c.Opportunity__c = o.Id;
                    c.AccountId = a.Id;
                    c.Status = 'Pre-Build Complete';
                    c.Origin = 'BTVNA Portal';
                    c.RequestedAction__c = 'Installation/Activation';
                    c.RecordTypeId = '0126000000017Vk';
                    c.Number_of_Accounts__c = 2;
                    c.CSG_Account_Number__c = '82551234567890';
                    
                    c.Requested_Actvation_Date_Time__c = system.Today();
                      insert c;
                    update c;

        }
}