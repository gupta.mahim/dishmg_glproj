public class Mute_CSV_FileUploader_LEX 
{
    ApexPages.StandardController stdCtrl;Id accId=null;public Mute_CSV_FileUploader_LEX (ApexPages.StandardController std){stdCtrl=std;accId=std.getRecord().Id;}
     
    public PageReference quicksave() 
    {update accstoupload ;return null;} 
    public PageReference fileAccess() {return null;}

public Mute_CSV_FileUploader_LEX(){

}

    public string nameFile{get;set;}
    public Blob contentFile{get;set;}
    String[] filelines = new String[]{};
    List<Mutes__c> accstoupload;
    
    public Pagereference ReadFile()
    {
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        accstoupload = new List<Mutes__c>();
        for (Integer i=1;i<filelines.size();i++)
        {
            String[] inputvalues = new String[]{};
            inputvalues = filelines[i].split(',');
            
            Mutes__c a = new Mutes__c();
            a.Account_Number__c = inputvalues[0];
            a.Account__c = accId;
            
            accstoupload.add(a);         
        }
        try{
              insert accstoupload;
        }
        catch (Exception e)
        {
            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'An error has occured. Please check the template');
            ApexPages.addMessage(errormsg);
        }      
                      
        return null ;}  public List<Mutes__c> getuploadedMutes() { if (accstoupload!= NULL) if (accstoupload.size() > 0) return accstoupload; else return null; else return null;}}