public class Smartbox_Equipment_Edit{
    
    public Smartbox__c[] equip = new Smartbox__c[0];
    
    public Smartbox_Equipment_Edit(ApexPages.StandardController controller) {

    }
    
    public PageReference onsave() {
     update equip;
     return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    }
    
    
public PageReference onclose() {
     return new PageReference('/' + ApexPages.currentPage().getParameters().get('Id'));
    } 
    
    public PageReference onadd() {
     return new PageReference('/apex/Smartbox_equipment_add_smatv?Id=' + ApexPages.currentPage().getParameters().get('Id') + '&st=' + + ApexPages.currentPage().getParameters().get('st'));
    } 


  public Smartbox__c[] getEquip() {
               
      equip = [SELECT ID, Name, Remove_Equipment__c, CAID__c, Chassis_Serial__c, Part_Number__c, Serial_Number__c, SmartCard__c, Type_of_Equipment__c, Status__c FROM Smartbox__c WHERE Opportunity__c = :ApexPages.currentPage().getParameters().get('Id') ORDER BY Smartbox__c.Name ];
// equip = [SELECT ID, Name, Remove_Equipment__c, CAID__c, Chassis_Serial__c, Part_Number__c, Serial_Number__c, SmartCard__c, Status__c FROM Smartbox__c WHERE Opportunity__c = :ApexPages.currentPage().getParameters().get('Id') ORDER BY Smartbox__c.Name ];       

        return equip;
        
    }

    
}