@isTest
private class AmDocs_CalloutClass_DBulkResendAll_Test{
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.Amdocs_CustomerID__c = '123';
            acct1.Amdocs_BarID__c = '456';
            acct1.Amdocs_FAID__c = '789';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '78664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Digital Bulk';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.Amdocs_FAID__c = '789';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
        insert opp1;
        
       Tenant_Equipment__c te = new Tenant_Equipment__c();
            te.Opportunity__c = opp1.Id;
            te.Name = 'R234567819';
            te.Smart_Card__c = 'S-TEST';
            te.Location__c = 'D';
            te.Action__c = 'Mass Trip / HIT - Pending';
            te.Mass_Trip_Requested__c = TRUE;
            te.Address__c ='1011 Coliie Path';
            te.City__c = 'Round Rock';
            te.State__c = 'TX';
            te.Zip_Code__c = '78664';
            te.Unit__c = '1A';
            te.Phone_Number__c = '5123835201';
            te.Email_Address__c = 'jerry.clifft@dish.com';
            te.Customer_First_Name__c = 'Jerry';
            te.Customer_Last_Name__c = 'Clifft';
        insert te;
        
        Tenant_Equipment__c te2 = new Tenant_Equipment__c();
            te2.Opportunity__c = opp1.Id;
            te2.Name = 'R345678190';
            te2.Smart_Card__c = 'S-TEST';
            te2.Location__c = 'D';
            te2.Action__c = 'Mass Trip / HIT - Pending';
            te2.Mass_Trip_Requested__c = TRUE;
            te2.Address__c ='1011 Coliie Path';
            te2.City__c = 'Round Rock';
            te2.State__c = 'TX';
            te2.Zip_Code__c = '78664';
            te2.Unit__c = '1A';
            te2.Phone_Number__c = '5123835201';
            te2.Email_Address__c = 'jerry.clifft@dish.com';
            te2.Customer_First_Name__c = 'Jerry';
            te2.Customer_Last_Name__c = 'Clifft';
            te2.Amdocs_Tenant_ID__c = '123';
        insert te2;
            
  }
  
  
    static testMethod void AmDocs_CalloutClass_DBulkResendAll_Test1(){
        Tenant_Equipment__c con1 = [select id from Tenant_Equipment__c where Name = 'R234567819' AND Action__c = 'Mass Trip / HIT - Pending' limit 1];
        Tenant_Equipment__c con2 = [select id from Tenant_Equipment__c where Name = 'R345678190' AND Action__c = 'Mass Trip / HIT - Pending' limit 1];
        
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(200,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_DBulkResendAll.AmDocs_CalloutClass_DBulkResendAll(con1.Id); 
            AmDocs_CalloutClass_DBulkResendAll.AmDocs_CalloutClass_DBulkResendAll(con2.Id); 
        Test.stopTest(); 
    }
    static testMethod void AmDocs_CalloutClass_DBulkResendAll_Test2(){
        Tenant_Equipment__c con2 = [select id from Tenant_Equipment__c where Name = 'R345678190' AND Action__c = 'Mass Trip / HIT - Pending' limit 1];
        
        Test.startTest(); 
            SingleRequestMock fakeResponse = new SingleRequestMock(400,
                                                 'Complete',
                                                 '{"ImplUpdateTVRestOutput":{"tenantserviceID":"1037992","orderID":"1037992","responseStatus":"SUCCESS","equipmentList":[{"action":"RESEND","receiverID":"R223456789"}],"spsList":[{"action":"ADD","caption":"AT120toAT200"}],"informationMessages":[{"errorCode":"1007","errorDescription":"Unable to add the Add-On AT120toAT200 since it is already exist for service ID 1037992."}],"ownerServiceId":"1335840"}}',
                                                 null);
            Test.setMock(HttpCalloutMock.class, fakeResponse);
            AmDocs_CalloutClass_DBulkResendAll.AmDocs_CalloutClass_DBulkResendAll(con2.Id); 
        Test.stopTest(); 
    }
}