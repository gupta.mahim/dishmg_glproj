@isTest
    private class Lead_MassEmail_FiberLead_Test {
      //Jerry Clifft 10-16-2020 Test Class for Lead_MassEmail_FiberLead
      
        @testSetup static void testSetupdata(){
    
        // Create Lead data
        Lead l1 = new Lead();
            l1.Company = 'Marketing Fiber Lead Test';
            l1.FirstName = 'Jerry';
            l1.LastName = 'Clifft';
            l1.Lead_Source_External__c = 'DISH FIBER';
            l1.Number_of_Units__c = 10;
            l1.Type_Of_Business__c = 'DISH FIBER';
            l1.Phone = '512-383-5201';
            l1.Email = 'Jerry.Clifft@dish.com';
        insert l1;
        
        Lead l2 = new Lead();
            l2.Company = 'Marketing Fiber Lead Test';
            l2.FirstName = 'Jerry';
            l2.LastName = 'Clifft';
            l2.Lead_Source_External__c = 'DISH FIBER';
            l2.Number_of_Units__c = 50;
            l2.Type_Of_Business__c = 'DISH FIBER';
            l2.Phone = '512-383-5201';
            l2.Email = 'Jerry.Clifft@dish.com';
        insert l2;
        
        Lead l3 = new Lead();
            l3.Company = 'Marketing Fiber Lead Test';
            l3.FirstName = 'Jerry';
            l3.LastName = 'Clifft';
            l3.Lead_Source_External__c = 'DISH FIBER';
            l3.Number_of_Units__c = 1000;
            l3.Type_Of_Business__c = 'DISH FIBER';
            l3.Phone = '512-383-5201';
            l3.Email = 'Jerry.Clifft@dish.com';
        insert l3;
        }
    
        @isTest
        static void Lead1(){
            Lead L01 = [Select Id FROM Lead WHERE Company = 'Marketing Fiber Lead Test' AND Number_of_Units__c = 10 Limit 1];
                List<Id> lstOfLeadIds = new List<Id>();
                lstOfLeadIds.add(L01.Id);

                DMLException dmlEx = new DMLException('Test Ex Email');
      }
        static void Lead02(){
            Lead L02 = [Select Id FROM Lead WHERE Company = 'Marketing Fiber Lead Test' AND Number_of_Units__c = 50 Limit 1];
                List<Id> lstOfLeadIds = new List<Id>();
                lstOfLeadIds.add(L02.Id);

                DMLException dmlEx = new DMLException('Test Ex Email');
      }      
        static void Lead03(){
            Lead L03 = [Select Id FROM Lead WHERE Company = 'Marketing Fiber Lead Test' AND Number_of_Units__c = 1000 Limit 1];
                List<Id> lstOfLeadIds = new List<Id>();
                lstOfLeadIds.add(L03.Id);

                DMLException dmlEx = new DMLException('Test Ex Email');
      }            
    }