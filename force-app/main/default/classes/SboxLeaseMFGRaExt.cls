public class SboxLeaseMFGRaExt {

    public SboxLeaseMFGRaExt(ApexPages.StandardController controller) {}     public SboxLeaseMFGRaExt() {}


   
    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null && ApexPages.currentPage().getParameters().get('cid') != null) {
  setCon = new ApexPages.StandardSetController(Database.getQueryLocator([SELECT ID, Name, Action__c, RA_Number__c, Shipping_Tracking__c, CAID__c, Smartbox_Leased2__c, Smartbox_Leased__c, Chassis_Serial__c, Part_Number__c, Serial_Number__c, SmartCard__c, Type_of_Equipment__c, Status__c FROM Smartbox__c WHERE CASEID__c = :ApexPages.currentPage().getParameters().get('cid') AND Smartbox_Leased__c = True AND Smartbox_Leased2__c = True AND RA_Number__c = NULL AND (Action__c = 'REMOVE' OR Action__c = 'Removed') ORDER BY Smartbox__c.Name ]));
                }
            return setCon;
        }
        set;
    }
    public List<Smartbox__c> getSmartbox() {
         return (List<Smartbox__c>) setCon.getRecords();
    }


    public PageReference onsave() {
        update (List<Smartbox__c>) setCon.getRecords();

        return new PageReference('http://dish.force.com/Maintenance/SmartboxLeaseMFG_ThankYou');
    }
}