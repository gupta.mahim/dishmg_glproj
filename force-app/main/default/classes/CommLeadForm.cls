public class CommLeadForm {

private final Lead L;
 public string error {get; set;}

       public CommLeadForm(ApexPages.StandardController stdcontroller) {
     L = (Lead)stdController.getRecord();
       }

    public PageReference onsave() {
    if (L.Company==''
     || L.FirstName==''
     || L.Street==''
     || L.LastName==''
     || L.City==''
     || L.Phone==''
     || L.State==''
     || L.PostalCode==''
     || L.Type_of_Business__c==''
     || L.Dish_Employee_ID__c==''
     || L.Dish_Employee_Name__c==''){
   error='Please Complete All Fields';
            return null;
            }
            else{ 
    IF ( L.Number_of_Units__c <= 49 ) {
  error='Units passed must be greater than 50 to qaulify';  
            return null;
            }
            else{ 
    L.OwnerId='00G60000002PXna';
    L.RecordTypeId='01260000000Lte8';
    L.ANI_DNIS__c='0000000';
    L.DNIS__c='0000000';
    L.LeadSource='ACN Bulk Referral Program';
    L.Lead_Source_External__c='ACN Bulk Referral Program';
    L.Number_of_TVs__c='1-2';
    L.Number_of_Locations__c=1;
    l.Building_Stories__c='1-2';    
     insert L;
    return new PageReference('http://dish.force.com/ACN/Comm_Lead_Confirmation');
     }
    }
}}