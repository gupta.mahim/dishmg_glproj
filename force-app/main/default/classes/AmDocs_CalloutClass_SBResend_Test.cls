@isTest
private class AmDocs_CalloutClass_SBResend_Test{
    //Implement mock callout tests here
  
    @testSetup static void testSetupdata(){

        // Create AmDocs_Login data
        AmDocs_Login__c aml = new AmDocs_Login__c();
            aml.UXF_Token__c = 'UXFToken0123456789';
        insert aml;

   // Create Account 1
        Account acct1 = new Account();
            acct1.Name = 'Test Account 1';
            acct1.ToP__c = 'Integrator';
            acct1.Pyscical_Address__c = '1011 Collie Path';
            acct1.City__c = 'Round Rock';
            acct1.State__c = 'TX';
            acct1.Zip__c = '78664';
            acct1.Amdocs_CreateCustomer__c = false;
            acct1.Distributor__c = 'PACE';
            acct1.OE_AR__c = '12345';
            acct1.Amdocs_CustomerID__c = '123';
            acct1.Amdocs_BarID__c = '456';
            acct1.Amdocs_FAID__c = '789';
        insert acct1;

        Opportunity opp1 = New Opportunity();
            opp1.Name = 'Test Opp 123456789 Testing opp1';
            opp1.First_Name_of_Property_Representative__c = 'Jerry';
            opp1.Name_of_Property_Representative__c = 'Clifft';
            opp1.Business_Address__c = '1011 Collie Path';
            opp1.Business_City__c = 'Round Rock';
            opp1.Business_State__c = 'TX';
            opp1.Business_Zip_Code__c = '786664';
            opp1.Billing_Contact_Phone__c = '512-383-5201';
            opp1.Billing_Contact_Email__c = 'jerry.clifft@dish.com';
            opp1.CloseDate=system.Today();
            opp1.StageName='Closed Won';
            opp1.Smartbox_Leased__c = false;
            opp1.AmDocs_ServiceID__c = '1111111111';
            opp1.AmDocs_SiteID__c = '22222';
            opp1.AccountId = acct1.Id;
            opp1.AmDocs_tenantType__c = 'Digital Bulk';
            opp1.Amdocs_CustomerID__c = '123';
            opp1.Amdocs_BarID__c = '456';
            opp1.Amdocs_FAID__c = '789';
            opp1.System_Type__c='QAM';
            opp1.Category__c='FTG';
            opp1.Address__c='address';
            opp1.City__c='Austin';
            opp1.State__c='TX';
            opp1.Zip__c='78664';
            opp1.Phone__c='5122965581';
            opp1.AmDocs_Property_Sub_Type__c='Hotel/Motel';
            opp1.Number_of_Units__c=100;
            opp1.Smartbox_Leased__c=false;
            opp1.RecordTypeId='012600000005ChE';
        insert opp1;
              
        Equipment__c et = new Equipment__c();
            et.Opportunity__c = opp1.Id;
            et.Name = 'R123456789';
            et.Receiver_S__c = 'S123456789';
            et.Location__c = 'C';
            et.Action__c = 'Send Trip / HIT';
        insert et;
        
       Tenant_Equipment__c te = new Tenant_Equipment__c();
            te.Opportunity__c = opp1.Id;
            te.Name = 'R234567819';
            te.Smart_Card__c = 'S1234567891';
            te.Location__c = 'D';
            te.Action__c = 'Send Trip / HIT';
            te.Address__c ='1011 Coliie Path';
            te.City__c = 'Round Rock';
            te.State__c = 'TX';
            te.Zip_Code__c = '78664';
            te.Unit__c = '1A';
            te.Phone_Number__c = '5123835201';
            te.Email_Address__c = 'jerry.clifft@dish.com';
            te.Customer_First_Name__c = 'Jerry';
            te.Customer_Last_Name__c = 'Clifft';
        insert te;

        Smartbox__c sb = new Smartbox__c();
            sb.Opportunity__c = opp1.Id;
            sb.CAID__c = 'R345678912';
            sb.SmartCard__c = 'S345678912';
            sb.Location__c = 'C';
            sb.Action__c = 'Send Trip / HIT';
            sb.Serial_Number__c = 'LALPSC011652';
            sb.Chassis_Serial__c = 'LALPFB001327';
            sb.NoTrigger__c = True;
        insert sb;
  }
  
  
 @isTest
  static void Acct1(){
    Smartbox__c opp = [Select Id, CAID__c FROM Smartbox__c WHERE CAID__c = 'R345678912' AND Action__c = 'Trip / HIT Pending' Limit 1];
      List<Id> lstOfOppIds = new List<Id>();
        lstOfOppIds.add(opp.Id);
        System.Debug('DEBUG 0 sendThisID : ' +opp); System.Debug('DEBUG 1 sendThisID : ' +lstOfOppIds);
        String sendThisID = opp.Id;
        Test.setMock(HttpCalloutMock.class, new AmDocs_CreateCustomerAcct_Mock());       // Set mock callout class
        Test.startTest();    // This causes a fake response to be sent from the class that implements HttpCalloutMock. 
            AmDocs_CalloutClass_SBResend.AmDocsMakeCalloutSBResend(sendThisID);
        Test.stopTest();    
      
        opp = [select CAID__c from Smartbox__c where id =: opp.Id];       // Verify that the response received contains fake values        
             System.assertEquals('R345678912',opp.CAID__c); 
//             System.assertEquals(null,opp.AmDocs_CustomerID__c); 
  }
}