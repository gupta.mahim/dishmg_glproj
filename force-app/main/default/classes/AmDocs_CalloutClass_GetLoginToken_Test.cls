@isTest
private class AmDocs_CalloutClass_GetLoginToken_Test{
  @testSetup
  static void setupTestData(){
    test.startTest();
    AmDocs_Login__c amdocs_login_Obj = new AmDocs_Login__c ( RefreshTokenNow__c = false, Name='AML Test 1', UXF_Token__c = '18');
    Insert amdocs_login_Obj; 

    test.stopTest();
  }
static testMethod void test_makeCallout_UseCase1(){

    List<AmDocs_Login__c> amdocs_login_Obj  =  [SELECT Id, RefreshTokenNow__c,UXF_Token__c from AmDocs_Login__c];
        System.assertEquals(true,amdocs_login_Obj.size()>0);
//        Test.setMock(HttpCalloutMock.class, new AmDocs_CallOutMockResponses());
    AmDocs_CalloutClass_GetLoginToken obj01 = new AmDocs_CalloutClass_GetLoginToken();
    AmDocs_CalloutClass_GetLoginToken.makeCallout('test data');
    }
    
static testMethod void test_makeCallout_UseCase2(){

    
    AmDocs_Login__c sbc = new AmDocs_Login__c();
        sbc.name='AML Test 2';
        sbc.RefreshTokenNow__c=false;
        insert sbc;
        System.debug('DEBUG: ' +sbc);
        
        sbc.RefreshTokenNow__c = True;
        update sbc;
        
    }
}