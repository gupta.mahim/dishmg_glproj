public class AmDocs_CalloutClass_CaseActivation {

public class person {
    public String serviceID;
    public String ownerServiceId;
    public String orderID;
    public String responseStatus;
    public cls_componentList[] componentList;
    public cls_informationMessages[] informationMessages;
    public String errorCode;
    public String backendErrorMessage;
}

class cls_componentList {
    public String action;
    public String caption;
}

class cls_informationMessages {
    public String errorCode;
    public String errorDescription;
}


    @future(callout=true)

    public static void AmDocsMakeCalloutCaseActivation(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

  list<case> cas = [select Id, Requested_Actvation_Date_Time__c from case where Id = :Id ];

  String SO = [select Opportunity__c from case where Id = :id LIMIT 1 ].Opportunity__c ;

 list<Opportunity> O = [select id, Name, No_Sports__c, NLOS_Locals__c, AmDocs_tenantType__c, Number_of_Units__c, Smartbox_Leased__c, AmDocs_SiteID__c, AmDocs_ServiceID__c, Amdocs_CustomerID__c, AmDocs_BarID__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Category__c, AmDocs_Site_Id__c from Opportunity where Id = :SO ];
     System.debug('RESULTS of the LIST lookup to the Opp object' +O);
     
    if(O[0].AmDocs_SiteID__c == Null || O[0].AmDocs_SiteID__c == '') {

    case ca = new case(); {
        ca.Id=Id;
        ca.Amdocs_Updated__c=FALSE;
        ca.Transaction_Description__c='ERROR: UPDATE BULK CASE ACTIVATION: The property can not be changed from Pre-Active to Active. Please first Pre-Activate the property then change it to Active.';
        ca.Transaction_Code__c='EC: 000';
        ca.Status='ERROR';    
    update ca;
    }
    
     API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;
          apil2.Object__c='CASE';
          apil2.Status__c='ERROR';
          apil2.Results__c='The property can not be changed from Pre-Active to Active. Please first Pre-Activate the property then change it to Active.';
          apil2.API__c='Change Property from Pre-Activation to Active.';
          apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
          apil2.User__c=UserInfo.getUsername();
      insert apil2;
      }
    }
    else if(O[0].AmDocs_SiteID__c != Null && O[0].AmDocs_SiteID__c != '') {

     
 list<OpportunityLineItem> C = [select id, Action__c, Quantity, IsPromotion__c, Sales_Price2__c, Amdocs_Caption2__c, Family__c, Opportunity.Id from OpportunityLineItem where Opportunity.Id = :SO AND ( Action__c = 'ADD' OR Action__c = 'Active' ) AND Family__c = 'Core' ];
     System.debug('RESULTS of the LIST lookup to the Core OpportunityLineItem records' +C);    

 list<OpportunityLineItem> P = [select id, Action__c, Quantity, IsPromotion__c, Sales_Price2__c, Amdocs_Caption2__c, Family__c, Opportunity.Id from OpportunityLineItem where Opportunity.Id = :SO AND Action__c = 'ADD'  AND Family__c = 'Promotion' ];
     System.debug('RESULTS of the LIST lookup to the Promotional OpportunityLineItem records' +P);

 list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);          
     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        jsonObj.writeFieldName('ImplUpdateBulkRestInput');
            jsonObj.writeStartObject();
                if( O.Size() > 0 ) {
                    jsonObj.writeStringField('orderActionType', 'CH');
                    jsonObj.writeStringField('reasonCode', 'CREQ');

                jsonObj.writeFieldName('componentList');
                    jsonObj.writeStartArray();
                        jsonObj.writeStartObject();
                            jsonObj.writeStringField('action', 'REMOVE');
                            jsonObj.writeStringField('caption', 'PreActivation' );
                            jsonObj.writeBooleanField('isPromotion', FALSE); 
                        jsonObj.writeEndObject();
                    jsonObj.writeEndArray();

                }
                jsonObj.writeEndObject();
            jsonObj.writeEndObject();
        String finalJSON = jsonObj.getAsString();
            System.debug('DEBUG Full Request finalJSON : ' +(jsonObj.getAsString()));
   
        HttpRequest request = new HttpRequest();
                 String endpoint = A[0].End_Point_Environment__c+'/commerce/service/'+O[0].AmDocs_ServiceID__c+'/updateBulk?sc=SS&lo=EN&ca=SF';
                 request.setEndPoint(endpoint);        
                 request.setBody(jsonObj.getAsString());
                 request.setTimeout(120000);
                 request.setHeader('Content-Type', 'application/json');
                 request.setMethod('POST');
                request.setHeader('User-Agent', 'SFDC-Callout/45.0');                 
                 String authorizationHeader = A[0].UXF_Token__c;
                 request.setHeader('Authorization', authorizationHeader);

         HttpResponse response = new HTTP().send(request);
                 System.debug(response.toString());
                 System.debug('STATUS:'+response.getStatus());
                 System.debug('STATUS_CODE:'+response.getStatusCode());
                 System.debug(response.getBody());
                 request.setTimeout(101000);

                
        if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { 
            String strjson = response.getbody();
            System.debug('DEBUG PRIOR to JSON DeSerialization ======== 1st STRING: ' +strjson);
            JSONParser parser = JSON.createParser(strjson); 
            parser.nextToken(); 
            parser.nextToken(); 
            parser.nextToken();
            person obj = (person)parser.readValueAs( person.class);

        System.debug('DEBUG 4 ======== obj.componentList: ' + obj.componentList);
        System.debug('DEBUG 5 ======== obj.informationMessages: ' + obj.informationMessages); 
        System.debug('DEBUG 6 ======== obj.orderID: ' + obj.orderID);


    case ca = new case(); {
        ca.Id=Id;
        if(obj.orderID != Null) {            ca.Amdocs_Updated__c=TRUE;            ca.Status = 'Request Completed';        
        }
        if ( obj.informationMessages != null) {            ca.Transaction_Description__c='UPDATE BULK CASE ACTIVATION: ' +obj.informationMessages[0].errorDescription;            ca.Transaction_Code__c='EC: ' +obj.informationMessages[0].errorCode;            
        }
        if(obj.informationMessages == Null) {
            ca.Transaction_Code__c=obj.responseStatus;
            ca.Transaction_Description__c='UPDATE BULK CASE Activation: ' +obj.responseStatus;
        }
        if(obj.responseStatus == Null) {
            ca.Transaction_Code__c=obj.responseStatus;
            ca.Transaction_Description__c='UPDATE BULK CASE Activation: ' +obj.backendErrorMessage;
        }
    update ca;
    }
    
     API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;
          apil2.Object__c='CASE';
          apil2.Status__c='SUCCESS';
          apil2.Results__c='The property has been be changed from Pre-Active to Active.';
          apil2.API__c='Change Property from Pre-Activation to Active.';
          apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
          apil2.User__c=UserInfo.getUsername();
      insert apil2;
      }

    Opportunity sbc = new Opportunity(); {
    sbc.id=SO; 
    sbc.AmDocs_Order_ID__c=obj.orderID;
    sbc.API_Status__c=String.valueOf(response.getStatusCode());
    if(C.size() > 0 || O[0].AmDocs_tenantType__c == 'Incremental' || O[0].AmDocs_tenantType__c == 'Bulk Only') {        sbc.Property_Status__c = 'Active';        sbc.Launch_Date__c=cas[0].Requested_Actvation_Date_Time__c;
    }
    if(O[0].AmDocs_tenantType__c == 'Individual' || O[0].AmDocs_tenantType__c == 'Incremental') {        sbc.Digital_Status__c = 'Active';        sbc.Digital_Launch_Date__c=cas[0].Requested_Actvation_Date_Time__c;
    }     
        
    if ( obj.informationMessages != null) {        if (obj.informationMessages.size() > 1) {            sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode + ' ' +obj.informationMessages[1].errorCode; sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription + '\n\n' +obj.informationMessages[1].errorDescription +'\n\n';
        }
    else if (obj.informationMessages.size() < 2) {sbc.AmDocs_Transaction_Code__c=sbc.AmDocs_Transaction_Code__c= +'EC: ' +obj.informationMessages[0].errorCode; sbc.AmDocs_Transaction_Description__c='CREATE BULK ' +obj.informationMessages[0].errorDescription;}
    }            
            
    if(obj.informationMessages == Null)
        {sbc.AmDocs_Transaction_Code__c=obj.responseStatus; sbc.AmDocs_Transaction_Description__c='UPDATE BULK CASE Activation: ' +obj.responseStatus;}
    if(obj.responseStatus == Null)
        {sbc.AmDocs_Transaction_Code__c= +'RS: UPDATE BULK CASE Activation: ';sbc.AmDocs_Transaction_Description__c=obj.backendErrorMessage;}
    
    update sbc;
    }
  }
  }
}}