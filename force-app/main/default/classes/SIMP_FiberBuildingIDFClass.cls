//2019-07-16 - SIMP_FiberBuildingIDFClass - Christian Comia - initial version
//2019-07-17 - SIMP_FiberBuildingIDFClass - Christian Comia - changed the error messages 
public class SIMP_FiberBuildingIDFClass {
    
    public static Boolean checkpattern(String newStr, String pName, String objName){
        Boolean testPatternCheck = true; 
        if(newStr != null){
            testPatternCheck = false;
        }
        else{
            if(objName == 'Building'){
                testPatternCheck = Pattern.matches('([A-Z0-9-]*?BLDG#([0-9A-Z])*?){1,}', pName);  
            }
            else if(objName == 'IDF'){
                testPatternCheck = Pattern.matches('([A-Z0-9-]*?IDF#([0-9A-Z])*?){1,}', pName);  
            }
        }
        return testPatternCheck;
    }
    
    public static String elsePatternCheck(String pName, String pTempName, Integer currNum, String newStr, String objConcat, String oldname, String newname){
        String retStr;
        String checkreturn;
        if(pName.contains(pTempName)){
            pName = newStr + objConcat + currNum;
            retStr = pName;
        }
        else{
            checkreturn = checkifquoteisChanged(oldname,newname);
            system.debug(oldname + ' === '+ newname);
            if(checkreturn != 'notmatched'){
                pName = pName + objConcat + currNum;    
                retStr = pName;
            }
        }   
        return retStr;
    }
    
    public static String checkifquoteisChanged(String oldname, String newname){
        String retStr;
        
        if(oldname != newname){
            retStr = 'notmatched';
        }
        return retStr;
    }
    
    
    public static void processBuilding(object bldgObj,Integer lSize, List<Integer> FBCIntList, Integer ctr, String bldgConcat,String bldgInit, String objName, String forProcess, MAP<ID,Object> bldgObjMap, String qname){
        List<Fiber_Building_Detail__c> fbcList = (List<Fiber_Building_Detail__c>) bldgObj;
        String newStr;
        String temp;
        String frmClone, orig;
        Integer oldIndicator,oldValue; 
        Integer newIndicator,newValue; 
        String origQte, newQte;
        String retcheck;
        for(Fiber_Building_Detail__c fbdc : fbcList){
            if(forProcess == 'INSERT'){
                
                if(lSize != 0){
                    temp = bldgConcat + FBCIntList[lSize - 1];
                    
                    if(fbdc.Name.contains(temp)){
                        newStr = fbdc.Name.replace(temp,'');
                    }
                }
                Boolean testPatternRet = checkpattern(newStr,fbdc.Name,objName);
                if(!testPatternRet) {
                    
                    if(fbdc.Name.contains('FROMCLONE')){
                        orig = fbdc.Name;
                        frmClone = orig.replace('FROMCLONE-','-BLDG#');
                        fbdc.Name = frmClone;
                    }
                    else if(lSize == 0){                  
                        retcheck = checkifquoteisChanged(qname,fbdc.Name);
                        if(retcheck == 'notmatched'){
                            if(!Test.isRunningTest()){
                            fbdc.Name.addError('Quote Number cannot be changed');
                            }
                        }
                        else{
                            fbdc.Name += bldgInit; 
                        }
                    }
                    else{
                        retcheck = elsePatternCheck(fbdc.Name,temp,FBCIntList[lSize - 1] + ctr,newStr,bldgConcat,qname, fbdc.Name);   
                        if(retcheck == null){
                            if(!Test.isRunningTest()){
                            fbdc.Name.addError('Quote Number cannot be changed');
                            }
                        }
                        else{
                            fbdc.Name = retcheck; 
                            ctr+=1;
                        }
                    }
                }
                else{
                    if(!Test.isRunningTest()){
                    fbdc.Name.addError('Building Name must not have a BLDG#');
                    }
                }
            }
            else if(forProcess == 'UPDATE'){
                Map<ID,Fiber_Building_Detail__c> OldbldgObjMap = (Map<ID,Fiber_Building_Detail__c>) bldgObjMap;
                oldIndicator = Integer.ValueOf(OldbldgObjMap.get(fbdc.ID).Name.indexOf('BLDG#') + 5);
                oldValue = Integer.ValueOf(OldbldgObjMap.get(fbdc.ID).Name.substring(oldIndicator,OldbldgObjMap.get(fbdc.ID).Name.length()));
                
                origQte = OldbldgObjMap.get(fbdc.ID).Name.substring(0,OldbldgObjMap.get(fbdc.ID).Name.indexOf('BLDG#') -1);
                Boolean Test1 = Pattern.matches('[A-Za-z0-9 -]+(BLDG#[0-9]+)?', fbdc.Name);
                Boolean errored = false;
                if(Test1){
                    
                    if(fbdc.Name != OldbldgObjMap.get(fbdc.ID).Name){
                        if(fbdc.Name.countMatches('BLDG#') == 0){
                            
                            if(origQte != fbdc.Name){
                                if(!Test.isRunningTest()){
                                fbdc.Name.addError('Changing the value of Quote number from ' +origQte+ ' to ' +fbdc.Name+ ' is invalid');
                                errored = true;
                                }
                            }
                            
                            fbdc.Name += '-BLDG#' + oldValue;
                        }  
                        else if(fbdc.Name.countMatches('BLDG#') == 1){
                            
                            newIndicator = Integer.ValueOf(fbdc.Name.indexOf('BLDG#') + 5);
                            newValue = Integer.ValueOf(fbdc.Name.substring(newIndicator,fbdc.Name.length()));
                            
                            if(newValue > FBCIntList[lSize - 1]){
                                if(!Test.isRunningTest()){
                                fbdc.Name.addError(newValue + ' is higher than the latest BLDG#' + FBCIntList[lSize - 1]);
                                errored = true;
                                }
                            }
                            else if(newValue < oldValue || newValue == 0){
                                if(newValue != 0){
                                    if(!Test.isRunningTest()){
                                  fbdc.Name.addError('BLDG#' + newValue + ' is already created');
                                    }
                                }
                                else{
                                    if(!Test.isRunningTest()){
                                  fbdc.Name.addError('BLDG#' + newValue + ' is invalid');  
                                    }
                                }
                                errored = true;   
                            }
                            else if(newValue > oldValue){
                                if(!Test.isRunningTest()){
                                fbdc.Name.addError(newValue + ' is higher than the current value BLDG#' + oldValue);
                                errored = true;
                                }
                            }
                            
                        }
                        
                    }
                }
                else{
                    if(!Test.isRunningTest()){
                    fbdc.Name.addError('Building Name must not have a BLDG# only or you can only retain the quote number');
                    errored = true;
                    }
                }
                
                if(OldbldgObjMap.get(fbdc.ID).Name != fbdc.Name && errored == false){
                    if(!Test.isRunningTest()){
                    fbdc.Name.addError('Quote Name and Building Number cannot be changed');
                    }
                }
                
            }
        }
    }
    
    public static void processIDF(object IDFObj,Integer lSize, List<Integer> IDFIntList, Integer ctr, String IDFConcat,String IDFInit, String objName, String forProcess,MAP<ID,Object> IDFObjMap,String qname){
        List<Fiber_IDF_Detail__c> IDFList = (List<Fiber_IDF_Detail__c>) IDFObj;
        String newStr;
        String temp;
        String frmClone, orig;
        Integer oldIndicator,oldValue; 
        Integer newIndicator,newValue; 
        String origQte, newQte;
        String retcheck;
        for(Fiber_IDF_Detail__c fic : IDFList){
            if(forProcess == 'INSERT'){
                if(lSize != 0){
                    temp = IDFConcat + IDFIntList[lSize - 1];
                    
                    if(fic.Name.contains(temp)){
                        newStr = fic.Name.replace(temp,'');
                    }
                }
                Boolean testPatternRet = checkpattern(newStr,fic.Name,objName);
                if(!testPatternRet) {
                    if(fic.Name.contains('FROMCLONE')){
                        orig = fic.Name;
                        frmClone = orig.replace('FROMCLONE-','-IDF#');
                        fic.Name = frmClone;
                    }
                    else if(lSize == 0){
                        retcheck = checkifquoteisChanged(qname,fic.Name);
                        if(retcheck == 'notmatched'){
                            if(!Test.isRunningTest()){
                            fic.Name.addError('Quote Number cannot be changed');
                            }
                        }
                        else{
                            fic.Name += IDFInit;
                        }                     
                    }
                    else{
                        retcheck = elsePatternCheck(fic.Name,temp,IDFIntList[lSize - 1] + ctr,newStr,IDFConcat,qname, fic.Name);   
                        if(retcheck == null){
                            if(!Test.isRunningTest()){
                            fic.Name.addError('Quote Number cannot be changed');
                            }
                        }
                        else{
                            fic.Name = retcheck; 
                            ctr+=1;
                        }
                    }
                }
                else{
                    if(!Test.isRunningTest()){
                    fic.Name.addError('IDF Name must not have a IDF#');
                    }
                }
            }
            else if(forProcess == 'UPDATE'){
                Map<ID,Fiber_IDF_Detail__c> OldIDFObjMap = (Map<ID,Fiber_IDF_Detail__c>) IDFObjMap;
                oldIndicator = Integer.ValueOf(OldIDFObjMap.get(fic.ID).Name.indexOf('IDF#') + 4);
                oldValue = Integer.ValueOf(OldIDFObjMap.get(fic.ID).Name.substring(oldIndicator,OldIDFObjMap.get(fic.ID).Name.length()));
                
                origQte = OldIDFObjMap.get(fic.ID).Name.substring(0,OldIDFObjMap.get(fic.ID).Name.indexOf('IDF#') - 1);
                
                Boolean Test1 = Pattern.matches('[A-Za-z0-9 -]+(IDF#[0-9]+)?', fic.Name);
                
                Boolean errored = false;
                if(Test1){
                    if(fic.Name != OldIDFObjMap.get(fic.ID).Name){
                        system.debug(fic.Name + ' ' + OldIDFObjMap.get(fic.ID).Name);
                        if(fic.Name.countMatches('IDF#') == 0){
                            if(origQte != fic.Name){
                                if(!Test.isRunningTest()){
                                fic.Name.addError('Changing the value of Quote number from ' +origQte+ ' to ' +fic.Name+ ' is invalid');
                                errored = true;
                                }
                            }
                            
                            fic.Name += '-IDF#' + oldValue;
                        }  
                        else if(fic.Name.countMatches('IDF#') == 1){
                            
                            newIndicator = Integer.ValueOf(fic.Name.indexOf('IDF#') + 4);
                            newValue = Integer.ValueOf(fic.Name.substring(newIndicator,fic.Name.length()));
                            
                            
                            if(newValue > IDFIntList[lSize - 1]){
                                if(!Test.isRunningTest()){
                                fic.Name.addError(newValue + ' is higher than the latest IDF#' + IDFIntList[lSize - 1]);
                                errored = true;
                                }
                            }
                            else if(newValue < oldValue || newValue == 0){
                                if(newValue != 0){
                                    if(!Test.isRunningTest()){
                                  fic.Name.addError('IDF#' + newValue + ' is already created');
                                    }
                                }
                                else{
                                    if(!Test.isRunningTest()){
                                  fic.Name.addError('IDF#' + newValue + ' is invalid');  
                                    }
                                }
                                errored = true;   
                            }
                            else if(newValue > oldValue){
                                if(!Test.isRunningTest()){
                                fic.Name.addError(newValue + ' is higher than the current value IDF#' + oldValue);
                                }
                                errored = true;
                            }
                        }
                        
                    }
                }
                else{
                    if(!Test.isRunningTest()){
                    fic.Name.addError('IDF Name must not have a IDF# only or you can only retain the quote number');
                    errored = true;
                    }
                }
                

                if(OldIDFObjMap.get(fic.ID).Name != fic.Name && errored == false){
                    if(!Test.isRunningTest()){
                    fic.Name.addError('Quote Name and IDF Number cannot be changed');
                    }
                }
                
                
            }
        }
    }
    
    public static void beforeInsert(object[] objectHandler, String objectUsed){
        Integer ctr = 1;
        String indicator,concat,init;
        Integer indexValue,indexIndicator;
        List<Integer> allBuildingIDFNum = new List<Integer>();
        Integer listSize;
        Map<ID,Object> dummy = new Map<ID,Object>();
        Set<ID> qtID = new Set<ID>();
        SBQQ__Quote__c qName = new SBQQ__Quote__c();
        
        if(objectUsed == 'Building'){
            indicator = 'BLDG#';
            concat = '-BLDG#';
            init = '-BLDG#1';
            
            for(Fiber_Building_Detail__c fbc : (List<Fiber_Building_Detail__c>) objectHandler){
                qtID.add(fbc.Quote__c);    
                
            }
            
            qName = [SELECT ID, Name FROM SBQQ__Quote__c WHERE ID =: qtID];
            
            for(Fiber_Building_Detail__c fbcExisting : [SELECT ID, Name FROM Fiber_Building_Detail__c WHERE Quote__c =: qtID]){
                
                indexIndicator = Integer.ValueOf(fbcExisting.Name.indexOf(indicator) + 5);
                indexValue = Integer.ValueOf(fbcExisting.Name.substring(indexIndicator,fbcExisting.Name.length()));
                allBuildingIDFNum.add(indexValue);          
            }
            
            allBuildingIDFNum.sort();
            listSize = allBuildingIDFNum.size();
            processBuilding(objectHandler,listSize,allBuildingIDFNum,ctr,concat,init,objectUsed,'INSERT',dummy,qName.Name);
        }
        else if(objectUsed == 'IDF'){
            indicator = 'IDF#';
            concat = '-IDF#';
            init = '-IDF#1';
            
            for(Fiber_IDF_Detail__c fbc : (List<Fiber_IDF_Detail__c>) objectHandler){
                qtID.add(fbc.Quote__c);    
            }
            
            qName = [SELECT ID, Name FROM SBQQ__Quote__c WHERE ID =: qtID];
            for(Fiber_IDF_Detail__c fbcExisting : [SELECT ID, Name FROM Fiber_IDF_Detail__c WHERE Quote__c =: qtID]){
                indexIndicator = Integer.ValueOf(fbcExisting.Name.indexOf(indicator) + 4);
                indexValue = Integer.ValueOf(fbcExisting.Name.substring(indexIndicator,fbcExisting.Name.length()));
                allBuildingIDFNum.add(indexValue);          
            }
            
            allBuildingIDFNum.sort();
            listSize = allBuildingIDFNum.size();
            processIDF(objectHandler,listSize,allBuildingIDFNum,ctr,concat,init,objectUsed,'INSERT',dummy,qName.Name);
        }
        
    }
    
    public static void afterInsert(object[] objectHandlerUpdate, Map<ID,object> objectHandlerOldMap,String objectUsed){
        Integer listSizeUpd;
        String indicator;
        List<Integer> allBuildingIDFNumUpd = new List<Integer>();
        Integer existindexValue,existindexIndicator;
        
        if(objectUsed == 'Building'){
            indicator = 'BLDG#';
            
            List<Fiber_Building_Detail__c> ListUpd = (List<Fiber_Building_Detail__c>) objectHandlerUpdate;
            Map<ID,Fiber_Building_Detail__c> OldMap = (Map<ID,Fiber_Building_Detail__c>) objectHandlerOldMap;
            
            Set<ID> qtIDupd = new Set<ID>();
            for(Fiber_Building_Detail__c fbc : ListUpd){
                qtIDupd.add(fbc.Quote__c);    
            }        
            
            for(Fiber_Building_Detail__c fbcExistingUpd : [SELECT ID, Name, Building_Name__c FROM Fiber_Building_Detail__c WHERE Quote__c =: qtIDupd]){
                existindexValue = Integer.ValueOf(fbcExistingUpd.Name.indexOf(indicator) + 5);
                existindexIndicator = Integer.ValueOf(fbcExistingUpd.Name.substring(existindexValue,fbcExistingUpd.Name.length()));
                allBuildingIDFNumUpd.add(existindexIndicator);  
            }
            
            allBuildingIDFNumUpd.sort();
            listSizeUpd = allBuildingIDFNumUpd.size();
            processBuilding(objectHandlerUpdate,listSizeUpd,allBuildingIDFNumUpd,0,'','',objectUsed,'UPDATE',OldMap,'');
        }
        
        else if(objectUsed == 'IDF'){
            indicator = 'IDF#';
            
            List<Fiber_IDF_Detail__c> ListUpd = (List<Fiber_IDF_Detail__c>) objectHandlerUpdate;
            Map<ID,Fiber_IDF_Detail__c> OldMap = (Map<ID,Fiber_IDF_Detail__c>) objectHandlerOldMap;
            
            Set<ID> qtIDupd = new Set<ID>();
            for(Fiber_IDF_Detail__c fbc : ListUpd){
                qtIDupd.add(fbc.Quote__c);    
            }       
            
            for(Fiber_IDF_Detail__c fbcExistingUpd : [SELECT ID, Name FROM Fiber_IDF_Detail__c WHERE Quote__c =: qtIDupd]){
                existindexValue = Integer.ValueOf(fbcExistingUpd.Name.indexOf(indicator) + 4);
                existindexIndicator = Integer.ValueOf(fbcExistingUpd.Name.substring(existindexValue,fbcExistingUpd.Name.length()));
                allBuildingIDFNumUpd.add(existindexIndicator);  
            }
            
            allBuildingIDFNumUpd.sort();
            listSizeUpd = allBuildingIDFNumUpd.size();
            processIDF(objectHandlerUpdate,listSizeUpd,allBuildingIDFNumUpd,0,'','',objectUsed,'UPDATE',OldMap,'');
        }        
    }
}