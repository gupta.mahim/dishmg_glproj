public class AmDocs_CalloutClass_CreateSite {

public class person {
    public String siteID;
    public String errorCode;
    public String errorMessage;
    public String backendErrorInfo;
}
public class FlowInputs
{
@invocablevariable
public string id;
}
public class FlowOutputs
{
   @invocablevariable
   public string AmdocsDescription;
   
   @invocablevariable
   Public string API_Status;
   
   @invocablevariable
   Public string Amdocs_Transaction_Code;
   
   @invocablevariable
   Public string Partner_BULK_Mute_UnMute;
   
   @invocablevariable
   Public string AmDocs_FullString_Return;
  
   @invocablevariable
   public string AmDocs_SiteID;
}




    @future(callout=true)

    public static void AmDocsMakeCalloutCreateSite(String Id) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];

list<Opportunity> O = [select id, Name, ThirdPartyBiller__c, AmDocs_tenantType__c, Account.OE_AR__c, AmDocs_ServiceID__c, Amdocs_CustomerID__c, Distributor__c, Phone__c, System_Type__c, Main_Head_End_Account__c, Main_Head_Shared_Physical_Plant__c, Shared_Head_End__c, Account.Id, Account.Name, Account.Distributor__c, Name_of_Property_Representative__c, First_Name_of_Property_Representative__c, Billing_Contact_Phone__c, Billing_Contact_Email__c, OE_AR__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Category__c, AmDocs_Site_Id__c, Address__c, City__c, State__c, Zip__c, Business_Address__c, Business_City__c, Business_State__c, Business_Legal_Name__c, Business_Zip_Code__c, Business_Phone_Number__c, Property_Representative_s_email__c from Opportunity where Id = :id LIMIT 1];
     System.debug('RESULTS of the LIST lookup to the Opp object' +O);

    if(O[0].OE_AR__c == '' || O[0].OE_AR__c == Null ){
        Opportunity sbc = new Opportunity(); {
          sbc.id=id;
          sbc.API_Status__c='ERROR';
          sbc.Amdocs_Transaction_Code__c='CREATE SITE ERROR';
          sbc.Partner_BULK_Mute_UnMute__c = 'Error';
          sbc.Amdocs_Transaction_Description__c='CREATE SITE ERROR: Missing OE #';
      update sbc;
      }

      API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=id;
          apil2.Object__c='Opportunity';
          apil2.Status__c='ERROR';
          apil2.Results__c='Missing OE';
          apil2.API__c='Bulk Create Site';
          apil2.User_Error__c = TRUE;
          apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
          apil2.User__c=UserInfo.getUsername();
      insert apil2;
  }
}
else if(O[0].OE_AR__c != '' || O[0].OE_AR__c != Null ){
  
list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        for(Integer i = 0; i < O.size(); i++){
            jsonObj.writeFieldName('ImplCreateSiteInput');
            jsonObj.writeStartObject();
                jsonObj.writeFieldName('address');
                    jsonObj.writeStartObject();
                        jsonObj.writeStringField('state', O[i].State__c);
                        jsonObj.writeStringField('country', 'US');
                        jsonObj.writeStringField('city', O[i].City__c);
                        jsonObj.writeStringField('addressLine1', O[i].Address__c);    
                        jsonObj.writeStringField('zipCode', O[i].Zip__c);
                    jsonObj.writeEndObject();
                    jsonObj.writeFieldName('siteDetails');
                        jsonObj.writeStartObject();
                            jsonObj.writeStringField('propertySubType',O[i] .AmDocs_Property_Sub_Type__c);
                            jsonObj.writeStringField('bulkInstType', O[i].System_Type__c);
                            jsonObj.writeStringField('sitePhoneNumber', O[i].Phone__c);
                            jsonObj.writeStringField('siteName', O[i].Name); if(O[0].ThirdPartyBiller__c == True || O[0].AmDocs_Customer_Type__c == 'PCO' || O[0].AmDocs_Customer_Type__c == 'Integrator' ){jsonObj.writeStringField('hierarchy', O[i].Account.OE_AR__c); }
                            jsonObj.writeStringField('propertyType', O[i].Category__c);
                            
                            if(O[0].Distributor__c != '' && O[0].Distributor__c != null ){ 
                                if(O[0].Distributor__c == 'Pace' ){ jsonObj.writeStringField('distributor', '21513280'); } 
                                else if(O[0].Distributor__c == 'SMS' ){ jsonObj.writeStringField('distributor', '9325'); }
                                else if(O[0].Distributor__c == '4COM' ){ jsonObj.writeStringField('distributor', '71701'); }
                                else if(O[0].Distributor__c == 'CVS Systems Inc' ){ jsonObj.writeStringField('distributor', '1001'); }
                                else if(O[0].Distributor__c == 'All-Systems Satellite' ){ jsonObj.writeStringField('distributor', '21844'); }
                                else if(O[0].Distributor__c == 'DOW Electronics' ){ jsonObj.writeStringField('distributor', '1007'); }
                                else if(O[0].Distributor__c == 'RS&I' ){ jsonObj.writeStringField('distributor', '1024'); }
                                else if(O[0].Distributor__c == 'Mid-State Distributing' ){ jsonObj.writeStringField('distributor', '23439'); }
                            }

                            jsonObj.writeStringField('tenantType', O[i].AmDocs_tenantType__c);
                            if( O[i].OE_AR__c != '' && O[i].OE_AR__c != null ){
                                jsonObj.writeStringField('dealerCode', O[i].OE_AR__c);
                            }
                        jsonObj.writeEndObject();
      //            jsonObj.writeStringField('accountID','');
      //            jsonObj.writeStringField('parentSiteID','' );
      //            jsonObj.writeFieldName('childSiteID');
                jsonObj.writeEndObject();
            jsonObj.writeEndObject();
String finalJSON = jsonObj.getAsString();
    System.debug('Jerry Debug 0 === json string: '   + finalJSON);
   

    HttpRequest request = new HttpRequest(); 
        String endpoint = A[0].End_Point_Environment__c+'/commerce/customer/'+O[0].Amdocs_CustomerID__c+'/createSite?lo=EN'; 
        request.setEndPoint(endpoint);
        request.setBody(jsonObj.getAsString());   
        request.setHeader('Content-Type', 'application/json'); 
        request.setHeader('Accept', 'application/json'); 
        request.setHeader('User-Agent', 'SFDC-Callout/45.0');
        request.setMethod('POST'); 
        String authorizationHeader = A[0].UXF_Token__c ; 
        request.setHeader('Authorization', authorizationHeader); 
        request.setTimeout(101000);        
    
    HttpResponse response = new HTTP().send(request);
                 System.debug(response.toString());
                 System.debug('STATUS:'+response.getStatus());
                 System.debug('STATUS_CODE:'+response.getStatusCode());
                 System.debug(response.getBody());
                
                 if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { String strjson = response.getbody();
                         System.debug('Jerry Debug 1 === RESPONSE json string: '   + strjson);

JSONParser parser = JSON.createParser(strjson);parser.nextToken(); parser.nextToken(); parser.nextToken(); person obj = (person)parser.readValueAs( person.class);

System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
System.debug('DEBUG 3======== obj.siteID: ' + obj.siteID);

                    
        Opportunity sbc = new Opportunity(); {
            sbc.id=id; sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
            sbc.AmDocs_SiteID__c=obj.siteID;
            if(obj.siteID != Null) 
                { 
                    sbc.AmDocs_Transaction_Code__c='SUCCESS'; 
                    sbc.AmDocs_Transaction_Description__c='SITE CREATED'; }
                    sbc.API_Status__c=String.valueOf(response.getStatusCode());
                    if(obj.siteID == Null)
                        { 
                            sbc.AmDocs_Transaction_Code__c='FAILED: ' +obj.errorCode;
                            sbc.AmDocs_Transaction_Description__c=obj.errorMessage + ' ' +obj.backendErrorInfo;
                        }
                update sbc;
                System.debug('sbc'+sbc);
            }
            
            API_Log__c apil2 = new API_Log__c();{
                apil2.Record__c=id;
                apil2.Object__c='Opportunity';
                apil2.Status__c='SUCCESS';
                apil2.Results__c='SUCCESS';
                apil2.API__c='Bulk Create Site';
                apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
                apil2.User__c=UserInfo.getUsername();
          insert apil2;
          }
      }
      else if (response.getStatusCode() <= 200 && response.getStatusCode() >= 600) { 
          Opportunity sbc = new Opportunity(); { 
              sbc.id=id; sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
              sbc.AmDocs_FullString_Return__c=String.valueOf(response.getBody()) + ' ' + system.now(); 
          update sbc;
          }

          API_Log__c apil2 = new API_Log__c();{
              apil2.Record__c=id;
              apil2.Object__c='Opportunity';
              apil2.Status__c='ERROR';
              apil2.Results__c=String.valueOf(response.getBody()) + ' ' + system.now();
              apil2.API__c='Bulk Create Site';
              apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
              apil2.User__c=UserInfo.getUsername();
          insert apil2;
      }


             }
        
    }
}


}
@InvocableMethod(label='Create Site')
 public static List<FlowOutputs>AmDocsMakeCalloutCreateSitefromFlow(List<FlowInputs>req) {
    
// conList=[SELECT Id,Name,Account.Name,AccountId,Email,Phone FROM Contact];
list<Flowoutputs> result= new list<flowoutputs>();
Flowoutputs output = new FlowOutputs();
list<Opportunity> O = [select id, Name, ThirdPartyBiller__c, AmDocs_tenantType__c, Account.OE_AR__c, AmDocs_ServiceID__c, Amdocs_CustomerID__c, Distributor__c, Phone__c, System_Type__c, Main_Head_End_Account__c, Main_Head_Shared_Physical_Plant__c, Shared_Head_End__c, Account.Id, Account.Name, Account.Distributor__c, Name_of_Property_Representative__c, First_Name_of_Property_Representative__c, Billing_Contact_Phone__c, Billing_Contact_Email__c, OE_AR__c, AmDocs_Property_Sub_Type__c, AmDocs_Customer_Type__c, AmDocs_Customer_SubType__c, Category__c, AmDocs_Site_Id__c, Address__c, City__c, State__c, Zip__c, Business_Address__c, Business_City__c, Business_State__c, Business_Legal_Name__c, Business_Zip_Code__c, Business_Phone_Number__c, Property_Representative_s_email__c from Opportunity where Id = :req[0].id LIMIT 1];
     System.debug('RESULTS of the LIST lookup to the Opp object' +O);

    if(O[0].OE_AR__c == '' || O[0].OE_AR__c == Null ){
        Opportunity sbc = new Opportunity(); {
          sbc.id=req[0].id;
          sbc.API_Status__c='ERROR';
          sbc.Amdocs_Transaction_Code__c='CREATE SITE ERROR';
          output.Amdocs_Transaction_Code='CREATE SITE ERROR';
          sbc.Partner_BULK_Mute_UnMute__c = 'Error';
          sbc.Amdocs_Transaction_Description__c='CREATE SITE ERROR: Missing OE #.';
             output.AmdocsDescription='CREATE SITE ERROR: Missing OE # on Account.';
          
      update sbc;
      }

      API_Log__c apil2 = new API_Log__c();{
          apil2.Record__c=req[0].id;
          apil2.Object__c='Opportunity';
          apil2.Status__c='ERROR';
          apil2.Results__c='Missing OE';
          apil2.API__c='Bulk Create Site';
          apil2.User_Error__c = TRUE;
          apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
          apil2.User__c=UserInfo.getUsername();
      insert apil2;
  }
}
else if(O[0].OE_AR__c != '' || O[0].OE_AR__c != Null ){
  
list<AmDocs_Login__c> A = [select id, UXF_Token__c, End_Point_Environment__c, CreatedDate from AmDocs_Login__c Order By CreatedDate DESC Limit 1 ];
     System.debug('RESULTS of the LIST lookup to the OpportunityLineItem object' +A);     
     
JSONGenerator jsonObj = JSON.createGenerator(true);
    jsonObj.writeStartObject();
        for(Integer i = 0; i < O.size(); i++){
            jsonObj.writeFieldName('ImplCreateSiteInput');
            jsonObj.writeStartObject();
                jsonObj.writeFieldName('address');
                    jsonObj.writeStartObject();
                        jsonObj.writeStringField('state', O[i].State__c);
                        jsonObj.writeStringField('country', 'US');
                        jsonObj.writeStringField('city', O[i].City__c);
                        jsonObj.writeStringField('addressLine1', O[i].Address__c);    
                        jsonObj.writeStringField('zipCode', O[i].Zip__c);
                    jsonObj.writeEndObject();
                    jsonObj.writeFieldName('siteDetails');
                        jsonObj.writeStartObject();
                            jsonObj.writeStringField('propertySubType',O[i] .AmDocs_Property_Sub_Type__c);
                            jsonObj.writeStringField('bulkInstType', O[i].System_Type__c);
                            jsonObj.writeStringField('sitePhoneNumber', O[i].Phone__c);
                            jsonObj.writeStringField('siteName', O[i].Name); if(O[0].ThirdPartyBiller__c == True || O[0].AmDocs_Customer_Type__c == 'PCO' || O[0].AmDocs_Customer_Type__c == 'Integrator' ){jsonObj.writeStringField('hierarchy', O[i].Account.OE_AR__c); }
                            jsonObj.writeStringField('propertyType', O[i].Category__c);
                            
                            if(O[0].Distributor__c != '' && O[0].Distributor__c != null ){ 
                                if(O[0].Distributor__c == 'Pace' ){ jsonObj.writeStringField('distributor', '21513280'); } 
                                else if(O[0].Distributor__c == 'SMS' ){ jsonObj.writeStringField('distributor', '9325'); }
                                else if(O[0].Distributor__c == '4COM' ){ jsonObj.writeStringField('distributor', '71701'); }
                                else if(O[0].Distributor__c == 'CVS Systems Inc' ){ jsonObj.writeStringField('distributor', '1001'); }
                                else if(O[0].Distributor__c == 'All-Systems Satellite' ){ jsonObj.writeStringField('distributor', '21844'); }
                                else if(O[0].Distributor__c == 'DOW Electronics' ){ jsonObj.writeStringField('distributor', '1007'); }
                                else if(O[0].Distributor__c == 'RS&I' ){ jsonObj.writeStringField('distributor', '1024'); }
                                else if(O[0].Distributor__c == 'Mid-State Distributing' ){ jsonObj.writeStringField('distributor', '23439'); }
                            }
                            
                            jsonObj.writeStringField('tenantType', O[i].AmDocs_tenantType__c);
                            if( O[i].OE_AR__c != '' && O[i].OE_AR__c != null ){
                                jsonObj.writeStringField('dealerCode', O[i].OE_AR__c);
                            }
                        jsonObj.writeEndObject();
      //            jsonObj.writeStringField('accountID','');
      //            jsonObj.writeStringField('parentSiteID','' );
      //            jsonObj.writeFieldName('childSiteID');
                jsonObj.writeEndObject();
            jsonObj.writeEndObject();
String finalJSON = jsonObj.getAsString();
    System.debug('Jerry Debug 0 === json string: '   + finalJSON);
   

    HttpRequest request = new HttpRequest(); 
        String endpoint = A[0].End_Point_Environment__c+'/commerce/customer/'+O[0].Amdocs_CustomerID__c+'/createSite?lo=EN'; 
        request.setEndPoint(endpoint);
        request.setBody(jsonObj.getAsString());   
        request.setHeader('Content-Type', 'application/json'); 
        request.setHeader('Accept', 'application/json'); 
        request.setHeader('User-Agent', 'SFDC-Callout/45.0');
        request.setMethod('POST'); 
        String authorizationHeader = A[0].UXF_Token__c ; 
        request.setHeader('Authorization', authorizationHeader); 
        request.setTimeout(101000);        
    
    HttpResponse response = new HTTP().send(request);
                 System.debug(response.toString());
                 System.debug('STATUS:'+response.getStatus());
                 System.debug('STATUS_CODE:'+response.getStatusCode());
                 System.debug(response.getBody());
                
                 if (response.getStatusCode() >= 200 && response.getStatusCode() <= 600) { String strjson = response.getbody();
                         System.debug('Jerry Debug 1 === RESPONSE json string: '   + strjson);

JSONParser parser = JSON.createParser(strjson);parser.nextToken(); parser.nextToken(); parser.nextToken(); person obj = (person)parser.readValueAs( person.class);

System.debug('DEBUG 0 ======== 1st STRING: ' + strjson);
System.debug('DEBUG 3======== obj.siteID: ' + obj.siteID);

                    
        Opportunity sbc = new Opportunity(); {
            sbc.id=req[0].id; sbc.AmDocs_FullString_Return__c=strjson + ' ' + system.now(); 
            sbc.AmDocs_SiteID__c=obj.siteID;
            if(obj.siteID != Null) 
                { 
                    sbc.AmDocs_Transaction_Code__c='SUCCESS'; 
                    output.Amdocs_Transaction_Code='SUCCESS';
                    sbc.AmDocs_Transaction_Description__c='SITE CREATED'; 
                    output.AmdocsDescription='SITE CREATED';
                    }
                    sbc.API_Status__c=String.valueOf(response.getStatusCode());
                    if(obj.siteID == Null)
                        { 
                            sbc.AmDocs_Transaction_Code__c='FAILED: ' +obj.errorCode;
                            output.Amdocs_Transaction_Code='FAILED: ' +obj.errorCode;
                            sbc.AmDocs_Transaction_Description__c=obj.errorMessage + ' ' +obj.backendErrorInfo;
                            output.AmdocsDescription=obj.errorMessage + ' ' +obj.backendErrorInfo;
                        }
                update sbc;
                System.debug('sbc'+sbc);
            }
            
            API_Log__c apil2 = new API_Log__c();{
                apil2.Record__c=req[0].id;
                apil2.Object__c='Opportunity';
                apil2.Status__c='SUCCESS';
                apil2.Results__c='SUCCESS';
                apil2.API__c='Bulk Create Site';
                apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
                apil2.User__c=UserInfo.getUsername();
          insert apil2;
          }
      }
      else if (response.getStatusCode() <= 200 || response.getStatusCode() >= 600) { 
          Opportunity sbc = new Opportunity(); { 
              sbc.id=req[0].id; sbc.API_Status__c=String.valueOf(response.getStatusCode()); 
              sbc.AmDocs_FullString_Return__c=String.valueOf(response.getBody()) + ' ' + system.now(); 
          update sbc;
          }

          API_Log__c apil2 = new API_Log__c();{
              apil2.Record__c=req[0].id;
              apil2.Object__c='Opportunity';
              apil2.Status__c='ERROR';
              apil2.Results__c=String.valueOf(response.getBody()) + ' ' + system.now();
              apil2.API__c='Bulk Create Site';
              apil2.ServiceID__c=O[0].AmDocs_ServiceID__c;
              apil2.User__c=UserInfo.getUsername();
          insert apil2;
      }


             }
        
    }
}
result.add(output);
return result;
}

}