trigger BTVNAportalCase on Opportunity (After Insert) {
List<Case> cas = new List<Case>();
    for (Opportunity opp: Trigger.New)
         if (opp.BTVNA_Request_Type__c == 'Site Survey' || opp.TVs_Installed__c == 'no' || opp.Landlord_Permission__c =='no' || opp.Point_of_Entry__c == 'no' || opp.Roof_Access__c == 'no' ){
                 cas.add (new Case(
                    AccountId = opp.AccountId,
                    Status = 'Form Submitted',
                    Origin = 'BTVNA Portal',
                    Opportunity__c = opp.Id,
                    Misc_Code_SIU__c = opp.Misc_Code_SIU__c,
                    RequestedAction__c = opp.BTVNA_Request_Type__c,
                    Requested_Actvation_Date_Time__c = opp.Required_Installation_Date__c,
                    Property_Type__c = opp.Property_Type__c,
                    Number_of_Accounts__c = 1,
                    RecordTypeId = '0126000000017Vk',
                    OwnerId = '00G60000001EqrG'));   
         }
             for (Opportunity opp: Trigger.New)
         if (opp.BTVNA_Request_Type__c == 'Installation/Activation' && opp.TVs_Installed__c == 'yes' && opp.Landlord_Permission__c == 'yes' && opp.Point_of_Entry__c == 'yes' && opp.Roof_Access__c == 'yes'){ cas.add (new Case( AccountId = opp.AccountId, Status = 'Form Submitted', Origin = 'BTVNA Portal', Opportunity__c = opp.Id, Misc_Code_SIU__c = opp.Misc_Code_SIU__c, RequestedAction__c = opp.BTVNA_Request_Type__c, Requested_Actvation_Date_Time__c = opp.Required_Installation_Date__c, Property_Type__c = opp.Property_Type__c, Number_of_Accounts__c = 1, RecordTypeId = '0126000000017Vk', OwnerId = '00G60000001Na1r'));   }

   insert cas;
 }