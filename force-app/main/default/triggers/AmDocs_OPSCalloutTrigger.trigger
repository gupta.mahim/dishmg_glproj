trigger AmDocs_OPSCalloutTrigger on Opportunity (before update) {

// Processes for SF->AmDocs to call @Future API callout triigers.
Opportunity S = trigger.new[0];

// UpdateBulk Everything at once except attributes
if(S.AmDocs_BulkUpdateAll__c == True)
    { 
        S.API_Status__c = ''; 
        S.AmDocs_Transaction_Code__c = ''; 
        S.AmDocs_Transaction_Description__c = ''; 
        S.AmDocs_FullString_Return__c = ''; 
        S.AmDocs_BulkUpdateAll__c = false; 
        AmDocs_CalloutClass_BulkUpdateAll.AmDocsMakeCalloutBulkUpdateAll(S.Id); 
            System.debug('IF 1' +S);
    }
else

// UpdateBulk - Resend a trip/hit to everything
if(S.AmDocs_ReSendAll__c == True)
    { 
        S.API_Status__c = ''; 
        S.AmDocs_Transaction_Code__c = ''; 
        S.AmDocs_Transaction_Description__c = ''; 
        S.AmDocs_FullString_Return__c = ''; 
        S.AmDocs_ResendAll__c = false; 
        AmDocs_CalloutClass_ReSendAll.AmDocsMakeCalloutReSendAll(S.Id); 
        System.debug('IF 4' +S);
    }
else

// UpdateBulk - Attributes
if(S.AmDocs_UpdateBulkAttribute__c == True)
    {
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_BulkUpdateAttributes.AmDocsMakeCalloutBulkUpdateAttributes(S.Id);
            System.debug('IF 5' +S);
    }
else

// Partner Mute 
if( S.Partner_BULK_Mute_UnMute__c == 'Mute')
    {
        S.Partner_BULK_Mute_UnMute__c ='Mute Pending';
        S.Mute_Disconnect__c = 'Mute Pending';
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(S.Id);
            System.debug('IF 6' +S);
    }
else

// Partner UnMute 
if( S.Partner_BULK_Mute_UnMute__c == 'UnMute')
    {
        S.Partner_BULK_Mute_UnMute__c ='UnMute Pending';
        S.Mute_Disconnect__c = 'UnMute Pending';
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(S.Id);
            System.debug('IF 6' +S);
    }
else

// DISH Mute 
if( S.Mute_Disconnect__c == 'Mute')
    {
        S.Partner_BULK_Mute_UnMute__c ='Mute Pending';
        S.Mute_Disconnect__c = 'Mute Pending'; 
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(S.Id);
            System.debug('IF 6' +S);
    }
else

// DISH UnMute 
if( S.Mute_Disconnect__c == 'UnMute')
    {
        S.Partner_BULK_Mute_UnMute__c ='UnMute Pending';
        S.Mute_Disconnect__c = 'UnMute Pending';
        S.API_Status__c = ''; 
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = ''; 
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(S.Id);
            System.debug('IF 6' +S);
    }
else

// DISH Non-Pay Disconnect
if( S.Mute_Disconnect__c == 'Non-Pay Disconnect')
    {
        S.Partner_BULK_Mute_UnMute__c ='Non-Pay Disconnect Pending';
        S.Mute_Disconnect__c = 'Non-Pay Disconnect Pending';
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(S.Id); 
            System.debug('IF 6' +S);
    }
else

// DISH Non-Pay Resume
if( S.Mute_Disconnect__c == 'Non-Pay Resume')
    {
        S.Partner_BULK_Mute_UnMute__c ='Non-Pay Resume Pending'; 
        S.Mute_Disconnect__c = 'Non-Pay Resume Pending'; 
        S.API_Status__c = ''; 
        S.AmDocs_Transaction_Code__c = ''; 
        S.AmDocs_Transaction_Description__c = ''; 
        S.AmDocs_FullString_Return__c = ''; 
        S.AmDocs_UpdateBulkAttribute__c = false; 
        AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(S.Id);
            System.debug('IF 6' +S);
    }
else

// Disconnect
if( S.Mute_Disconnect__c == 'Disconnect')
    {
        S.Partner_BULK_Mute_UnMute__c ='Disconnect Pending';
        S.Mute_Disconnect__c = 'Disconnect Pending';
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(S.Id); 
            System.debug('IF 6' +S);
    }
else

// DISH Re-Connect
if( S.Mute_Disconnect__c == 'Re-Connect')
    {
        S.Partner_BULK_Mute_UnMute__c ='Re-Connect Pending';
        S.Mute_Disconnect__c = 'Re-Connect Pending';
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_BulkMuteDC.AmDocsMakeCalloutBulkMuteDC(S.Id);
            System.debug('IF 6' +S);
    }
else


// CreateCustomer - Used to create the customer account and financial account.
if(S.AmDocs_CreateCustomer__c == True)
    {
        S.AmDocs_CreateCustomer__c = False;
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_CreateCustomer.AmDocsMakeCalloutCreateCustomer(S.Id);
            System.debug('IF 7' +S);
    }
else

// Create Site / Property
if(S.AmDocs_CreateSite__c == True)
    {
        S.AmDocs_CreateSite__c = False;
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_CreateSite.AmDocsMakeCalloutCreateSite(S.Id);
            System.debug('IF 8' +S);
    }
else

// Create a Bulk on  a SITE
if(S.AmDocs_CreateBulk__c == True)
    {
        S.AmDocs_CreateBulk__c = False;
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_BulkCreate.AmDocsMakeCalloutBulkCreate(S.Id);
            System.debug('IF 9' +S);
    }
else

// Create a Bulk - OFFLINE
//if(S.AmDocs_CreateBulk__c == True){ S.AmDocs_CreateBulk__c = False; S.API_Status__c = '000'; S.AmDocs_Transaction_Code__c = '000'; S.AmDocs_Transaction_Description__c = 'The Bulk Create function is currently OFFLINE. Function will return 6am Sunday Decemeber 9th 2018'; S.AmDocs_FullString_Return__c = ''; S.AmDocs_UpdateBulkAttribute__c = false; AmDocs_CalloutClass_BulkCreate.AmDocsMakeCalloutBulkCreate(S.Id); System.debug('IF 9' +S);}
//else

// Customer Master - Create Customer
if(S.CM_Create_Customer__c == True) {S.CM_Create_Customer__c = False;S.API_Status__c = '';S.AmDocs_Transaction_Code__c = '';S.AmDocs_Transaction_Description__c = '';S.AmDocs_FullString_Return__c = '';S.AmDocs_UpdateBulkAttribute__c = false;CMaster_CalloutClass_CreateCustomerOPP.CMasterCalloutCreateCustomerOPP(S.Id);System.debug('IF 9' +S); }
else

// Customer Master - Create Login
if(S.CM_Create_Login__c == True)
    {
        S.CM_Create_Login__c = False;
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        CMaster_CalloutClass_CreateLoginOPP.CMasterCalloutCreateLoginOPP(S.Id); 
            System.debug('IF 9' +S);
    }
else

//Customer Master - Create PIN
if(S.CM_AddSecurityPin__c == True)
    {
        S.CM_AddSecurityPin__c = False;
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        CMaster_CalloutClass_CreateCstmerPINOPP.CMasterCalloutCreateCstmrPINOPP(S.Id);
            System.debug('IF 9' +S);
    }
else

// Customer Master - Search
if(S.CM_UpdateFromCustomerMaster__c == True)
    {
        S.CM_UpdateFromCustomerMaster__c = False;
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        CMaster_CalloutClassOPP_Search.CMasterCalloutSearchOPPCustomer(S.Id);
            System.debug('IF 9' +S);
    }
else

// Amdocs - Bulk UPDATE NONE
if(S.Amdocs_HIDDEN_BulkUpdateNONE__c == True)
    {
        S.Amdocs_HIDDEN_BulkUpdateNONE__c = False;
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = '';
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        AmDocs_CalloutClass_BulkUpdateNONE.AmDocsMakeCalloutBulkUpdateNONE(S.Id);
            System.debug('IF 9' +S);
    }
 else

// Customer Master - Forgot Username
if(S.CM_Forgot_Username__c == True)
    {
        S.CM_Forgot_Username__c = False; 
        S.API_Status__c = ''; 
        S.AmDocs_Transaction_Code__c = ''; 
        S.AmDocs_Transaction_Description__c = ''; 
        S.AmDocs_FullString_Return__c = ''; 
        S.AmDocs_UpdateBulkAttribute__c = false; 
        CMaster_CalloutClassForgotUsernameOPP.CMasterCalloutForgotUserNameOPP(S.Id);
            System.debug('IF 9' +S);
    }
else

// Customer Master - Change Password
if(S.Update_Password__c == True) 
    {   
        S.Update_Password__c = False;
        S.API_Status__c = '';
        S.AmDocs_Transaction_Code__c = ''; 
        S.AmDocs_Transaction_Description__c = '';
        S.AmDocs_FullString_Return__c = '';
        S.AmDocs_UpdateBulkAttribute__c = false;
        CMaster_CalloutClass_CreateOPPPWU.CMasterCalloutCreateOPPPWU(S.Id);
        System.debug('IF 9' +S);
    }
else

// Get Tenant by Bulk
if(S.AmDocs_GetTenantByBulk__c == True){S.AmDocs_GetTenantByBulk__c = False; S.API_Status__c = ''; S.AmDocs_Transaction_Code__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_FullString_Return__c = ''; S.AmDocs_UpdateBulkAttribute__c = false; GetTenantByBulkCtrl.UpdateOppApiStatus(S.Id); System.debug('IF 9' +S);}    
else

// Request MASS Tenant Equipment (Digital BULK) Resend All / Trip
if(S.Amdocs_ResendAllDigitalBulk__C == True)
    {
        S.Amdocs_ResendAllDigitalBulk__c = False;
        S.API_Status__c = ''; 
        S.AmDocs_Transaction_Code__c = ''; 
        S.AmDocs_Transaction_Description__c = ''; 
        S.AmDocs_FullString_Return__c = ''; 
        S.AmDocs_UpdateBulkAttribute__c = false; 
        AmDocs_ReSendAllDigitalBulk_Prep.AmDocsMakeReSendAll(S.Id);
            System.debug('IF 10' +S);
    }
else

// Check the ORDER status in Amdocs.
if(S.Amdocs_ChkOrderStatus__c == True)
    {
        S.Amdocs_ChkOrderStatus__c = False;
        S.API_Status__c = ''; 
        S.AmDocs_Transaction_Code__c = ''; 
        S.AmDocs_Transaction_Description__c = ''; 
        S.AmDocs_FullString_Return__c = ''; 
        AmDocs_CalloutClass_OPPOrderIDCheck.AmDocsCalloutClassOPPOrderIDCheck(S.Id);
            System.debug('IF 11' +S);
    }
else{}

}