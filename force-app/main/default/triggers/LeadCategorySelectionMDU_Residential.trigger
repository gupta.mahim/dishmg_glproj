trigger LeadCategorySelectionMDU_Residential on Lead (Before Update, Before Insert) { 
Lead L = trigger.new[0];
if(L.Type_of_Business__c == 'MDU Residential' && L.Category__c != 'MDU Residential')
{L.Category__c = 'MDU Residential';} 
// else nothing
}