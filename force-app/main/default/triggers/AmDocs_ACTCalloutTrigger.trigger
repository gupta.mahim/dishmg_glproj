trigger AmDocs_ACTCalloutTrigger on Account (before update) {

// Processes for SF->AmDocs to call @Future API callout triigers.
Account S = trigger.new[0];

// CreateCustomer - Used to create the customer account and financial account.
if(S.AmDocs_CreateCustomer__c == True)
{ S.AmDocs_CreateCustomer__c = False; S.API_Status__c = ''; S.AmDocs_Transaction_Code__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_FullString_Return__c = ''; AmDocs_CalloutClassAcct_CreateCustomer.AmDocsMakeCalloutCreateCustomer(S.Id); System.debug('IF 7' +S);}
else
// CreateCustomer PR/VI
if(S.AmDocs_CreateCustomerPRVI__c == True)
{S.AmDocs_CreateCustomerPRVI__c = False; S.API_Status__c = ''; S.AmDocs_Transaction_Code__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_FullString_Return__c = ''; AmDocs_CalloutClassAcct_CrtCustmrPRVI.AmDocsMakeCalloutCreateCustomer(S.Id); System.debug('IF 7' +S);}
// else
}