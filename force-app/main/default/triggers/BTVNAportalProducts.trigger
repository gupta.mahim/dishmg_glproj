trigger BTVNAportalProducts on Opportunity (After Insert) {

List<OpportunityLineItem> toInsert = new List<OpportunityLineItem>();

 for (Opportunity opp: Trigger.New)
     if (opp.Restricted_Programming__c == 'True' && opp.Property_type__c == 'Private'){account a = [SELECT ID, Name, Programming__c  from Account where Id = :opp.AccountId]; String[] progs = a.programming__c.split(';'); for(PriceBookEntry d:[select Id, UnitPrice, Name from pricebookEntry where Name = :progs and Pricebook2Id = '01s600000001w1DAAQ'])     { toInsert.add(new opportunityLineItem(OpportunityId=opp.Id, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice, Quantity=1, Status__c='Add Requested'));} insert toInsert;
}
 for (Opportunity opp: Trigger.New)
     if (opp.Restricted_Programming__c == 'True' && opp.Property_type__c == 'Public'){ account a = [SELECT ID, Name, Programming__c  from Account where Id = :opp.AccountId];  String[] progs = a.programming__c.split(';'); for(PriceBookEntry d:[select Id, UnitPrice, Name from pricebookEntry where Name = :progs and Pricebook2Id = '01s600000001w18AAA']) { toInsert.add(new opportunityLineItem(OpportunityId=opp.Id, PriceBookEntry=d, PriceBookEntryId=d.Id, UnitPrice=d.UnitPrice, Quantity=1, Status__c='Add Requested'));       }      insert toInsert;}}