trigger LeadCategorySelectionPublic on Lead (Before Update, Before Insert) { 
Lead L = trigger.new[0];
if(L.Type_of_Business__c == 'Retirement Community' && L.Location_of_Service__c == 'Common Area' && L.Category__c != 'Public')
{L.Category__c = 'Public';} 
else
if(L.Type_of_Business__c == 'Assisted Living' && L.Location_of_Service__c == 'Common Area' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c == 'Common Area' && L.Category__c != 'Public'){L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'University' && L.Location_of_Service__c == 'Common Area' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Common Area' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Food Service' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'RV Park' && L.Location_of_Service__c == 'Common Area' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'RV Park' && L.Location_of_Service__c == 'Food Service' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'RV Park' && L.Location_of_Service__c == 'Other' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Marinas' && L.Location_of_Service__c == 'Common Area' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Marinas' && L.Location_of_Service__c == 'Food Service' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Marinas' && L.Location_of_Service__c == 'Other' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Casinos' && L.Location_of_Service__c == 'Common Area' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Beauty Salon' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Airports' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Lobbies' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Golf Course' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Bar/Restaurant' && L.Category__c != 'Public')
{L.Category__c = 'Public';}
else
if(L.Type_of_Business__c == 'Bars/Restaurants' && L.Category__c != 'Public')
{L.Category__c = 'Public'; L.Type_of_Business__c = 'Bar/Restaurant';}
// else nothing
}