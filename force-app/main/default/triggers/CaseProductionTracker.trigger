trigger CaseProductionTracker on Case (before update) {     
List<Production_Tracker__c> sr = new List<Production_Tracker__c>();
    for (Case c: Trigger.New){
   
    
        
if (((c.Status == 'Request Completed' || c.Status == 'Rejected' || c.Delinquent__c == '' ) && c.Origin == 'PRM' && c.Opportunity__c != null) || (c.Status == 'Pre-Build Complete' && c.Origin != 'BTVNA Portal' || c.Delinquent__c == '' ) ){
         Opportunity o = [SELECT Id, Prin_Hidden__c, Number_of_Units__c, Misc_Code_SIU__c, HIU__c, 
             CSG_Account_Number__c  FROM Opportunity WHERE Id = :c.Opportunity__c]; 
                 sr.add (new Production_Tracker__c(
                     Account_Name__c = c.Opprtunity_Name_Hidden__c,
                     Request_Type__c = c.RequestedAction__c,
                     Account_Number__c = c.CSG_Account_Number__c,
                     Number_of_Accounts__c = c.Number_of_Accounts__c,
                     Email_Subject__c = o.Prin_Hidden__c + '/' + o.Misc_Code_SIU__c + '/' + o.HIU__c + '/' + c.Opprtunity_Name_Hidden__c + '/' + c.RequestedAction__c + '/' + c.Status,
                     Date_Received__c = c.CreatedDate,
                     Prin__c = o.Prin_Hidden__c,
                     Number_of_Drops__c = o.Number_of_Units__c,
                     Future_Date__c = c.Requested_Actvation_Date_Time__c,
                     SIU__c = o.Misc_Code_SIU__c));   
         }
   insert sr;

 }}