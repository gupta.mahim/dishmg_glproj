trigger AmenityCreateCase on Front_Office_Request__c (After Update) {


for (Front_Office_Request__c fo: Trigger.New) {

if(fo.Front_Office_Decision__c == 'Approved' && fo.Request_Type__c=='Amenity Incentive Verification' ){

List<Case> cs = new List<Case>();

Opportunity o = [SELECT Id, Name, Property_Status__c, RecordTypeId, Launch_Date__c FROM Opportunity WHERE Id = :fo.Opportunity__c];
Case[] c = [Select Id, RequestedAction__c from Case where Opportunity__c = :o.Id ];

if (c.size()==0 && fo.Front_Office_Decision__c == 'Approved' && fo.Request_Type__c=='Amenity Incentive Verification' && fo.RecordTypeId == '0126000000017k7'){


cs.add(new Case(
     ownerId='00G60000001Na1r',
     AccountId=fo.Account__c,
     Opportunity__c=fo.Opportunity__c,
     RequestedAction__c='Activation',
     Requested_Actvation_Date_Time__c = o.Launch_Date__c,
     Status='Form Submitted',
     RecordTypeId='0126000000017LQ'));
     
     
// Messaging.reserveSingleEmailCapacity(2);        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); String[] toAddresses = new String[] {'dishnetworkcommercialoperations@dish.com'};mail.setToAddresses(toAddresses);mail.setReplyTo('salesforceadmin@dish.com');mail.setSenderDisplayName('Salesforce Admin');mail.setSubject('Amenity Verification Request has been approved for ' + o.Name);mail.setBccSender(false);mail.setUseSignature(false); mail.setPlainTextBody('Amenity Verification Request has been approved for ' + o.Name + ' Link: https://na4.salesforce.com/' + o.Id);Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });     
     
}
insert cs;


}
}}