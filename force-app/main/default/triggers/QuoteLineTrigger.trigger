trigger QuoteLineTrigger on SBQQ__QuoteLine__c (before insert, before update ) {
    if (Trigger.isBefore) {
    if (Trigger.isInsert){
            System.debug('Insert');
            QuoteLineTriggerHandler.handleQuoteLines(Trigger.New);
            //dlrs.RollupService.triggerHandler(SBQQ__QuoteLine__c.SObjectType);
        }
        if(Trigger.isUpdate){ 
                
                QuoteLineTriggerHandler.handleQuoteLines(Trigger.New);
            }
        
    }
}