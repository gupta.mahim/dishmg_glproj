trigger CaseProductStatus2 on Case (After Insert) {

Case cas = trigger.new[0];
 //   if (cas.Opportunity__r.Hidden_Case_History__c == True && cas.Opportunity__c != null && cas.Status == 'Form Submitted' && cas.Hidden_Case_History__c == False &&(cas.RequestedAction__c == 'Activation' || cas.RequestedAction__c == 'Change' ||  cas.RequestedAction__c == 'Installation/Activation' ||  cas.RequestedAction__c == 'Pre-Activation' ||   cas.RequestedAction__c == 'Restart' ||   cas.RequestedAction__c == 'Two (2) Hour Activation' ||   cas.RequestedAction__c == 'Pre-Build' || cas.RequestedAction__c == 'Disconnect' ))
        {
            Set<Id> caseIds = new Set<Id>();
            Set<Id> opIds = new Set<Id>();
               opIds.add(cas.Opportunity__c);
               caseIds.add(cas.Id);
        if ( Cas.RecordTypeID == '01260000000YLR2' || Cas.RecordTypeID == '012f20000005Pu4' || Cas.RecordTypeID == '0126000000017LQ' || Cas.RecordTypeID == '012600000001Ccy' || Cas.RecordTypeID == '0126000000018w6' || Cas.RecordTypeID == '01260000000JO8Y')
        {
        list<opportunityLineItem> xoli = [select id, OpportunityId, Status__c, Submitted__c, CaseID__c from opportunityLineItem where OpportunityId in :opIds AND CaseID__C not in :caseIds AND (Status__c = 'Add Requested' OR Status__c = 'Remove Requested') AND Submitted__c != True ];
        list<case> xc2 = [select id, Status, CaseNumber, Hidden_Case_History__c, Product_Change_Request_s__c, Requested_Actvation_Date_Time__c from case where Id in :caseIds AND Hidden_Case_History__c = False];
        list<opportunity> xopp = [select id, Hidden_Case_History__c, Hidden_Products__c, Property_Status__c, Last_Case_Email_Address__c, Claim_Number__c, Launch_Date__c, Disconnect_Date__c, StageName, Activation_Timer_On__c from opportunity where Id in :opIds AND Hidden_Case_History__c = True ];
        
        if(xoli.size() > 0 && xc2.size() > 0 && xopp.size() > 0)
            {
            for (opportunityLineItem xoli2: xoli) 
                {
                    if(xoli2.Status__c == 'Add Requested' && xoli2.CaseID__c == NULL)
                        {
                           xoli2.CaseID__c = xc2[0].Id;
                           xoli2.Change_Schedule__c = xc2[0].Requested_Actvation_Date_Time__c;
                           xoli2.Submitted__c = True;
                        }
                    if(xoli2.Status__c == 'Remove Requested' && xoli2.CaseID__c == NULL)
                        {
                           xoli2.CaseID__c = xc2[0].Id;
                           xoli2.Change_Schedule__c = xc2[0].Requested_Actvation_Date_Time__c;
                           xoli2.Submitted__c = True;
                        }
            for (opportunity xopp2: xopp) 
                {
                    if(xopp2.Hidden_Case_History__c == True && (xoli2.Status__c == 'Add Requested' || xoli2.Status__c == 'Remove Requested'))
                        {
                           xopp2.Hidden_Case_History__c = False; xopp2.Hidden_Products__c = Null;
                        }
               }
           }
        }
        {
            Database.update(xoli);
            Database.update(xopp);
        }
    }
}}