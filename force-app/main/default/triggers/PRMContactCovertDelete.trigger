// Deletes all PRM Contacts created - needed in order to remove contacts created on convert.

trigger PRMContactCovertDelete on Contact (After Update) {

Contact CD = trigger.new[0];
//    if (CD.RecordTypeId == '012600000001B8p' && CD.Delete__c == True )
        if ( CD.Status__c == 'Delete' )        {            Set<Id> CTCid = new Set<Id>();                   CTCid.add(CD.id);                   list<Contact> CD3 = [select id from Contact where ID in :CTCid ];            Database.delete(CD3);
            }}