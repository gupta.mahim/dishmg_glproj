trigger SBoxEquipmentRecallRejectDelete on Smartbox__c (After Update) 
{
Smartbox__c sbeqa = trigger.new[0];
    if (sbeqa.Opportunity__c != null && sbeqa.Flags__c == 'Delete' )
       {    Set<Id> sbeqaIds = new Set<Id>();           
            Set<Id> sbopIds = new Set<Id>();               
     for (Smartbox__c sbeqa2: Trigger.New)
     {      sbopIds.add(sbeqa2.Opportunity__c);                    
            sbeqaIds.add(sbeqa2.Id);        
     }           
            list<Smartbox__c> sbeqa3 = [select id, Flags__c, Status__c, Opportunity__c from Smartbox__c where ID in :sbeqaIds AND Flags__c = 'Delete' ];            Database.delete(sbeqa3);
    
       }
}