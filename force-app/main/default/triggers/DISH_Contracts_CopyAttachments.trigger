// This should copy all attachments from the DISH Contract record to the DISH Contract record specified in the field Related DISH Contract.
trigger DISH_Contracts_CopyAttachments on DISH_Contract__c (after update) {
    
    // Map<Id,DISH_Contract__c> dcMapp=new  Map<Id,DISH_Contract__c> ([select Id, Status__c, Related_DISH_Contract__c from DISH_Contract__c where Id = :Trigger.new[0].Id]);
    list<DISH_Contract__c> dc = [select id, Status__c, Related_DISH_Contract__c from DISH_Contract__c where Id = :Trigger.new[0].Id];
    String recordID = Trigger.new[0].Id+'';
    if(dc[0].Status__c == 'Send attachment to parent contract record for E-Signature')
        
    {
        
        for(Dish_Contract__c dcu: dc)
            
        {
            
            dcu.Status__c = 'Attachment has been sent to the parent contract record for E-Signature';
            
        }
        ID acctID ;
        Attachments_Classic_Lightning__c obj = Attachments_Classic_Lightning__c.getOrgDefaults();
        System.debug('KA:: Is File Upload: '+  obj.Is_File_Upload__c);
        if(obj.Is_File_Upload__c == true)
        {
            //DISH_Contract__c accontdata = [select account__c from DISH_Contract__c where id =:Trigger.new[0].Id];
            //Attachment[] attList = [select id, name, Description, body  from Attachment where ParentId = :Trigger.new[0].Id];
            ContentDocumentLink[] contDocLinkList = [select id, LinkedEntityId, ContentDocumentId from ContentDocumentLink  where LinkedEntityId=:dc[0].id ];
            //System.debug('KA:: contDocLinkList Size' + contDocLinkList.size());
            List<ContentDocumentLink> insertLinks = new List<ContentDocumentLink>();
            List<ContentDocumentLink> deleteLinks = new List<ContentDocumentLink>();
            if(contDocLinkList.size() > 0)
            {
                for(ContentDocumentLink condcLink : contDocLinkList )
                {
                    ContentDocumentLink newclnk = condcLink.clone();
                    ContentDocument contDocList = [Select id, title from ContentDocument where id=:condcLink.ContentDocumentId];
                    String s1 = contDocList.title;
                    // System.debug('KA:: '+s1);
                    String s2 = s1.substringBeforeLast('.');
                    String s3 = s2.substringBeforeLast('.');
                    String s4 = s3.Right(18);
                    acctID = s4;
                    System.debug('KA Account ID: ' + acctID);
                    newclnk.LinkedEntityId = dc[0].Related_DISH_Contract__c;
                    newclnk.ShareType = 'V';
                    insertLinks.add(newclnk);
                    deleteLinks.add(condcLink);
                }
                if(insertLinks.size() > 0)
                {
                    try{
                        insert insertLinks; 
                        delete deleteLinks;
                    }catch(DmlException ex)
                    {
                        System.debug('KA:: DmlException ' + ex.getMessage());
                    }
                    
                }   
            }   
        }
        else
        {
            Attachment[] attList = [select id, name, body from Attachment where ParentId = :dc[0].Related_DISH_Contract__c];
            
            Attachment[] insertAttList = new Attachment[]{};
   
            for(Attachment a: attList)
            {
                
                Attachment att = new Attachment(name = a.name, body = a.body, parentid = dc[0].Related_DISH_Contract__c);
                
                insertAttList.add(att);
                
            }
            if(insertAttList.size() > 0)
                
            {
                
                insert insertAttList;
                delete attList;
                update dc;
                
            }
        }
    }
}