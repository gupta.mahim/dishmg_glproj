trigger SmartBoxEquipment_HiddenFieldOpportunity on Smartbox__c (After Insert, After Update) {

Set<Id> oppId=new Set<Id>();
Set<Id> eqId=new Set<Id>();
for(Smartbox__c CA1:Trigger.New)    {
    oppId.add(CA1.Opportunity__c);
    eqId.add(CA1.Id);
}//End of for loop

if(oppId.size()>0){         
    List<Opportunity> opp = [select Id, (select id, Name, status__c, Serial_Number__c, Type_of_Equipment__c, Part_Number__c, CAID__c, SmartCard__c, Opportunity__c, Chassis_Serial__c, lastModifiedDate, LastModifiedBy.Name from smartbox__r where (status__c = 'Activation Requested' OR status__c = 'Drop Requested' ) AND CaseID__c = null) from Opportunity where Id in :oppId Limit 1 ];
        String OI = [select Opportunity__c from Smartbox__c where Opportunity__c in :oppId LIMIT 1 ].Opportunity__c ;
    List<Smartbox__c> SB = [select id, Name, status__c, Serial_Number__c, Type_of_Equipment__c, Part_Number__c, CAID__c, SmartCard__c, Opportunity__c, Chassis_Serial__c, lastModifiedDate, LastModifiedBy.Name from smartbox__c  where Opportunity__c in :oppId AND (Action__c = 'ADD' OR Action__c = 'REMOVE' ) AND CaseID__c = null AND NoTrigger__c != true ];
        System.debug('=== contents of List Opp with EQ: ' +opp );


    if( SB.size() > 0) {
        List<String> NewList= new List<String>();
        for(Integer i = 0; i < SB.size(); i++){
            Smartbox__c bu = new Smartbox__c();
                NewList.add(SB[i].id);
                NewList.add(SB[i].Status__c);
                NewList.add(' - ');
                NewList.add(SB[i].Name);
                NewList.add(' - Chassis S/N ');
                NewList.add(SB[i].Chassis_Serial__c);
                NewList.add(' - S/N ');
                NewList.add(SB[i].Serial_Number__c);
                NewList.add(' - P/N');
                NewList.add(SB[i].Part_Number__c);
                NewList.add(' - EQ Type ');
                NewList.add(SB[i].Type_Of_Equipment__c);
                NewList.add(' - CAID ');
                NewList.add(SB[i].CAID__c);
                NewList.add(' - SmartCard ');
                NewList.add(SB[i].SmartCard__c);
                NewList.add(' - ');
                NewList.add(SB[i].LastModifiedBy.Name);
                NewList.add(' on ');
                String str1 = '' + SB[i].lastModifiedDate;
                NewList.add(str1);
                NewList.add(' GMT 0');
                NewList.add(' <br />');
                System.debug('=== contents of NewList: ' +NewList);
         }
         String s = '';
         for(String c : NewList)    {
             s = s + c;
             system.debug('This data should go into the Opp' +  c);
         }
         Opportunity uopp = new Opportunity();
            uopp.Id = OI;
            uopp.Hidden_Smartbox_Equipment__c = s;

            Database.update(uopp);
        }}
    }//End of if(oppId.size()>0)