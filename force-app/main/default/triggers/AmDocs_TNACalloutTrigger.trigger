trigger AmDocs_TNACalloutTrigger on Tenant_Account__c (before update) {

// Processes for SF->AmDocs to call @Future API callout trigers.
Tenant_Account__c S = trigger.new[0];

if(S.OkForAPI__c == TRUE) {

// CreateUnit Creates a Tenant (Incremental or Individual) account.
if(S.AmDocs_CreateUnit__c == True)
{ S.LastActivateUpdate__c = System.Now(); S.AmDocs_CreateUnit__c = false; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_Transaction_Code__c = ''; S.API_Status__c =''; S.AmDocs_FullString_Return__c = ''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_CreateUnit.AmDocsMakeCalloutCreateUnit(S.Id); System.debug('IF 1' +S);}
else

// UpdateUnit Updates a Tenant (Incremental or Individual) account.
if(S.AmDocs_UpdateUnit__c == True){ S.LastActivateUpdate__c = System.Now(); S.AmDocs_UpdateUnit__c = false; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_Transaction_Code__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_UpdateUnit.AmDocsMakeCalloutUpdateUnit(S.Id); System.debug('IF 2' +S);}
else

if( S.Action__c == 'Mute' ){ S.Action__c = 'Pending Mute'; S.LastActivateUpdate__c = System.Now(); S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_TNAMuteDC.AmDocsMakeCalloutTNAMuteDC(S.Id); System.debug('IF 3' +S);}
else

if( S.Action__c == 'UnMute' ){ S.Action__c = 'Pending UnMute'; S.LastActivateUpdate__c = System.Now(); S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_TNAMuteDC.AmDocsMakeCalloutTNAMuteDC(S.Id); System.debug('IF 4' +S);}
else

if( S.Action__c == 'Non-Pay Disconnect' ){ S.Action__c = 'Pending Non-Pay Disconnect'; S.LastActivateUpdate__c = System.Now(); S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_TNAMuteDC.AmDocsMakeCalloutTNAMuteDC(S.Id); System.debug('IF 5' +S);}
else

if( S.Action__c == 'Non-Pay Resume' ){ S.Action__c = 'Pending Non-Pay Resume'; S.LastActivateUpdate__c = System.Now(); S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_TNAMuteDC.AmDocsMakeCalloutTNAMuteDC(S.Id); System.debug('IF 6' +S);}
else

if( S.Action__c == 'Disconnect' ){ S.Action__c = 'Pending Disconnect'; S.LastActivateUpdate__c = System.Now(); S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_TNAMuteDC.AmDocsMakeCalloutTNAMuteDC(S.Id); System.debug('IF 7' +S);}
else

if( S.Action__c == 'Re-Connect' ){ S.Action__c = 'Pending Re-Connect'; S.LastActivateUpdate__c = System.Now(); S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_TNAMuteDC.AmDocsMakeCalloutTNAMuteDC(S.Id); System.debug('IF 8' +S);}
else

if( S.Action__c == 'Disconnect Resume' ){ S.Action__c = 'Pending Disconnect Resume'; S.LastActivateUpdate__c = System.Now(); S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_Transaction_Description__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_TNAMuteDC.AmDocsMakeCalloutTNAMuteDC(S.Id); System.debug('IF 9' +S);}
else

if( S.CM_CreateCustomer__c == True){ S.CM_CreateCustomer__c=False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; CMaster_CalloutClass_CreateCustomerTA.CMasterCalloutCreateCustomerTA(S.Id); System.debug('IF 10' +S);}
else

if( S.CM_Create_Login__c == True ){ S.CM_Create_Login__c = False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; CMaster_CalloutClass_CreateCustomerLgnTA.CMasterCalloutCreateCustomerLgnTA(S.Id); System.debug('IF 11' +S);}
else

if( S.CM_UpdateFromCustomerMaster__c == True ){ S.CM_UpdateFromCustomerMaster__c = False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; CMaster_CalloutClassTA_Search.CMasterCalloutSearchTACustomer(S.Id); System.debug('IF 12' +S);}
else

if( S.CM_AddSecurityPin__c == True){ S.CM_AddSecurityPin__c = False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; CMaster_CalloutClass_CreateCstmerPINTA.CMasterCalloutCreateCstmrPINTA(S.Id); System.debug('IF 13' +S);}
else

if( S.CM_Forgot_Username__c == True){ S.CM_Forgot_Username__c = False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; CMaster_CalloutClassTA_ForgotUserName.CMasterCalloutSearchTAFrgtUserName(S.Id); System.debug('IF 14' +S);}
else

if( S.Update_Password__c == True){ S.Update_Password__c = False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; CMaster_CalloutClass_CreateCustomerTAPWU.CMasterCalloutCreateCustomerTAPWU(S.Id); System.debug('IF 15' +S);}
else

if( S.GetTenantByBulk__c == True){ S.GetTenantByBulk__c = False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_GetTenantByBulk_TA.AmDocsMakeCalloutGetTenantByBulkTA(S.Id); System.debug('IF 16' +S);}
else

if( S.CM_ADMIN_SearchDANYUserName__c == True){ S.CM_ADMIN_SearchDANYUserName__c = False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; CMaster_ADMIN_TA_ForgotUserName.CMasterCalloutSearchTAFrgtUserName(S.Id); System.debug('IF 17' +S);}
else

if( S.CM_ADMIN_SearchCustomerMaster__c == True){ S.CM_ADMIN_SearchCustomerMaster__c = False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; CMaster_ADMIN_ClassTA_Search.CMasterCalloutSearchTACustomer(S.Id); System.debug('IF 18' +S);}
else

if( S.Amdocs_ResendAll__c == True){ S.Amdocs_ResendAll__c = False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_TNAReSendAll.AmDocsMakeCalloutTNAReSendAll(S.Id); System.debug('IF 19' +S);}
else

if( S.Amdocs_MACRO__c== True){ S.Amdocs_MACRO__c = False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_UpdateUnitNone.AmDocsMakeCalloutUpdateUnit(S.Id); System.debug('IF 20' +S);}
else

if( S.Amdocs_ChkOrderStatus__c== True){ S.Amdocs_ChkOrderStatus__c = False; S.AmDocs_Transaction_Description__c = ''; S.API_Status__c =''; S.API_String__c = ''; S.AmDocs_STString_Return__c = ''; AmDocs_CalloutClass_TAOrderIDCheck.AmDocsCalloutClassTAOrderIDCheck(S.Id); System.debug('IF 21' +S);}
else
{}
}}