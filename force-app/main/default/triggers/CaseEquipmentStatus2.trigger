trigger CaseEquipmentStatus2 on Case (After Insert) {

Case cas = trigger.new[0];
         {
            Set<Id> caseIds = new Set<Id>();
            Set<Id> opIds = new Set<Id>();
               opIds.add(cas.Opportunity__c);
               caseIds.add(cas.Id);

        if ( Cas.RecordTypeID == '01260000000YLR2' ||Cas.RecordTypeID == '01260000000JO8Y' || Cas.RecordTypeID == '012f20000005Pu4' || Cas.RecordTypeID == '0126000000017LQ' || Cas.RecordTypeID == '012600000001Ccy' || Cas.RecordTypeID == '0126000000018w6')
        {
        
        list<equipment__c> xequp = [select id, Opportunity__c, Statis__c, Submitted__c, CaseID__c, Change_Schedule__c from equipment__c where Opportunity__c in :opIds AND CaseID__C not in :caseIds AND (Statis__c = 'Activation Requested' OR Statis__c = 'Drop Requested') AND Submitted__c != True ];
        list<case> xc2 = [select id, Status, CaseNumber, Hidden_Case_History__c, Product_Change_Request_s__c, Requested_Actvation_Date_Time__c from case where Id in :caseIds];
        list<opportunity> xopp = [select id, Property_Status__c, Hidden_Bulk_Equipment__c  from opportunity where Id in :opIds ];
        
        if(xequp.size() > 0 && xc2.size() > 0 && xopp.size() > 0)
            {
            for (equipment__c xequp2: xequp) 
                {
                   // if(xequp2.Statis__c == 'Add Requested' && xequp2.CaseID__c == NULL)
                        {
                           xequp2.CaseID__c = xc2[0].Id;
                           xequp2.Change_Schedule__c = xc2[0].Requested_Actvation_Date_Time__c;
                           xequp2.Submitted__c = True;
                        }
               }
            for (opportunity xopp2: xopp) 
                {
                    xopp2.Hidden_Bulk_Equipment__c = Null;
                }
        {
            Database.update(xequp);
            Database.update(xopp);
        }
    }
}}}