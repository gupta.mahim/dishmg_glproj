trigger Fiber_Building_Detail_Trigger on Fiber_Building_Detail__c (before insert, after insert, after update, before update) {
    if (Trigger.isBefore) {
        if (Trigger.isInsert){
            SIMP_FiberBuildingIDFClass.beforeInsert(Trigger.New,'Building');
        }
       
        if (Trigger.isUpdate){
            SIMP_FiberBuildingIDFClass.afterInsert(Trigger.New, Trigger.oldMap,'Building');
        }

    }
}