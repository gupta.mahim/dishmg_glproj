trigger updateRetailLeads on Lead (after insert) {
	
	list<string> leadList = new list<string>();
	
	for (Lead l : trigger.new){
		leadList.add(l.Id);
	}
	
	if (leadList.size() > 0){
		updateRetailLeads.updateLeads(leadList);
	}

}