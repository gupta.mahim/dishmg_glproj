trigger Lead_MassEmail_FiberLead on Lead (after insert) {
    Lead L1 = trigger.new[0];
    if(L1.Lead_Source_External__c == 'DISH FIBER' && L1.Number_of_Units__c < 20 && L1.Company == 'Marketing Fiber Lead Test') {

        Final String template = '0004168_DishFiber_20_AUG';
        Id templateId; 
        
        try {
            templateId = [select id from EmailTemplate where Name = :template].id; } catch (QueryException e) {
        }

        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        for (Lead L : [select Id, Email from Lead where Id in :Trigger.new]) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setTemplateId(templateId);
            message.setTargetObjectId(L.Id);
            message.setSaveAsActivity(true);
        //  message.setWhatId(L.Id);
            message.setToAddresses(new String[] {L.Email});
            messages.add(message);
            Messaging.sendEmail(messages);
        }
    }
    else if(L1.Lead_Source_External__c == 'DISH FIBER' && L1.Number_of_Units__c > 20 && L1.Number_of_Units__c < 100 && L1.Company == 'Marketing Fiber Lead Test') {
            Final String template = '0004169_DishFiber_21_99_AUG';
        Id templateId; 
        
        try {
            templateId = [select id from EmailTemplate where Name = :template].id; }  catch (QueryException e) {
        }

        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        for (Lead L : [select Id, Email from Lead where Id in :Trigger.new]) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setTemplateId(templateId);
            message.setTargetObjectId(L.Id);
            message.setSaveAsActivity(true);
        //  message.setWhatId(L.Id);
            message.setToAddresses(new String[] {L.Email});
            messages.add(message);
            Messaging.sendEmail(messages);
        }
    }
    else if(L1.Lead_Source_External__c == 'DISH FIBER' && L1.Number_of_Units__c > 99 && L1.Company == 'Marketing Fiber Lead Test') {
            Final String template = '0004170_DishFiber_100_AUG';
        Id templateId; 
        
        try {
            templateId = [select id from EmailTemplate where Name = :template].id; }  catch (QueryException e) {
        }

        List<Messaging.SingleEmailMessage> messages = new List<Messaging.SingleEmailMessage>();
        for (Lead L : [select Id, Email from Lead where Id in :Trigger.new]) {
            Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
            message.setTemplateId(templateId);
            message.setTargetObjectId(L.Id);
            message.setSaveAsActivity(true);
        //  message.setWhatId(L.Id);
            message.setToAddresses(new String[] {L.Email});
            messages.add(message);
            Messaging.sendEmail(messages);
        }
    }    
}