/*
* Author: M Fazal Ur Rehan
* Master trigger to hold the logic for equipment dupes, dupe equipment case creation, AmDocs BulkEQ Callout,
* hidden field populate on Opportunity, Equipment Recall Reject Delete handling
* Invokes the appropriate   HeadEndEquipmentTrgHandler mehtods for event handling
*/
trigger headEndEquipmentTrg on Equipment__c (before insert,after insert,before update, after update)
{
    if(Trigger.isBefore && Trigger.IsInsert)
    {
        HeadEndEquipmentTrgHandler.doBeforeInsert(Trigger.new);
    }
    if(Trigger.isBefore && Trigger.IsUpdate)
    {
        HeadEndEquipmentTrgHandler.doBeforeUpdate(Trigger.newMap, Trigger.oldMap);
    }
    if(Trigger.isAfter && Trigger.IsInsert)
    {
        HeadEndEquipmentTrgHandler.doAfterInsert(Trigger.newMap, Trigger.oldMap);
    }
    if(Trigger.isAfter && Trigger.IsUpdate)
    {
        HeadEndEquipmentTrgHandler.doAfterUpdate(Trigger.newMap, Trigger.oldMap);
    }
}