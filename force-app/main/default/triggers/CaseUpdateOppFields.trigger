trigger CaseUpdateOppFields on Case (After Insert, After Update)
    {
//        // Collect the ID's from the Case and the Opportunity listed in the case.
//        Case ca = trigger.new[0];
//            Set<Id> caseIds = new Set<Id>();
//            Set<Id> opIds = new Set<Id>();
//            for (Case c: Trigger.New){
//                opIds.add(c.Opportunity__c);
//                caseIds.add(c.Id);
//    }
//     Map<Id,Case> CaseMapp=new  Map<Id,Case> ([select Id,Bulk_Load__c from Case where Id in :caseIds and Bulk_Load__c = False]);
//     Map<Id,Case> CaseMappRId=new  Map<Id,Case> ([select Id, RecordTypeId from Case where Id in :caseIds and RecordTypeId != '012600000001EeC' AND RecordTypeID != '01260000000YLPz' ]);
//    
//      if(CaseMapp.size() > 0 && CaseMappRId.size() > 0){
//    //Create a bunch of lists, each list is unique for certian processes.
//    list<opportunityLineItem> oli = [select id, OpportunityId, Status__c, CaseID__c, Submitted__c from opportunityLineItem where OpportunityId in :opIds AND CaseID__c in :caseIDs AND Submitted__c = True ];
//    list<case> c2 = [select id, CaseNumber, Created_By_Email__c, CSG_Account_Number__c, Status, RequestedAction__c, Requested_Actvation_Date_Time__c from case where Id in :caseIds AND Status = 'Request Completed' AND (RequestedAction__c = 'Pre-Activation + Activation' OR RequestedAction__c = 'Activation' OR RequestedAction__c = 'Change' OR  RequestedAction__c = 'Installation/Activation' OR  RequestedAction__c = 'Restart' OR RequestedAction__c = 'Two (2) Hour Activation' )];
//    list<case> c3 = [select id, CaseNumber, Created_By_Email__c, CSG_Account_Number__c, Status, RequestedAction__c, Requested_Actvation_Date_Time__c from case where Id in :caseIds AND Status = 'Request Completed' AND RequestedAction__c = 'Disconnection' AND RecordTypeID != '012600000001EeC' AND RecordTypeID != '01260000000YLPz' ];
//    list<case> c4 = [select id, CaseNumber, Created_By_Email__c, Claim_Number__c, CSG_Account_Number__c, Status, RequestedAction__c, Requested_Actvation_Date_Time__c from case where Id in :caseIds AND Status = 'Request Completed' AND RequestedAction__c = 'Pre-Build' ];
//    list<case> c5 = [select id, CaseNumber, Created_By_Email__c, Claim_Number__c, CSG_Account_Number__c, Status, RequestedAction__c, Requested_Actvation_Date_Time__c from case where Id in :caseIds AND Status = 'Request Completed' AND RequestedAction__c = 'Pre-Activation' ];
//    list<case> c6 = [select id, CaseNumber, Created_By_Email__c, Claim_Number__c, CSG_Account_Number__c, Status, RequestedAction__c, Requested_Actvation_Date_Time__c from case where Id in :caseIds AND Status = 'Activation Pending' AND RequestedAction__c = 'Pre-Activation + Activation' ];
//    list<opportunity> opp = [select id, Hidden_Products__c, Hidden_Case_History__c, Property_Status__c, Last_Case_Email_Address__c, Claim_Number__c, Launch_Date__c, Disconnect_Date__c, StageName, CSG_Account_Number__c, Activation_Timer_On__c from opportunity where Id in :opIds ];
//    list<opportunity> ops = [select id, Hidden_Products__c, Hidden_Case_History__c, Property_Status__c, Last_Case_Email_Address__c, Claim_Number__c, Launch_Date__c, Disconnect_Date__c, StageName, CSG_Account_Number__c, Activation_Timer_On__c from opportunity where Id in :opIds AND Hidden_Case_History__c = True Limit 1]; 
// Used to skip CHANGE request on Pre-Active/Pre-Activation properties.
//    list<opportunity> opps = [select id, Property_Status__c from opportunity where (Property_Status__c = 'Pre-Built' OR Property_Status__c = 'Pending Installation' OR Property_Status__c = 'Ready for Activation' OR Property_Status__c = 'Approved - Pending Activation' OR Property_Status__c  = 'Pre-Activation' OR Property_Status__c = 'Pending Activation' ) AND Id in :opIds ];
//    list<case> c7 = [select id, CaseNumber, Created_By_Email__c, Claim_Number__c, CSG_Account_Number__c, Status, RequestedAction__c, Requested_Actvation_Date_Time__c from case where Id in :caseIds AND Status = 'Request Completed' AND RequestedAction__c = 'Change' ];
//
//  // #1.0 For Activations, Changes, Restarts and similar. Update Opportunity information Property Status, Launch Date, Stage Name, Close Date, CSG Account Number and turns off the activation timer.
//     if(opps.size() > 0 && c7.size() > 0 && CaseMapp.size() > 0)
//     {
//     }
//     else
//        if(opp.size() > 0 && c2.size() > 0 && CaseMapp.size() > 0)
//        {
//            for (opportunity opp1: opp) 
//                if(opp1.Property_Status__c != 'Active' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c == NULL )                { opp[0].Disconnect_Date__c = NULL; opp[0].Property_Status__c = 'Active'; opp[0].Activation_Timer_On__c = False; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c2[0].CSG_Account_Number__c; }
//                else
//                    if(opp1.Property_Status__c != 'Active' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c == NULL)
//                        { opp[0].Disconnect_Date__c = NULL; opp[0].Property_Status__c = 'Active'; opp[0].Activation_Timer_On__c = False; opp[0].CSG_Account_Number__c = c2[0].CSG_Account_Number__c; }
//                else
//                    if(opp1.Property_Status__c != 'Active' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c != NULL )                   { opp[0].Disconnect_Date__c = NULL; opp[0].Property_Status__c = 'Active'; opp[0].Activation_Timer_On__c = False; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c2[0].CSG_Account_Number__c; }
//                 else
//                    if(opp1.Property_Status__c != 'Active' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c != NULL )   
//                 { opp[0].Disconnect_Date__c = NULL; opp[0].Property_Status__c = 'Active'; opp[0].Activation_Timer_On__c = False; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c2[0].CSG_Account_Number__c; }
//                else
//                    if(opp1.Property_Status__c == 'Active' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c != NULL )                    { opp[0].Disconnect_Date__c = NULL; opp[0].Activation_Timer_On__c = False; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); }
//                else
//                    if(opp1.Property_Status__c == 'Active' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c == NULL )                    { opp[0].Disconnect_Date__c = NULL; opp[0].Activation_Timer_On__c = False; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c2[0].CSG_Account_Number__c; }
//                else
//                    if(opp1.Property_Status__c == 'Active' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c != NULL )
//                        { opp[0].Disconnect_Date__c = NULL; opp[0].Activation_Timer_On__c = False; }
//       }
//        // #2.0 For Disconnects. Update Opportunity information Property Status, Stage Name, Close Date, Launch, CSG Account Number and turns off the activation timer.
//        if(opp.size() > 0 && c3.size() > 0 && CaseMapp.size() > 0)
//        {
//            for (opportunity opp1: opp) 
//                if(opp1.Property_Status__c != 'Disconnected' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c == NULL && opp[0].Disconnect_Date__c == NULL )                          { opp[0].Property_Status__c = 'Disconnected'; opp[0].Activation_Timer_On__c = False; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c3[0].CSG_Account_Number__c; opp[0].Disconnect_Date__c = System.Today(); }
//                else
//                    if(opp1.Property_Status__c != 'Disconnected' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c == NULL && opp[0].Disconnect_Date__c != NULL )                              { opp[0].Property_Status__c = 'Disconnected'; opp[0].Activation_Timer_On__c = False; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c3[0].CSG_Account_Number__c; }
//                else
//                    if(opp1.Property_Status__c != 'Disconnected' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c != NULL && opp[0].Disconnect_Date__c == NULL )                                 { opp[0].Property_Status__c = 'Disconnected'; opp[0].Activation_Timer_On__c = False; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].Disconnect_Date__c = System.Today();}
//                else
//                    if(opp1.Property_Status__c != 'Disconnected' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c == NULL && opp[0].Disconnect_Date__c == NULL )
//                        { opp[0].Property_Status__c = 'Disconnected'; opp[0].Activation_Timer_On__c = False; opp[0].CSG_Account_Number__c = c3[0].CSG_Account_Number__c; opp[0].Disconnect_Date__c = System.Today(); }            
//                else
//                    if(opp1.Property_Status__c != 'Disconnected' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c != NULL && opp[0].Disconnect_Date__c == NULL )                                 { opp[0].Property_Status__c = 'Disconnected'; opp[0].Activation_Timer_On__c = False; opp[0].Disconnect_Date__c = System.Today(); }
//                else
//                    if(opp1.Property_Status__c != 'Disconnected' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c == NULL && opp[0].Disconnect_Date__c != NULL )                                 { opp[0].Property_Status__c = 'Disconnected'; opp[0].Activation_Timer_On__c = False; opp[0].CSG_Account_Number__c = c3[0].CSG_Account_Number__c;  }
//                else
//                    if(opp1.Property_Status__c != 'Disconnected' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c != NULL && opp[0].Disconnect_Date__c != NULL )                                 { opp[0].Property_Status__c = 'Disconnected'; opp[0].Activation_Timer_On__c = False; }
//                else
//                    if(opp1.Property_Status__c == 'Disconnected' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c == NULL && opp[0].Disconnect_Date__c == NULL )                                  { opp[0].Activation_Timer_On__c = False; opp[0].CSG_Account_Number__c = c3[0].CSG_Account_Number__c; opp[0].Disconnect_Date__c = System.Today(); }          
//                else
//                    if(opp1.Property_Status__c == 'Disconnected' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c != NULL && opp[0].Disconnect_Date__c == NULL )                               { opp[0].Activation_Timer_On__c = False; opp[0].Disconnect_Date__c = System.Today(); }
//                else
//                    if(opp1.Property_Status__c == 'Disconnected' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c == NULL && opp[0].Disconnect_Date__c != NULL )                              { opp[0].Activation_Timer_On__c = False; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c3[0].CSG_Account_Number__c;  }
//                else
//                    if(opp1.Property_Status__c == 'Disconnected' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c != NULL && opp[0].Disconnect_Date__c != NULL )
//                        { opp[0].Activation_Timer_On__c = False; }
//                else      
//                    if(opp1.Property_Status__c == 'Disconnected' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c != NULL && opp[0].Disconnect_Date__c != NULL )                        { opp[0].Activation_Timer_On__c = False;  opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); }
//        }
//    // #3.0 For Pre-Builds. Update Opportunity information Property Status, Stage Name, Close Date, Launch, Last Case Email address, CSG Account Number and turns off the activation timer.
//    if(opp.size() > 0 && c4.size() > 0 && CaseMapp.size() > 0)        {            for (opportunity opp1: opp)                 if(opp1.Property_Status__c != 'Pre-Built' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c == NULL )                  { opp[0].Disconnect_Date__c = NULL; opp[0].Property_Status__c = 'Pre-Built'; opp[0].Activation_Timer_On__c = True; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c4[0].CSG_Account_Number__c; opp[0].Launch_Date__c = System.Today(); opp[0].Last_Case_Email_Address__c = c4[0].Created_By_Email__c; opp[0].Claim_Number__c = c4[0].Claim_Number__c; } 
//                else
//                    if(opp1.Property_Status__c != 'Pre-Built!' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c == NULL)                             { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c4[0].Created_By_Email__c; opp[0].Claim_Number__c = c4[0].Claim_Number__c; opp[0].Property_Status__c = 'Pre-Built'; opp[0].Activation_Timer_On__c = True; opp[0].CSG_Account_Number__c = c4[0].CSG_Account_Number__c; opp[0].Launch_Date__c = System.Today(); }
//                else
//                    if(opp1.Property_Status__c != 'Pre-Built' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c != NULL )                            { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c4[0].Created_By_Email__c; opp[0].Claim_Number__c = c4[0].Claim_Number__c; opp[0].Property_Status__c = 'Pre-Built'; opp[0].Activation_Timer_On__c = True; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c4[0].CSG_Account_Number__c; opp[0].Launch_Date__c = System.Today(); }
//                 else
//                    if(opp1.Property_Status__c != 'Pre-Built' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c != NULL )          
//                 { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c4[0].Created_By_Email__c; opp[0].Claim_Number__c = c4[0].Claim_Number__c; opp[0].Property_Status__c = 'Pre-Built'; opp[0].Activation_Timer_On__c = True; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c4[0].CSG_Account_Number__c;  opp[0].Launch_Date__c = System.Today(); }
//                else
//                    
//                    if(opp1.Property_Status__c == 'Pre-Built' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c != NULL )                             { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c4[0].Created_By_Email__c; opp[0].Claim_Number__c = c4[0].Claim_Number__c; opp[0].Activation_Timer_On__c = True; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today();  opp[0].Launch_Date__c = System.Today(); }
//                else
//                   if(opp1.Property_Status__c == 'Pre-Built' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c == NULL )                        { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c4[0].Created_By_Email__c; opp[0].Claim_Number__c = c4[0].Claim_Number__c; opp[0].Activation_Timer_On__c = True; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c4[0].CSG_Account_Number__c;  opp[0].Launch_Date__c = System.Today(); }
//                else
//                    if(opp1.Property_Status__c == 'Pre-Built' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c != NULL )           
//                { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c4[0].Created_By_Email__c; opp[0].Claim_Number__c = c4[0].Claim_Number__c; opp[0].Activation_Timer_On__c = True;  opp[0].Launch_Date__c = System.Today(); }
//                else        
//                    if(opp1.Property_Status__c == 'Pre-Built' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c == NULL )                        { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c4[0].Created_By_Email__c; opp[0].Claim_Number__c = c4[0].Claim_Number__c; opp[0].Activation_Timer_On__c = True; opp[0].CSG_Account_Number__c = c4[0].CSG_Account_Number__c;  opp[0].Launch_Date__c = System.Today(); }
//                }            
//        // #4.0 For Pre-Activations. Update Opportunity information Property Status, Stage Name, Close Date, Launch, Last Case Email address, CSG Account Number and turns off the activation timer.
//        if(opp.size() > 0 && c5.size() > 0 && CaseMapp.size() > 0)            for (opportunity opp1: opp)                {                                           if(opp1.Property_Status__c != 'Pre-Activation' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c == NULL )                        { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c5[0].Created_By_Email__c; opp[0].Claim_Number__c = c5[0].Claim_Number__c; opp[0].Property_Status__c = 'Pre-Activation'; opp[0].Activation_Timer_On__c = True; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c5[0].CSG_Account_Number__c; opp[0].Launch_Date__c = System.Today(); } 
//                    else
//                        if(opp1.Property_Status__c != 'Pre-Activation' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c == NULL)                               { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c5[0].Created_By_Email__c; opp[0].Claim_Number__c = c5[0].Claim_Number__c; opp[0].Property_Status__c = 'Pre-Activation'; opp[0].Activation_Timer_On__c = True; opp[0].CSG_Account_Number__c = c5[0].CSG_Account_Number__c; opp[0].Launch_Date__c = System.Today(); }
//                    else
//                        if(opp1.Property_Status__c != 'Pre-Activation' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c != NULL )                            { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c5[0].Created_By_Email__c; opp[0].Claim_Number__c = c5[0].Claim_Number__c; opp[0].Property_Status__c = 'Pre-Activation'; opp[0].Activation_Timer_On__c = True; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c5[0].CSG_Account_Number__c;  opp[0].Launch_Date__c = System.Today(); }
//                     else
//                        if(opp1.Property_Status__c != 'Pre-Activation' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c != NULL )        
//                     { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c5[0].Created_By_Email__c; opp[0].Claim_Number__c = c5[0].Claim_Number__c; opp[0].Property_Status__c = 'Pre-Activation'; opp[0].Activation_Timer_On__c = True; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c5[0].CSG_Account_Number__c;  opp[0].Launch_Date__c = System.Today(); }
//                    else
//                        if(opp1.Property_Status__c == 'Pre-Activation' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c != NULL )                            { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c5[0].Created_By_Email__c; opp[0].Claim_Number__c = c5[0].Claim_Number__c; opp[0].Activation_Timer_On__c = True; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today();  opp[0].Launch_Date__c = System.Today(); }
//                    else
//                        if(opp1.Property_Status__c == 'Pre-Activation' && opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c == NULL )                            { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c5[0].Created_By_Email__c; opp[0].Claim_Number__c = c5[0].Claim_Number__c; opp[0].Activation_Timer_On__c = True; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c5[0].CSG_Account_Number__c;  opp[0].Launch_Date__c = System.Today(); }
//                    else
//                        if(opp1.Property_Status__c == 'Pre-Activation' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c != NULL )       
//                    { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c5[0].Created_By_Email__c; opp[0].Claim_Number__c = c5[0].Claim_Number__c; opp[0].Activation_Timer_On__c = True;  opp[0].Launch_Date__c = System.Today(); }
//                    else
//                        if(opp1.Property_Status__c == 'Pre-Activation' && opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c == NULL )                            { opp[0].Disconnect_Date__c = NULL; opp[0].Last_Case_Email_Address__c = c5[0].Created_By_Email__c; opp[0].Claim_Number__c = c5[0].Claim_Number__c; opp[0].Activation_Timer_On__c = True; opp[0].CSG_Account_Number__c = c5[0].CSG_Account_Number__c;  opp[0].Launch_Date__c = System.Today(); }
//        }
//      if(opp.size() > 0 && c6.size() > 0 && CaseMapp.size() > 0)
//        {
//            for (opportunity opp1: opp)              
//                    if( opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c == NULL )   
//                        { opp[0].Disconnect_Date__c = NULL; opp[0].Property_Status__c = 'Activation Pending'; opp[0].Activation_Timer_On__c = True; opp[0].StageName = 'Closed Won'; opp[0].CloseDate = System.Today(); opp[0].CSG_Account_Number__c = c6[0].CSG_Account_Number__c; }
//                else
//                    if( opp1.StageName == 'Closed Won' && opp1.CSG_Account_Number__c == NULL )  
//                { opp[0].Disconnect_Date__c = NULL; opp[0].Property_Status__c = 'Activation Pending'; opp[0].Activation_Timer_On__c = True; opp[0].CSG_Account_Number__c = c6[0].CSG_Account_Number__c; }
//              else
//                    if( opp1.StageName != 'Closed Won' && opp1.CSG_Account_Number__c != NULL )     
//                { opp[0].Disconnect_Date__c = NULL; opp[0].Property_Status__c = 'Activation Pending'; opp[0].Activation_Timer_On__c = True; }
//                else
//                    if( opp1.CSG_Account_Number__c != NULL)          
//                { opp[0].Disconnect_Date__c = NULL; opp[0].Property_Status__c = 'Activation Pending'; opp[0].Activation_Timer_On__c = True; }
//       }
//          
// Update opp[0];
//}
}