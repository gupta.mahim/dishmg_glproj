trigger LeadSumTV on Lead (Before Update, Before Insert) { 
Lead L = trigger.new[0];
if(L.Number_of_TVs__c == null && L.TV_s_HD__c  == null && L.TV_s_Non_HD__c >= 1 && L.TV_s_Non_HD__c <= 2)
{L.Number_of_TVs__c = '1-2';}
else
if(L.Number_of_TVs__c == null && L.TV_s_HD__c  == null && L.TV_s_Non_HD__c >= 3 && L.TV_s_Non_HD__c <= 6)
{L.Number_of_TVs__c = '3-6';}
else
if(L.Number_of_TVs__c == null && L.TV_s_HD__c  == null && L.TV_s_Non_HD__c >= 7 )
{L.Number_of_TVs__c = '7+';}
else
if(L.Number_of_TVs__c == null && L.TV_s_HD__c  >= 1 && L.TV_s_HD__c <= 2 && L.TV_s_Non_HD__c == null)
{L.Number_of_TVs__c = '1-2';}
else
if(L.Number_of_TVs__c == null && L.TV_s_HD__c  >= 3 && L.TV_s_HD__c <= 6 && L.TV_s_Non_HD__c == null)
{L.Number_of_TVs__c = '3-6';}
else
if(L.Number_of_TVs__c == null && L.TV_s_HD__c  >= 7 && L.TV_s_Non_HD__c == null )
{L.Number_of_TVs__c = '7+';}
else
if(L.Number_of_TVs__c == null && L.TV_s_HD__c != null && L.TV_s_Non_HD__c != null && L.TV_s_HD__c + L.TV_s_Non_HD__c >= 1 && L.TV_s_HD__c + L.TV_s_Non_HD__c <= 2)
{L.Number_of_TVs__c = '1-2';}
else
if(L.Number_of_TVs__c == null && L.TV_s_HD__c != null && L.TV_s_Non_HD__c != null && L.TV_s_HD__c + L.TV_s_Non_HD__c >= 3 && L.TV_s_HD__c + L.TV_s_Non_HD__c <= 6)
{L.Number_of_TVs__c = '3-6';}
else
if(L.Number_of_TVs__c == null && L.TV_s_HD__c != null && L.TV_s_Non_HD__c != null && L.TV_s_HD__c + L.TV_s_Non_HD__c >= 7)
{L.Number_of_TVs__c = '7+';}
}