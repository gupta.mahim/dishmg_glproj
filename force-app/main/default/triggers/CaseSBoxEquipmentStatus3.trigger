trigger CaseSBoxEquipmentStatus3 on Case (After Update) {

Case ca = trigger.new[0];
    if (ca.Opportunity__c != null && (ca.Status == 'Request Completed' || ca.Status == 'Rejected' || ca.Status == 'Request Recalled') && (ca.RequestedAction__c == 'Pre-Activation + Activation' || ca.RequestedAction__c == 'Activation' || ca.RequestedAction__c == 'Change' ||  ca.RequestedAction__c == 'Installation/Activation' ||  ca.RequestedAction__c == 'Pre-Activation' ||   ca.RequestedAction__c == 'Restart' ||   ca.RequestedAction__c == 'Two (2) Hour Activation' ||   ca.RequestedAction__c == 'Pre-Build' || ca.RequestedAction__c == 'Disconnect' || ca.RequestedAction__c == 'Disconnection'))
        {
            Set<Id> caseIds = new Set<Id>();
            Set<Id> opIds = new Set<Id>();
                for (Case c: Trigger.New){
                    opIds.add(c.Opportunity__c);
                    caseIds.add(c.Id);
         }
            list<opportunity> olic1 = [select id, Hidden_Smartbox_Equipment__c from opportunity where Id in :opIds ];
            list<smartbox__c> sbeq3 = [select id, Status__c, CaseID__c, Remove_Equipment__c, Submitted__c, Opportunity__c from smartbox__c where Opportunity__c in :opIds AND CaseID__c in :caseIDs AND Submitted__c = True ];
            list<case> c2 = [select id, CaseNumber, Requested_Actvation_Date_Time__c from case where Id in :caseIds AND Status = 'Request Completed' AND (RequestedAction__c = 'Pre-Activation + Activation' OR RequestedAction__c = 'Activation' OR RequestedAction__c = 'Change' OR  RequestedAction__c = 'Installation/Activation' OR  RequestedAction__c = 'Pre-Activation' OR  RequestedAction__c = 'Restart' OR RequestedAction__c = 'Two (2) Hour Activation' OR RequestedAction__c = 'Pre-Build' OR RequestedAction__c = 'Disconnect' OR RequestedAction__c = 'Disconnect')];
            list<case> c223 = [select id, CaseNumber, Requested_Actvation_Date_Time__c from case where Id in :caseIds AND (Status = 'Request Recalled' OR Status = 'Rejected') AND (RequestedAction__c = 'Pre-Activation + Activation' OR RequestedAction__c = 'Activation' OR RequestedAction__c = 'Change' OR  RequestedAction__c = 'Installation/Activation' OR  RequestedAction__c = 'Pre-Activation' OR  RequestedAction__c = 'Restart' OR RequestedAction__c = 'Two (2) Hour Activation' OR RequestedAction__c = 'Pre-Build' OR RequestedAction__c = 'Disconnect' OR RequestedAction__c = 'Disconnect')];

                for (smartbox__c sbeq32: sbeq3)                     {                        if(sbeq3.size() > 0 && c2.size() > 0 && c223.size() < 1 ){                        if(sbeq32.Status__c == 'Activation Requested' && sbeq32.Submitted__c == True )                            {                                sbeq32.Status__c = 'Activation Completed';                                sbeq32.CaseID__c = NULL;                                sbeq32.Change_Schedule__c = NULL;                                sbeq32.Submitted__c = False;                                for (Opportunity olic32: olic1)                                     {                                        olic32.Hidden_Smartbox_Equipment__c = null;
                                     }
                             }
                      if(sbeq32.Status__c == 'Drop Requested' && sbeq32.Submitted__c == True )                             {                               sbeq32.Status__c = 'Drop Completed';                               sbeq32.CaseID__c = NULL;                               sbeq32.Change_Schedule__c = NULL;                               sbeq32.Submitted__c = False;                               sbeq32.Remove_Equipment__c = False;                               for (Opportunity olic32: olic1)                                 {                                    olic32.Hidden_Smartbox_Equipment__c = null;
                                 }        
                              }
                      }
/* REMOVED 6/10/2020 - Jerry Clifft - This piece is no longer compatable with current processes                      
                      if(sbeq3.size() > 0 && c2.size() < 1 && c223.size() > 0 ){                              if(sbeq32.Status__c == 'Activation Requested' && sbeq32.Submitted__c == True )
                             {
                               sbeq32.CaseID__c = NULL;                               sbeq32.Change_Schedule__c = NULL;                               sbeq32.Submitted__c = False;                               sbeq32.Flags__c = 'Delete';                               for (Opportunity olic32: olic1)                                    {                                       olic32.Hidden_Smartbox_Equipment__c = null;
                                    }
                             }    
                        }
*/                        
                        if(sbeq32.Status__c == 'Drop Requested' && sbeq32.Submitted__c == True ){                             {                               sbeq32.Status__c = 'Active';                               sbeq32.CaseID__c = NULL;                               sbeq32.Remove_Equipment__c = False;                               sbeq32.Change_Schedule__c = NULL;                               sbeq32.Submitted__c = False;                               for (Opportunity olic32: olic1)                                   {                                      olic32.Hidden_Smartbox_Equipment__c = null;
                                   }      
                             }      
                        }
                    }        
               Database.update(sbeq3);
               Database.update(olic1);
    }
}