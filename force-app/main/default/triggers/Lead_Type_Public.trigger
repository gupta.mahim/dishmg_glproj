trigger Lead_Type_Public on Lead (Before Update, Before Insert) { 
Lead L = trigger.new[0];
if(L.Type_of_Business__c == 'Retirement Community' && L.Location_of_Service__c == 'Common Area' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';} 
else
if(L.Type_of_Business__c == 'Assisted Living' && L.Location_of_Service__c == 'Common Area' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Hospitals' && L.Location_of_Service__c == 'Common Area' && L.sundog_deprm2__Lead_Type__c != 'P/P'){L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'University' && L.Location_of_Service__c == 'Common Area' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Common Area' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Hotel/Motel' && L.Location_of_Service__c == 'Food Service' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'RV Park' && L.Location_of_Service__c == 'Common Area' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'RV Park' && L.Location_of_Service__c == 'Food Service' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'RV Park' && L.Location_of_Service__c == 'Other' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Marinas' && L.Location_of_Service__c == 'Common Area' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Marinas' && L.Location_of_Service__c == 'Food Service' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Marinas' && L.Location_of_Service__c == 'Other' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Casinos' && L.Location_of_Service__c == 'Common Area' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Beauty Salon' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Airports' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Lobbies' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Golf Course' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
else
if(L.Type_of_Business__c == 'Bar/Restaurant' && L.sundog_deprm2__Lead_Type__c != 'P/P')
{L.sundog_deprm2__Lead_Type__c = 'P/P'; L.sundog_deprm2__Lead_Sub_Type__c = 'Primary';}
// else nothing
}