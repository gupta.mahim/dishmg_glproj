// Author: Jerry Clifft
// Created on: 20th March 2020
// Created for: CONGA Orchestrate - Product BUG Work-Around related to Customer Object with Master-Detail being used as an Initator Object.
// Description: Trigger will cause insertion of Intator

trigger DISHContractORC on DISH_Contract__c (after insert, after update) {     
    string operation = Trigger.isUpdate ? 'Update' : 'Insert';     
    FSTR.ProcessComposerInitiatorUtils.EvaluateInitiators(Trigger.oldMap, Trigger.newMap, operation); 
}