({
    init : function(component, event, helper) 
    {
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        var sParameterName, i, equipIds, OppID, TAId, Prg, equipValues;
        
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == 'equipIds')
                equipIds = sParameterName[1];
            if (sParameterName[0] == 'oppId') 
                OppID = sParameterName[1];
            if (sParameterName[0] == 'TAId') 
                TAId = sParameterName[1];
            if (sParameterName[0] == 'Prg') 
                Prg = sParameterName[1];           
        }
        
        if(equipIds != '' && equipIds != null && equipIds != 'undefined')
            equipValues = equipIds.split(',');
        
        component.set("v.Equips",equipValues);
        component.set("v.SelectedPrg",Prg);        
        
        console.log('E_'+equipIds);console.log('O_'+OppID);console.log('T_'+TAId);console.log('P_'+Prg);
        
        var action = component.get("c.getActiveEquips");
        var hasOverrideCase=component.get("v.hasOverrideCase");
        action.setParams({
            equipIds: component.get("v.Equips"),
            oppId:OppID,
            taId:TAId
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.Equips", response.getReturnValue());
                var delay=2000; //2 seconds
                setTimeout(function() {
                    console.log('Inside delay: ');
                    window.print();
                }, delay);
            }
            else{
                console.log('Error while getting Tenant Account details. Error Detail: '+JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },      
})