({
    quoteSelectionHelper : function(component, event, helper) {
        var quoteRec = component.get("v.tempoQuoteRec");
        var quoteLineRec = quoteRec.SBQQ__LineItems__r[0];
        console.log(quoteRec.SBQQ__LineItems__r[0].ARPU__c);
        //console.log(quoteLineRec);
        
        var action = component.get("c.updateObjectRec");
        action.setParams({
            "record": quoteRec,
            "childRecord" : quoteLineRec 
        });
        action.setCallback(this, function(response){
            var state = response.getState();
            console.log('state: ' + state);
            if (state === "SUCCESS") {
                var isSuccess = response.getReturnValue();
                if(isSuccess){
                    helper.showToast(component, event, helper, 'Quote Update Successful!', 'success', 'Success');
                    helper.redirectUrl(component, event, helper);
                    helper.refresh(component, event, helper);
                    component.destroy();
                }
                else{
                    helper.showToast(component, event, helper, 'Quote Update Failed. Please check your record', 'error', 'Failed');
                }
            }
            component.set("v.isCalculating", false);
        })
        $A.enqueueAction(action);
    },
    
    showToast : function(component, event, helper, message, type, title){
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            message: message,
            type : type, 
            title : title 
        });
        toastEvent.fire();
    },
    
    redirectUrl : function(component, event, helper){
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": component.get("v.retUrl")
        });
        urlEvent.fire();
    },
    
    refresh : function(component, event, helper) {
        
        $A.get('e.force:refreshView').fire();
                               
    },
})