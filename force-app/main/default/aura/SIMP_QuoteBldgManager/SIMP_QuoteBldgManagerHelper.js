({
    getBldgCount : function(component) {
        var action = component.get("c.getExistingRelatedRecordList");
        action.setParams({ 
            "parentRecId": component.get("v.recordId"),
            "parentApiName" : "Quote__c",
            "objApiName": "Fiber_Building_Detail__c",
            "recordfieldList" : "Quote__c"
        });
        action.setCallback(this, function(response) {
            component.set("v.numberOFExisting",response.getReturnValue().length);
            // Add another Calback function here.
        }); 
        $A.enqueueAction(action);
    },
    
    getRecordType : function(component, event){
        var getQuoteRecType = component.get("c.fetchQuoteName");
        getQuoteRecType.setParams({ 
            "parentId" : component.get("v.recordId"),
            "parentApiName" : "SBQQ__Quote__c",
            "purpose" : "RecType",
        });
        
        getQuoteRecType.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS"){ 
                component.set("v.disableMe",response.getReturnValue());
                //alert(response.getReturnValue());
            }
        }); 
        $A.enqueueAction(getQuoteRecType);
    },
    
    createRecord: function(component, event){
        //tin - added 07092019
        var inputNum = component.get("v.bldgCount");
        
        if(inputNum > 200){
            var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Warning!",
                    "message": "Input Limit is 200",
                    "type" : "warning"
                });
                toastEvent.fire();
        }
        else{
        var getQuoteName = component.get("c.fetchQuoteName");
        getQuoteName.setParams({ 
            "parentId" : component.get("v.recordId"),
            "parentApiName" : "SBQQ__Quote__c",
            "purpose" : "Name",
        });
        
        getQuoteName.setCallback(this, function(response) {
            if(response.getState() == "SUCCESS"){ 
                var createRecordEvent = $A.get("e.force:createRecord");
                createRecordEvent.setParams({ 
                    "entityApiName": "Fiber_Building_Detail__c",
                    "defaultFieldValues": {
                        "Quote__c" : component.get("v.recordId"),
                        "Name" : response.getReturnValue(),
                    },
                    "navigationLocation": "RELATED_LIST"
                });
                createRecordEvent.fire();
                
            }
        }); 
        $A.enqueueAction(getQuoteName);
        }
    },
    //tin - added 03/07/19
    deleteAllRecord: function(component, event){
        var action = component.get("c.deleteAllRelatedRecords");
        action.setParams({ 
            "parentId" : component.get("v.recordId"),
            "parentApiName" : "Quote__c",
            "objectApiName" : "Fiber_Building_Detail__c"
        });
        component.set("v.Spinner", true);
        //console.log(component.get("v.Spinner"));
        action.setCallback(this, function(response) {
            console.log(response.getState());
            
            if(response.getState() == "SUCCESS"){ 
                var toastEvent = $A.get("e.force:showToast");
                toastEvent.setParams({
                    "title": "Success!",
                    "message": "The Buildings were deleted Successfully.",
                    "type" : "success"
                });
                toastEvent.fire();
                $A.get('e.force:refreshView').fire();
            }
        }); 
        $A.enqueueAction(action);
    },
    
    callCreateBldgCopies : function(component, event){
        var message = JSON.stringify(event.getParams().message);
        if(message.includes("Building Detail") && message.includes("was created")){
            var idfId = event.getParams().messageTemplateData[1].executionComponent.attributes.recordId;
            var count = component.get("v.bldgCount") - 1; 
            if(count > 0){
                var action = component.get("c.createClones");
                action.setParams({ 
                    "recordIdToClone": idfId,
                    "numberOfCopies" : count
                });
                action.setCallback(this, function(response) {
                    console.log(response.getState());
                    if(response.getState() == "SUCCESS"){
                        component.set("v.bldgCount", 0);
                        $A.get('e.force:refreshView').fire();
                    }
                    // Add another Calback function here.
                }); 
                $A.enqueueAction(action); 
            }
            else{
                component.set("v.bldgCount", 0);
                $A.get('e.force:refreshView').fire();
            }
            
        }
        
    }
})