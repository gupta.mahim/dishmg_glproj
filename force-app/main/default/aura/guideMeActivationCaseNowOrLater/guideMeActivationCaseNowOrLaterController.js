({
    init : function(component, event, helper) {
        var getDisconnectEndTime = parseInt(component.get("v.disconnectEndTime"), 10);
        var disconnectEndTime = new Date(getDisconnectEndTime);
        var buttonNext = component.find('btnNext');
        var currentTime = new Date().getTime();
                
        //setting default value of radio button rdbActCaseId to 'Now' 
        component.find('rdbActCaseId').set('v.value','Now');
        component.set("v.activateNowOrLater", 'Now');
        
        //if Equipment disconnect request is not raised 
        if(component.get("v.disconnectEndTime") == '' || component.get("v.disconnectEndTime") == null)
            buttonNext.set('v.disabled',false);
        else
            component.set("v.equipDisconnected", true);
        
        //if Equipment disconnect request timer is already expired              
        if(parseInt(currentTime, 10) > parseInt(getDisconnectEndTime, 10)){
            component.set("v.timerExpired", true);
            buttonNext.set('v.disabled',false);
        }
        else{
            // Update the count down every 1 second
            var timer = setInterval(function() {
                var now = new Date().getTime();
                var diff = disconnectEndTime - now;
                
                // Time calculations for hours, minutes and seconds
                var hours = Math.floor((diff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((diff % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((diff % (1000 * 60)) / 1000);
                
                var timeLeft =  minutes + "min " + seconds + "sec ";
                component.set("v.timeLeft", timeLeft);
                
                if (timeLeft == '0min 0sec ' || parseInt(seconds, 10) < 0)  { 
                    clearInterval(timer);
                    buttonNext.set('v.disabled',false);
                    component.set("v.timerExpired", true);
                }
            }, 1000);
        }
    },
    
    rdbActCaseOnChange : function(component, event, helper) {       
        var rdbActivateCase = event.getSource().get("v.value");
        var timerExp = component.get("v.timerExpired");
        var buttonNext = component.find('btnNext');
        var equipDisconnect = component.get("v.equipDisconnected");
        
        component.set("v.activateNowOrLater", rdbActivateCase);
        console.log('Test'+rdbActivateCase);
        if(rdbActivateCase == 'Later'){
            buttonNext.set('v.disabled',false);
        }
        if(rdbActivateCase == 'Now' && timerExp == true){
            buttonNext.set('v.disabled',false);
        }
        if(rdbActivateCase == 'Now' && timerExp == false && equipDisconnect == true){            
            buttonNext.set('v.disabled',true);
        }
        
    },
    
    btnNextClick : function(component, event, helper) {       
        var navigate = component.get("v.navigateFlow");
        navigate("NEXT");
    },
    
    btnPreviousClick : function(component, event, helper) {
        var navigate = component.get("v.navigateFlow");
        navigate("BACK");        
    },
})