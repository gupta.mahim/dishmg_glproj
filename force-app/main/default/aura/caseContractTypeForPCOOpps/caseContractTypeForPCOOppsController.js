({
	doInit : function(component, event, helper) {
		var OpptyId = component.get("v.opportunityId");
        var action = component.get("c.getTenantType");
        action.setParams({OppId: OpptyId});
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                component.set("v.tenantType", response.getReturnValue());
                if(response.getReturnValue() == 'Digital Bulk')
                    component.set("v.contractType", 'PCO Bulk');
                if(response.getReturnValue() == 'Incremental')
                    component.set("v.contractType", 'PCO Bulk + PCO Digital + PCO NVP');
            }else{
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
    },
})