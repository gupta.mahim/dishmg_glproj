({
    initDefault : function(component, event, helper)
    {
        var varDefault=component.get("v.selectedValue");
        component.set("v.selectedValue",varDefault);
        if(varDefault)
        {
        	//Show Selected row content as a SLDS - pill
            var selectedItemPill = component.find('selected-item-pill');
            helper.showElement(selectedItemPill);
            
            //Hide the search-input control
            var inputElement = component.find('input-area');
            helper.hideElement(inputElement);    
        }
        
    },
    onLeaveLookupPane : function(component, event, helper) {
        //Search - Input control focus removed on mouse leave
        var inputContainerCmp = component.find('master-container');
        helper.removeClass(inputContainerCmp,'slds-has-input-focus');
    },
    remove : function (component, event, helper) {
        //Hide the active SLDS - pill
        var selectedItemPill = component.find('selected-item-pill');
        helper.hideElement(selectedItemPill);
        
        //Show search-input control
        var inputElement = component.find('input-area');
        helper.showElement(inputElement);
        
        component.set("v.selectedValue",null);
        component.set("v.returnId",null);
    },
    onInputChange : function (component, event, helper) {
        var searchContent = component.get("v.searchText");
        var lookupContainerCmp = component.find("lookUpPane");
        if ( searchContent && searchContent.trim().length >= 3 ) {
            searchContent = searchContent.trim();
            helper.addClass(lookupContainerCmp,'slds-is-open');
            helper.removeClass(lookupContainerCmp,'slds-is-close');
            helper.searchContent(component,searchContent);
        } else {
            helper.removeClass(lookupContainerCmp,'slds-is-open');
            helper.addClass(lookupContainerCmp,'slds-is-close');
        }
        
    },
    onSearchInputClick : function (component, event, helper) {
        //input control foucs enabled by adding focus style class
        var inputContainerCmp = component.find('master-container');
        helper.addClass(inputContainerCmp,'slds-has-input-focus');
    },
    selectedRecordRowClick : function (component, event, helper) {
        //event triggered on lookup row selection
        //fetching the details of selected row and it's index
        var targetSource = event.currentTarget;
        var selectedIndex = targetSource.dataset.index;
        
        console.log(targetSource,selectedIndex);
        var listedRecords = component.get("v.fetchedRecords");
        if (listedRecords && listedRecords.length > 0 && +selectedIndex >=0) {
            var selectedRecord = listedRecords[selectedIndex];
            //Search input control value resets
            component.find("searchContent").set("v.value",""); 
            component.set("v.selectedValue",selectedRecord.Name);
            component.set("v.returnId",selectedRecord.Id);
            
            //Hide the lookup
            var lookupContainerCmp = component.find("lookUpPane");
            helper.removeClass(lookupContainerCmp,'slds-is-open');
            helper.addClass(lookupContainerCmp,'slds-is-close');
            
            //Show Selected row content as a SLDS - pill
            var selectedItemPill = component.find('selected-item-pill');
            helper.showElement(selectedItemPill);
            
            //Hide the search-input control
            var inputElement = component.find('input-area');
            helper.hideElement(inputElement);
        }
    }
})