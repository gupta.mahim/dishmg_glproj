({
    getOppRecordctlr : function(component, event, helper) 
    {
        var action = component.get("c.getOppRecords");
        action.setParams({
           
            id:component.get("v.SelectedOpportunity"),
            CaseId:component.get("v.CaseId"),
            CasePSAId:component.get("v.CaseIdPSA")
        });
        action.setCallback(this,function(a){
            component.set("v.oppDatalist", a.getReturnValue());
        });
        $A.enqueueAction(action); 
    },
    
    //MS-001021 - 11Sep20 - Start
    handlePrint: function(component, event, helper) {
        var navigateURL = "/lex/s/GuideMeCreateOppSummaryPage?oppId="+component.get("v.SelectedOpportunity");
        window.open(navigateURL, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=80,left=200,width=800,height=500");
    }
    //MS-001021 - End
})