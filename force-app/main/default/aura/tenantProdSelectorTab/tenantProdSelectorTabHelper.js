({
    searchProds : function(component, event, helper,isInit) {        
      component.set("v.isLoading", true);  
      var action = component.get("c.loadProductData");
      var priceBookId=component.get("v.priceBookId");
      var oppIdText=component.get("v.recordId");
      var familyText=component.get("v.prodFamily");
      var searchText=component.get("v.searchText");
      var languages=component.get("v.languages");
      var languageLoaded=component.get("v.languageLoaded");
      var transportLoaded=component.get("v.transportLoaded");
      var tenantType=component.get("v.tenantType");
      var existingGFProdData=component.get("v.existingGFProdData");
	  var strExistingGFProd=JSON.stringify(existingGFProdData);      
      var selectedLanguage="";
      
      var selectedLanguageCmp=component.find("prodLanguage");
        if(selectedLanguageCmp)
        {
            selectedLanguage=selectedLanguageCmp.get("v.value");
            if(!selectedLanguage)
            {
                selectedLanguage="English";
            }
        }
        if(!searchText){searchText="";}
        if(familyText=='Transport' && transportLoaded!=true)
        {
            var transports = [];
 			transports.push({
                  class: "optionClass",
                  label: 'Transport HD',
                  value: 'Transport HD'
              });
            transports.push({
                  class: "optionClass",
                  label: 'Transport SD',
                  value: 'Transport SD'
              });
          component.find("prodTransport").set("v.options", transports);
            component.set("v.transportLoaded",true);
            familyText='Transport HD';
        }
        if(familyText=='Transport' && transportLoaded==true)
        {
            familyText=component.find("prodTransport").get("v.value");
        }
        if(familyText=='International' && languageLoaded!=true)
        {
      	  var languageOpts = [];
 			     
          for (var i = 0; i < languages.length; i++) {
              languageOpts.push({
                  class: "optionClass",
                  label: languages[i],
                  value: languages[i]
              });
          }
            component.find("prodLanguage").set("v.options", languageOpts);
            component.set("v.languageLoaded",true);
        }
      action.setParams({
            taId: oppIdText
          ,tenantType: tenantType 
          ,theBookId: priceBookId
           ,family:familyText
           ,searchText:searchText
           ,prodLanguage:selectedLanguage
          ,strExistingGFProd:strExistingGFProd
      });
      action.setCallback(this, function (response) {
      var state = response.getState();
      
      if (component.isValid() && state === "SUCCESS")
      {
          var prodResp=response.getReturnValue();
          var availableprods=prodResp.availableProducts;
          component.set("v.totalPages", Math.ceil(availableprods.length/component.get("v.pageSize")));
          component.set("v.allData",availableprods);
          if(availableprods.length==0)
          {
              component.set("v.isEmpty",true);
              component.set("v.errorMessage","There are no "+familyText+" products available for selection at this time.");
          }
          component.set("v.currentPageNumber",1);
          helper.buildData(component, helper);
          if(isInit)
          {
              component.set("v.loadSize",availableprods.length-1);
          }
          component.set("v.isLoading", false);
      }});
        
      var requestInitiatedTime = new Date().getTime();
      $A.enqueueAction(action);
    },handleSelectAllChange: function(component,event) {
    
        var tabData=component.get("v.data");
        var isSelected=component.get("v.isAllSelected");
        for(var i = 0; i < tabData.length; i++)
        {
            tabData[i]['productEntry'].isSelected=isSelected;
        }
        component.set("v.data",tabData);
        
        /*if(component.get('v.isAllSelected') == false) {
      component.set('v.isAllSelected', true);
    }
    const myCheckboxes = component.find('myCheckboxes'); 
    let chk = (myCheckboxes.length == null) ? [myCheckboxes] : myCheckboxes;
    chk.forEach(checkbox => checkbox.set('v.checked', component.get('v.isAllSelected')));*/
  },
    
    /*
     * this function will build table data
     * based on current page selection
     * */
    buildData : function(component, helper)
    {
        var data = [];
        var pageNumber = component.get("v.currentPageNumber");
        var pageSize = component.get("v.pageSize");
        var allData = component.get("v.allData");
        var existingProducts=component.get("v.existingProducts");  
        var x = (pageNumber-1)*pageSize;
        var coreSelected=false;
        
        //creating data-table data
        for(; x<=(pageNumber)*pageSize; x++){
            if(allData[x])
            {
                for(var ep=0;ep<existingProducts.length;ep++)
                {
                    if(allData[x].productEntry.Id==existingProducts[ep].Tenant_ProductEntry__r.Id)
                    {
                        allData[x].productEntry.isSelected=true;
                        allData[x].productEntry.isExisting=true;
                        if(existingProducts[ep].Family__c=='Core')
                        {
                            coreSelected=true;
                        }
                    }
                }
                data.push(allData[x]);
            }
        }
        component.set("v.data", data);
        component.set("v.isCoreSelected",coreSelected);
        
        helper.generatePageList(component, pageNumber);
    },
    
    /*
     * this function generate page list
     * */
    generatePageList : function(component, pageNumber){
        pageNumber = parseInt(pageNumber);
        var pageList = [];
        var totalPages = component.get("v.totalPages");
        if(totalPages > 1){
            if(totalPages <= 10){
                var counter = 2;
                for(; counter < (totalPages); counter++){
                    pageList.push(counter);
                } 
            } else{
                if(pageNumber < 5){
                    pageList.push(2, 3, 4, 5, 6);
                } else{
                    if(pageNumber>(totalPages-5)){
                        pageList.push(totalPages-5, totalPages-4, totalPages-3, totalPages-2, totalPages-1);
                    } else{
                        pageList.push(pageNumber-2, pageNumber-1, pageNumber, pageNumber+1, pageNumber+2);
                    }
                }
            }
        }
        component.set("v.pageList", pageList);
    },
   
 })