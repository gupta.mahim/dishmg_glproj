({
    init: function (component, event, helper) {
      var action = component.get("c.initProdSelector");
      
      var recIdSD = component.get("v.recordId");//"00663000008fO8H";//
      
      component.set("v.isLoading", true);
      action.setParams({taId: recIdSD});
      
      action.setCallback(this, function (response) {
      var state = response.getState();
      if (component.isValid() && state === "SUCCESS")
      {
          var prodResp=response.getReturnValue();
          
          var existingProducts=prodResp.existingProducts;
          if(existingProducts && existingProducts.length>0){component.set("v.hasExisting",true);}
          else{component.set("v.hasExisting",false);}
          
          component.set("v.prodCategories",prodResp.prodFamilies);
          component.set("v.existingProducts",existingProducts);
          component.set("v.existingProdData",prodResp.existingProdData);
          component.set("v.existingGFProdData",prodResp.existingGFEntries);
          component.set("v.priceBookId",prodResp.priceBookId);
          component.set("v.languages",prodResp.languageList);
          component.set("v.existingGrandTotal",prodResp.GrandTotal);
          component.set("v.tenantType",prodResp.tenantType);
          
          if(prodResp.tenantType=="Incremental")
          {
              component.set("v.selectedTabId","Add-On")
          }
          
          
      }});
      $A.enqueueAction(action);
        
        component.set('v.validate', function() {
           
            helper.saveChanges(component, event,helper); 
            
            var recMessage=component.get("v.recordStatusMessage");
            var hideStatusMessage=component.get("v.hideStatusMessage");
            if(hideStatusMessage==false)
            {
                return { isValid: false, errorMessage: recMessage };
            }
            else
            {
                return { isValid: true };
            }
            });
        
        
    },reviewSummary: function (component, event, helper)
    {
        helper.reviewSummary(component, event, helper);
    }
    ,removeRow: function (component, event, helper)
    {
        var selectedItem = event.currentTarget;
        var recIndex = selectedItem.dataset.record;
        var prodCategory = selectedItem.dataset.prodcateg;
        
        var cmp=component.find(prodCategory);
        if(cmp)
        {
            var selectedData=cmp.get("v.selectedData");
            var pageData=cmp.get("v.data");
            var prodEntries= selectedData[prodCategory];
            for(var d=0;d<pageData.length;d++)
        	{
                if(pageData[d].productEntry.Id==recIndex)
                {
                   pageData[d].productEntry.isSelected=false;
                }
        	}
            for(var i = 0; i < prodEntries.length; i++)
            { 
               if ( prodEntries[i].productEntry.Id === recIndex){prodEntries.splice(i, 1);}
            }
            
            selectedData[prodCategory]=prodEntries;
            cmp.set("v.selectedData",selectedData);
            cmp.set("v.data",pageData);
            helper.reviewSummary(component, event, helper);
        }
        
    }
    ,closePage: function (component, event, helper) 
    {
        var ID= component.get("v.recordId");
        var isCommunity=component.get("v.isCommunity");
        var isGuideMe=component.get("v.isGuideMe");
        if(!isGuideMe)
        {
        	if(!isCommunity)
            {
                window.open('/lightning/r/Tenant_Account__c/'+ID+'/view',"_self");    
            }
            else
            {
                window.open('/lex/s/tenant-account/'+ID+'/view',"_self");
            }    
        }
    }
    ,saveChanges: function (component, event, helper) 
    {
    	helper.saveChanges(component, event,helper); 
        
    }
    ,loadProducts: function (component, event, helper) 
    {	
        var tab = event.getSource();
        var tabId=tab.get('v.id');
        var selectedData=component.get("v.selectedData");
        var loadedTabs=component.get("v.loadedTabs");
        var taType=component.get("v.tenantType");
        var existingGFProdData=component.get("v.existingGFProdData");
        var loadTab=true;
        if(!loadedTabs){loadedTabs=[];}
        helper.reviewSummary(component, event,helper); 
        var coreSelected=component.get("v.isCoreSelected");
        if(!coreSelected && tabId!='Core' && taType!='Incremental')
        {
            component.set("v.recordStatusMessage","Please select a Core before prceeding.");
            component.set("v.hideStatusMessage",false);
            component.find("prodTabs").set("v.selectedTabId", "Core");
            return;
        }
        //component.set("v.hideStatusMessage",true);
        for(var i = 0; i < loadedTabs.length; i++)
        { 
               if (loadedTabs[i] === tabId){loadTab=false;break;}
        }
        if(loadTab)
        {
        	loadedTabs.push(tabId);
            component.set("v.loadedTabs",loadedTabs);
      		var recIdSD = component.get("v.recordId");//"00663000008fO8H";//
            $A.createComponent("c:tenantProdSelectorTab",{existingGFProdData:existingGFProdData,tenantType:component.get("v.tenantType"),existingProducts:component.get("v.existingProducts"),languages:component.get("v.languages"),"aura:id":tabId,selectedData:selectedData,recordId:recIdSD,prodFamily:tabId,priceBookId:component.get("v.priceBookId")}, 
          	function (contentComponent, status, error) {
            if (status === "SUCCESS"){
                tab.set('v.body', contentComponent);}else
            {throw new Error(error);}
        });    
        }
    },hideStatusMesssage : function(component, event, helper) {
        
        component.set("v.hideStatusMessage", true);
    },openModel: function(component, event, helper) {
      	try{helper.reviewSummary(component, event, helper);}catch(e){}
        
        var allSaveData=component.get("v.allSaveData");
        var allRemoveData=component.get("v.allRemoveData");
        
        var changesFound=false;
        
        if(allSaveData && allSaveData!=null && allSaveData.length>0){changesFound=true;}
        if(allRemoveData && allRemoveData!=null && allRemoveData.length>0){changesFound=true;}
        
        if(changesFound==true)
        {
      		component.set("v.isOpen", true);      
        }
        else
        {
            var ID= component.get("v.recordId");
            var isCommunity=component.get("v.isCommunity");
            var isGuideMe=component.get("v.isGuideMe");
            if(!isGuideMe)
            {
                if(!isCommunity)
                {
                    window.open('/lightning/r/Tenant_Account__c/'+ID+'/view',"_self");    
                }
                else
                {
                    window.open('/lex/s/tenant-account/'+ID+'/view',"_self");
                }    
            }
        }
   },
 
   closeModel: function(component, event, helper) {
      // for Hide/Close Model,set the "isOpen" attribute to "Fasle"  
      component.set("v.isOpen", false);
   }
})