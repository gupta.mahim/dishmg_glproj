({
    doInit : function(component, event, helper) {
        
        var action=component.get("c.getStateOptions");
        action.setCallback(this,function(response){
            var state=response.getState();
            if(state==="SUCCESS"){
                console.log('response.getReturnValue-'+response.getReturnValue());
                component.set("v.options",response.getReturnValue());
            }
            else{
                console.log(response.getError());
            }
        });
        $A.enqueueAction(action);
        
        // Set the validate attribute to a function that includes validation logic
        /*component.set('v.validate', function() {
            var userInputBusinesslegalName = component.get('v.BusinesslegalName');
            var userInputBusinessAddress = component.get('v.BusinessAddress');
            if(userInputBusinesslegalName && userInputBusinesslegalName.length>0 && userInputBusinessAddress && userInputBusinessAddress.length>0) {return { isValid: true };}
            else {
                if(!userInputBusinesslegalName || userInputBusinesslegalName.length==0){
                    return { isValid: false, errorMessage: 'Please enter some valid input for Billing Legal Name. Input is not optional.' }};
                if(!userInputBusinessAddress || userInputBusinessAddress.length==0){
                    return { isValid: false, errorMessage: 'Please enter some valid input for Billing Address. Input is not optional.' }};
            }})*/
    }, 
    
    onCheckPropertyAddress: function(component, event) {
        var checkCmp = event.getSource().get('v.checked');// component.find("chkPropertyAddress");
        var propAddress=component.get("v.Address"); 
        var propCity=component.get("v.City"); 
        var propState=component.get("v.State"); 
        var propZipCode=component.get("v.ZipCode"); 
        //var propPhone=component.get("v.Phone"); 
        
        //console.log('checkCmp-'+checkCmp.get("v.value"));
        if(checkCmp){
            component.set('v.BusinessAddress',propAddress);
            component.set('v.BusinessCity',propCity);
            component.set('v.BusinessState',propState);
            component.set('v.BusinessZipcode',propZipCode);
            //component.set('v.BillingContactphone',propPhone);
        }else{
            component.set('v.BusinessAddress','');
            component.set('v.BusinessCity','');
            component.set('v.BusinessState','');
            component.set('v.BusinessZipcode','');
            //component.set('v.BillingContactphone','');
        }
    },
    
    onCheckThirdPartyBiller: function(component, event) {
        var checkCmp = event.getSource().get('v.checked');
        if(checkCmp){
            component.set('v.ThridPartyBiller',true);
        }else{
            component.set('v.ThridPartyBiller',false);
        }
    },
    
    btnNextClick : function(component, event, helper) {
        var allValid = component.find('fieldId').reduce(function (validSoFar, inputCmp) {
            inputCmp.showHelpMessageIfInvalid();
            return validSoFar && !inputCmp.get('v.validity').valueMissing;
        }, true);
        if (allValid) {
            //alert('All form entries look valid. Ready to submit!');
            if(component.get("v.BusinessState") == null || component.get("v.BusinessState")== '')
                component.set("v.BusinessState", "AK");
            
            //navigate to the next screen
            var response = event.getSource().getLocalId();
            component.set("v.value", response);
            var navigate = component.get("v.navigateFlow");
            navigate("NEXT");
        } else {
            //alert('Please update the invalid form entries and try again.');
        }
    },
    
    btnPreviousClick : function(component, event, helper) {
        var response = event.getSource().getLocalId();
        component.set("v.value", response);
        var navigate = component.get("v.navigateFlow");
        navigate("BACK");        
    },
})