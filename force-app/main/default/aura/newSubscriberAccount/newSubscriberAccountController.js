({
    init : function (component) {
        // Find the component whose aura:id is "flowData"
        var flow = component.find("flowData");
        var varRecordId=component.get("v.recordId");
        
        //Project # PJ-000515 - 18 June 2020 - Start
        var isComm;
        if((new RegExp('.*?\/s\/','g')).exec(window.location.href) != null)
            isComm = true;
        else
            isComm = false;
        //Project #PJ-000515 - End
               
        var inputVariables = [
            { name : "recordId", type : "String", value: varRecordId }
            //Project # PJ-000515 - 18 June 2020 - Start
            //,{ name : "isCommunity", type : "Boolean", value: true }      
            ,{ name : "isCommunity", type : "Boolean", value: isComm }
            //Project # PJ-000515 - End
        ];
        
        // In that component, start your flow. Reference the flow's API Name.
        flow.startFlow("Create_New_Subscriber_Account",inputVariables);
    },
    
    
})