({
	
 equipmentRead : function(component, event, helper) {
        
        component.set("v.isLoading", true);
        var action = component.get("c.getTenantEquipmentRecords");
        component.set("v.equipmentEdit", null);
     
        action.setParams({
            oppId: component.get("v.recordId")
        });
        console.log("KA:: " +  action);
        action.setCallback(this,function(a){
            component.set("v.equipmentEdit", a.getReturnValue());
            component.set("v.isLoading", false);
        });
        $A.enqueueAction(action);
    }

})