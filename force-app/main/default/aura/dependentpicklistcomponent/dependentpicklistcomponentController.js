({
    /* Calling helper method to get the field dependency map at initialization of component */
	doInit : function(component, event, helper) {
        console.log('Check-'+component.get("v.selectedBrandValue"));
        //Setting labels for the picklist
        var cFieldLabel=component.get("v.controllingFieldApiName");
        cFieldLabel=cFieldLabel.replace('__c','');
        component.set("v.controllingPicklistLabel",cFieldLabel.replace('_',' '));
                
        var dFieldLabel=component.get("v.dependentFieldApiName");
        dFieldLabel=dFieldLabel.replace('__c','');
        component.set("v.dependentPicklistLabel",dFieldLabel.replace('_',' '));
        
        helper.getFieldDependencyMap(component,event);
        helper.handleRecordTypeChange(component, event, helper); 
        
        //Milestone #MS-000971 -- Mahim --Start
        if(component.get('v.selectedControllingPicklistValue') != 'FTG' || component.get('v.selectedDependentPicklistValue') != 'Hotel/Motel'){
            //component.set("v.selectedValue",null);
            //component.set("v.selectedBrandValue",null);
            //component.set("v.selectedSubBrandValue",null);
        }
        //Milestone #MS-000971 --End
                
    },
    
    /* Calling helper methods on Record type picklist change to get the Picklist values based on the record type */
  
    
    /* Based on the selected value in controlling picklist, getting the dependent picklist values from the field dependency map */
    handleControllingPicklistChange : function(component, event, helper) {
        console.log('handleControllingPicklistChange Called' );
        var selectedControllingPicklistValue=component.get("v.selectedControllingPicklistValue");
        
        var availablePicklistValuesCacheSet=[];
        availablePicklistValuesCacheSet=component.get("v.dependentPicklistValuesCache");
        var dependencyMap=component.get("v.fieldDependencMap");
        var availablePicklistValuesMapSet=dependencyMap[component.get("v.selectedControllingPicklistValue")];
        
        var dependentPicklistValues=[];
        if(availablePicklistValuesCacheSet.length>0){
            for(var i=0;i<availablePicklistValuesCacheSet.length;i++){
                
                if(availablePicklistValuesMapSet.includes(availablePicklistValuesCacheSet[i]))
                    dependentPicklistValues.push(availablePicklistValuesCacheSet[i]);
            }
        }
        component.set("v.dependentPicklistValues",dependentPicklistValues);
        
        //Milestone #MS-000971 -- Mahim --Start
        component.set("v.selectedDependentPicklistValue",null);
        if(component.get('v.selectedControllingPicklistValue') != 'FTG' || component.get('v.selectedDependentPicklistValue') != 'Hotel/Motel'){
            component.set("v.selectedValue",null);
            component.set("v.selectedBrandValue",null);
            component.set("v.selectedSubBrandValue",null);
        }
        //Milestone #MS-000971 --End
    },
    
})