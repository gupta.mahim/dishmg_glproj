({
    equipmentRead : function(component, event, helper) {
        var action = component.get("c.getEquipments");
        component.set("v.isLoading", true);
        action.setParams({
            oppId: component.get("v.recordId")
        });
        console.log("KA:: " +  action);
        action.setCallback(this,function(a){
            //var state = a.getState();
            //console.log(a.getReturnValue())
            //component.set("v.isLoading", false);
            component.set("v.equipmentDelete", a.getReturnValue());
            component.set("{!v.isLoading}", false);
            
        });
        $A.enqueueAction(action);
    },
     SaveProduct  : function(component, event, helper){
        
        component.set("v.isLoading", true);
        var equipData=component.get("v.equipmentDelete")
        var jsonData=JSON.stringify(equipData);
        console.info(jsonData);
        var action = component.get("c.deleteRecords");
        action.setParams({
            strEquipData: jsonData
        });
        action.setCallback(this, function(a) {
            component.set("v.isLoading", false);
            var state = a.getState();
            if (state === "SUCCESS") {
                component.set("v.isStatusMessage", "SaveSuccess");
                component.set("{!v.recordStatusMessage}", "Your record has been deleted successfully");
                //component.set("v.uploadedEquipment", null);
                    
                            component.set("v.isLoading", false);
                            // show toast message
                            
                            //$A.get("e.force:closeQuickAction").fire();
                            $A.get('e.force:refreshView').fire();
                            
                var name = a.getReturnValue();
               // $A.get('e.force:refreshView').fire();
            }
        });
             /*   $A.get('e.force:refreshView').fire();
                var name = a.getReturnValue();
				 
                alert("hello from here"+name);
                
            }
        }); */
       var ID= component.get("v.recordId");
       window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");
        $A.enqueueAction(action);  
    }, 
     onCancel : function(component, event, helper) {
        
       var ID= component.get("v.recordId");
      
       window.open('/lightning/r/Opportunity/'+ID+'/view',"_self");
           
} ,
    closeComponent  : function(component, event, helper){
        $A.get("e.force:closeQuickAction").fire();
    },
     doInit : function(component,event,helper){
         $A.get('e.force:refreshView').fire();
     },
    reset : function(component, event, helper) {
        alert('Reset called');
       // helper.updateRecordHelper(component, event, helper, 'reset');
    },
    hideStatusMesssage : function(component, event, helper) {
        
        component.set("v.isStatusMessage", "hideMessage");
    }
 	
})