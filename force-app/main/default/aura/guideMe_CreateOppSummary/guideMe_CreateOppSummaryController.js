({
	getOppRecordctlr : function(component, event, helper) 
    {
        var sPageURL = decodeURIComponent(window.location.search.substring(1));
        var sURLVariables = sPageURL.split('&');
        var sParameterName, i, OppID, CaseId, CasePSAId;
        
        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == 'oppId') 
                OppID = sParameterName[1];
            if (sParameterName[0] == 'caseId') 
                CaseId = sParameterName[1];
            if (sParameterName[0] == 'casePSAId') 
                CasePSAId = sParameterName[1];           
        }
        console.log('OppID-'+OppID);console.log('CaseId-'+CaseId);console.log('CasePSAId-'+CasePSAId);
        
        var action = component.get("c.getOppRecords");
        action.setParams({
            id:OppID,
            CaseId:CaseId,
            CasePSAId:CasePSAId
        });
        action.setCallback(this,function(response){
            var state = response.getState();
            if (state === "SUCCESS") {
                component.set("v.oppDatalist", response.getReturnValue());
                var delay=2000; //2 seconds
                setTimeout(function() {
                    console.log('Inside delay: ');
                    window.print();
                }, delay);
            }
            else{
                console.log('Error while getting opportunity details. Error Detail: '+JSON.stringify(response.getError()));
            }
        });
        $A.enqueueAction(action);
    },
})