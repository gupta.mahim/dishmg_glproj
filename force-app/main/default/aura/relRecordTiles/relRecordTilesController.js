({
    getNewButton: function(cmp,event)
    {
        var createRecordEvent = $A.get("e.force:createRecord");
        var parentField=cmp.get("v.NewRecordParentAPI");
        createRecordEvent.setParams({ 
            "entityApiName": cmp.get("v.NewRecordObjectAPI"),
            "defaultFieldValues": 
            {"Opportunity__c":cmp.get("v.recordId") }
        });
        createRecordEvent.fire();
    },
    getCompnent1: function(cmp,event) {
        //Create component dynamically
        $A.createComponent(
            cmp.get("v.Component1"),{recordId:cmp.get("v.recordId"),isCommunity:true},
            function(newcomponent){
               
                if (cmp.isValid()) {
                    var body = cmp.get("v.body");
                    
                    body=newcomponent;
                    cmp.set("v.body", body);             
                }
            }            
        );
        cmp.set("v.isOpen", true);
        var cmpTarget = cmp.find('Modalbox');
		var cmpBack = cmp.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
    },getCompnent2: function(cmp,event) {
        //Create component dynamically
        $A.createComponent(
            cmp.get("v.Component2"),{recordId:cmp.get("v.recordId"),isCommunity:true},
            function(newcomponent){
               
                if (cmp.isValid()) {
                    var body = cmp.get("v.body");
                    
                    body=newcomponent;
                    cmp.set("v.body", body);             
                }
            }            
        );
        cmp.set("v.isOpen", true);
        var cmpTarget = cmp.find('Modalbox');
		var cmpBack = cmp.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
    },getCompnent3: function(cmp,event) {
        //Create component dynamically
        $A.createComponent(
            cmp.get("v.Component3"),{recordId:cmp.get("v.recordId"),isCommunity:true},
            function(newcomponent){
               
                if (cmp.isValid()) {
                    var body = cmp.get("v.body");
                    
                    body=newcomponent;
                    cmp.set("v.body", body);             
                }
            }            
        );
        cmp.set("v.isOpen", true);
        var cmpTarget = cmp.find('Modalbox');
		var cmpBack = cmp.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
    },getCompnent4: function(cmp,event) {
        //Create component dynamically
        $A.createComponent(
            cmp.get("v.Component4"),{recordId:cmp.get("v.recordId"),isCommunity:true},
            function(newcomponent){
               
                if (cmp.isValid()) {
                    var body = cmp.get("v.body");
                    
                    body=newcomponent;
                    cmp.set("v.body", body);             
                }
            }            
        );
        cmp.set("v.isOpen", true);
        var cmpTarget = cmp.find('Modalbox');
		var cmpBack = cmp.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
    },
    removeComponent:function(cmp,event){
        cmp.set("v.isOpen", false);
        var cmpTarget = cmp.find('Modalbox');
		var cmpBack = cmp.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open');
        var comp = event.getParam("comp");
        comp.destroy();
    },    
    closeModal:function(component,event,helper){    
		component.set("v.isOpen", false);
        var cmpTarget = component.find('Modalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.removeClass(cmpBack,'slds-backdrop--open');
		$A.util.removeClass(cmpTarget, 'slds-fade-in-open'); 
    	},
	openmodal:function(component,event,helper) {
		component.set("v.isOpen", true);
        var cmpTarget = component.find('Modalbox');
		var cmpBack = component.find('Modalbackdrop');
		$A.util.addClass(cmpTarget, 'slds-fade-in-open');
		$A.util.addClass(cmpBack, 'slds-backdrop--open');
	}   
,doInit : function(component, event, helper) {
    console.log("relRecordTiles is starting up...");

    //Pull values from component 
    //var currentID = component.get("v.currentID");
    //var objectName = component.get("v.objectName");
    //var lookupField = component.get("v.lookupField");
    var txtfield1 = component.get("v.field1");
    var txtfield2 = component.get("v.field2");
    var txtfield3 = component.get("v.field3");
	var txtfield4 = component.get("v.field4");
    var txtfield5 = component.get("v.field5");
    var txtfield6 = component.get("v.field6");
    var txtfield7 = component.get("v.field7");
    var txtfield8 = component.get("v.field8");
    var txtfield9 = component.get("v.field9");
    //call relRecordTilesController.getResults server side controller
    var action = component.get('c.getResults');
	//Set the variables for the server side controller
    action.setParams({
        currentID : component.get("v.currentID"),
        objectName : component.get("v.objectName"),
        lookupField : component.get("v.lookupField"),
        field1 : txtfield1,
        field2 : txtfield2,
        field3 : txtfield3,
        field4 : txtfield4,
        field5 : txtfield5,
        field6 : txtfield6,
        field7 : txtfield7,
        field8 : txtfield8,
        field9 : txtfield9
    });

    // Set up the callback
    action.setCallback(this, $A.getCallback(function (response) {
        var state = response.getState();
        var resultsToast = $A.get("e.force:showToast");
        console.info(state);
        if(state === "SUCCESS"){
            //if successful stores query results in serverResult
            component.set('v.serverResult', response.getReturnValue());
        } else if (state === "ERROR") {
            //otherwise write errors to console for debugging
            alert('Problem with connection. Please try again. Error Code: relRecordTiles.doInit.action.setCallback');
            resultsToast.setParams({
                "title": "Error",
                "message": "relRecordTiles failed to load due to: " + JSON.stringify(response.error)
            });
            resultsToast.fire();
            var errors = response.getError();
            console.error(errors);
        }
    }));

    $A.enqueueAction(action);
}})