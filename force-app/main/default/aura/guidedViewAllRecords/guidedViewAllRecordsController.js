({
    loadEquip : function(component, event, helper) 
    {
        //MS-001015 - 9Sep20 - Start
        var varEquipsId = component.get("v.Equips"); 
        component.set("v.equipsId", varEquipsId);
        //MS-001015 - End
        
        var action = component.get("c.getActiveEquips");
        var hasOverrideCase=component.get("v.hasOverrideCase");
        action.setParams({
            equipIds: component.get("v.Equips"),
            oppId:component.get("v.SelectedOpportunity"),
            taId:component.get("v.SelectedTA")
        });
        action.setCallback(this,function(a){
            var taData=a.getReturnValue();
            component.set("v.Equips", taData);
            if(taData[0].overrideAddress==true 
               || taData[0].hasOtherActiveElsewhere==true 
               || taData[0].hasOwnActiveElsewhere==true
                || hasOverrideCase==true)
            {
                component.set("v.hasErrors",true);
            }
        });
        $A.enqueueAction(action);
    },
    
    //MS-001015 - 9Sep20 - Start
    handlePrint: function(component, event, helper) {
        var navigateURL = "/lex/s/GuideMeCreateTASummaryPage?equipIds="+component.get("v.equipsId");
        navigateURL = navigateURL+"&oppId="+component.get('v.SelectedOpportunity');
        navigateURL = navigateURL+"&TAId="+component.get('v.SelectedTA');
        navigateURL = navigateURL+"&Prg="+component.get('v.SelectedPrg');
        window.open(navigateURL, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=80,left=200,width=800,height=500");
    }
    //MS-001015 - End
})